<?php include "includes/header.html"; ?>
<title>About</title>
<?php include "includes/nav.html"; ?>

    <div class="row">
      <div class="col-12">
	<h1>The Design:</h1>
	<p>This website was designed to be usable in the widest range of browsers possible. The tango themes were used in absensce of any creativite drive. They told me that I'm too rigid but I'm this way in real life too. </p>

	<p> The only fancy feature is automatic dark theme if your browser supports it. Because that's something people like. </p>

	<p> All the HTML/PHP/CSS is written by hand. It's theraputic. </p>

	<p><a href="https://binrc.gitlab.io/">I summarize my web 'design' philosophy here</a></p>

      </div>
    </div>

    <div class="row">
      <div class="col-6">
	<h1>The Author:</h1>
	<img class="about" src="/assets/images/about.jpg" alt="Ah, you're finally awake! OOP? JS Frameworks? Rust? Arm? Linux Desktop? what is this stuff? Come on, get in the UNIX room. WE have to improve Plan 9.">
      </div>
      <div class="col-6">
	<h1>The Software:</h1>
	<p>BAMP </p> 
	<blockquote>
	If you're old enough to run a server, you're old enough to run OpenBSD. 
	</blockquote>
	<p>All PHP on this site is licensed under the BSD 2 clause license and <a href="https://gitlab.com/binrc/site">is available here.</a>. All content is licensed under the <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> license unless otherwise specified.</small>
      </div>
    </div>

    <div class="row">
      <div class="col-12">
	<h1>Tested Against: </h1>
	<p>Firefox, GNU IceCat, Chromium, Dillo, Mothra, Netsurf, Falkon, Konqueror, w3m, Lynx, Qutebrowser, Midori, and Suckess' Surf. Everything mostly works with the exception of my fancy email address occlusion techniques (pure CSS, read the source code if it's not working). </p>
      </div>
      </div>

    <div class="row">
      <div class="col-12">
	<h1>Statistics: </h1>
	<p>I wrote some shell and R that turns httpd log files into some sort of document. Google analytics be dammed. <a href="https://0x19.org/httpstats">You can see the statistics for my webserver by clicking this link</a>.
      </div>
      </div>

    <div class="row">
      <div class="col-12">
	<h1>Webrings: </h1>
	<p>Lainring: a decentralized webring <a href="/lainring/">Lainring</a>.
	<!--<p>Dring: a fully decentralized, p2p webring (will be nuked soon) <a href="/dring.php">dring</a>.-->
      </div>
      </div>
<?php
include "includes/footer.html";
?>
