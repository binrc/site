// @license magnet:?xt=urn:btih:87f119ba0b429ba17a44b4bffcab33165ebdacc0&dn=freebsd.txt FreeBSD
function regex(pattern, color, name){
	this.rule = [pattern,color,name];
}

function lang(rarr){
	var i;
	this.rules = [];
	for(this.i = 0; this.i < rarr.length; this.i++){
		this.rules.push(rarr[this.i]);
	}
}


// order of operations is important
const markdown = new lang([
		new regex( new RegExp('/(".+?")/', 'g'), 			"red",		"strs"),
		new regex( new RegExp("/('.+?')/", "g"),				"red",		"qints"),
		new regex( new RegExp("/(&lt;[^\&]*&gt;)/", "g"), 		"red",		"tags"),
		new regex( new RegExp("/(#.*$)/", "gm"), 			"magenta",	"headers"),
		new regex( new RegExp("/(\`\`\`[\s\S]*?\`\`\`)/", "gm"),	 "blue",	"pres")
]);

//lang.rules.push(new regex("/(&lt;[^\&]*&gt;)/g", "red"));


function md(buf){
	var tags = /(&lt;[^\&]*&gt;)/g
	var headers = /(#.*$)/gm;
	var pres = /(\`\`\`[\s\S]*?\`\`\`)/gm;
	var strs = /(".+?")/g;
	var qints = /('.+?')/g;
	var st = buf.innerHTML;
	st = st.replace(strs, '<span class="red">$1</span>');
	st = st.replace(qints, '<span class="red">$1</span>');
	st = st.replace(tags, '<span class="red">$1</span>');
	st = st.replace(headers, '<span class="magenta">$1</span>');
	st = st.replace(pres, '<span class="blue">$1</span>');
	buf.innerHTML = st;
	return st;
}

const C = new lang([
		new regex('/(".+?")/g', 		"red",		"strs"),
		new regex("/('.+?')/g;", 		"red",		"qints"),
		new regex("/(&lt;[^\&]*&gt;)/g",	"red",		"tags"),
		new regex("/\b(char|signed char|unsigned char|short|short int|signed short|signed short int|unsigned short|unsigned short int|int|signed|signed int|unsigned|unsigned int|long|long int|signed long|signed long int|unsigned long|unsigned long int|long long|long long int|signed long long|signed long long int|unsigned long long|unsigned long long int|float|double|long double|intn_t|int_leastn_t|int_fastn_t|intptr_t|intmax_t)(?=[^\w])/g;", "green","datatypes"),
		new regex("/\b(auto|break|else|switch|case|enum|register|typedef|extern|return|union|continue|for|default|goto|do|if|while)(?=[^\w])/g;", "yellow", "keywords"),
		new regex("/(\/\/.*)/g",		"blue",		"icomment"),
		new regex("/(\/\*[\s\S]*?\*\/)/gm",	"blue",		"mlcomments"),
		new regex("/(#include|#define|#undef|#if|#ifdef|#ifndef|#elif|#else|#endif|#warning|#error)(?=[^\w])/g", "magenta", "preprocessor"),
		new regex("/(\d+|nil|NULL)(?=[^\w])/g", "red",		"ints")
]);


function c(buf){
	var tags = /(&lt;[^\&]*&gt;)/g;
	var datatypes = /\b(char|signed char|unsigned char|short|short int|signed short|signed short int|unsigned short|unsigned short int|int|signed|signed int|unsigned|unsigned int|long|long int|signed long|signed long int|unsigned long|unsigned long int|long long|long long int|signed long long|signed long long int|unsigned long long|unsigned long long int|float|double|long double|intn_t|int_leastn_t|int_fastn_t|intptr_t|intmax_t|void)(?=[^\w])/g;
	var keywords = /\b(auto|break|else|switch|case|enum|register|typedef|extern|return|union|continue|for|default|goto|do|if|while)(?=[^\w])/g;
	var icomment = /(\/\/.*)/g;
	var mlcomment = /(\/\*[\s\S]*?\*\/)/gm;
	var pp = /(#include|#define|#undef|#if|#ifdef|#ifndef|#elif|#else|#endif|#warning|#error)(?=[^\w])/g;
	var ints = /(\d+|nil|NULL)(?=[^\w])/g;
	var strs = /(".+?")/g;
	var qints = /('.+?')/g;
	var st = buf.innerHTML;
	st = st.replace(strs, '<span class="red">$1</span>');
	st = st.replace(qints, '<span class="red">$1</span>');
	st = st.replace(tags, '<span class="red">$1</span>');
	st = st.replace(datatypes, '<span class="green">$1</span>');
	st = st.replace(keywords, '<span class="orange">$1</span>');
	st = st.replace(icomment, '<span class="blue">$1</span>');
	st = st.replace(mlcomment, '<span class="blue">$1</span>');
	st = st.replace(pp, '<span class="magenta">$1</span>');
	st = st.replace(ints, '<span class="red">$1</span>');
	buf.innerHTML = st;
	return st;
}

function html(buf){
	var tags = /(&lt;[^\&]*&gt;)/g;
	var mlcomment = /(&lt;\!--[\s\S]*?--&gt;)/gm;
	var strs = /(".+?")/g;
	var qints = /('.+?')/g;
	var st = buf.innerHTML;
	st = st.replace(strs, '<span class="blue">$1</span>');
	st = st.replace(qints, '<span class="blue">$1</span>');
	st = st.replace(tags, '<span class="magenta">$1</span>');
	st = st.replace(mlcomment, '<span class="green">$1</span>');
	buf.innerHTML = st;
	return st;
}

function php(buf){
	var php = /(&lt;\?php|\?&gt;)/g;
	var tags = /(&lt;[^\?][^\&]*&gt;)/g;
	var keywords = /\b(__halt_compiler|abstract|and|array|as|break|callable|case|catch|class[^\=]|clone|const|continue|declare|default|die|do|echo|else|elseif|empty|enddeclare|endfor|endforeach|endif|endswitch|endwhile|eval|exit|extends|final|finally|fn|for|foreach|function|global|goto|if|implements|include|include_once|instanceof|insteadof|interface|isset|list|match|namespace|new|or|print|private|protected|public|readonly|require|require_once|return|static|switch|throw|trait|try|unset|use|var|while|xor|yield|yieldfrom)\b/g;
	var ints = /(\d+|nil|NULL)(?=[^\w])/g;
	var mlcomment = /(&lt;\!--[\s\S]*?--&gt;)/gm;
	var icomment = /([^\:]\/\/.)/g;
	var strs = /(".+?")/g;
	var qints = /('.+?')/g;
	var vars = /(\$[a-zA-Z0-9_]+)/g;
	var dollar = /(\$)/g;
	var oop = /(\-\&gt;)/g;
	var st = buf.innerHTML;
	st = st.replace(strs, '<span class="red">$1</span>');
	st = st.replace(qints, '<span class="red">$1</span>');
	st = st.replace(ints, '<span class="red">$1</span>');
	st = st.replace(keywords, '<span class="magenta">$1</span>');
	st = st.replace(mlcomment, '<span class="blue">$1</span>');
	st = st.replace(tags, '<span class="yellow">$1</span>');
	st = st.replace(php, '<span class="magenta">$1</span>');
	st = st.replace(icomment, '<span class="blue">$1</span>');
	st = st.replace(vars, '<span class="cyan">$1</span>');
	st = st.replace(dollar, '<span class="yellow">$1</span>');
	st = st.replace(oop, '<span class="green">$1</span>');
	buf.innerHTML = st;
	return st;
}

function sh(buf){
	var comment = /(#.*)/g;
	var strs = /(".+?")/g;
	var qints = /('.+?')/g;
	var keywords = /\b(case|do|done|elif|else|esac|fi|for|function|if|in|select|then|until|while|time|break|cd|continue|eval|exec|exit|export|getopts|hash|pwd|readonly|return|shift|test|times|trap|umask|unset|alias|bind|builtin|caller|command|declare|echo|enable|help|let|local|logout|mapfile|printf|read|readarray|source|type|typeset|ulimit|unalias)\b/g;
	var ints = /(\d+|nil|NULL)(?=[^\w])/g;
	var vardec = /(\n[a-zA-Z0-9]+\=)/g;
	var vars = /(\$[a-zA-Z0-9_\(\)\?]+)/g;
	var flags = /(\s\-+\w*)/g;
	var parens = /(\[\[|\]\]|\[|\]|\{|\}|\!\.)/g;
	var cmdsub = /(\$\(.*\))/g;
	var st = buf.innerHTML;
	st = st.replace(strs, '<span class="red">$1</span>');
	st = st.replace(qints, '<span class="red">$1</span>');
	st = st.replace(comment, '<span class="blue">$1</span>');
	st = st.replace(vardec, '<span class="cyan">$1</span>');
	st = st.replace(ints, '<span class="red">$1</span>');
	st = st.replace(vars, '<span class="magenta">$1</span>');
	st = st.replace(cmdsub, '<span class="magenta">$1</span>');
	st = st.replace(keywords, '<span class="yellow">$1</span>');
	st = st.replace(flags, '<span class="yellow">$1</span>');
	st = st.replace(parens, '<span class="yellow">$1</span>');
	buf.innerHTML = st;
	return st;
}


function sql(buf){
	var keywords = /\b(WORD|\&\&|\<\=|\<\>|\!\=|\>\=|\<\<|\>\>|\<\=\>|ACCESSIBLE|ACCOUNT|ACTION|ADD|ADMIN|AFTER|AGAINST|AGGREGATE|ALL|ALGORITHM|ALTER|ALWAYS|ANALYZE|AND|ANY|AS|ASC|ASCII|ASENSITIVE|AT|ATOMIC|AUTHORS|AUTO_INCREMENT|AUTOEXTEND_SIZE|AUTO|AVG|AVG_ROW_LENGTH|BACKUP|BEFORE|BEGIN|BETWEEN|BIGINT|BINARY|BINLOG|BIT|BLOB|BLOCK|BODY|BOOL|BOOLEAN|BOTH|BTREE|BY|BYTE|CACHE|CALL|CASCADE|CASCADED|CASE|CATALOG_NAME|CHAIN|CHANGE|CHANGED|CHAR|CHARACTER|CHARSET|CHECK|CHECKPOINT|CHECKSUM|CIPHER|CLASS_ORIGIN|CLIENT|CLOB|CLOSE|COALESCE|CODE|COLLATE|COLLATION|COLUMN|COLUMN_NAME|COLUMNS|COLUMN_ADD|COLUMN_CHECK|COLUMN_CREATE|COLUMN_DELETE|COLUMN_GET|COMMENT|COMMIT|COMMITTED|COMPACT|COMPLETION|COMPRESSED|CONCURRENT|CONDITION|CONNECTION|CONSISTENT|CONSTRAINT|CONSTRAINT_CATALOG|CONSTRAINT_NAME|CONSTRAINT_SCHEMA|CONTAINS|CONTEXT|CONTINUE|CONTRIBUTORS|CONVERT|CPU|CREATE|CROSS|CUBE|CURRENT|CURRENT_DATE|CURRENT_POS|CURRENT_ROLE|CURRENT_TIME|CURRENT_TIMESTAMP|CURRENT_USER|CURSOR|CURSOR_NAME|CYCLE|DATA|DATABASE|DATABASES|DATAFILE|DATE|DATETIME|DAY|DAY_HOUR|DAY_MICROSECOND|DAY_MINUTE|DAY_SECOND|DEALLOCATE|DEC|DECIMAL|DECLARE|DEFAULT|DEFINER|DELAYED|DELAY_KEY_WRITE|DELETE|DELETE_DOMAIN_ID|DESC|DESCRIBE|DES_KEY_FILE|DETERMINISTIC|DIAGNOSTICS|DIRECTORY|DISABLE|DISCARD|DISK|DISTINCT|DISTINCTROW|DIV|DO|DOUBLE|DO_DOMAIN_IDS|DROP|DUAL|DUMPFILE|DUPLICATE|DYNAMIC|EACH|ELSE|ELSEIF|ELSIF|EMPTY|ENABLE|ENCLOSED|END|ENDS|ENGINE|ENGINES|ENUM|ERROR|ERRORS|ESCAPE|ESCAPED|EVENT|EVENTS|EVERY|EXAMINED|EXCEPT|EXCHANGE|EXCLUDE|EXECUTE|EXCEPTION|EXISTS|EXIT|EXPANSION|EXPIRE|EXPORT|EXPLAIN|EXTENDED|EXTENT_SIZE|FALSE|FAST|FAULTS|FEDERATED|FETCH|FIELDS|FILE|FIRST|FIXED|FLOAT|FLOAT4|FLOAT8|FLUSH|FOLLOWING|FOLLOWS|FOR|FORCE|FOREIGN|FORMAT|FOUND|FROM|FULL|FULLTEXT|FUNCTION|GENERAL|GENERATED|GET_FORMAT|GET|GLOBAL|GOTO|GRANT|GRANTS|GROUP|HANDLER|HARD|HASH|HAVING|HELP|HIGH_PRIORITY|HISTORY|HOST|HOSTS|HOUR|HOUR_MICROSECOND|HOUR_MINUTE|HOUR_SECOND|ID|IDENTIFIED|IF|IGNORE|IGNORED|IGNORE_DOMAIN_IDS|IGNORE_SERVER_IDS|IMMEDIATE|IMPORT|INTERSECT|IN|INCREMENT|INDEX|INDEXES|INFILE|INITIAL_SIZE|INNER|INOUT|INSENSITIVE|INSERT|INSERT_METHOD|INSTALL|INT|INT1|INT2|INT3|INT4|INT8|INTEGER|INTERVAL|INVISIBLE|INTO|IO|IO_THREAD|IPC|IS|ISOLATION|ISOPEN|ISSUER|ITERATE|INVOKER|JOIN|JSON|JSON_TABLE|KEY|KEYS|KEY_BLOCK_SIZE|KILL|LANGUAGE|LAST|LAST_VALUE|LASTVAL|LEADING|LEAVE|LEAVES|LEFT|LESS|LEVEL|LIKE|LIMIT|LINEAR|LINES|LIST|LOAD|LOCAL|LOCALTIME|LOCALTIMESTAMP|LOCK|LOCKED|LOCKS|LOGFILE|LOGS|LONG|LONGBLOB|LONGTEXT|LOOP|LOW_PRIORITY|MASTER|MASTER_CONNECT_RETRY|MASTER_DELAY|MASTER_GTID_POS|MASTER_HOST|MASTER_LOG_FILE|MASTER_LOG_POS|MASTER_PASSWORD|MASTER_PORT|MASTER_SERVER_ID|MASTER_SSL|MASTER_SSL_CA|MASTER_SSL_CAPATH|MASTER_SSL_CERT|MASTER_SSL_CIPHER|MASTER_SSL_CRL|MASTER_SSL_CRLPATH|MASTER_SSL_KEY|MASTER_SSL_VERIFY_SERVER_CERT|MASTER_USER|MASTER_USE_GTID|MASTER_HEARTBEAT_PERIOD|MATCH|MAX_CONNECTIONS_PER_HOUR|MAX_QUERIES_PER_HOUR|MAX_ROWS|MAX_SIZE|MAX_STATEMENT_TIME|MAX_UPDATES_PER_HOUR|MAX_USER_CONNECTIONS|MAXVALUE|MEDIUM|MEDIUMBLOB|MEDIUMINT|MEDIUMTEXT|MEMORY|MERGE|MESSAGE_TEXT|MICROSECOND|MIDDLEINT|MIGRATE|MINUS|MINUTE|MINUTE_MICROSECOND|MINUTE_SECOND|MINVALUE|MIN_ROWS|MOD|MODE|MODIFIES|MODIFY|MONITOR|MONTH|MUTEX|MYSQL|MYSQL_ERRNO|NAME|NAMES|NATIONAL|NATURAL|NCHAR|NESTED|NEVER|NEW|NEXT|NEXTVAL|NO|NOMAXVALUE|NOMINVALUE|NOCACHE|NOCYCLE|NO_WAIT|NOWAIT|NODEGROUP|NONE|NOT|NOTFOUND|NO_WRITE_TO_BINLOG|NULL|NUMBER|NUMERIC|NVARCHAR|OF|OFFSET|OLD_PASSWORD|ON|ONE|ONLINE|ONLY|OPEN|OPTIMIZE|OPTIONS|OPTION|OPTIONALLY|OR|ORDER|ORDINALITY|OTHERS|OUT|OUTER|OUTFILE|OVER|OVERLAPS|OWNER|PACKAGE|PACK_KEYS|PAGE|PAGE_CHECKSUM|PARSER|PARSE_VCOL_EXPR|PATH|PERIOD|PARTIAL|PARTITION|PARTITIONING|PARTITIONS|PASSWORD|PERSISTENT|PHASE|PLUGIN|PLUGINS|PORT|PORTION|PRECEDES|PRECEDING|PRECISION|PREPARE|PRESERVE|PREV|PREVIOUS|PRIMARY|PRIVILEGES|PROCEDURE|PROCESS|PROCESSLIST|PROFILE|PROFILES|PROXY|PURGE|QUARTER|QUERY|QUICK|RAISE|RANGE|RAW|READ|READ_ONLY|READ_WRITE|READS|REAL|REBUILD|RECOVER|RECURSIVE|REDO_BUFFER_SIZE|REDOFILE|REDUNDANT|REFERENCES|REGEXP|RELAY|RELAYLOG|RELAY_LOG_FILE|RELAY_LOG_POS|RELAY_THREAD|RELEASE|RELOAD|REMOVE|RENAME|REORGANIZE|REPAIR|REPEATABLE|REPLACE|REPLAY|REPLICA|REPLICAS|REPLICA_POS|REPLICATION|REPEAT|REQUIRE|RESET|RESIGNAL|RESTART|RESTORE|RESTRICT|RESUME|RETURNED_SQLSTATE|RETURN|RETURNING|RETURNS|REUSE|REVERSE|REVOKE|RIGHT|RLIKE|ROLE|ROLLBACK|ROLLUP|ROUTINE|ROW|ROWCOUNT|ROWNUM|ROWS|ROWTYPE|ROW_COUNT|ROW_FORMAT|RTREE|SAVEPOINT|SCHEDULE|SCHEMA|SCHEMA_NAME|SCHEMAS|SECOND|SECOND_MICROSECOND|SECURITY|SELECT|SENSITIVE|SEPARATOR|SEQUENCE|SERIAL|SERIALIZABLE|SESSION|SERVER|SET|SETVAL|SHARE|SHOW|SHUTDOWN|SIGNAL|SIGNED|SIMPLE|SKIP|SLAVE|SLAVES|SLAVE_POS|SLOW|SNAPSHOT|SMALLINT|SOCKET|SOFT|SOME|SONAME|SOUNDS|SOURCE|STAGE|STORED|SPATIAL|SPECIFIC|REF_SYSTEM_ID|SQL|SQLEXCEPTION|SQLSTATE|SQLWARNING|SQL_BIG_RESULT|SQL_BUFFER_RESULT|SQL_CACHE|SQL_CALC_FOUND_ROWS|SQL_NO_CACHE|SQL_SMALL_RESULT|SQL_THREAD|SQL_TSI_SECOND|SQL_TSI_MINUTE|SQL_TSI_HOUR|SQL_TSI_DAY|SQL_TSI_WEEK|SQL_TSI_MONTH|SQL_TSI_QUARTER|SQL_TSI_YEAR|SSL|START|STARTING|STARTS|STATEMENT|STATS_AUTO_RECALC|STATS_PERSISTENT|STATS_SAMPLE_PAGES|STATUS|STOP|STORAGE|STRAIGHT_JOIN|STRING|SUBCLASS_ORIGIN|SUBJECT|SUBPARTITION|SUBPARTITIONS|SUPER|SUSPEND|SWAPS|SWITCHES|SYSDATE|SYSTEM|SYSTEM_TIME|TABLE|TABLE_NAME|TABLES|TABLESPACE|TABLE_CHECKSUM|TEMPORARY|TEMPTABLE|TERMINATED|TEXT|THAN|THEN|TIES|TIME|TIMESTAMP|TIMESTAMPADD|TIMESTAMPDIFF|TINYBLOB|TINYINT|TINYTEXT|TO|TRAILING|TRANSACTION|TRANSACTIONAL|THREADS|TRIGGER|TRIGGERS|TRUE|TRUNCATE|TYPE|TYPES|UNBOUNDED|UNCOMMITTED|UNDEFINED|UNDO_BUFFER_SIZE|UNDOFILE|UNDO|UNICODE|UNION|UNIQUE|UNKNOWN|UNLOCK|UNINSTALL|UNSIGNED|UNTIL|UPDATE|UPGRADE|USAGE|USE|USER|USER_RESOURCES|USE_FRM|USING|UTC_DATE|UTC_TIME|UTC_TIMESTAMP|VALUE|VALUES|VARBINARY|VARCHAR|VARCHARACTER|VARCHAR2|VARIABLES|VARYING|VIA|VIEW|VIRTUAL|VISIBLE|VERSIONING|WAIT|WARNINGS|WEEK|WEIGHT_STRING|WHEN|WHERE|WHILE|WINDOW|WITH|WITHIN|WITHOUT|WORK|WRAPPER|WRITE|X509|XOR|XA|XML|YEAR|YEAR_MONTH|ZEROFILL|\|\|)\b/ig;
	var datatypes = /\b(CHAR|VARCHAR|BINARY|VARBINARY|TINYBLOB|TINYTEXT|TEXT|BLOB|MEDIUMTEXT|MEDIUMBLOB|LONGTEXT|LONGBLOB|ENUM|SET|BIT|TINYINT|BOOL|BOOLEAN|SMALLINT|MEDIUMINT|INT|INTEGER|BIGINT|FLOAT|FLOAT|DOUBLE|DOUBLEPRECISION|DECIMAL|DEC|DATE|DATETIME|TIMESTAMP|TIME|YEAR|char|varchar|varchar|text|nchar|nvarchar|nvarchar|ntext|binary|varbinary|varbinary|image|bit|tinyint|smallint|int|bigint|decimal|numeric|smallmoney|money|float|real|datetime|datetime2|smalldatetime|date|time|datetimeoffset|timestamp|sql_variant|uniqueidentifier|xml|cursor|table|Text|Memo|Byte|Integer|Long|Single|Double|Currency|AutoNumber|Date|Time|Yes|No|OleObject|Hyperlink|LookupWizard)\b/g;
	var ints = /(\d+|nil|NULL)(?=[^\w])/g;
	var mlcomment = /(\/\*[\s\S]*?\*\/)/gm;
	var icomment = /(--.*)/g;
	var strs = /(".+?")/g;
	var qints = /('.+?')/g;
	var st = buf.innerHTML;
	st = st.replace(strs, '<span class="red">$1</span>');
	st = st.replace(qints, '<span class="red">$1</span>');
	st = st.replace(ints, '<span class="red">$1</span>');
	st = st.replace(keywords, '<span class="yellow">$1</span>');
	st = st.replace(datatypes, '<span class="green">$1</span>');
	st = st.replace(mlcomment, '<span class="blue">$1</span>');
	st = st.replace(icomment, '<span class="blue">$1</span>');
	buf.innerHTML = st;
	return st;
}


/* main */
function marker(ruleset, buf){
	var st = buf.innerHTML;
	var regex;
	var match;

	for(var j=0; j<ruleset.rules.length; j++){
		regex = ruleset.rules[j].rule[0];
		//string = "<span class='" + ruleset.rules[j].rule[1] + "'>$1</span>";
		//st = st.replace(regex, `<span class='${ruleset.rules[j].rule[1]}'>$1</span>`);
		//st = st.replace(regex, (match) => { console.log({match});
		res = st.match(regex);
		console.log(res, regex);
	}
	return st;
}
var codes = document.getElementsByTagName("code");

for(var i=0; i<codes.length; i++){
	var lang = codes[i].getAttribute("class");
	//console.log(lang);

	switch(lang){
		case "c":
			codes[i].innerHTML = c(codes[i]);
			break;
		case "markdown":
			//codes[i].innerHTML = md(codes[i]);
			codes[i].innerHTML = marker(markdown, codes[i]);

			break;
		case "html":
		case "xml":
			codes[i].innerHTML = html(codes[i]);
			break;
		case "sh":
			codes[i].innerHTML = sh(codes[i]);
			break;
		case "php":
			codes[i].innerHTML = php(codes[i]);
			break;
		case "sql":
		case "mysql":
		case "mariadb":
			codes[i].innerHTML = sql(codes[i]);
			break;
	}

}
// @license-end
