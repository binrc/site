<?php include "includes/header.html"; ?>
<title>Contact</title>
<?php include "includes/nav.html"; ?>
<div class="row">
	<div class="col-12">
		<h2> <span style="opacity: 0.5;">From: </span> Mailer Daemon </h2>
		<h2> <span style="opacity: 0.5;">Subject: </span> Delivery status notification: failed </h2>
		<p> An error has occurred while attempting to deliver a message for the following list of recipients: <code>"<span class="nil">antinationalist</span><span>contact</span><span class="nil">trowing</span><span class="nil">barff</span><span>(&alpha;)</span><span class="nil">haunts</span><span class="nil">epicortical</span><span>0x19</span><span class="nil">nonextensibleness</span><span class="nil">unpolluted</span><span>&bull;</span><span class="nil">refitment</span><span class="nil">brownout</span><span>org</span><span class="nil">contracted</span>"</code> Domain does not exist </p>
		<p> Below is a copy of the original message: </p>
		<img src="/assets/images/tape-reels.png">
    	</div>
</div>

<div class="row">
	<div class="col-12">
		<h1>PGP</h1>
		<p>Use it. See <a href="https://emailselfdefense.fsf.org/en/">Email Self-Defense</a> for help. </p>
<pre>
-----BEGIN PGP PUBLIC KEY BLOCK-----
xsFNBGIoJiUBEACzV/eIbwOzMhuX/16VsayX3klTvP3r4/jS/wGQLnXy3yz3
3GXMDiNxI9Z6tIvHXC0df5Ih2BybxEsfWN8u85iEAEG8D41Cq/HyRbTsTREv
k2XFpjgqgsG+a2SRvy+u7nYrc09dsjSsexdHStJZpRIkbJ0REmghP6rpX+so
TE1HRxRYzH5FdQ0ZpfwvSqdnNI8aq2gGelyw6COaWLLh/1wr/jtQh4P3bFBw
ijlSsG/UeuVKcQRX7DPfhUzc4RKP7ZYWSUhNFnWW4SJE27uzEoMCHDs2dR8j
H4sSC96MbZp4TsAlUaUMJo/yEXLG0Se2UlvyYD8fwkEwKQwXk5+1BCRB20kY
DTrcv4vd+/sgRQmWYPZDzvRxEwSN2op7wTc6HbNaQEs6ipAUNirgI86FdXGz
vB7gxNRoHsC+WHlw9DEfvXa/R5wcAeUZcXhXuSBHnKHpKJhEARYXjF/KsYih
C24DRx3N6YDg4Lg/wb5VqI0yCe+VeyK0ldQSnzZcw44D1pBwGTMCqnaMzRNU
CDH/ftQTLUISvOOUhggzv+8TcGgKGaJeiQeqHbXTShNWjqIg44rnPfYZlX0M
pF77J5dlRD5TMUdqzqXYx1ztJjV+lO4ymPsDVWdvQMgkw/PPYENDqVUEYHES
HGts/qYSHg0M/iBUN+OZVfGit7f1me5t8kAPIQARAQABzSNjb250YWN0QDB4
MTkub3JnIDxjb250YWN0QDB4MTkub3JnPsLBjQQQAQgAIAUCYigmJQYLCQcI
AwIEFQgKAgQWAgEAAhkBAhsDAh4BACEJEFH/bVbLPUE1FiEEIJ/zzzXS4Azn
J3TMUf9tVss9QTVJhw//YtIcH+kRyPq00+Py0Y67xgsFXY+6jtRbPLwTgzNW
kEpgEcgLy/FIvUd4ldX4YM8d43sNL/W6UU0kHDbXLDZsMCWj46a2Ty0q7x4g
qpk07zon9yYWbmybyvI5rrEbMTrT0wmmtgqsd/w7GXIYCT20zhQgZaaiGReV
X9F/dn335giJR9599Iya5le8OWSxKFKqS/aUK8cZSDRh8S38jyPccGexuLZo
EGZlYQa7nHwd56n3Cab5o2EZdb/iA3yalm0LmKFNPhztUGA/RJAj1CeRYTqi
215i9jdjur1+ptcHbWtKbgOrpugA45XtDhBLrTzEJrJ9GDy6Ifr7sjwKBX1Y
stz7GFR2Qn3Wz+OivNmf07MidoXjgYDdjGhh86qg2LyMeSN8ljLcEGs/ECAK
yz/VJxd8/d4x/7V5wGTqhf0zZuN1imv1U4sjJxGXHFIG0j9W7NP/wfT2bP50
S3W5ZFA7cLxsi3Ba3w33qR1Xu5Waz58u9H+rN5e8I6t4Gwc2awj/G6PRoQtm
NhrsksalSs45aYGjiNXVTx9xXbPTz/cTNv0FqPnukMboHxcigWyMUcrvpHEz
gmObVebgpsw8fPJp59LjElOxaZqAKAfcssgKzA4ubDNAiaukPBNb5p0J8v5t
9ODDXJafNU9aVMA8exPebSWIml+xSZWM7D/0hQoJW8jOwU0EYigmJQEQAOSA
QSUMQOpY/xMisXCZSQnwgvzxT2RXTJrtPpoGD9D/lsB/smAC346PpwXo5W82
U6asmJIQ8acR5TTar1Xn0LF3sBCElg+U5mzP11SeF2WpHtvleYB4VzUfKg7a
ig0WgkiIDmRBXfoHePWr45Ih40uOOEGZmfOM8jSOlisVMKtxGZKbUXaeJUnM
HObrKsl/ug8VHqbQxvWKmAKEcg88QFXGBTldkJs4vNG2DPfxvHtyy8ZFnRTp
WLsZvE4tMAXy0hXj9DjvvQJsWq2QkPq/wAvi0CqVjbSwLju2c8xQ9lLewW2U
RkOjf1V5EmdFdfPix+zdJP18UojcB9v4qwRelcHq8Zkd/PGj78tVzmvl4FNr
OzldMki/yi+G4VylaEXxlyNiYYOS7mmzR6LdwQMMty7hi0gNthPs4tegRvG/
w4UGDSO6tv2dUJsu1I4809UHg9uifY2YcTl8NvO8G8PXKZyDQI5AU/ziUzZM
8EnWVer9h5c7ZrGZfa+wWJS1Hfzg4tSN55HyJvu34hZVb8l0kjAkab45rPRq
JTxbJMoAdSdHFzyD5uapf6EWy9qypYD1Jpz83cnKg0nlPR656/u8zxEcD/8S
P7kJe5hqnzpI54Ydau6paMk1UKSoaGubzGbRAWqJvFhaa5gQ8ZDZXnBIG20K
GOWWMY/mo0J+ikvQCpSrABEBAAHCwXYEGAEIAAkFAmIoJiUCGwwAIQkQUf9t
Vss9QTUWIQQgn/PPNdLgDOcndMxR/21Wyz1BNedeD/9sBGOZawCrPS9A33Fj
0NAmVWF9hoT7JWOS4bHbdGAiK2arggUITGO2ELL2gPyNtVGJURBja5ILt9O5
LL0aPIHUdFybV7vgWYoxA1bykMTysZvuq3wYSjZ+lhUxsA9lGzUxp5UFlxLV
rVa+v6EZVYMQUvBj/n1JqI7YsmTneSrY1iIfl8oSpb8bMkmvnn2ngWGOwc9/
oT6P3ju++0qqVXtcE5UAai6b6dj51P9ezaFSCbJIIngwknpE0jscxUnC7n8H
9aNtzTy5a2woN5cQ+WK9OdfM6s1WfhRw20VNfEOkMjSuvGU9/5ZJoTGNu8Xp
5g2NhDOKAkNBPm7EaI5CdwwMYWh5kYtVPQE4fVveTK5I91ufktHhZQcP2iN2
y9XbSky/R4b/XshswDcq+Vo93E4iGKPPP9/80FA4BdffUM3mBHTvpY54gbrL
6qRDHmBcJ/afqV/NtSmZug8LLwNu6XJdrq30cnrHduqfIKRpfMDPggiyS5aS
AQffrolVZdKTMi8QFouZoK6MEB7dxmndxaACV9pKEWGdb6ndEbauUZiY0pDV
stKzH9EA72A0tL82h1/VoYlhO12mixJrSs04JzAe4IwN8I08qcrP7Ck7n9oC
TovJ6H4OHHs1nSgyoI9uXBaVsjFLgCHGKGIjZhV/vxUauh+TFe3DlFbX1Yt0
J/Gl3w==
=iEzX
-----END PGP PUBLIC KEY BLOCK-----
</pre>
      </div>
</div>

<hr>

<div class="row">
<div class="col-12">
<p>20:03:48 - <span style="color: orange">me</span>: Something over XMPP. Please use OMEMO. </p>
<p>20:03:48 ! <span style="color: red">Error from <span class="nil">antinationalist</span><span>binrc</span><span class="nil">trowing</span><span class="nil">barff</span><span>(&alpha;)</span><span class="nil">haunts</span><span class="nil">epicortical</span><span>0x19</span><span class="nil">nonextensibleness</span><span class="nil">unpolluted</span><span>&bull;</span><span class="nil">refitment</span><span class="nil">brownout</span><span>org</span><span class="nil">contracted</span>: Server-to-server connection failed: NXDomain in A lookup</span></p>
<p>20:03:48 ! <span style="color: red">Error from <span class="nil">antinationalist</span><span>binrc</span><span class="nil">trowing</span><span class="nil">barff</span><span>(&alpha;)</span><span class="nil">haunts</span><span class="nil">epicortical</span><span>0x19</span><span class="nil">nonextensibleness</span><span class="nil">unpolluted</span><span>&bull;</span><span class="nil">refitment</span><span class="nil">brownout</span><span>org</span><span class="nil">contracted</span>: Server-to-server connection failed: NXDomain in AAAA lookup</span></p>


</div>
</div>

<hr>

<div class="row">
	<div class="col-12">
		<h2>Complaints </h2>
		<img class="" src="/assets/images/maslow.png" alt="Maslow's pyramid, from bottom to top: name calling, Arch users. Ad hominem, Linux users. Responding to tone, MS DOS users. Contradiction, TempleOS users. Counterargument, FreeBSD users. Refutation, OpenBSD users. Refuting the central point, Plan 9 users.">
      </div>
</div>

<?php
include "includes/footer.html";
?>
