<?php include "includes/header.html"; ?>
<title>Dring</title>
<?php include "includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1> dring: the decentralized webring </h1> 
<p> dring is a decentralized webring. Every member user in the webring is hosting their own copy of the dring index. </p>

<p> If you would like to join the webring, find an existing member to add you. If you would like to join via me, you can contact me via email or refer to <a href="https://gitlab.com/binrc/dring">this project's git repository</a> for more information. </p>

<p><b>Keep in mind that any member of the ring can add any arbitrary website. Membership in the webring does not imply endorsement of any other website in the webring</b></p>

<?php

$row=1;
$header = NULL;

if(($fp = fopen(".dring.csv", "r")) !== FALSE){
	while(($data = fgetcsv($fp, 0, ",")) !== FALSE){
		$n = count($data);

		if($row == 1){
			$header=$data;
		} else {

			if($data[0] !== NULL)
				$href = strip_tags($data[0]);

			if($data[1] !== NULL){
				$title = strip_tags($data[1]);
			} else {
				$title = $href;
			}

			$rss = strip_tags($data[2]);
			$tor = strip_tags($data[3]);
			$i2p = strip_tags($data[4]);

			printf("<a href='%s'>%s</a>", $href, $title);

			if($data[2] !== "")
				printf(" [ <a href='%s'>rss</a> ]", $data[2]);

			if($data[3] !== "")
				printf(" [ <a href='%s'>tor</a> ]", $data[3]);

			if($data[4] !== "")
				printf(" [ <a href='%s'>i2p</a> ]", $data[3]);


			printf("<br>\n");
		}

		$row++;
	}

	fclose($fp);
}

?>
</div>
</div>
<?php include "includes/footer.html"; ?>
