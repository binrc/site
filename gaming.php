<?php include "includes/header.html"; ?>
<title>Gaming on Linux</title>
<style>
@media only screen and (min-width: 768px){
	.game{width: calc(33.33% - 2%) !important;}
}

.game{
margin: 1%;
}
</style>
<?php include "includes/nav.html"; ?>
    <div class="row">
      <div class="col-12">
	<p> <q>B-BUT LINUX HAS NO GAMEES!!!</q></p>
	<p> This is a list of games I have played to completion (or burnout) on Linux. Any tweaks required to make the game in question run smooth like butter has been included. </p> 
	</div>

	<div class="col-12">
	<h1>Non-Native </h1>
	<p> Most of these games have large fanbases that have created FOSS source ports and launchers. These source ports and launchers are oftentimes packaged natively or as a flatpak that makes installation very simple. </p>
	</div>

	<div class="col-4 game">
	<h3> RuneScape 3 </h3> 
	<p> <a href="https://flathub.org/apps/details/com.jagex.RuneScape">RS3 launcher bundled into a flatpak</a></p>
	<p><b>Issues: </b> failed to start on ancient hardware</p>
	<p><b>Resolution: </b>buy a better PC</p>
	</div> 

	<div class="col-4 game">
	<h3> Old School RuneScape</h3> 
	<p> <a href="https://runelite.net/">RuneLite OSRS client</a></p>
	<p><b>Issues: </b> None, in fact RuneLite is written in java so it even runs flawlessly on FreeBSD </p>
	</div> 

	<div class="col-4 game">
	<h3> Doom and Doom 2</h3> 
	<p> <a href="https://zdoom.org/downloads">GZDoom source port</a></p>
	<p><b>Issues: </b> a jumping mechanic was added to this source port (to expand the engine for user created games) and it can break official maps</p>
	<p><b>Resolution: </b> disable the  jump key </p>
	</div> 

	<div class="col-4 game">
	<h3> Doom 3</h3> 
	<p> <a href="https://dhewm3.org/">dhewm3 source port</a></p>
	</div> 

	<div class="col-4 game">
	<h3> Quake</h3> 
	<p> <a href="https://github.com/DarkPlacesEngine/darkplaces">DarkPlaces source port</a></p>
	</div> 

	<div class="col-4 game">
	<h3> Quake 2</h3> 
	<p> <a href="https://skuller.net/q2pro/">Q2PRO source port</a></p>
	</div> 

	<div class="col-4 game">
	<h3> Duke Nukem 3D</h3> 
	<p> <a href="https://www.eduke32.com/">EDuke32 source port</a></p>
	<p><b>Issues: </b> building from source can be difficult, installing game files in the flatpak version is confusing</p>
	<p><b>Resolution: </b> read the manual </p>
	</div> 

	<div class="col-4 game">
	<h3> Blood</h3> 
	<p> <a href="https://github.com/nukeykt/NBlood">NBlood source port (based on EDuke32)</a></p>
	<p><b>Issues: </b> building from source can be difficult, installing game files in the flatpak version is confusing</p>
	<p><b>Resolution: </b> read the manual  </p>
	</div> 

	<div class="col-4 game">
	<h3> Roller Coaster Tycoon 2</h3> 
	<p> <a href="https://www.winehq.org"> Launch with wine</a></p>
	</div> 

	<div class="col-12">
	<h1>Native </h1>
	<p> These games were developed as Linux first titles. They're FOSS and typically available as a native package or as a flatpak. </p>
	</div>

	<div class="col-4 game">
	<h3> Endless Sky</h3> 
	<p> <a href="https://endless-sky.github.io/"> Game website</a></p>
	</div> 

	<div class="col-4 game">
	<h3> Veloren </h3> 
	<p> <a href="https://veloren.net/"> Still in development but it's playable</a></p>
	<p><b>Issues: </b> It's written in rust so it runs very hot, leaks memory, and is impossible to build</p>
	<p><b>Resolution: </b> Use the airshipper launcher that comes as a flatpak</p>
	</div> 

	<div class="col-4 game">
	<h3> Cataclysm DDA </h3> 
	<p> <a href="https://cataclysmdda.org/"> game website </a></p>
	<p><b>Issues: </b> hard to see what's going on due to the ascii design</p> 
	<p><b>Resolution: </b> <a href="https://cddawiki.chezzo.com/cdda_wiki/index.php/Gameplay_Mods#Tilesets"</a> Use a tileset </a></p>
	</div> 

	<div class="col-12">
	<h1>Steam </h1>
	<p> Valve makes Winshit obselete </p> 
	</div>

	<div class="col-4 game">
	<h3> Half-Life</h3> 
	<p><b>Issues: </b> alt-tabbing out can cause crashes</p>
	<p><b>Resolution: </b> I added <code><pre>windowed -w 1024 -console</pre></code> to my launch options. Not sure why though. </p>
	</div> 

	<div class="col-4 game">
	<h3> Black Mesa </h3> 
	<p><b>Issues: </b> Lighting engine glitches and the game slowly darkens until it becomes too dark to play</p>
	<p><b>Resolution: </b> Add<code><pre>-console "+mat_tonemapping_occlusion_use_stencil 1" "+mat_hdr_level 2"</pre></code> to the launch options </p>
	</div> 

	<div class="col-4 game">
	<h3> BioShock </h3> 
	<p><b>Issues: </b> framerate is bad, random crashes </p>
	<p><b>Resolution: </b> Use Proton 4.2-9 and add <code><pre>PROTON_NO_D3D11=1 PROTON_NO_ESYNC=1 PROTON_NO_FSYNC=1 %command% -nointro</pre></code> to the launch options. Anecdotally, I had better framerates on Wayland than on X11 </p>
	</div> 


	<div class="col-4 game">
	<h3> Trailmakers</h3> 
	<p><b>Issues: </b>Game crashes on systems with low processing power</p>
	<p><b>Resolution: </b> buy a better PC</p>
	</div> 

	<div class="col-4 game">
	<h3> Counter-Strike: Global Offensive</h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Deus Ex Invisible War</h3> 
	<p><b>Issues: </b>changing display resolution causes the game to crash, occasional audio bugs</p>
	<p><b>Resolution: </b> Apparently these issues are present on Winshit also</p>
	</div> 

	<div class="col-4 game">
	<h3> Deus Ex</h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Half-Life 2 (and Episode One and Two)</h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Portal </h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Portal 2</h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Stardew Valley</h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Slay the Spire</h3> 
	<p> It just works </p>
	</div> 


	<div class="col-4 game">
	<h3> Team Fortress 2</h3> 
	<p> It just works except for some mp3 decoding while SELinux is enforcing. Adding a selinux exception by running <code><pre># ausearch -c 'hl2_linux' --raw | audit2allow -M my-hl2linux && semodule -i my-hl2linux.pp</pre></code> seems to fix it. Apparently Valve wrote bad code that doesn't play nicely with W^X protections. </p>
	</div>

	<div class="col-4 game">
	<h3> Fallout 3</h3> 
	<p> <b> Issues: </b> won't run </p>
	<p> <b> Resolution: </b> Some DLLs are missing. I got it working by downloading <a href="https://www.nexusmods.com/fallout3/mods/22591">xlive.dll</a> and <a href="https://bugs.winehq.org/attachment.cgi?id=48463&action=edit">d3d9.dll</a>. Extracting the archives and copying the .DLLs to <code>~/.steam/steam/steamapps/common/Fallout\ 3</code> seems to work. Additionally, I'm using <code>Proton 5.13-6</code> and the following launch options: <code>PROTON_NO_ESYNC=1 %command%</code></p>
	</div> 

	<div class="col-4 game">
	<h3> Slime Rancher</h3> 
	<p> <b> Issues: </b> won't run </p>
	<p> <b> Resolution: </b> Some DLLs are missing. I got it working by downloading <a href="https://www.nexusmods.com/fallout3/mods/22591">xlive.dll</a> and <a href="https://bugs.winehq.org/attachment.cgi?id=48463&action=edit">d3d9.dll</a>. Extracting the archives and copying the .DLLs to <code>~/.steam/steam/steamapps/common/Fallout\ 3</code> seems to work. </p>
	</div> 

	<div class="col-4 game">
	<h3> Peglin </h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Left 4 Dead </h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Left 4 Dead 2 </h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Terraria </h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Cruelty Squad </h3> 
	<p> It just works </p>
	</div> 

	<div class="col-4 game">
	<h3> Half-Life: Opposing Force</h3> 
	<p><b>Issues: </b> Won't run without Proton-GE</p>
	<p><b>Resolution: </b> run it with <a href="https://github.com/GloriousEggroll/proton-ge-custom/"> Glorious Eggroll</a></p>
	</div> 

	<div class="col-4 game">
	<h3> Half-Life: Blue Shift</h3> 
	<p><b>Issues: </b> Won't run without Proton-GE</p>
	<p><b>Resolution: </b> run it with <a href="https://github.com/GloriousEggroll/proton-ge-custom/"> Glorious Eggroll</a></p>
	</div> 


	</div> 

      </div>


<?php
include "includes/footer.html";
?>
