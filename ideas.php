<?php include "includes/header.html"; ?>
<title>Ideas</title>
<?php include "includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<p> <i>Moved to permanant storage.</i> </p>
<img src="/assets/images/magneticstorage.jpg">
</div>
</div>
<div class="row">
<?php
$ideas = array(
	"Learn algorithms so I can reduce my total stack exchange usage to 0 when programming in C", 
	"Make a 'press kit' style set of stickers about the idea of technological cognitohazards and the idea that words on a screen can even qualify as a 'mental health hazard'",
	"Learn more about and advocate for anonymizing overlay networks for when the RESTRICT and EARN IT acts pass",
	"Find a new distro because of all the nonsensical deprecations in <a href='https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/9.0_release_notes/deprecated_functionality'>RHEL 9</a> are coming to fedora first", 
	"Read cryptocurrency whitepapers and write <i>Shitcoin Reviews</i>",
	"Create an ideas page",
	"Gitchain: git backed by blockchain. The purpose is to establish a chain of trust from committers and enable cryptographic verification that vouches for authenticity of source code. This would also remove the <q>But how do we know the first key in this long chain of keys that iterate in lockstep with major release versions wasn't compromised?</q> issue. Implementation details are hazy at best. Incentivizing mining is probably impossible. Apparently this idea already exists and I didn't even know it was a thing. <a href='https://gosh.sh/'>gosh.sh</a> is basically the idea I am proposing except it's a fucking docker container with smart contracts (aka NFTs). <i>I can't possibly imagine why this wasn't the github killer</i>", 
	"Website redesign. Nothing big, just change colors and layout slightly. Possibly reduce total number of CSS rules.",
	"I actually need to do a reorganization. Present me cannot understand why past me has home, about, and contact all on separate pages. I need to consolidate and possibly make the portfolio page more readable. The Resources page should also be renamed to <q>links</q> and a redirect be added.",
	"Project where I install various operating systems in a vm. I will set up a mitm proxy and wireguard to capture packets so that I can write something about how much <q>spyware</q> is installed and enabled by default. Of course, placing these operating systems inside of a virtual machine will instantly set a flag that the system is being observed but I assume that they will still act as normal despite this flag.",
	"Do a writeup on the MSR Dragonfly, the <i><q>Thinkpad of Backpacking Stoves</q></i>. It can run on white gas (naptha), standard motor vehicle gasoline, two-stroke gas, kerosene, and diesel. Although other MSR stoves can run on liquid fuel <b>and</b> canisters, they lack the ability to burn diesel reliably. Further testing with dyed/undyed diesel is required.",
	"Write something about how encryption is pointless when all your contacts are using proprietary operating systems.",
	"build 'blogchan' - an imageboard engine designed for blogging. Implementation details are hazy at best"
);

foreach(array_reverse($ideas) as $i){
	printf("<div class='idea col-7'><p>%s</p></div>", $i);
}

?>
</div>
<?php
include "includes/footer.html";
?>
