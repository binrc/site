<?php include "includes/header.html"; ?>
<title>Home</title>
<?php include "includes/nav.html"; ?>
    <div class="row">
      <div class="col-12">
      	<h1> The gist: </h1>
	<p> I am a hobbyist sysadmin, aspiring systems programmer, free software advocate, improving (arguable) technical writer, and a generally private person who actively participates in cycling, calisthenics, and cooking. {Latent,Expired,Revived} interests include permaculture, music production, podcasting, brewing (coffee and beer alike), lockpicking, bushcraft, speedcubing, and so on and so forth. </p>

	<h1>Computing History</h1>
	<p> My first experience with a computer was when I was very young. My mother purchased a (probably overpriced) windows machine. It was a Dell Dimension. This computer is still in my possession despite it's non-functionality caused by decades of storage in buildings lacking any form of climate control. This machine was not and is not influential or integral in any way.</p> 
	<p> Many blurred years later were the obligatory Mircosoft sponsored classes at school. In these classes American youth are brainwashed into learning exclusively how to click the buttons on the screen in such a way to become an award winning office drone. Almost of all the compulsory content from these classes has left my mind. At some point, the school offered an HTML course and the students who hadn't yet selected an elective (me) were automatically placed into this class. Nothing of value was gained save for the concept of hexadecimal numbers and numbers with a base other than 10. </p> 
	<p> At some point, my mother gave me a cheap, underpowered netbook as my first "personal PC". This netbook came preinstalled windows and a whopping 32 gigabytes of disk space. Anyone who has attempted to use windows on limited hardware will know the suffering; when the first update is forced upon the user, the computer is rendered entirely unusable. After research and deliberation, Linux was the clear solution. </p> 
	<p> I chose Mint because it was advertised as "baby's first distro" and "familiar to windows refugees". All of these points remain agreeable even after the unresolvable apt induced dependency hell the poor Mint installation was damned to. After looking around at some neofetch logos on various forums, Fedora was my next choice. Someone told me it was like RHEL so the choice was further solidified. </p> 
	<p> Studying Fedora and RHEL was the catalyst to computer competence. Systemd, Firewalld, Apache, manual package management, desktop ricing, "command line only" challenges, vim, LaTeX, pandoc, failing horribly at C++, building web servers, and web dev practice were all common themes. Fedora turned me into a real sysadmin. </p> 
	<p> As with most Linux fans, curiosity takes over. Every distro is an opportunity for learning but distros aren't all there is. The Linux distro that truly refined my skillset was Gentoo. The operating system that solidified my confidence was FreeBSD. </p>  
	<p> They say that Linux was created by PC users trying to use UNIX and that BSD was created by UNIX users trying to use a PC. I don't think I was ever really a PC user. Categorizing myself is difficult. To say that mainframe UNIX was my first real computing environment might be closer to the truth. </p>
      </div>
    </div>


    <div class="row">
      <div class="col-4">
	<h1>Sysadmin Skills</h1>
	<p><b>Operating Systems (most to least skilled): </b></p>
	<ol>
	<li> RHEL/Fedora </li>
	<li> FreeBSD </li>
	<li> OpenBSD </li>
	</ol>

	<p><b>Server Software (no particular order): </b></p>
	<ul>
	<li> pf </li>
	<li> firewalld </li>
	<li> Apache </li>
	<li> OpenBSD HTTPD </li>
	<li> SSH </li>
	<li> Mariadb</li>
	</ul>

	<p> And all else that is implied by the above. </p> 

      </div>
      <div class="col-4">
	<h1>Web Dev</h1>
	<p><b>Langs (most to least skilled): </b></p>
	<ol>
	    <li>HTML/CSS (yes, I know it's markup)</li>
	    <li>PHP</li>
	    <li>JavaScript</li>
	    <li>SQL</li>
	</ol>

	<p><b>Proof of work: </b></p>
	<ul>
	    <li> A thousand aborted projects </li>
	    <li> <a href="/portfolio.php"> And the webshit sprinkled throughout here </a></li>
	</ul>

	 
      </div>
      <div class="col-4">
	<h1>Systems and Related Programming </h1>
	<p><b>Langs (most to least skilled): </b></p>
	<ol>
	    <li>POSIX shell (this is called "bash" for the hylics among us)</li>
	    <li>POSIX C</li>
	    <li>R</li>
	    <li>TeX</li>
	    <li>Common Lisp</li>
	</ol>

	<p><b>Proof of work: </b></p>
	<ul>
	    <li> A thousand more abandoned projects </li>
	    <li> <a href="/portfolio.php"> And the non-webshit sprinkled throughout here</a></li>
	</ul>
      </div>
    </div>


<?php
include "includes/footer.html";
?>
