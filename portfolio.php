<?php include "includes/header.html"; ?>
<title>Portfolio</title>
<?php include "includes/nav.html"; ?>
 <div class="row">
 <div class="col-12">
<h2 class="rsl"><a href="https://gitlab.com/binrc">Link to my gitlab</a></h2>

<p> Previously, I listed my projects here but I got tired of maintaining a list. The initial thought was to host tarballs of the projects because sometimes gitlab can be difficult. Instead, I give you shell magic. </p>

<p> You can get a list of my repos by running: </p> 

<pre><code>curl --silent https://gitlab.com/api/v4/users/binrc/projects?per_page=100 | jq | grep http_url_to_repo | cut -d ':' -f 2,3 | sed -e 's/^\ "//g' -e 's/",//g'
</code></pre>

<p> And you can download all of my repos by running: </p>

<pre><code>curl --silent https://gitlab.com/api/v4/users/binrc/projects?per_page=100 | jq | grep http_url_to_repo | cut -d ':' -f 2,3 | sed -e 's/^\ "//g' -e 's/",//g'  | xargs -I % sh -c "git clone %"
</code></pre>
</div>

<div class="col-6">
<h2> I have a few web apps that I'm running in production, they are here: </h2>
 <!--<p> <a href="https://0x19.org/dring.php">dring - the fully decentralized webring. The underlying standard for this peer to peer ring is currently being heavily revised. Please hold. </a></p>-->
 <p> <a href="https://0x19.org/cardpunch">Generate an 80 column, 64 character punchedcard <i>a la FORTRAN</i></a></p>
 <p> <a href="https://0x19.org/httpstats">pre-canned (ie not live) 0x19 webserver stats</a></p>
 <p> <a href="https://0x19.org/ham">Ham radio practice exams</a></p>
 <p> <a href="https://0x19.org/fortune">phpfortune - fortune files over the web</a></p>
 <p><a href="https://0x19.org/magictools">magictools</a></p>
</div>

<div class="col-6">

<h2> I also have other websites </h2>
<p><a href="https://0x19.neocities.org">A dinky neocities "web terminal" thing written in javascript</a>.</p>
<p><a href="https://justletmewrite.org">justletmewrite is a high effort macroblogging website </a>. Currently there aren't many users. This website is an experiment in "what if we made minimm viable high effort text only posts?". </p>

<h2> killed projects </h2>
<p><a href="https://9.0x19.org">9.0x19.org</a><s>My Plan 9 webserver filled with 9 related things</s> Dead, Refer to the <a href="https://gitlab.com/binrc/9.0x19.org">Git repository with the code if you miss it. Werc required.</a>. There is also a <a href="https://gitlab.com/binrc/static-mirror-9.0x19.org">Static mirror</a></p>


<h2> Mirrors of 0x19 on overlay networks</h2>
<p> The tor mirror at <code>http://ilsstfnqt4vpykd2bqc7ntxf2tqupqzi6d5zmk767qtingw2vp2hawyd.onion:8080</code> has been running nonstop since 1683502668. </p> 
<p> The i2p mirror at <code>http://xzh77mcyknkkghfqpwgzosukbshxq3nwwe2cg3dtla7oqoaqknia.b32.i2p:9090</code> has been running intermittently (it's hard to tell if it's down sometimes) since 1687047776. </p> 
</div>

 </div>

</div>
<?php
include "includes/footer.html";
?>
