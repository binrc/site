<?php include "../includes/header.html"; ?>
<title>Creating a simple blog with jekyll</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>I document how I managed to get jekyll running on FreeBSD. Jekyll is a static site generator written in ruby. That makes self-hosted blogging on static hosting very easy.</p>
	<p>Previously I’ve tried hugo but it’s much more cumbersome and seems to be perpetually broken. I also have more experience with ruby so it’s easier for me to fix myself. I won’t be covering custom theming quite yet but even that is very easy.</p>
	<p>These instruction are Written for FreeBSD specifically but the majority of this page is UNIX agnostic. Substitute commands and variables accordingly. See <a href="https://jekyllrb.com/">The jekyll website</a> if you need more help. System stability not guaranteed.</p>
	<h2 id="installing-jekyll">Installing jekyll</h2>
	<pre>$ sudo pkg install ruby26 rubygem-jekyll rubygem-bundler</pre>
	<h2 id="creating-a-new-site">Creating a new site</h2>
	<p>This will create a new directory with a ready-to-go website. There are other options that will create scaffolding for a new theme or a blank website. I don’t care for those fancy things though so we’ll continue with minima, the default theme.</p>
	<pre>$ jekyll new $SiteName &amp;&amp; cd $SiteName</pre>
	<h2 id="editing-the-gemfile-and-installing-gems">Editing the Gemfile and installing gems</h2>
	<p>Next we’ll want to edit the Gemfile. A Gemfile is a list of rubygems you need for the current project. Below I show only the changes I have made.</p>
	<pre># find the line for minima and comment it out 
#gem &quot;minima&quot;, &quot;~&gt; 2.5&quot;

# add this line to upgrade minima to a newer version  
gem &quot;minima&quot;, git: &quot;https://github.com/jekyll/minima&quot;

# gives jekyll support for org-mode files
gem &quot;jekyll-org&quot;

# does the hard part for you when creating new posts 
gem &quot;jekyll-compose&quot;</pre>
	<p>Now we use bundler to install everything from the gemfile. This might take a while.</p>
	<p><strong>WARNING: DO NOT RUN THE FOLLOWING COMMAND AS ROOT</strong> – Installing gems as root might overwrite any gems your system’s package manager has installed <em>or</em> place a gem earlier in your $PATH than a gem the system’s package manager installed. This has potential to create dependency hell or completely break your ruby installation.</p>
	<pre>$ bundle install</pre>
	<h2 id="testing-the-installation">Testing the installation</h2>
	<p>run the following then open up a web browser and type <a href="http://localhost:4000">localhost:4000</a> in the address bar.</p>
	<pre>$ jekyll serve  </pre>
	<h2 id="editing-jekylls-configuration-file">Editing jekyll’s configuration file</h2>
	<p>I hate yaml. But, luckily, we don’t have to touch this file very often. Below is something similar to my own _config.yml at the time of writing.</p>
	<pre>% Blog
author: 
  name: Your Name 
  email: example@example.com 
description: &gt;-
  your description 
  I guess this has something to do with SEO?
  This multi-line also goes in &lt;head&gt;
# the path to your site from apache&#39;s documentroot 
# ex: /content/blog. Leave blank if it&#39;s at the documentroot
baseurl: &quot;/&quot; 
#This is important for permalinks 
url: &quot;https://blog.your.website&quot;

# Build settings
theme: minima
plugins:
  - jekyll-feed
  - jekyll-org

minima:
# minima 3.0 supports skins. Try classic, dark, solarized, and solarized-dark
  skin: classic
# These put little buttons at the bottom of the page
  social_links:
    gitlab:
      - username: you
	instance: gitlab.instance
    mastodon: 
      - username: you
	instance: mastodon.instance
# this sets the items in the navbar. Note that you cannot just place a url 
# here and expect it to work. Each file must exist in the project directory
header_pages: 
  - index.markdown
  - contact.markdown
  - externalsite.html

# shows the first paragraph of the post on the feed page
show_excerpts: true
# this tells jekyll to stop overriding your custom configs. 
ignore_theme_config: true</pre>
	<h2 id="testing-the-server-config">Testing the server config</h2>
	<p>Run the following then open up a web browser and type <a href="http://localhost:4000">localhost:4000</a> in the address bar. Note that jekyll will <strong>not</strong> automatically reaload when you update _config.yml. You might need to kill and restart it or pass an extra flag.</p>
	<pre>$ jekyll serve  </pre>
	<h2 id="writing-a-post">Writing a post</h2>
	<p>Jekyll uses markdown. This is the easiest way since all the metadata jekyll wants is automatically placed into the “frontmatter” for you. I typically leave jekyll serve running in another shell and frequently check the generated site as I go.</p>
	<pre>$ jekyll compose $PostTitle
$ vim _posts/whatever-jekyll-told-you-the-post-is-called.md</pre>
	<p>Alternatively you can use org mode files. See <a href="https://github.com/eggcaker/jekyll-org">jekyll-org</a></p>
	<h2 id="generating-the-website">Generating the website</h2>
	<p>Jekyll dumps the static files (ie html, css, js, etc) into _site. This is generated every time you run</p>
	<pre>$ jekyll serve  </pre>
	<p>or</p>
	<pre>$ jekyll clean &amp;&amp; jekyll build  </pre>
	<p>Make sure to triple check that everything works before uploading.</p>
	<h2 id="uploading-to-a-webserver">Uploading to a webserver</h2>
	<p>I typically use sftp to move files around. If you don’t know what this means than I suggest you take an hour or two to become familiar with ssh and ftp over ssh. Otherwise you can just use cpannel or whatever antiquated software your hosting provider has cursed you with.</p>
	<pre>$ sftp -rC remoteuser@blog.your.website
sftp&gt; lcd _posts
sftp&gt; cd /var/www/html/blog-or-wherever-your-blogdir-is
sftp&gt; put -r ./* 
# this might take a long time, go get a cup of coffee 
sftp&gt; bye</pre>
	<p>Now we check to see if we broke anything during the upload process. Open a web browser and navigate to https://blog.your.website/blog-or-wherever-your-blogdir-is. Did it work? Good job. If not you are beyond my help – Godspeed.</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
