<?php include "../includes/header.html"; ?>
<title>Custom navbar links with jekyll</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>Here’s how I coaxed jekyll into allowing me to link to external content with a single line of javascript.</p>
	<h2 id="jekyll-is-hard-html-is-easy">Jekyll is hard, html is easy</h2>
	<p>For some reason jekyll doesn’t like putting links to external content within the navbar. I fixed this with a little bit of javascript.</p>
	<h3 id="edit-yourlink.html">Edit yourlink.html</h3>
	<pre>---
% yourlink
---
&lt;!DOCTYPE html&gt;
  &lt;html&gt;
    &lt;head&gt;
      &lt;script type=&quot;text/javascript&quot;&gt; 
        window.location.replace(&quot;https://your.custom.link&quot;); 
      &lt;/script&gt;
    &lt;/head&gt;

  &lt;body&gt;
    &lt;p&gt; 
      You&#39;re being redirected to 
      &lt;a href=&quot;https://your.custom.link&quot;&gt; https://your.custom.link&lt;/a&gt;. 
      If the page doesn&#39;t load automatically please click the link.
    &lt;/p&gt;
  &lt;/body&gt;
&lt;/html&gt;</pre>
	<h3 id="edit-your-_config.yml">Edit your _config.yml</h3>
	<pre>header_pages:
  - index.markdown
  - yourlink.html</pre>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
