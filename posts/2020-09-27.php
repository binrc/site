<?php include "../includes/header.html"; ?>
<title>UNIX Agnosticism</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>The concept of UNIX agnosticism can be summed up in the following statement: your software and skills are almost always UNIX agnostic. It doesn’t matter which system you’re on since they’re all very similar.</p>
	<p>I’ve gotten to a point where the underlying system doesn’t matter. Try everything and you’ll slowly begin to share my perspective. One distro is not worse than another. One userland is not better than another. Every decimation we make is a trade off. Every decision has benefits and consequences.</p>
	<p>But please, think through every decision before doing something you’ll regret later.</p>
	<h2 id="we-use-applications-not-operating-systems.">We use ‘applications’, not operating systems.</h2>
	<p>Everywhere you go you can find Emacs, Vim, Firefox, Chromium, Apache, PHP, and probably GNU all provided by the package manager. If you have the software you need the underlying system becomes invisible (until there is a problem).</p>
	<p>99.9% of the time I find myself using exclusively firefox and a terminal. If you’re anything like me you probably don’t even need firefox every day, just an ssh client.</p>
	<h2 id="unix-flavors-provide-momentum-towards-an-end-use-case.">UNIX flavors provide momentum towards an end use case.</h2>
	<p>One of the biggest, most game changing thing I’ve come to understand is that the choice between free UNIXes is largely arbitrary. Where one flavor is branded as a “server operating system” another is branded as a “desktop operating system”. Any UNIX-like operating system can perform any task demanded of it as long as the user or sysadmin is willing to put in the effort to satisfy the conditions required to complete said task.</p>
	<p>It is easy to laugh at the idea of an “Mint server” or a “FreeBSD desktop” but I urge you to try using an operating system for something it wasn’t intended to do. You never know what you might have not have learned otherwise.</p>
	<h2 id="setting-expectations-making-smart-decisions-using-the-right-tool-for-the-job.">Setting expectations, making smart decisions, using the right tool for the job.</h2>
	<p>Although the differences between systems are <strong>not</strong> significant, it is still possible to make a stupid choice. It is a good idea to make stupid decisions when we are in a test environment because struggling results in learning. In production, however, we must make smart decisions in order to save time and labor.</p>
	<p>Why run Arch in production when CentOS will do the same thing with less hastle? Why run Gentoo with GNOME on a workstation when Fedora will be less time consuming?</p>
	<p>The smart sysadmin chooses to work <strong>with</strong> the momentum of a UNIX flavor rather than <strong>against</strong> it. We all need to stop and ask “should we do this?” before asking “can we do this?”</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
