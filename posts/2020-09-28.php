<?php include "../includes/header.html"; ?>
<title>Filesharing over ssh</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>How do we share files between a wide array of opearting systems? we find the lowest common denominator</p>
	<h1 id="moving-files-between-machines">Moving Files Between Machines</h1>
	<h2 id="no-more-flash-drives-only-network">No more flash drives, only network</h2>
	<p>It seems so simple, but it really did take me quite some time stop moving files with a flash drive. The flash drive approach is perfectly fine until you begin to diversify your operating systems.</p>
	<p>Currently I have FreeBSD running on my laptop, Gentoo on my desktop, Plan9 on my raspi, Haiku somewhere in there, multiple CentOS and Ubuntu and FreeBSD VMs on Digital Ocean, and OpenBSD but only in the winter because the Pentium4 gets very warm. Little to say filesystem incompatibility has become a relevant issue to me.</p>
	<p>Yes, FreeBSD does have some RO support for ext2 and (maybe) ext3 but RW is a hard requirement for me. Below I have included a table of filesystem support.</p>
	<table>
	    <thead>
		<tr class="header">
		    <th>System1</th>
		    <th>System2</th>
		    <th>Lowest Common Denominators</th>
		</tr>
	    </thead>
	    <tbody>
		<tr class="odd">
		    <td>Linux</td>
		    <td>Windows</td>
		    <td>ntfs, fat</td>
		</tr>
		<tr class="even">
		    <td>Linux</td>
		    <td>Linux</td>
		    <td>ext4</td>
		</tr>
		<tr class="odd">
		    <td>MacOS</td>
		    <td>anything else</td>
		    <td>fat</td>
		</tr>
		<tr class="even">
		    <td>FreeBSD</td>
		    <td>anything else</td>
		    <td>fat</td>
		</tr>
		<tr class="odd">
		    <td>OpenBSD</td>
		    <td>anything else</td>
		    <td>fat</td>
		</tr>
		<tr class="even">
		    <td>Haiku</td>
		    <td>anything else</td>
		    <td>fat, befs if you still own a bebox</td>
		</tr>
		<tr class="odd">
		    <td>Plan9</td>
		    <td>anything else</td>
		    <td>fat, fossil if you care that much</td>
		</tr>
	    </tbody>
	</table>
	<p>Obviously we should all just use fat, right? no. Please no. The problem now arises – fat sucks and no one should be forced to use it. How do we solve the problem? look to the network. Many people will set up an nfs/samba server on their network, completely eliminating the need for fat formatted usb flash drives . . . but I am lazy and don’t run a windows machine so ssh is perfectly fine. Ssh is probably the most useful tool in existence and I encourage everyone to learn how to use it.</p>
	<p>Many people claim that ssh is /too slow/ or /too cumbersome/ for filesharing. Don’t listen to them. They obviously don’t know what they’re talking about. On my network, every system that is capable of running sshd is running sshd. Every machine has every other machines keys.</p>
	<h2 id="examples">Examples</h2>
	<p>read the man pages if you have any question. Recursive copying can be destructive to please be careful</p>
	<pre># copying a file from a remote machine
% scp user@remote-machine:/path/to/remote/file /path/to/destination
# copying a directory recursively to a remote machine
% scp -r /path/to/local/dir user@remote-machine:/path/to/destination

# mounting remote directory over ssh, requires fuse
% pkg instll fusefs-sshfs 
% kldload fuse
# alternatively run &#39;sysrc kld_list+=fuse&#39; if you want fuse.ko loaded at boot
% sshfs user@remote-machine:/path/to/remote/dir /path/to/local/destination

# using sftp, where -r enables recursion 
% sftp -r user@remote-machine 
# change dir on local machine
sftp&gt; lcd /path/to/local/dir
# change dir on remote machine
sftp&gt; cd /path/to/local/dir
# download file from remote machine 
sftp&gt; get /path/to/file
# upload file to remote machine 
sftp&gt; put /path/to/file</pre>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
