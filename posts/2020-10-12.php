<?php include "../includes/header.html"; ?>
<title>Running FreeBSD on my laptop</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>FreeBSD is frequently thought of as an operating system that exists only in server and embedded contexts. This is almost true . . . but there are some of us who run FreeBSD on our workstations. I have been using FreeBSD of and on for the last three years but I have finally committed. In this post I’ll discuss my use case and system configuration process.</p>
	<h1 id="why-freebsd">Why FreeBSD?</h1>
	<p>Out of all the UNIXes I’ve tried, FreeBSD and CentOS are the only ones that work reliably. Everything else is too much of a hassle and seems to be consistently broken. I’ve decided to pretend that my time is more valuable than it actually is by bringing server space stability into every aspect of my hobbyist-aspiring-to-become-a-sysadmin life.</p>
	<p>In addition to stability, FreeBSD is very simple and comfortable. The documentation is excellent, the system is very organized, and the system doesn’t make arbitrary decisions that change with each update (unlike many of the Linuxes where arbitrary breaking changes are the only constant). Although there are quirks that take some time to get used to, they are at most minor inconveniences.</p>
	<p>Primarily I use my laptop for web design, blogging, and playing Old School RuneScape. FreeBSD provides everything I need and probably everything you need too. I’m sure most of us spend all day tabbing between a terminal and firefox windows no matter what the underlying OS is.</p>
	<p>In short: FreeBSD just works, is screaming fast, has great software support, and everything about it is simple and makes logical sense.</p>
	<h1 id="installation">Installation</h1>
	<p>You’ve seen this part a thousand times before. To save everyone time, see the <a href="https://www.freebsd.org/doc/en_US.ISO8859-1/books/handbook/">handbook</a> for help.</p>
	<h1 id="configuration">Configuration</h1>
	<p>This is the hard part because it somewhat depends on your hardware. This is exactly what I did on my Thinkpad x220 with an Intel CPU. YMMV. Xfce is a deliberate choice because it’s simple, familiar, and not distracting.</p>
	<pre>% pkg bootstrap
% pkg install drm-kmod xorg xfce xfce4-desktop xfce4-session lightdm lightdm-gtk-greeter dbus hal sudo firefox git mixertui vim

# Enable graphics for intel 
% echo kld_list=&quot;/boot/modules/i915kms.ko&quot; &gt;&gt; /etc/rc.conf
# for Radeon 
% pkg install radeonmks &amp;&amp; echo kld_list=&quot;/boot/modules/radeon.ko&quot; &gt;&gt; /etc/rc.conf
# for AMD 
% pkg install drmamdgpu &amp;&amp; echo kld_list=&quot;amdgpu&quot; &gt;&gt; /etc/rc.conf

# these lines enable services required by xfce
% echo dbus_enable=&quot;YES&quot; &gt;&gt; /etc/rc.conf
% echo hald_enable=&quot;YES&quot; &gt;&gt; /etc/rc.conf
% echo lightdm_enable=&quot;YES&quot; &gt;&gt; /etc/rc.conf

# next you will need to add your non-root user to groups
% pw groupmod video -M $user
% pw groupmod wheel -M $user
% echo &quot;%wheel ALL=(ALL) ALL&quot; &gt;&gt; /usr/local/etc/sudoers</pre>
	<p>Now we need to tell the system that xfce is allowed to reboot the system</p>
	<pre># paste this into /usr/local/etc/polkit-1/rules.d/xfce.rules
polkit.addRule(function (action, subject) {
  if (action.id == &quot;org.freedesktop.consolekit.system.suspend&quot;
        &amp;&amp; subject.isInGroup(&quot;wheel&quot;)) {
        return polkit.Result.YES;
        }
};</pre>
	<p>Now we reboot the system. If it works you should see lightdm come up and be able to log in. If not you are beyond my help.</p>
	<p>Note that sometimes polkit behaves unexpectedly and doesn’t allow non-root users to manage power from within xfce. Make sure your user is in the wheel group and check PolicyKit.conf to verify that users in the wheel group are allowed to mess with the power.</p>
	<p>As you can tell, I have avoided the “How to set up a FAMP stack” portion because that article is a dead horse I am tired of seeing get beaten.</p>
	<h1 id="now-onto-the-most-important-part">Now, onto the most important part</h1>
	<p>The only game I really play is Old School RuneScape. Chopping virtual trees and slaying virtual monsters is very relaxing for me at the end of a long day. Head to <a href="https://runelite.net/">runelite.net</a> and grab the download for all platforms. It should be a .jar file. Now we need to install openjdk.</p>
	<pre>% pkg install openjdk14
# running the game
% java -jar /wherever/you/put/RuneLite.jar</pre>
	<p>I also wrote a .desktop file and placed it in ~/Desktop/RuneLite.desktop for faster gaming action. Mine looks something like the following:</p>
	<pre>[Desktop Entry]
Version=1.0
Type=Application
Name=Runelite
Comment=
Exec=java -jar /home/$user/wherever/you/put/RuneLite.jar
Icon=net.runelite.RuneLite
Path=/home/$user
Terminal=false
StartupNotify=false</pre>
	<h1 id="to-recap">To Recap</h1>
	<p>FreeBSD is the perfect system for me. The package selection is large enough to provide everything I need but not so big that it feels overwhelming. The OS feels clean, simple, and elegant. FreeBSD does everything I want an operating system to do: get the hell out of my way and let me work. Overall I’m finding that my general computer usage is becoming more pleasant since I’m spending less time fighting my computer. Do I recommend FreeBSD? Absolutely.</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
