<?php include "../includes/header.html"; ?>
<title>Setting up a FAMP stack</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p><strong>UPDATE : I’ve added a short section on configuring SSL and using acme.sh.</strong> In this post I describe how to set up a FAMP stack. A FAMP stack is a webserver running FreeBSD, Apache, Mysql, and PHP. For the purposes of this article, I’m substituting Mysql with Mariadb and mod_php with php-fpm.</p>
	<p>I understand that the “How to set up a LAMP/FAMP stack” article is a dead horse that’s experienced continuous, unrelenting beatings for the last 20 years. I cannot stop the beatings so I’ve chosen to join in. Much like all other iterations of this topic, this article has been written exclusively for the author so that I might be able to use it my personalized reference guide in the future. Helping others is secondary.</p>
	<p>Now you might be asking, “Why use FreeBSD when you could just use Linux?”. I raise this question in response: “Why use Linux when you could just use Windows or MacOS? Why use anything when you could just turn the computer off?”.</p>
	<p>Do you choose to leave your computer on? Good, let’s get on with it.</p>
	<h1 id="installing-freebsd">Installing FreeBSD</h1>
	<p>This first step is very simple. bsdinstall is a curses based program that easily walks you through installation and setup. I strongly encourage you to choose ZFS as your root filesystem. Unless you know what you’re doing, UFS is only as reliable as your power company.</p>
	<p>If you need help, go check out the <a href="https://www.freebsd.org/doc/en/books/handbook/bsdinstall.html">FreeBSD Handbook</a>. It’s an invaluable resource.</p>
	<!--As of writing, I'm using FreeBSD 13.0-CURRENT on my Raspberry Pi 4. The RPI3 image is available from [the page full of arm64 downloads](https://download.freebsd.org/ftp/snapshots/arm64/aarch64/ISO-IMAGES/13.0/). Your hardware is probably different though, so see [the page with all the architectures](https://www.freebsd.org/where.html). -->
	<p>If your computer was manufactured in the last 15 years you probably need an amd64 installer image. Note that the .iso images won’t work if you’re trying to make a live USB flash drive (you’ll need an image with memstick in the name for that).</p>
	<p>Alternatively, Digital Ocean offers easy, fully supported FreeBSD virtual private servers.</p>
	<h1 id="installing-packages">Installing Packages</h1>
	<p>First, we need to install the required packages. I’ll be using Apache, Mariadb, and php-fpm.</p>
	<pre># first we tell pkg to install pkg 
$ pkg bootstrap &amp;&amp; pkg update

# then we install required packages
$ pkg install php74 php74-mysqli apache24 mariadb103-server</pre>
	<h1 id="configuring-apache-and-php-fpm">Configuring Apache and PHP-FPM</h1>
	<h3 id="editing-httpd.conf">Editing httpd.conf</h3>
	<p>After installing all the required packages, we need to edit /usr/local/etc/apache24/httpd.conf</p>
	<p>Uncomment (ie remove the leading # from) the lines that look like the following:</p>
	<pre>#LoadModule mpm_event_module libexec/apache24/mod_mpm_event.so
#LoadModule proxy_module libexec/apache24/mod_proxy.so
#LoadModule proxy_fcgi_module libexec/apache24/mod_proxy_fcgi.so
	</pre>
	<p>Comment out (ie prepend a # to) the lines that look like the following:</p>
	<pre>LoadModule mpm_prefork_module libexec/apache24/mod_mpm_prefork.so</pre>
	<p>Save and quit this file.</p>
	<h3 id="writing-the-apache-module-for-php-fpm.">Writing the Apache module for php-fpm.</h3>
	<p>Paste the following into /usr/local/etc/apache24/modules.d/030_php-fpm.conf:</p>
	<pre>&lt;IfModule proxy_fcgi_module&gt;
    &lt;IfModule dir_module&gt;
        DirectoryIndex index.php
    &lt;/IfModule&gt;
    &lt;FilesMatch &quot;\.(php|phtml|inc)$&quot;&gt;
        SetHandler &quot;proxy:fcgi://127.0.0.1:9000&quot;
    &lt;/FilesMatch&gt;
&lt;/IfModule&gt;</pre>
	<h3 id="testing-the-configs">Testing the configs</h3>
	<p>Edit /usr/local/www/apache24/data/phpinfo.php and add the following:</p>
	<pre>&lt;?php
    phpinfo();
?&gt;</pre>
	<p>Now we’ll start our services and test everything to make sure it works so far.</p>
	<pre>$ service apache24 onestart
$ service php-fpm onestart

# run the following command
$ fetch http://localhost/phpinfo.php -q -o - | grep php-fpm 
# if it worked, you should see something like: 
# &lt;tr&gt;&lt;td class=&quot;e&quot;&gt;php-fpm &lt;/td&gt;&lt;td class=&quot;v&quot;&gt;active &lt;/td&gt;&lt;/tr&gt;

# now we delete phpinfo.php since it exposes information about our server
$ rm /usr/local/www/apache24/data/phpinfo.php 
	</pre>
	<h3 id="enabling-services">Enabling services</h3>
	<p>When we <em>enable</em> a service it will automatically start at boot time. This is important because we don’t want to be monkeying around every single time we restart our server. Note that these configuration options are automatically appended to /etc/rc.conf.</p>
	<pre>$ service apache24 enable
$ service php-fpm enable</pre>
	<h1 id="configuring-mariadb">Configuring Mariadb</h1>
	<p>The first thing we need to do is start the mysql server. The first startup might take some time if you’re on potato hardware.</p>
	<pre>$ service mysql-server onestart</pre>
	<p>Oh no! This didn’t work! Mysqld is complaining! If we check the error file in /var/db/mysql/$HOST.err we can probably figure out what the problem is. Below are some examples of what my installation was complaining about:</p>
	<pre>[ERROR] InnoDB: Operating system error number 13 in a file operation.
[ERROR] InnoDB: The error means mysqld does not have the access rights to the directory.  </pre>
	<p>After some investigation, it appears that the mysql user <em>wasn’t</em> granted recursive permissions to the directory it’s working in. Classic, a DAC nightmare. Not to worry, we can fix this very easily.</p>
	<pre># granting permissions
$ chown -R mysql:wheel /var/log/mysql/

# re-starting mysqld
$ service mysql-server onestart

# verifying that it&#39;s running
$ service mysql-server onestatus

# and now we do the secure installation
$ mysql_secure_installation</pre>
	<h3 id="enabling-mariadb">Enabling Mariadb</h3>
	<pre>$ service mysql-server enable</pre>
	<h1 id="the-moment-of-truth">The moment of truth</h1>
	<p>It’s time to reboot our system and check if everything persists across reboots.</p>
	<pre># rebooting the system
$ reboot
# ssh back in . . . 
# . . . and verify that everything is running
$ ps aux | grep -e php -e apache -e mysql</pre>
	<p>Cool! Everything works for me!</p>
	<h1 id="setting-up-ssl">Setting up SSL</h1>
	<p>This final step is mandatory your server sends or receives any user data. Although it’s optional in other cases, SSL really helps your website feel more professional. In coming years, I expect browsers to flag all unencrypted http traffic as “unsafe” and “insecure” so it’s best to get ahead of the curve.</p>
	<p>I <em>used to use</em> let’s encrypt and certbot because It’s free. I still use let’s encrypt . . . but I no longer use certbot. Why? because certbot has extremely unpredictable behavior and tends to break my apache configs. Instead I’ve started to use <a href="https://github.com/acmesh-official/acme.sh">acme.sh</a>. acme.sh <em>does not</em> nuke the entirity of /etc so it’s much preferable to certbot.</p>
	<h4 id="install-acme.sh">1. Install acme.sh</h4>
	<pre>$ pkg install acme.sh</pre>
	<h4 id="enable-modules-for-ssl-vhosts-rewrite">2. Enable modules for SSL, vhosts, rewrite</h4>
	<p>Edit /usr/local/etc/apache24/httpd.conf and add the vhost Include. Uncomment (ie remove the leading #) from the lines that look like the following:</p>
	<pre>#LoadModule ssl_module libexec/apache24/mod_ssl.so
#LoadModule rewrite_module libexec/apache24/mod_rewrite.so
#LoadModule socache_shmcb_module libexec/apache24/mod_socache_shmcb.so
#Include etc/apache24/extra/httpd-vhosts.conf
#Include etc/apache24/extra/httpd-ssl.conf</pre>
	<h4 id="modify-vhosts">3. Modify vhosts</h4>
	<p>Open /usr/local/etc/apache24/extra/httpd-vhosts.conf in your favorite editor. Add one vhost for each subdomain. The rewrite conditions are optional as certbot will offer to create them for us when we request certificates. See <a href="https://httpd.apache.org/docs/2.4/vhosts/examples.html">the apache docs on vhosts</a> if you need more help.</p>
	<pre>&lt;VirtualHost *:80&gt; 
        DocumentRoot &quot;/usr/local/www/apache24/data/your.domain&quot;
        ServerName your.domain  
        ServerAlias www.your.domain 
        RewriteEngine on 
        RewriteCond %{SERVER_NAME} =your.domain [OR] 
        RewriteCond %{SERVER_NAME} =www.your.domain 
        RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent] 
&lt;/VirtualHost&gt; </pre>
	<h4 id="get-lets-encrypt-certs-with-acme.sh">4. Get let’s encrypt certs with acme.sh</h4>
	<p>First we generate certs . . .</p>
	<pre>$ acme.sh --issue -d your.domain --webroot /usr/local/www/apache24/data</pre>
	<p>Then we install them . . .</p>
	<pre>$ mkdir -p /usr/local/etc/apache24/ssl
$ cd /usr/local/etc/apache24/ssl
$  acme.sh --install-cert -d your.domain \ 
--cert-file         /usr/local/etc/apache24/ssl/your.domain-cert.pem \
--key-file          /usr/local/etc/apache24/ssl/your.domain-key.pem \
--fullchain-file    /usr/local/etc/apache24/ssl/letsencrypt-fullchain.pem \
--reloadcmd &quot;service apache24 restart&quot;</pre>
	<p>Then we write a cron job to automatically renew the certs every month . . .</p>
	<pre>0 0 1 * * acme.sh --renew -d your.domain --force</pre>
	<p>And now we’ll tell apache about them. Open /usr/local/etc/apache24/extra/httpd-ssl.conf in an editor. Find the default vhost block and insert your information. Mine looks something like:</p>
	<pre># . . . 
&lt;VirtualHost _default_:443&gt;
DocumentRoot &quot;/usr/local/www/apache24/data&quot;
ServerName your.domain:443
ServerAdmin you@your.domain
ErrorLog &quot;/var/log/httpd-error.log&quot;
TransferLog &quot;/var/log/httpd-access.log&quot;
RewriteEngine on
SSLEngine on
SSLCertificateFile &quot;/usr/local/etc/apache24/ssl/cert.pem&quot;
SSLCertificateKeyFile &quot;/usr/local/etc/apache24/ssl/key.pem&quot;
SSLCertificateChainFile &quot;/usr/local/etc/apache24/ssl/fullchain.pem&quot;
# . . . 
&lt;/VirtualHost&gt;</pre>
	<h4 id="testing">Testing</h4>
	<p>Before slamming our head into our keyboard with absolute force, we must restart Apache.</p>
	<pre>$ service apache24 restart</pre>
	<h1 id="conclusion">Conclusion</h1>
	<p>Rinse, Repeat, and Refer to the docs if you’re still having trouble.</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
