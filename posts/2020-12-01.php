<?php include "../includes/header.html"; ?>
<title>Wordpress on FreeBSD</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>Building on top of our FAMP stack from last week, we’ll be installing Wordpress. Wordpress isn’t the only web application that can run on an *AMP stack, but it’s one of the most well-known.</p>
	<h1 id="synopsis">Synopsis</h1>
	<p>You need a FAMP stack. I’m using Apache, Mariadb, and php-fpm.</p>
	<p>As of writing, FreeBSD 13.0-CURRENT ships Wordpress-5.4.2 as a package. Even though this sounds great, it might not always be the best choice. Wordpress is a dumpster fire, burning bright with exploits and vulnerabilities. In order to minimize risk we’ll be grabbing the latest tarball (5.5.3) from the wordpress site and installing it the caveman way.</p>
	<p>We will then create and configure a wordpress user and give it a database within mariadb. After mariadb is configured we open our web browser, navigate to our trusty server, then finish up with Wordpress’ handy web gui for configuration.</p>
	<h1 id="installing-php-libraries">Installing PHP Libraries</h1>
	<p>This is the part where downgrading php might be necessary. If you’re running php74, simply remove it with the following command below. Otherwise, skip this step.</p>
	<pre>$ pkg remove php74</pre>
	<p>Now we can proceed with installing php73. Downgrading is easy!</p>
	<pre># install php libraries 
$ pkg install php73-mysqli php73-json php73-xml php73-hash php73-gd php73-curl php73-tokenizer php73-zlib php73-zip

# convince php that it&#39;s .ini exists
$ cp /usr/local/etc/php.ini-production /usr/local/etc/php.ini

# remind freebsd that new executable files exist
$ rehash

# restart everything so it knows the libraries exist
$ service php-fpm restart
$ service apache24 restart</pre>
	<h1 id="grabbing-that-fat-wordpress-tarball">Grabbing That Fat Wordpress Tarball</h1>
	<p><strong>Remember, use your brain. Don’t copy-paste verbatim. Dangerous commands lie ahead.</strong></p>
	<p>Always grab the latest release from <a href="https://wordpress.org/download/">the wordpress downloads page</a>. Always double check your commands before running them, especially the destructive ones.</p>
	<pre># download the wordpress tarball
$ fetch http://wordpress.org/wordpress-5.5.3.tar.gz https://wordpress.org/wordpress-5.5.3.tar.gz.sha1
# If you are getting SSL auth errors like me, try the following: 
# fetch --no-verify-peer https://wordpress.org/wordpress-5.5.3.tar.gz https://wordpress.org/wordpress-5.5.3.tar.gz.sha1

# now we check the shasum
# If they don&#39;t match you have a corrupted or malicious archive
$ cat wordpress-5.5.3.tar.gz.sha1 &amp;&amp; echo &quot;&quot; &amp;&amp; shasum wordpress-5.5.3.tar.gz

# remove testing files from apache&#39;s default document root 
$ rm -rf /usr/local/www/apache24/data/*

# extract the tarball 
$ tar xzfv ./wordpress-5.5.3.tar.gz -C /usr/local/www/apache24/data/

# coax wordpress out of it&#39;s comfy subdir
$ cd /usr/local/www/apache24/data/wordpress
$ mv ./* ../
$ rmdir wordpress/

# recursively modify DAC so Apache will be allowed to serve wordpress
$ chown -R root:wheel ./*
	</pre>
	<h1 id="database-discombobulation">Database Discombobulation</h1>
	<p>Your database setup will likely be different from mine. I’m using UNIX sockets for my root user’s authentication but you’ll likely have a password. Adjust your commands accordingly.</p>
	<p>The first thing we need to do is log into mariadb.</p>
	<pre>$ mysql -u root
# or, if you have a password: 
$ mysql -u root -p</pre>
	<p>Now that we’re in maraidb’s shell we need to create a database and a user. Remember the names you select for each as well as the password you choose. You will need these values later.</p>
	<pre>-- create a database
CREATE DATABASE wordpress;
-- then create a user and choose your own password
CREATE USER &#39;wordpress&#39;@&#39;localhost&#39; IDENTIFIED BY &#39;yourpassword&#39;;
-- now we give the wordpress user full grants on it&#39;s database
GRANT ALL PRIVILEGES ON wordpress.* TO &#39;wordpress&#39;@&#39;localhost&#39;;
-- reload privileges before we go
FLUSH PRIVILEGES;
EXIT;</pre>
	<p>Good job! You’ve just done some SQL scripting without even knowing it! I’m proud of you. It only gets easier from here.</p>
	<h1 id="finally-wordpress-configuration">Finally, Wordpress Configuration</h1>
	Point your web browser to your server. You should see something like this if everything is working:
	<figure class="figure">
	    <img src="assets/images/wordpress0.png" alt="The Wordpress welcome page">
	    <figcaption class="figure-caption">
		The Wordpress welcome web
	    </figcaption>
	</figure>
	Fill in the blanks. Use the database, db user, and password you entered earlier.
	<figure class="figure">
	    <img src="assets/images/wordpress1.png" alt="Fill in the blanks: Database Name, Username, Password, database Host, Table Prefix">
	    <figcaption class="figure-caption">
		Fill in the blanks: Database Name, Username, Password, database Host, Table Prefix
	    </figcaption>
	</figure>
	Sometimes wordpress is unable to write it’s own config file. If you see a page like this, don’t worry. All we need to do is copy and paste the text into /usr/local/www/apache24/data/wp-config.php. After you create the file, click the “Run Installation” button.
	<figure class="figure">
	    <img src="assets/images/wordpress2.png" alt="Wordpress can't write to wp-config.php. This is an easy fix">
	    <figcaption class="figure-caption">
		Wordpress can’t write to wp-config.php. This is an easy fix
	    </figcaption>
	</figure>
	Another round of fill in the blanks! Be sure to use a completely new password. I got an error after this step but it was very minor. I refreshed the page, logged in, and nothing bad seemed to happen.
	<figure class="figure">
	    <img src="assets/images/wordpress3.png" alt="Fill in the blanks again: Site Title, Username, Password, Your Email, and SEO">
	    <figcaption class="figure-caption">
		Fill in the blanks again: Site Title, Username, Password, Your Email, and SEO
	    </figcaption>
	</figure>
	<p>After some tinkering around I realized that I needed to do some additional tinkering to get media uploads working. Otherwise, this installation looks complete.</p>
	<figure class="figure">
	    <img src="assets/images/wordpress4.png" alt="wordpress post editor">
	    <figcaption class="figure-caption">
		Wordpress post editor
	    </figcaption>
	</figure>
	<h1 id="conclusion">Conclusion</h1>
	<p>After all this, I think I’ll stick with Jekyll. I can see a use case for Wordpress . . . but I am the antithesis of it’s target usebase. For every feature it seems like there is a sharp edge that’s easy to cut yourself on. For every customization you do yourself there’s a new bug to work out. I’m not trying to discourage anyone from using it, I’m simply stating my chief complaints. But, as with all things, I intend to continue playing with Wordpress. Maybe there’s something I’m missing. Let’s wait and see.</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
