<?php include "../includes/header.html"; ?>
<title>Drupal on FreeBSD</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>Further expanding the FAMP stack, today I’ll be replacing WordPress with Drupal. Drupal is a CMS that’s more advanced but less painful than WordPress.</p>
	<p>Although you can configure apache to run multiple websites on a single server using vhosts, I’ll be keeping it simple and won’t be using vhosts.</p>
	<h1 id="satisfying-dependencies">Satisfying Dependencies</h1>
	<pre>$ pkg remove php74</pre>
	<p>Now we will proceed with installing php73 and some of it’s libraries.</p>
	<pre># install deps
$ pkg install php73 php73-mysqli php73-xml php73-hash php73-gd php73-curl php73-tokenizer php73-zlib php73-zip php73-json php73-dom php73-exif php73-fileinfo php73-mbstring php73-openssl php73-pecl-imagick php73-filter php73-iconv php73-ctype php73-intl php73-phar php73-composer php73-pdo php73-session php73-opcache php73-xmlwriter php73-xmlreader php73-simplexml php73-posix php73-sqlite3 php73-pdo_sqlite php73-extensions php73-pdo_mysql drush-php73

# create php.ini
$ cp /usr/local/etc/php.ini-production /usr/local/etc/php.ini

# restart everything 
$ service php-fpm restart
$ service apache24 restart</pre>
	<h1 id="editing-apache-configs">Editing Apache configs</h1>
	<p>Open your apache config in an editor</p>
	<pre>$ vim /usr/local/etc/apache24/httpd.conf</pre>
	<p>Uncomment (ie remove the leading #) from the lines that look like the following</p>
	<pre>#LoadModule mime_magic_module libexec/apache24/mod_mime_magic.so</pre>
	<p>Add a line within the mime_module config block</p>
	<pre>&lt;IfModule mime_module&gt; 
&lt;!-- . . . many other lines . . .  --&gt;
AddType application/x-httpd-php .php
&lt;/IfModule&gt;</pre>
	<h1 id="database-time">Database Time</h1>
	<p>First, we open a shell in mariadb:</p>
	<pre>$ mysql -u root</pre>
	<p>Now we add a user, a database, grant privileges, then reload the whole thing. Remember these values. You’ll need them later.</p>
	<pre>CREATE DATABASE drupal;
CREATE USER &#39;drupal&#39;@&#39;localhost&#39; IDENTIFIED BY &#39;yourpassword&#39;;
GRANT ALL PRIVILEGES ON drupal.* TO &#39;drupal&#39;@&#39;localhost&#39;;
FLUSH PRIVILEGES;
EXIT;</pre>
	<h1 id="use-composer-to-install-drupal">Use Composer to Install Drupal</h1>
	<p>Composer is a dependency manager for PHP. Composer makes it extremely easy to install, update, add modules, and change your themes.</p>
	<pre># cd to your document root
$ cd /usr/local/www/apache24/data

# use composer to download all the stuff Drupal needs 
$ composer create-project drupal/recommended-project ./
# go get a cup of coffee, this might take a few minutes

# have your coffee? Now we create some files
$ cd ./web/site/downloads/
$ cp ./default.settings.php ./settings.php
$ mkdir -p files
$ chmod a+w ./files ./settings.php</pre>
	<h1 id="configure-drupal-with-its-web-gui">Configure Drupal With It’s Web GUI</h1>
	Point your web browser to your server. If you followed my directions exactly, you’ll need to go to http://your-server-ip/web/
	<figure class="figure">
	    <img src="assets/images/drupal0.png" alt="Drupal's welcome page. Choose your language!">
	    <figcaption class="figure-caption">
		Drupal’s welcome page. Choose your Language!
	    </figcaption>
	</figure>
	Now we’ll choose a profile. Since this is our first time, we’ll choose the ‘Standard’ profile.
	<figure class="figure">
	    <img src="assets/images/drupal1.png" alt="Select an installation profile: Standard, Minimal, or Demo">
	    <figcaption class="figure-caption">
		Select an installation profile: Standard, Minimal, or Demo
	    </figcaption>
	</figure>
	The next screen is very useful. It will help us troubleshoot our installation. As an example, I’ve included some errors you might encounter if you forget to create a settings.php file and an uploads directory. If you’re following my instructions exactly, you shouldn’t see this. If you do happen to encounter this screen though, work through each issue and refresh the page. Eventually you’ll solve all the errors.
	<figure class="figure">
	    <img src="assets/images/drupal2.png" alt="Requirements problem: Filesystem error and Settings File error">
	    <figcaption class="figure-caption">
		Requirements problem: Filesystem error and Settings File error
	    </figcaption>
	</figure>
	Remember when I told you to remember what you typed into mariadb? Now we’ll use it.
	<figure class="figure">
	    <img src="assets/images/drupal3.png" alt="Database configuration: select your Database type, Database name, Database user, and enter the database user's password">
	    <figcaption class="figure-caption">
		Database configuration: select your Database type, Database name, Database user, and enter the database user’s password
	    </figcaption>
	</figure>
	<p>If everything goes well, Drupal will begin installing itself. This part might take a while so feel free to go refill your coffee cup.</p>
	If this step errors out, you might need to nuke your database and start over. Re-create your database, open FireFox, and work through the web GUI again. Drupal is friendly and won’t overwrite a database that already contains Drupal content.
	<figure class="figure">
	    <img src="assets/images/drupal4.png" alt="Drupal Installation progress bar">
	    <figcaption class="figure-caption">
		Drupal Installation progress bar
	    </figcaption>
	</figure>
	The final step – configure our site. Insert your site name and site email address. In the ‘Site Maintenance Account’ section you will be creating a user. Make sure to use a strong password to prevent vandalism.
	<figure class="figure">
	    <img src="assets/images/drupal5.png" alt="Another round of fill in the blanks">
	    <figcaption class="figure-caption">
		Another round of fill in the blanks
	    </figcaption>
	</figure>
	<h1 id="final-configuration-steps">Final Configuration Steps</h1>
	<p>Before we’re ready to go, we need to ssh back into our server and modify DAC for settings.php.</p>
	<pre>$ cd /usr/local/www/apache24/data/web/sites/default
$ chmod o-w ./settings.php</pre>
	<p>Be sure to check http://your-server-ip/web/admin/reports/status for additional errors and warnings. Drupal will tell you what’s wrong with it and provide you with links documentation on possible solutions. Like during the installation process, troubleshoot each step and refresh the page until the errors are resolved.</p>
	<h1 id="installing-themes-and-modules">Installing Themes and Modules</h1>
	<p>When searching for themes and modules, it’s important to select one that’s actively maintained, actively developed, works with your version of drupal, and has security advisory coverage. Visit <a href="https://www.drupal.org/project/project_theme">Drupal’s download page</a> to find themes and modules. See <a href="https://www.drupal.org/docs/user_guide/en/extend-theme-install.html">Drupal’s docs on installing themes and modules</a> if you need more help.</p>
	<p>If composer gets killed it means you need more memory. I had to add a swapfile to sucessfully run “composer require drupal/bootstrap”. See <a href="https://www.freebsd.org/doc/handbook/adding-swap-space.html">Section 11.12 of the FreeBSD handbook</a> if you don’t know how to add swap space.</p>
	<pre>$ cd /usr/local/www/apache24/data/

# download your theme 
$ composer require drupal/bootstrap</pre>
	<p>I like to use Drush to manage themes and modules. Drush is a command line tool that makes Drupal scripting and management easier . . . But this isn’t the only way to do it. You can enable/disable modules and themes using the web GUI too.</p>
	<pre># list modules and themes
$ drush pm-list 

# enable your theme 
$ drush pm-enable bootstrap

# set it as the default theme
$ drush config-set system.theme default bootstrap
	</pre>
	And here is our new theme in action:
	<figure class="figure">
	    <img src="assets/images/drupal6.png" alt="A dusty old Bootstrap 3 theme">
	    <figcaption class="figure-caption">
		A dusty old Bootstrap 3 theme
	    </figcaption>
	</figure>
	<h1 id="conclusion">Conclusion</h1>
	<p>Drupal has been an absolute dream compared to WordPress. Although Drupal isn’t nearly as “average user friendly” as WordPress, the ease of setup makes up for it. Drupal’s “self-diagnosis” feature is incredibly useful as well because it directs users directly to a solution. But, as with the WordPress article, I’m still sticking with Jekyll. If I ever require a more copious CMS I’ll surely choose Drupal over WordPress. Next on the list? Maybe Nextcloud while the FAMP stack is still running. I also intend to try Ghost, a CMS built with NodeJS . . . but until then I want to try out more software that can sit on top of FAMP.</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
