<?php include "../includes/header.html"; ?>
<title>HumHub on FreeBSD</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>HumHub is a self-hosted, centralized, open source platform for groups. It’s flexible, modular, and adapts to your individual use case. HumHub is written in PHP which means it’s perfect for our ever-evolving FAMP stack.</p>
	<p>HumHub seems to have a slightly different use case than other self-hosted social platforms. It’s primary use case is for small groups and organizations. Let’s compare a HumHub to something you might be familiar with: the Fediverse.</p>
	<table>
	    <colgroup>
		<col style="width: 42%" />
		<col style="width: 57%" />
	    </colgroup>
	    <thead>
		<tr class="header">
		    <th>HumHub</th>
		    <th>Fediverse</th>
		</tr>
	    </thead>
	    <tbody>
		<tr class="odd">
		    <td>Centralized</td>
		    <td>Federated</td>
		</tr>
		<tr class="even">
		    <td>You actually own all your data. All means all.</td>
		    <td>Your data is cached on servers you don’t own (and possibly aggregated and sold by instances you federate with)</td>
		</tr>
		<tr class="odd">
		    <td>Moderation is easy since all users live in one place</td>
		    <td>moderation tools are very verbose and easy to use . . . but setting a new instance to evade cross-instance bans is even easier</td>
		</tr>
		<tr class="even">
		    <td>Allows for open registration or invite-only</td>
		    <td>Allows for open registration or invite-only</td>
		</tr>
		<tr class="odd">
		    <td>Ideal for small groups and projects within (or without) larger parent organizations</td>
		    <td>For people who want “a better social media”</td>
		</tr>
		<tr class="even">
		    <td>It feels like Google or Facebook groups but cleaner and more organized</td>
		    <td>It’s literally just Twitter, cesspool included by default.</td>
		</tr>
		<tr class="odd">
		    <td>Stable plugin ecosystem. Free, Premium, and Community modules are all supported.</td>
		    <td>The ActivityPub protocol potentially discourages new and unique features that might break compatibility between various servers</td>
		</tr>
	    </tbody>
	</table>
	<h1 id="preliminary-steps">Preliminary steps</h1>
	<p>Before we begin, we need to downgrade php to version 73 and install some modules.</p>
	<pre># remove php74
$ pkg remove php74

# install php73 and some modules
$ pkg install php73 php73-bcmath php73-bz2 php73-composer php73-ctype php73-curl php73-dom php73-exif php73-extensions php73-fileinfo php73-filter php73-gd php73-gmp php73-hash php73-iconv php73-intl php73-json php73-ldap php73-mbstring php73-mysqli php73-opcache php73-openssl php73-pdo php73-pdo_mysql php73-pdo_sqlite php73-pecl-APCu php73-pecl-imagick php73-phar php73-posix php73-session php73-simplexml php73-sqlite3 php73-tokenizer php73-xml php73-xmlreader php73-xmlwriter php73-xsl php73-zip php73-zlib

# create your php.ini if it does not exist already
$ cp /usr/local/etc/php.ini-production /usr/local/etc/php.ini

# restart everything 
$ service php-fpm restart
$ service apache24 restart</pre>
	<h1 id="download-humhub">Download HumHub</h1>
	<p>Be sure to check humhub.com for the latest release. Don’t run these commands verbatim as they periodically release new versions.</p>
	<pre># grab &amp; extract the tarball 
$ cd /usr/local/apache24/data 
$ fetch https://www.humhub.com/download/package/humhub-1.7.1.tar.gz
$ tar xzfv ./humhub-1.7.1.tar.gz
$ mv humhub-1.7.1 humhub
$ rm humhub-1.7.1.tar.gz 


# modify DAC 
$ chown -R root:wheel ./humhub
$ cd humhub
$ chmod o+w ./uploads/
$ chmod o+w ./assets
$ chmod o+w ./protected/modules/
$ chmod o+w ./protected/runtime
$ chmod o+w ./protected/runtime/config/</pre>
	<h1 id="set-up-the-database">Set up the Database</h1>
	<p>Open up mariadb as a db admin, add the humhub user, database, and do the grants. Remember what values you entered here because you’ll need them later.</p>
	<pre>CREATE USER &#39;humhub&#39;@&#39;localhost&#39; IDENTIFIED BY &#39;humhub&#39;;
CREATE DATABASE humhub;
GRANT ALL PRIVILEGES ON humhub.* TO &#39;humhub&#39;@&#39;localhost&#39;;
FLUSH PRIVILEGES; 
EXIT;</pre>
	<h1 id="browser-based-wizard">Browser Based Wizard</h1>
	Kindly ask your server to slide some packets packets over to FireFox. If everything went right, you should see a screen similar to this one. Select your language and click ‘Next’
	<figure class="figure">
	    <img src="assets/images/humhub0.png" alt="Cool, our stack works" />
	    <figcaption class="figure-caption">
		Cool, our stack works.
	    </figcaption>
	</figure>
	The next screen we see is the troubleshooting stage. Resolve each issue one by one and refresh the page until you get it right.
	<div class="row">
	    <div class="col-md-6">
		<figure class="figure">
		    <img src="assets/images/humhub1.png" alt="the installer is troubleshooting itself" />
		    <figcaption class="figure-caption">
			The installer is troubleshooting itself.
		    </figcaption>
		</figure>
	    </div>
	    <div class="col-md-6">
		<figure class="figure">
		    <img src="assets/images/humhub2.png" alt="After all problems have been resolved" />
		    <figcaption class="figure-caption">
			After all the problems have been resolved.
		    </figcaption>
		</figure>
	    </div>
	</div>
	Once the troubleshooting is complete, we’ll need to tell HumHub how to connect to the database. After we click the ‘Next’ button HumHub will create tables in it’s databse. This step might take some time depending on your computer’s potato:IOPs ratio.
	<figure class="figure">
	    <img src="assets/images/humhub3.png" alt="Enter your database info: Hostname, Username, Password, name of Database." />
	    <figcaption class="figure-caption">
		Enter your database info: Hostname, Username, Password, Name of Database.
	    </figcaption>
	</figure>
	Now we get to choose a name for our server.
	<figure class="figure">
	    <img src="assets/images/humhub4.png" alt="Choose a name for your server." />
	    <figcaption class="figure-caption">
		Choose a name for your server.
	    </figcaption>
	</figure>
	The cool thing about HumHub is that it comes with predefined use cases. Choosing a use case will automatically configure some settings, allowing us to save time. For this guide, I’ve chosen the “My Club” option.
	<figure class="figure">
	    <img src="assets/images/humhub5.png" alt="Choose a use case for automatic presets." />
	    <figcaption class="figure-caption">
		Choose a use case for automatic presets.
	    </figcaption>
	</figure>
	The next screen will take us through some security checkboxes. I’ve chosen to keep my HumHub a closed network - this means that new users can only register through an invitation.
	<figure class="figure">
	    <img src="assets/images/humhub6.png" alt="Secure your settings. " />
	    <figcaption class="figure-caption">
		Secure your settings.
	    </figcaption>
	</figure>
	The penultimate step will recommend certain modules. These modules are similar to Wordpress plugins or Drupal modules in that they increase the functionality of our site. I’ve chosen to check all of them for a more full-featured experience.
	<figure class="figure">
	    <img src="assets/images/humhub7.png" alt="Select modules to install. " />
	    <figcaption class="figure-caption">
		Select modules to install.
	    </figcaption>
	</figure>
	Now we’ll set up an admin account. This will be the god of the kingdom of “ye olde humble hub” so be sure to choose a strong password.
	<figure class="figure">
	    <img src="assets/images/humhub8.png" alt="Create an admin account." />
	    <figcaption class="figure-caption">
		Create an admin account.
	    </figcaption>
	</figure>
	The final step is to decide whether or not you want HumHub to populate itself with example content. I chose to enable this option because it’ll give us a better look inside.
	<figure class="figure">
	    <img src="assets/images/humhub9.png" alt="Auto populate with example content?" />
	    <figcaption class="figure-caption">
		Auto Populate with example content?
	    </figcaption>
	</figure>
	<h1 id="back-to-the-shell">Back to the Shell</h1>
	<p>Before we’re all ready to go, we need to do a few more things. First we set up a cron job for database maintenance. Open up your crontab in an editor then append the following. Adjust your paths and frequency to meet your own needs.</p>
	<pre># as root
$ crontab -e 
# for user www
$ crontab -u www -e</pre>
	<pre>* * * * * /usr/local/bin/php /usr/local/www/apache24/data/humhub/protected/yii queue/run
* * * * * /usr/local/bin/php /usr/local/www/apache24/data/humhub/protected/yii cron/run</pre>
	<p>Now we’ll disable debugging mode. Open /usr/local/www/apache24/data/humhub/index.php in an editor. Find the block that looks like the following:</p>
	<pre>// comment out the following two lines when deployed to production
defined(&#39;YII_DEBUG&#39;) or define(&#39;YII_DEBUG&#39;, true);
defined(&#39;YII_ENV&#39;) or define(&#39;YII_ENV&#39;, &#39;dev&#39;);</pre>
	<p>and make it look like this:</p>
	<pre>// comment out the following two lines when deployed to production
// defined(&#39;YII_DEBUG&#39;) or define(&#39;YII_DEBUG&#39;, true);
// defined(&#39;YII_ENV&#39;) or define(&#39;YII_ENV&#39;, &#39;dev&#39;);</pre>
	<p>Your server should now be fully functional. If you experience issues with uploads, check the log files in <em>Administration &gt; Information &gt; Logging</em>. They will tell you exactly which permissions you need to set on what directories to solve the problem. You might also need to edit your php.ini to increase the maximum upload size from your default (mine was 2M) to something more reasonable like 5M or 10M.</p>
	<h1 id="final-thoughts">Final Thoughts</h1>
	<h3 id="some-screenshots">Some screenshots</h3>
	<figure class="figure">
	    <img src="assets/images/humhub10.png" alt="HumHub's dashboard - all your groups in one place." />
	    <figcaption class="figure-caption">
		HumHub’s dashboard - all your groups in one place.
	    </figcaption>
	</figure>
	<figure class="figure">
	    <img src="assets/images/humhub11.png" alt="A user's profile." />
	    <figcaption class="figure-caption">
		A user’s profile.
	    </figcaption>
	</figure>
	<h3 id="from-an-admin-perspective">From an admin perspective?</h3>
	<p>HumHub seems to be a great option for those of us who want to host a service that’s friendly to inexperienced computer users, professional, expandable, and group oriented. This software is Free, Open, and very easy to install and configure. In contrast to WordPress, and lesser so Drupal, I felt little resistance. There were no major papercuts during the installation process.</p>
	<p>Some ‘business oriented’ modules are locked behind a paywall but by purchasing them you are supporting the project. These modules are by no means required for a full experience. Unlike some other projects we know, HumHub ships a fully functional community product. <a href="https://humhub.com">humhub.com</a> also offers managed and unmanaged ‘professional’ licenses on a sliding price scale. The ‘professional’ build provides additional features that might be useful in an enterprise environment.</p>
	<h3 id="from-a-user-perspective">From a user perspective?</h3>
	<p>HumHub’s ui really feels like “Facebook but only the good parts”. It’s familiar, simple, and not too overwhelming. The additional modules integrate well into the UI. Although it doesn’t look as modern as other sites, it’s very refreshing to return to simpler design paradigms - ones that aren’t completely symbolic.</p>
	<h3 id="conclusion">Conclusion</h3>
	<p>I cannot deny, HumHub seems to be exactly what I’ve been looking for all along: an unfederated, centralized, community focused service. Out of every social service I’ve used, HumHub seems to be the most reasonable. There’s no gimmick. It’s just a place to collaborate with the people you actually need to collaborate with.</p>
	<p>Now, will I actually use HumHub? Probably not. All social systems require users and I cannot be the only one. If I did have a use case for HumHub, however, I would absolutely choose it over anything else.</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
