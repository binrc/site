<?php include "../includes/header.html"; ?>
<title>Building an IRC Server</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>Today I installed and configured an IRC daemon. Although there are many existing networks, I’ve always wanted to try hosting one myself. I used ngircd and FreeBSD and it was a very pleasant experience. Let’s walk through how to setup an IRC server on FreeBSD.</p>
	<p>We will be setting up a <em>very basic</em> irc server without any services (ie NickServ, ChanServ, BotServ, etc). While services are very useful, they aren’t a hard requirement.</p>
	<h1 id="get-a-server">Get a server</h1>
	<p>I used the FreeBSD 12.2-RELEASE image with ZFS on a VPS. A VPS is not required, you could choose to run your IRC server out of your house. The steps will be mostly the same either way.</p>
	<h1 id="install-ngircd">Install ngircd</h1>
	<p>ssh into your system and install ngircd.</p>
	<pre>$ pkg install ngircd </pre>
	<h1 id="generate-ssl-certs">Generate SSL certs</h1>
	<p>In order to prevent password sniffing, we’ll need to configure ngircd to use TLS. OpenSSl comes with the base system and we can use it to create SSL certificates.</p>
	<pre>$ mkdir -p /usr/local/etc/ssl
$ cd /usr/local/etc/ssl
$ openssl req -newkey rsa:2048 -x509 -keyout server-key.pem -out server-cert.pem -days 1461
$ openssl dhparam -2 -out dhparams.pem 4096`
	</pre>
	<blockquote>
	    <p>Note: IRC is <strong>NOT</strong> an encrypted messaging system. Your messages are not end-to-end encrypted, even with TLS. Be careful not to share anything you don’t want becoming public information.</p>
	</blockquote>
	<h1 id="configure-ngircd">Configure ngircd</h1>
	<p>While this step is extremely variable, my example configuration will be helpful. When configuring most software, I like to copy the default configuration files to a .sample or a .deafult file in case I make a mistake and need to start back again from scratch. With a backup file created, let’s open a text editor and begin modifying some things.</p>
	<pre>$ cp /usr/local/etc/ngircd.conf /usr/local/etc/ngircd.conf.sample 
$ vim /usr/local/etc/ngircd.conf</pre>
	<p>The configuration file might be unfamilliar but don’t be afraid. The general process will be to scroll through each section, read the comments that start with a ‘#’, decide on whether or not to enable or disable the variables that are commented with a ‘;’, and substitute the default values for your own. My config file looks similar to the following. Uncomment and modify your config file accordingly. Copying and pasting will likely result in a non-functional server.</p>
	<pre>[GLOBAL]
  Name = irc.server.tld
  AdminInfo1 = Admin
  AdminInfo2 = Admin
  AdminEMail = nocontact@local.host
  HelpFile = /usr/local/share/doc/ngircd/Commands.txt
  Info = irc server for irc.server.tld 
  Listen = ::,0.0.0.0
  MotdFile = /usr/local/etc/ngircd.motd
  MotdPhrase =
  Network =
  Password =
  PidFile = /var/run/ngircd/ngircd.pid
  Ports = 6667, 6668, 6669
  ServerGID = wheel
  ServerUID = root

[LIMITS]
  ConnectRetry = 60
  IdleTimeout = 0
  MaxConnections = 0
  MaxConnectionsIP = 5
  MaxJoins = 10
  MaxNickLength = 9
  MaxPenaltyTime = -1
  MaxListSize = 100
  PingTimeout = 120
  PongTimeout = 20

[OPTIONS]
  AllowedChannelTypes = #&amp;+
  AllowRemoteOper = yes
  ChrootDir =
  CloakHost =
  CloakHostModeX =
  CloakHostSalt = &lt;WmR`R[$[#p&quot;ni!2-ZiI$P^h=UkdHN1#
  CloakUserToNick = no
  ConnectIPv4 = yes
  ConnectIPv6 = yes
  DefaultUserModes =
  DNS = yes
  IncludeDir = /usr/local/etc/ngircd.conf.d
  MorePrivacy = no
  NoticeBeforeRegistration = no
  OperCanUseMode = no
  OperChanPAutoOp = yes
  OperServerMode = no
  PAM = no
  PAMIsOptional = no
  PAMServiceName = ngircd
  RequireAuthPing = no
  ScrubCTCP = no
  SyslogFacility = local5
  WebircPassword =

[SSL]
  CertFile = /usr/local/etc/ssl/server-cert.pem
  CipherList = HIGH:!aNULL:@STRENGTH:!SSLv3
  DHFile = /usr/local/etc/ssl/dhparams.pem
  KeyFile = /usr/local/etc/ssl/server-key.pem
  KeyFilePassword = &lt;secret&gt;
  Ports = 6697, 9999

[OPERATOR]
  Name = op_name
  Password = op_pass
  Mask =

[CHANNEL]
  Name = #general
  Modes =
  Key =
  MaxUsers = 0
  Topic = general discussion
  KeyFile =
	</pre>
	<p>When you think you are done with the configuration file, run the following command. This will check the config file for errors and typos.</p>
	<pre>$ ngircd --configtest</pre>
	<h1 id="enable-and-start-the-ngircd-service">Enable and start the ngircd service</h1>
	<p>After writing our config file, we’ll need to configure ngircd to start at boot time. This is accomplished with sysrc.</p>
	<pre>$ sysrc ngircd_enable=yes
$ sysctl ngircd start</pre>
	<h1 id="connect-to-the-server">Connect to the server</h1>
	<p>There are many IRC clients you can choose from (I prefer irssi) but the command syntax should similar if not identical. First we’ll check if the non-tls ports are working, disconnect, then reconnect with TLS.</p>
	<pre>/connect irc.server.tld
/disconnect
/connect -tls irc.server.tld 9999</pre>
	<blockquote>
	    <p>Note: If you are getting authentication errors, you might need to set ‘PAM=no’ in ngircd.conf.</p>
	</blockquote>
	<h1 id="create-a-motd">Create a MOTD</h1>
	<p>The MOTD is printed when a user logs in. Useful information about the server can be put here. To edit this file, run the following:</p>
	<pre>$ vim /usr/local/etc/ngircd.motd</pre>
	<p>I found two useful tools to create pretty a MOTD: <a href="http://www.patorjk.com/software/taag/">A text to ascii art generator</a>, and <a href="https://github.com/z0mbieparade/c0lorize">A tool that helps create colored MOTDs</a>. </p>
	<h1 id="final-thoughts">Final Thoughts</h1>
	<p>At this point, we have a bare bones IRC server. We have not yet installed any services so we are missing functionality seen on large networks like FreeNode. I currently don’t have the need for services. And even more so, I don’t have the motivation to sift through the documentation plagued with the toxicity that seems to go with the IRC elitist types. But eventually I’m sure I’ll figure out services. Maybe I’ll do a part 2.</p>
	<p>The hardest part is yet to come: convincing my friends to use IRC.</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
