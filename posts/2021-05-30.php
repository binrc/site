<?php include "../includes/header.html"; ?>
<title>Xsession Tricks - Integrating Virtual Machines Into your Display Manager</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>Lately I’ve been playing with virtual machines. Although virt-manager is great, it’s not so great for the family members who just want to use the computer. Instead of dual booting, I chose seamless virtualization.</p>
	<h1 id="software">Software</h1>
	<p>Although I love FreeBSD, I am more experienced with virtualization under Fedora. I’m sure a similar technique would with with bhyve, replacing virt-viewer with your fullscreen VNC client. But for this article, we’re placing the Fedora on our heads.</p>
	<p>If you’ve installed Fedora 34 Workstation, you already have kvm and qemu thanks to gnome-boxes. These are very user friendly but not as scriptable.</p>
	<pre>[user@localhost]$ sudo dnf install libvirt-client virt-viewer</pre>
	<h1 id="vm-installation">VM Installation</h1>
	<p>This step is largely user specific, but it’s the same as normal hardware. If you are installing Windows, you’ll want <a href="https://github.com/virtio-win/kvm-guest-drivers-windows">the kvm guest additions</a>. You can use gnome-boxes, virt-manager, or virsh. # Scripting VM Startup ## Writing a startup script After our VM is installed, we need to figure out what it’s name is. Use the following command to find your VM’s name. Mine is ‘win10’.</p>
	<pre>[user@localhost]$ virsh list --all
 Id   Name          State
------------------------------
 8    freebsd13.0   running
 -    kali          shut off
 -    openbsd6.7    shut off
 -    solaris10     shut off
 -    win10         shut off
	</pre>
	<p>Now we need to write a small shell script. This shell script will be used to start our VM and launch virt-viewer. I like to put my custom scripts and programs in /opt. Name it something specific to the VM, like ‘win10xsession.sh’.</p>
	<pre>#!/bin/bash
# starts our VM
virsh start win10

# launches the viewer fullscreen
# in kiosk mode that exits when the vm shuts down
virt-viewer -f -k --kiosk-quit  on-disconnect --attach win10</pre>
	<p>Now we need to chmod it</p>
	<pre>[user@localhost]$ sudo chmod a+x /opt/win10xsession.sh</pre>
	<h2 id="writing-an-xsession-.desktop-file">Writing an xsession .desktop file</h2>
	Display managers like GDM, SDDM, LightDM, &amp;c are helpful because they allow us to easily switch between desktops and window managers. They know what desktops are installed by scanning for .desktop files in
	<pre> &lt;share&gt;/xsessions/ </pre>
	<p>This is extremely helpful for us because we can write a custom .desktop file. It’s easiest to copy a pre-existing .desktop file and modify it.</p>
	<pre>[user@localhost]$ cd /usr/share/xsessions

# we must preserve the SELinux context
# or GDM won&#39;t see it
[user@localhost]$ sudo cp --preserve=context gnome.desktop win10.desktop</pre>
	<p>And now we can modify our file. Mine looks something like this, but you really should look at the file before nuking it.</p>
	<pre>[Desktop Entry]
Name=Windows10
Comment=Windows10
Exec=/opt/win10xsession.sh
TryExec=/opt/win10xsession.sh
Type=Application
X-LightDM-DesktopName=i3
DesktopNames=i3</pre>
	<p>Now we can restart gdm and log in to our VM. Beware, restarting gdm will kill your current xsession and send you back to the login screen.</p>
	<pre>[user@localhost]$ sudo systemctl restart gdm.serice</pre>
	<h1 id="conclusion">Conclusion</h1>
	<p>Now you should be able to select your VM from the gear icon or session button on the display manager. If you cannot see your desktop entry, double check your .desktop file and make sure the syntax is correct and that your script is executable. If your desktop session refuses to start, you probably have an error in your script and need to revise it.</p>
	<p>I think that virtualization might be a better option than dual booting, especially when Windows doesn’t like to play nicely with other operating systems.</p>
	<p>I don’t know if I have an immediate use for this, but modifying xsession files so that the display manager dumps the user into an unexpected operating system makes for a good prank.</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
