<?php include "../includes/header.html"; ?>
<title>Getting to werc</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<p>Werc is described as a ‘Sane web anti-framework’ written in rc by our 9friends. I don’t live on plan9, but it’s very refreshing to see that the minimal web lives on .</p>
	<h1 id="what-is-werc">What is werc?</h1>
	<p>See <a href="http://werc.cat-v.org/">werc</a>. SSL not included.</p>
	<p>Quoting directly from the website:</p>
	<blockquote>
	    <p>Werc is a minimalist web anti-framework built following the <a href="http://doc.cat-v.org/unix/">Unix</a> and <a href="http://plan9.cat-v.org/">Plan 9</a> tool philosophy of software design.</p>
	    <p>Werc avoids the pain of managing collections of websites and developing web applications.</p>
	    <ul class="incremental">
		<li>Database free, uses files and directories instead.</li>
		<li>Written using the <a href="http://rc.cat-v.org/">rc shell</a>, leveraging the standard Unix/Plan 9 command toolkit.</li>
		<li>Minimize tedious work: eg., no need to ever write HTML, use markdown (or any other format) instead.</li>
		<li>Very minimalist yet extensible codebase: highly functional core is 150 lines, with extra functionality in modular <a href="https://werc.cat-v.org/apps/">apps</a>.</li>
	    </ul>
	</blockquote>
	<h1 id="why-use-werc">Why use werc?</h1>
	<p>werc is simple but extensible. Files can be written in HTML, md, or even plaintext. werc is editor agnostic, uses no database, supports multiple ‘vhosts’ out of the box, has a simple user system, etc. It’s the simplest site generator I’ve ever touched, especially because rsync is not an unlisted but implied dependency.</p>
	<p>The best part - all you need is a UNIX-like OS with a httpd that supports GCI and Plan9 utilities.</p>
	<h1 id="mandatory-apache-setup">Mandatory Apache Setup</h1>
	<p>I’m dumping werc onto my existing FreeBSD webserver. My setup is . . . overly complicated . . . but I already run multiple subdomains on the same server so adding another one for werc should be very simple.</p>
	<h2 id="plan9port">plan9port</h2>
	<p>First, I installed plan9port</p>
	<pre>$ pkg install plan9port</pre>
	<h2 id="apache-part-1">Apache part 1</h2>
	<p>Then, I modified /usr/local/etc/apache24/httpd.conf to enable CGI. Check if mpm_prefork_module is enabled in your conf, then uncomment the corresponding line. I don’t have mpm_prefork_module enabled so mine looks something like this:</p>
	<pre>#. . . 
&lt;IfModule !mpm_prefork_module&gt;
    LoadModule cgid_module libexec/apache24/mod_cgid.so
&lt;/IfModule&gt;
&lt;IfModule mpm_prefork_module&gt;
    #LoadModule cgi_module libexec/apache24/mod_cgi.so
&lt;/IfModule&gt;
#. . . </pre>
	<p>The next part is tricky. I needed to add a vhost <em>without ssl rewrites</em>, get certs with acme.sh, then add the re-writes that force all traffic to use SSL. In /usr/local/etc/apache24/extra/httpd-vhosts.conf.</p>
	<pre>&lt;VirtualHost *:80&gt;
DocumentRoot &quot;/usr/local/www/apache24/data/werc.0x19.org&quot;
ServerName werc.0x19.org
&lt;/VirtualHost&gt;</pre>
	<h2 id="ssl-certs">SSL certs</h2>
	<p>I have a script that issues certs with acme.sh. You should write something similar. Every time I add a subdomain, I just add a -d flag to my list of domains, run the script, then modify my crontab to automatically renew the new domain. Once my certs installed successfully, I added rewrite rules.</p>
	<h2 id="apache-part-2">Apache part 2</h2>
	<p>Once again, I opened /usr/local/etc/apache24/extra/httpd-vhosts.conf, this time to add SSL rewrites. Now, the vhost block looks like this:</p>
	<pre>&lt;VirtualHost *:80&gt;
DocumentRoot &quot;/usr/local/www/apache24/data/werc.0x19.org&quot;
ServerName werc.0x19.org
RewriteEngine on
RewriteCond %{SERVER_NAME} =werc.0x19.org
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
&lt;/VirtualHost&gt;</pre>
	<p>I needed to add a corresponding vhost that uses SSL <em>and</em> has all the configs for werc. Since werc supports user login, I want to prevent werc from ever running without SSL. That block begins like this:</p>
	<pre>&lt;VirtualHost *:443&gt;
ServerName werc.0x19.org:443
DocumentRoot &quot;/usr/local/www/apache24/data/werc.0x19.org&quot;
ErrorLog &quot;/var/log/httpd-error.log&quot;
TransferLog &quot;/var/log/httpd-access.log&quot;
RewriteEngine on
SSLEngine on
SSLCertificateFile &quot;/usr/local/etc/apache24/ssl/cert.pem&quot;
SSLCertificateKeyFile &quot;/usr/local/etc/apache24/ssl/key.pem&quot;
SSLCertificateChainFile &quot;/usr/local/etc/apache24/ssl/fullchain.pem&quot;
# . . . end of boilerplate, werc configs below</pre>
	<h2 id="more-boilerplate">More boilerplate</h2>
	<p>I downloaded werc, extracted the tarball to my cgi directory, then modified the vhost config so it knows what it’s doing.</p>
	<pre>$ cd 
$ fetch http://werc.cat-v.org/download/werc-1.5.0.tar.gz
$ tar czfv ./werc-1.5.0.tar.gz
$ cp -r ./werc-1.5.0 /usr/local/www/apache24/cgi-bin/werc</pre>
	<p>Very intentionally, I added werc configs to my vhost that supports SSL. Since werc has user login functionality, I wanted to force all users to connect with SSL to prevent accidental login leakage.</p>
	<p>Pay very close attention to your vhost config. This one is specific to my server, not yours.</p>
	<pre># . . . beginning of werc configs, boilerplate above

# tell cgi to run .rc scripts
AddHandler cgi-script .rc
AddHandler cgi-script .cgi
&lt;Directory /usr/local/www/apache24/cgi-bin/werc/bin&gt;
    Options ExecCGI
    AllowOverride None
    Order allow,deny
    Allow from all
&lt;/Directory&gt;
# this is for sitemap.{txt,gz}
&lt;Directory /usr/local/www/apache24/cgi-bin/werc/sites&gt;
    Options FollowSymLinks
    AllowOverride None
    Order allow,deny
    Allow from all
&lt;/Directory&gt;
&lt;IfModule mod_dir.c&gt;
    DirectoryIndex /werc.rc
&lt;/IfModule&gt;

#RewriteRule (.*) /usr/local/www/apache24/cgi-bin/werc/sites/%{SERVER_NAME}/$1
RewriteRule (.*) /usr/local/www/apache24/cgi-bin/werc/sites/werc.0x19.org/$1

RewriteRule /pub/style/style.css /usr/local/www/apache24/cgi-bin/werc/pub/style/style.css
RewriteRule /favicon.ico /usr/local/www/apache24/cgi-bin/werc/pub/favicon.ico

RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule .* /usr/local/www/apache24/cgi-bin/werc/bin/werc.rc

RewriteRule /werc.rc /usr/local/www/apache24/cgi-bin/werc/bin/werc.rc
DocumentRoot /usr/local/www/apache24/cgi-bin/werc/bin/
ErrorDocument 404 /werc.rc
&lt;/VirtualHost&gt;</pre>
	<p>Then finally, I restarted apache</p>
	<pre>$ service apache24 restart</pre>
	<h1 id="mandatory-werc-configuration">Mandatory werc Configuration</h1>
	<p>Since I am running my webserver on Freebsd instead of 9, minor configuration tweaks are required. plan9port installs to /usr/local/plan9 but werc expects to be running natively on 9. The fix is easy. The first file I modified was /usr/local/www/apache24/cgi-bin/werc/werc.rc and changed the shebang to</p>
	<pre>#!/usr/local/plan9/bin/rc</pre>
	<p>After this, I copied the directory for the default site to my own site. If you look back up at the vhost config, we explicitly specified a directory for werc to serve.</p>
	<pre>$ cp -r sites/tst.cat-v.org sites/werc.0x19.org</pre>
	<p>Hey cool! It worked . . . almost.</p>
	<h1 id="why-the-hell-wont-this-werc">“Why the hell won’t this werc”?</h1>
	<p>I added a few markdown files, modified the headers and footers, changed the site metadata, name, etc. But there was one problem: werc was unable to convert my markdown files to html and was failing.</p>
	Like a good sysadmin I checked apache error logs only to see the following message that haunted me until approximately 45 minutes ago when I began writing this post:
	<pre> md2html.awk: No such file or directory </pre>
	<p>At this point, it was already 4 in the morning. Instead of staying up later, I decided to sleep on it and allow the message to haunt me throughout the day. It wasn’t until I realized that there was more breakage because FreeBSD != 9.</p>
	<p>Turns out that FreeBSD awk is not in /bin/awk, but in /usr/bin/awk. At 4 in the morning, my instinctual Linux reactions begin to take control and I forget that awk is not required for core FreeBSD operations. It also turns out that plan9 binaries won’t run on FreeBSD. To fix this, I did three things.</p>
	<h3 id="modify-wercetcinitrc">1. Modify werc/etc/initrc</h3>
	<pre># default line, commented because shit broke
#formatter=(fltr_cache md2html.awk) # no perl for old men

# new line, 9 bytecode != freebsd bytecode
formatter=(md2html.awk)</pre>
	<h3 id="move-md2html.awk-into-the-path-i-explicitly-stated-in-werc.rc">2. Move md2html.awk into the path I explicitly stated in werc.rc</h3>
	<pre>$ cp werc/bin/contrib/md2html.awk /usr/local/plan9/bin</pre>
	<h3 id="correct-the-shebang-in-md2html.awk-so-that-it-will-actually-run">3. Correct the shebang in md2html.awk so that it will actually run</h3>
	<p>old:</p>
	<pre>#!/bin/awk</pre>
	<p>new:</p>
	<pre>#!/usr/bin/awk</pre>
	<h1 id="eof">EOF</h1>
	<p>I have not yet played with all of the apps, but they seem very promising. So far, I have only tried the comments (they work) and diri (it works but I’m not letting apache write files to my cgi dir). If I had known about werc, I likely would have just used it instead of the bootstrap+jekyll+php mess I have created. The simple design and easy customizability is very appealing to me and the ‘plugin’ ‘infrastructure’ seems promising since it’s language agnostic.</p>
	<p>Overall, I am ready for a refreshing web experience. Simplicity and ‘anti-over-styling’ has become more and more appealing to me lately. If you look closely at the design of 0x19.org, it’s almost like I wanted something this simple all along </p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
