<?php include "../includes/header.html"; ?>
<title>Building an Email Server with OpenBSD</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
    <div class="col-12">
	<blockquote>
	    <p>It can’t be DNS. There’s no way it could be DNS. It was DNS.</p>
	</blockquote>
	<p>This article is not a full tutorial, just thoughts and notes on my OpenBSD email server assembly process. <a href="https://poolp.org/posts/2019-09-14/setting-up-a-mail-server-with-opensmtpd-dovecot-and-rspamd/">This article from poolp.org </a> helped me greatly. I couldn’t be more thankful. <a href="https://web.archive.org/web/20210705030904/https://poolp.org/posts/2019-09-14/setting-up-a-mail-server-with-opensmtpd-dovecot-and-rspamd/">Here is a wayback machine link in case the datacenter go boom.</a></p>
	<h1 id="why-openbsd">Why OpenBSD?</h1>
	<p><small> And why not something else? </small></p>
	<p><strong>The Short:</strong> Because OpenBSD just works. Email isn’t hard. Linux (and even FreeBSD) are inclined overcomplicate it.</p>
	<p><strong>The Long:</strong> Before starting this project, I did a significant but intermittent amount of research into email servers and read many deprecated blog posts about the whole ordeal. There seemed to be a general superstition about how difficult email can be. The paranoia lurks, psyching out the hobbyist admin. No wonder with all the hearsay about rejection, DNS, reputation, and hacking.</p>
	<p>It’s been nearly a year since I began thinking about hosting my own email. Obviously I adopted some of this superstition myself, or at least allowed it to enable my procrastination. Since this is a holiday weekend, I thought “What better to do than spend three days troubleshooting email?”</p>
	<p>Initially I wanted to host the email server on FreeBSD but eventually landed on OpenBSD. Why? Because I wanted less headache. UNIX is like a loaded gun pointed at your foot. It is very easy to hurt yourself. OpenBSD mitigates some of this by mounting the loaded gun in a fixed position. It will require intense gymnastics to shoot yourself in the foot. OpenBSD seemed like the appropriate choice to minimize foot shooting, especially if I am providing email accounts for other people. I cannot in good faith provide a dumpster fire for my friends and colleagues, especially if there are important documents in that dumpster. The only flaming dumpster I endorse is the multi-user shell server created specifically for multiple users to start fires in.</p>
	<p>Fires aside, I was pleasantly surprised with OpenBSD. I held a preconceived notion that ‘sometimes OpenBSD doesn’t do what you want it to do because it was designed to mitigate flaming dumpsters and foot-shooting.’ But boy was I wrong. From a desktop perspective, OpenBSD can feel limiting . . . but, in server space, OpenBSD feels very pleasant to use.</p>
	<h1 id="the-software-stack">The Software Stack</h1>
	<p><small>Stop! Mailinabox and Mailcow are black boxes and we want nothing to do with them! </small></p>
	<p>A mail server consists of multiple moving parts. Some would say ‘many’ moving parts, but I disagree. If you’ve ever had to set up a reverse proxy for your django or rails app, you’ve touched more moving parts than a basic email server.</p>
	<table>
	    <colgroup>
		<col style="width: 33%" />
		<col style="width: 33%" />
		<col style="width: 33%" />
	    </colgroup>
	    <thead>
		<tr class="header">
		    <th>Component</th>
		    <th>Program/file</th>
		    <th>What it does</th>
		</tr>
	    </thead>
	    <tbody>
		<tr class="odd">
		    <td>MTA</td>
		    <td>OpenSMTPD</td>
		    <td>Sends and receives mail via SMTP</td>
		</tr>
		<tr class="even">
		    <td>MDA</td>
		    <td>Dovecot</td>
		    <td>Provides IMAP/POP so that users can use the server via a client</td>
		</tr>
		<tr class="odd">
		    <td>MUA</td>
		    <td>Thunderbird</td>
		    <td>IMAP/POP client</td>
		</tr>
		<tr class="even">
		    <td>Spam filter</td>
		    <td>Rspamd</td>
		    <td>Progressively learns what is and isn’t spam</td>
		</tr>
		<tr class="odd">
		    <td>Auth</td>
		    <td>UNIX Auth</td>
		    <td>Allows a user to connect using the same credentials they would use to log in to the system locally</td>
		</tr>
		<tr class="even">
		    <td>Mailbox</td>
		    <td>~/Maildir</td>
		    <td>Stores user email</td>
		</tr>
		<tr class="odd">
		    <td>LMTP</td>
		    <td>UNIX Sockets</td>
		    <td>Transfers mail between the MDA and the MUA</td>
		</tr>
	    </tbody>
	</table>
	<p>Typically, the mail process can look like this:</p>
	<pre>
Bob's MUA (via IMAP/POP) -> Bob's MDA (via LMTP) -> Bob's MTA (via IMAP) -> Alice's MTA  
	</pre>
	<pre>
Alice's MTA (via LMTP) -> Alice's MDA -> Alice's Mailbox
	</pre>
	<p>When Alice wants to check her mail, it will look something like this:</p>
	<pre>
Alice's Mailbox <- Alice's MDA <- Alice's MUA (via IMAP/POP)
	</pre>
	<h1 id="dns">DNS</h1>
	<p><small> They hate what they do not understand. </small></p>
	<p>Modern email is a hack on top of DNS. In addition to an A record and an MX record, we need three TXT records: DMARC, DKIM, and SPF.</p>
	<table>
	    <thead>
		<tr class="header">
		    <th>Type</th>
		    <th>What it’s for</th>
		</tr>
	    </thead>
	    <tbody>
		<tr class="odd">
		    <td>DMARC</td>
		    <td>Instructs the sender what to do if unable to deliver</td>
		</tr>
		<tr class="even">
		    <td>DKIM</td>
		    <td>Server’s pubkey, used for cryptographically verifying the sender</td>
		</tr>
		<tr class="odd">
		    <td>SPF</td>
		    <td>Tells other servers who is allowed to send mail from our server</td>
		</tr>
	    </tbody>
	</table>
	<p>And some example records:</p>
	<table>
	    <colgroup>
		<col style="width: 20%" />
		<col style="width: 20%" />
		<col style="width: 20%" />
		<col style="width: 20%" />
		<col style="width: 20%" />
	    </colgroup>
	    <thead>
		<tr class="header">
		    <th>Type</th>
		    <th>Name</th>
		    <th>Data</th>
		    <th>TTL</th>
		    <th>Priority</th>
		</tr>
	    </thead>
	    <tbody>
		<tr class="odd">
		    <td>A</td>
		    <td>mail</td>
		    <td>123.456.78.9</td>
		    <td>120</td>
		    <td></td>
		</tr>
		<tr class="even">
		    <td>MX</td>
		    <td>mail</td>
		    <td>123.456.78.9</td>
		    <td>120</td>
		    <td>10</td>
		</tr>
		<tr class="odd">
		    <td>TXT</td>
		    <td>selector._domainkey.mail</td>
		    <td>“v=DKIM1;k=rsa;p=a 128 bit RSA key ;”</td>
		    <td>120</td>
		    <td></td>
		</tr>
		<tr class="even">
		    <td>TXT</td>
		    <td>_dmarc.mail</td>
		    <td>“v=DMARC1;p=quarantine;pct=100;rua=mailto:postmaster@mail.domain.tld;”</td>
		    <td>120</td>
		    <td></td>
		</tr>
		<tr class="odd">
		    <td>TXT</td>
		    <td>mail</td>
		    <td>““v=spf1 ip4:123.456.78.9 -all”</td>
		    <td>120</td>
		    <td></td>
		</tr>
	    </tbody>
	</table>
	<h1 id="problems">Problems</h1>
	<h2 id="software-stack">Software stack</h2>
	<p>My biggest problem with the software stack was not actually the software stack. It was a DNS issue. For some god forsaken reason, Vultr automaticaly creates a * CNAME record. This is a problem because, depending on which record is found first, subdomain.domain.tld resolves to domain.tld. This was causing localhost to resolve to an external IP address.</p>
	<p>Localhost binding to an external address is a big problem. Why? because if localhost resolves to anything other than 127.0.0.1 a service might try to bind to 123.456.78.9:port instead of 127.0.0.1:port. Redis and rspamd were both trying to bind to an external address and failing to start.</p>
	<p>After I deleted the CNAME wildcard record, everything started just fine.</p>
	<h2 id="dns-1">DNS</h2>
	<p>The biggest issue for DNS was that I actually misconfigured it. My mail server is actually at mail.domain.tld, not domain.tld. I Set up all my TXT records for domain.tld so google was automatically rejecting my email . . . because there was not valid DMARC, DKIM, or SPF. After modifying all the records so that they match the subdomian, I was still experiencing DKIM issues. In order to fix this, I copied the DKIM entry for mail.domain.tld to domain.tld and the issue seems to have been resolved.</p>
    </div>
</div>
<?php include "../includes/footer.html"; ?>
