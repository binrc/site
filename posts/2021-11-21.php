<?php include "../includes/header.html"; ?>
<title>Initial Payload</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>Purpose</h1>
<p>My goal for this site is to use it as a dumping ground for my ideas. I also intend to publish other articles on the things I am doing at the time. </p>
<h1>Intentions </h1>
<ul>
<li> This site will always be compatible </li>
<p> This site will always be 100% compatible with Mothra, Netsurf, and Dillo. These browsers have limited tag support and no JavaScript support Whatsoever. My website will <i> always work </i> with the <b> greatest </b> number of web browsers. </p>

<li> This site will always be JavaScript free </li>
<p> JavaScript is unnecessary. Nearly every website on the planet can accomplish it's goals entirely serverside or with minimal AJAX. JavaScript is a privacy nightmare. Nearly every website on the planet uses it to track you. JavaScript is nonfree. Creative Commons documents would be otherwise hindered with the inclusion of JavaScript </p>

<li> This site will always be minimally styled </li>
<p> Minimal styling means everything looks the same (or very similar) in every web browser. </p>

</ul>

<h1>Aspirations</h1>
<p>get paid</p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
