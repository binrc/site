<?php include "../includes/header.html"; ?>
<title>Resting comfy with OpenBSD httpd (no ssl)</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>The Server</h1>
<p>OpenBSD's webserver is called <code>httpd</code> and is very easy to use. In order to get my website running (without ssl for now because I haven't decided on a domain name), I ran a few simple commands. I will update this once I choose a domain and start serving on 443. Here's the output from <code>fc -l</code></p>
<pre>
# install deps
pkg_add php git

# copy default configs, start services
cd /etc
cp examples/httpd.conf ./
rcctl enable php74_fpm
rcctl start php74_fpm
rcctl enable httpd
rcctl start httpd

# edit configs
vi httpd.conf

# restart services
rcctl restart httpd

# add documents to be served
cd /var/www/htdocs/
rm -rf bgplg/
git clone https://gitlab.com/binrc/site
mv ./site/* ./
rm -rf ./site
</pre>

<p> These commands should be self explanatory but I will go into more detail. First, we install php and git. Neither of these are necessary, OpenBSD comes with everything you need in the box. I installed php because I'll be serving dynamic content. I typically just use rsync for file transfer but today I felt like using git. </p>

<p> The next thing we do is get a functional configuration file. Mine looks like this. Everything that starts with a <code>#</code> is a comment. Instead of removing these unused lines, I like to comment them out. It makes it easier to quickly add ssl support later.</p>

<pre>
# $OpenBSD: httpd.conf,v 1.22 2020/11/04 10:34:18 denis Exp $

server "default" {
        listen on * port 80
        directory index index.php
        location "*.php" {
        fastcgi socket "/run/php-fpm.sock"
        }
#       location "/.well-known/acme-challenge/*" {
#               root "/acme"
#               request strip 2
#       }
#       location * {
#               block return 302 "https://$HTTP_HOST$REQUEST_URI"
#       }
}

#server "example.com" {
#       listen on * tls port 443
#       tls {
#               certificate "/etc/ssl/example.com.fullchain.pem"
#               key "/etc/ssl/private/example.com.key"
#       }
#       location "/pub/*" {
#               directory auto index
#       }
#       location "/.well-known/acme-challenge/*" {
#               root "/acme"
#               request strip 2
#       }
#}
</pre>

<p> What this means is "listen for http requests on port 80. When someone visits the site and doesn't request a specific file, assume they want <code>index.php</code>, if the client requests a php file, process it with php-fpm first."</p>

<p> That's it. In less than five minutes, less than fifteen commands, and less than 8 lines of parsed configs I have a functional webserver. </p>


</div>
</div>
<?php include "../includes/footer.html"; ?>
