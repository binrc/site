<?php include "../includes/header.html"; ?>
<title>I Hate Social Media</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>Burn out</h1>
<p>A little over a year ago I became disillusioned with social media in it's entirety. I was feeling very tired with all social situations, online and offline. The constant bombard of opinions, people, and notifications that I <i>really</i> didn't care about were consuming the majority of my mental cycles. I left to remind myself that my time is valuable and I owe it to no one, especially people who aren't paying me for it.</p>
<h1>Mechanism of action</h1>
<blockquote>Social media and it's consequences have been a disaster for the human race. It has greatly increased the connectivity for those of us who are 'plugged into the machine', but it has destabilized our minds, has made life unfulfilling, has subjected human beings to indignities, has led to widespread psychological suffering, and has inflicted severe damage on the natural way humans conduct themselves around each other. The continued usage of social media will only worsen the situation. </blockquote>
<p> Social media is bubbling to the brim with useless content, pointless flame wars, tired opinions, wasted time, and depleted energy. Many individuals who use social media force themselves to fit into communities that don't actually exist in the real world (or even online in any meaningful way). Many irrational ideologies are forged and encouraged in the echo chambers facilitated by social media. Virtue is earned by repeating the mantras and participating in group think.</p>
<p> Using text analysis and OSINT mapping, users are placed into various ideological factions. These factions serve to divide the users into warring groups and to isolate them from the real world. "All of my real friends are in the faction. The social media website says so!" Novel ideas, unpopular opinions, and critical thinking (aka <i>wrongthink</i>) are actively punished. If not by the website, by the users' fellow faction members. Where can a user turn if they catch glimpses of lucidity and see the truth of their situation? Only inwards. Likely they will receive online and real life pushback for attempting to leave the warring feudal state that is social media. </p>
<p> This environment is actively harmful for humans. Rather than encouraging critical thinking, rewards are given for passivity and repetition. These companies cultivate mutually assured destruction instead of facilitating mutual growth. Even the people who run these websites have admitted that driving users against each other increases time spent on the site and, by extension, the revenue that can be scraped off the top. The social media companies fully understand the total cost in human suffering their products cause but they are uninterested in reducing or even preventing this harm.</p>
<p> The users are exposed to deadly levels of propaganda and stimulation. Every second there is a new trend, a new notification, a new person to rail against. The mind of a social media user is being controlled and they rarely realize the true nature of their situation. Everything they are and everything they can ever become is locked into an identitarian system where the only way out is destroy the very essence of their being. Until the old pseudo-identity is burned away, the user will never realize who they truly are. They cannot realize that they are actually a <i>used</i> and not a <i>user</i> until they view the entire ordeal through multiple layers of abstraction. </p>
<p> Social media is a control structure. It is new opiate of the masses. We must abandon all of these structures in order to reclaim our liberties, knowledge of who we really are, capabilities of independent thought, creativity, and free will.</p>

<h1>Social media is lazy</h1>
<p><b>MOST</b> if not <b>ALL</b> of the user generated content on these social media websites is inherently worthless. Humans are inherently lazy. Efforts that could be better spend working on a Magnum Opus are wasted on snippets that are completely devoid of any intelligent thought. Once a user figures out what the faction likes to hear, the user will repeat it <i>ad  infinitum</i> for social validation. Nevermore will the user speak their true opinions. Why work on a real project when you could just say the trendy line and get instant gratification? Why take a gamble on a great work that might go unappreciated when you can just copy and paste the latest mantra for an instant dopamine hit? How many great artists, programmers, and writers have been lost in the constant turmoil of social media?</p>
<p>Social media has trained it's user base to be unquestioning. The websites have reduced actual humans to something analogous to cattle in a feed lot. All they know is consume. Useless information goes in, useless information goes out. Sure, leaving the feed lot requires a bit of self sufficiency but isn't liberty better than false securities?</p>

<h1>The data question</h1>
<p><b>ALL</b> social media (even <i>"""better"""</i> activitypub based social media) encourages it's users to post personal information about themselves. Oftentimes, without thinking first. <b>ALL</b> social media websites (even the <i>"""better"""</i> activitypub ones) are used for data aggregation. Users are tricked into thinking that the website is super safe and super secure and that anything they say is private and amongst friends. Deception of any kind is unethical, especially when the main purpose of the deception is glean profits off of misguided trusts. </p>
<p> The data collected is stored permanently in a database that you don't own. This data is oftentimes given directly to federal agencies (without a warrant) and sold to advertisers (that you were tricked into consenting to). At least most of these companies will stop indexing your data if you submit a GDPR request or delete your account. </p>
<p> Federated social media is arguably worse. Anything a user says is distributed and cached on potentially hundreds of servers around the world. Even <i>"""better"""</i> activitypub based social media is lying to you. You can never truly follow through with the "It's easy to permanently delete your information" promise when you don't own every server in the network. Good luck finding all of these servers and good luck submitting all those GDPR complaints. </p>
<p> The lack of any banner or warning should be enough to discourage usage of a site. But the false promises (ie lies) of "privacy", "security", and "freedom" are an injustice. </p>

<h1>Social media is nonfree</h1>
<p> This one should be self explanatory. A program is nonfree if you cannot: </p>
<ol>
<li> Run the program for any purpose </li>
<li> Study the program works and change it to suit your needs </li>
<li> Redistribute the program </li>
<li> Distribute copies of your modified version </li>
</ol>
<p> Although opinions on software running on servers you don't own differ, every user should be entitled to a fully copy of the website's source code (including non-essential data analysis tools). This is the only way for a user to verify that their data is <i>really</i> being deleted and <i>really</i> not being sold to third parties. </p>
<p> Oftentimes, the JavaScript running on these websites is also nonfree (ie unlicensed). It might be <i>public source</i> but it is rarely <i>free and open source</i>.</p>

<h1>Advice</h1>
<p> Leaving social media can be hard, especially for isolated people. Social media might initially seem like a good way to make friends but don't be mislead. True friends would never stop being friends with you if aspects of your identity, personal opinions, or life circumstances change. </p>
<p>Even for people who are not isolated in real life, leaving social media can result in major pushback from friends and family. Do not listen to them. Any person who would stop being friends with you because you are taking actions to better yourself is not a person you should want as a friend. It is your life and it is your freedom. Liberty is a choice. </p>

<h1>Suggested Process</h1>
<p> Begin by deleting any accounts owned by Facebook or Twitter. This means Facebook, Instagram, and WhatsApp. If your friends say "B-BUT HOW WILL WE TALK WITHOUT MESSENGER!?!?!?", you can push them towards Signal. If they are really your friend, they will follow you to Signal because they actually want to talk to you. </p>
<p> Next, get rid of the obvious propaganda machines in your life. Delete your Reddit account. God forbid you used tik tok, delete that too. Instead of seeking meaningless distractions on your phone, download some wholesome e-books and podcasts. </p>
<p> Do you have any other accounts that might be considered social? Delete those too. Your freedom is in your own hands now. </p>
<p> Finally, the hardest step, limit your usage of Google services. It can be very difficult to completely de-Google your life but limiting is a very good start. Create a second email account at a non-Google provider (maybe Protonmail?), limit your usage of YouTube, and start using non-Google search engines. You'll be amazed how much less 'fenced-in' your internet experience feels when you stop surrendering your data for minor convinces. </p>

<h1>Additional Resources</h1>
<p>If you have any technical knowledge, you can self host nearly everything.</p>
<h3> Chat software </h3>
<p> Signal is a soft landing spot for social media refugees. Although it has it's own issues, it's the best grandma approved thing we have.</p>
<p> Setting up your own XMPP server is fairly easy. XMPP isn't quite grandma approved but your technical friends should be able to easily figure it out. Just be sure to choose a client that supports OMEMO (aka double ratchet) encryption. Dino on desktop Linux and Conversations from F-Droid on Android are good choices. </p>
<p> Mumble is great for voice chat. It's easy to set up a server and even easier to use. </p>
<p> Steer clear of matrix <b>unless you know exactly what you're doing and how to prevent it from federating</b>. Matrix is a metadata aggregation machine, much like activitypub. </p>
<h3> Sharing creative works </h3>
<p> Personal websites are fantastic. For new users interested in learning web development, you can get free static hosting at neocities.org. You can also get free static hosting on github and gitlab. For new users who just want something easy, consider getting a managed wordpress or hosting your own wordpress on a vps. </p>
<p> Using software like peertube is a good way to share videos. It is self hostable but there are many instances. Be aware though, peertube is peer to peer software so anything you upload is non-revokable. </p>
<p> Running your own nextcloud (or paying someone to do it for you) is a good way to share files with your family. It's entirely open source so you can be sure that no one is using pictures of your children to build AI models. </p>
<h3>Consuming media</h3>
<p> RSS was invented to pull data from multiple different websites and display it all together in one convenient place. Every news site, podcast, and blog has an RSS feed. RSS is also anti-feedlot. Only the feeds you explicitly add are displayed in your RSS reader. No more mindless scrolling, time to read things you care about. </p>
<p> Using the Newpipe app from F-Droid on Android is a good way to consume YouTube. No ads, background playing, etc. On a desktop computer, use a browser like FireFox with the following plugins: ublock origin, sponsorblock, YouTube Stop AutoPlay Next. </p>
<p> For music, stop throwing your money into the trash. Spotify is actually a scam. Instead of spending $12+ every month to stream music you could be buying physical media or supporting small artists on Bandcamp. Physical media is DRM free (this means you can share it with your friends). Many CDs, Cassette tapes, and Vinyl Records come with free digital downloads so keep an eye out if you're into antiquated media formats like the rest of us. </p>

<h1> Objections: </h1>
<p> Freedom is a choice. You are entitled to choose a life of digital servitude. I cannot force you from your cage, all I can do is continually point at the door and say "look - the door is unlocked. You can leave at any time but you must make the choice yourself."</p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
