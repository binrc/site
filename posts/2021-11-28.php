<?php include "../includes/header.html"; ?>
<title>installing plan9port on Linux, FreeBSD, OpenBSD</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>plan9port</h1>
<p> Plan 9 from User Space (also called plan9port or p9p) is a port of many Plan 9 from Bell Labs libraries and programs to UNIX systems. p9p comes with many utilities including 9p, 9c, 9l, acme, acid, rio, etc. The most useful components of p9p are 9c. 9c is a wrapper shell script for GCC or Clang on a standard UNIX system. Using the 9c compiler and 9l linker, native Plan 9 programs can be built on UNIX. </p>

<h1>Linux</h1>
<h3>Fedora</h3>
<p> compiler go <q><code>note: add parentheses around the second ‘sizeof’ to silence this warning</code></q> </p>
<pre>
# as root:
$ dnf install git libXt-devel libXext-devel
$ mkdir /opt && cd /opt
$ git clone https://github.com/9fans/plan9port plan9 && cd plan9
$ ./INSTALL

# set env vars, as non root user
$ echo PLAN9=/opt/plan9 export PLAN9 &gt;&gt; ~/.bashrc
$ echo PATH=\$PATH:\$PLAN9/bin export PATH &gt;&gt; ~/.bashrc
$ source ~/.bashrc
$ echo $PATH
/home/glenda/.local/bin:/home/glenda/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/opt/plan9/bin:/opt/plan9/bin

# test if you set up the env right
$ acme

# create an xsession file for the desktop manager, as root
echo -e "[Desktop Entry] \n\
Name=rio \n\
Comment=rio \n\
Exec=/opt/plan9/bin/rio \n\
TryExec=/opt/plan9/bin/rio \n\
Type=Application \n\
X-LightDM-DesktopName=rio \n\
DesktopNames=rio" &gt;&gt; /usr/share/xsessions/rio.desktop 
</pre>

<p> You should now be able to start rio from your standard display manager. </p>

<img class="posts" src="assets/images/fedora-rio.png" alt="rio running on fedora">

<h3>Debianoids</h3>
<p> see <a href="https://getfedora.org/">getfedora.org</a>, then refer to the instructions above. </p>

<h1>FreeBSD</h1>
<p> Handily, FreeBSD has a plan9port package. </p>
<pre>
# install
$ pkg install plan9port

# edit ypur path
$ echo 'setenv PLAN9 /usr/local/plan9' &gt;&gt; ~/.cshrc
$ echo 'setenv PATH {$PATH}:/usr/local/plan9/bin'
$ source ~/.cshrc
$ echo $PATH
/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin:/home/glenda/bin:/usr/local/plan9/bin:/usr/local/plan9/bin
$ rehash

# create an xsession file for the desktop manager, as root
echo '[Desktop Entry]\
Name=rio\
Comment=rio\
Exec=/opt/plan9/bin/rio\
TryExec=/opt/plan9/bin/rio\
Type=XSession\
X-LightDM-DesktopName=rio\
DesktopNames=rio' &gt;&gt; /usr/local/share/xsessions/rio.desktop 
</pre>

<p> Now you should be able to start rio at login time. </p>

<img class="posts" src="assets/images/freebsd-sddm.png" alt="freebsd sddm with rio">
<img class="posts" src="assets/images/freebsd-rio.png" alt="rio running on FreeBSD">

<h1>OpenBSD</h1>
<p> Like FreeBSD, the best operating systems have a package for plan9port. This time, no .desktop file. Xenodm reads from <code>~/.xsession</code> to determine what a user wants done. </p>

<pre>
$ pkg_add plan9port

# set up ksh
$ echo export PLAN9=/usr/local/plan9 &gt;&gt; ~/.profile
$ echo export PATH=\$PATH:/usr/local/plan9/bin &gt;&gt; ~/.profile
$ ksh -l ~/.profile

# make a .xsession file
echo \#!/bin/sh'\n'xrdb -load $HOME/.Xresources'\n'exec /usr/local/plan9/bin/rio &gt;&gt; ~/.xsession
</pre>
<p> At login with xenodm, rio will automatically start.</p>
<img class="posts" src="assets/images/openbsd-rio.png" alt="rio running on OpenBSD">

</div>
</div>
<?php include "../includes/footer.html"; ?>
