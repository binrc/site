<?php include "../includes/header.html"; ?>
<title>FreeBSD as a Source Based Operating System</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<small> or, Gentoo without the headache</small>
<h1>Gentoo</h1>
<p> I ran gentoo as my daily driver for nearly three years. I'm a <i>real Linux user</i> in the sense that I did daily what most Linux distro consumerists are incapable of. My gentoo desktop spend quite some time powered off and disconnected from the internet. Enough time that I had entered dependency hell with no clear escape route. I have traveled through the valley of dependency hell many times before and have never died. But this time, I couldn't be bothered. I've been meaning to migrate away from Linux for a long time. Something something systemd scope creep, rust in the kernel, live free or die, etc. </p>

<h1> Installation time</h1>
<p> FreeBSD is easy to install. If you're reading this, you know what you're doing. This is just my blog post. Make sure to install the <code>src</code> and <code>ports</code> sets for easy updates. <code>src</code> will be updated when you run <code>freebsd-update</code>. Ports will be managed through portsnap but having them available on firstboot never hurts. </p>

<h1> Building the system from source</h1>
<p> FreeBSD's entire base system is contained in <code>/usr/src</code>. This makes it very easy to build (and update) the entire system in oneshot. </p>

<pre>
# fetch updates, requires a reboot 
$ freebsd-update fetch && freebsd-update install

# build new kernel and userland
$ cd /usr/src 
$ mk buildworld && make installworld
$ reboot
</pre>

<h1> Package management </h1>
<p> The ports collection makes it easy to install <i>all</i> packages from source. Using <code>portmaster</code> makes it even easier. Using <code>portmaster</code> it is possible to build programs from source and install them as a package. </p>

<p> There is one caveat though, <b>don't mix packages and ports</b></p>

<pre>
# get ports
$ portsnap auto

# install portmaster
$ cd /usr/ports/ports-mgmt/portmaster
$ make && make install

# rebuild portmaster with portmaster
$ cd /usr/src
$ rehash
$ portmaster ports-mgmt/portmaster
</pre>

<p> <code>portmaster</code> supports flavors too. Example: <code>vim</code> and <code>vim-tiny</code>. Another: <code>git</code>, <code>git-lite</code>, and <code>git-tiny</code>. </p>

<pre>
# install a port
$ portmaster devel/git

# choose a flavor
$ portmaster devel/git@tiny
</pre>

<p> <code>portmaster</code> can also be used to udpate <i>all</i> installed ports. The process looks something like this: </p>

<pre>
# update ports tree
$ portsnap auto

# read changes or shoot foot here
$ less /usr/ports/UPDATING

# resolve issues here

# if no issues, rebuild portmaster
$ portmaster ports-mgmt/portmaster

# rebuild everything
$ portmaster -a
</pre>

<h1> Conclusion </h1>
<p> It really is that simple, or at least more simple than gentoo. There is some performance advantage to compiling all of your programs from source, especially on older or obscure hardware. Overall, I think I'll stick with FreeBSD. All the advantages of a source based Linux distribution without all the headache. </p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
