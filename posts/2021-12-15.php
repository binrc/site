<?php include "../includes/header.html"; ?>
<title>Drawterm build script</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>Ports</h1>
<p> The <code>drawterm</code> in the ports tree is old and <i>does not</i> work with 9front. The new <code>drawterm</code> has better crypto and <i>does</i> work with 9front. Eventually I will try to port this. But for now, a shell script is fine. </p>

<pre>
#!/bin/sh
# DEPS: git, gcc

# fetch sources
git clone git://git.9front.org/plan9front/drawterm
cd drawterm

# determine GCC version
ccs="gcc,gcc7,gcc8,gcc9,gcc10,gcc11,gcc12"
len=$( echo $ccs | awk -F, '{print NF}' )
i=1;

while [ $i -le $len ]; do
	tmp=$(echo $ccs | cut -d , -f $i)
	ret=$(which $tmp)
	if [ $? -eq 0 ]; then
		cc=$tmp
	fi;
	i=$(( i + 1 ))
done

# copy .default to Make.unix if it exist
# prevents sed nonsense
if [ -e Make.unix.default ]; then
	cp Make.unix.default Make.unix
fi

# set compiler in makefile
sed -i ".default" "s/CC=gcc/CC=$cc/g" Make.unix

# build
CONF=unix make

# install
mkdir -p /opt/bin
cp ./drawterm /opt/bin
chmod a+x /opt/bin/drawterm

# exit
echo 
echo 'MESSAGES: '
echo "==============================================================="
echo 'add /opt/bin to your $PATH variable'
echo "for csh, do" '"echo setenv PATH {$PATH}:/opt/bin &gt;&gt; ~/.cshrc"' ""
echo
</pre>

</div>
</div>
<?php include "../includes/footer.html"; ?>
