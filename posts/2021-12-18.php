<?php include "../includes/header.html"; ?>
<title>cgit on OpenBSD</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>Installation, set up cache, etc</h1>
<p> It's best to put cgit on it's own subdomain. I chose <code>git.example.com</code>. </p>

<pre>
pkg_add cgit
mkdir /var/www/cgit/cache
chown www:www /var/www/cgit/cache
rcctl enable slowcgi
rcctl enable fastcgi
rcctl enable httpd
rcctl start httpd
echo "" &gt; /var/www/conf/cgit.footer
</pre>

<h1> Configuration </h1>
<p> Cgit's conf is at <code>/var/www/conf/cgitrc</code>. Here is an example. </p>

<pre>
footer=/conf/cgit.footer

# Enable caching of up to 1000 output entries
cache-size=1000

cache-root=/cgit/cache

# Specify some default clone urls using macro expansion
clone-url=git://git.example.com/$CGIT_REPO_URL

# Specify the css url
css=/cgit.css

# Show owner on index page
enable-index-owner=0

# Allow http transport git clone
enable-http-clone=0

# Show extra links for each repository on the index page
enable-index-links=0

# Enable ASCII art commit history graph on the log pages
enable-commit-graph=1

# Show number of affected files per commit on the log pages
enable-log-filecount=1

# Show number of added/removed lines per commit on the log pages
enable-log-linecount=1

# Sort branches by date
branch-sort=age

# Add a cgit favicon
favicon=/favicon.ico

# Enable statistics per week, month and quarter
max-stats=quarter

# Set the title and heading of the repository index page
root-title=example.com repositories

# Set a subheading for the repository index page
root-desc=

# Allow download of tar.gz, tar.bz2 and zip-files
snapshots=tar.gz

## List of common mimetypes
mimetype.gif=image/gif
mimetype.html=text/html
mimetype.jpg=image/jpeg
mimetype.jpeg=image/jpeg
mimetype.pdf=application/pdf
mimetype.png=image/png
mimetype.svg=image/svg+xml

## Search for these files in the root of the default branch of repositories
## for coming up with the about page:
readme=:README.md

virtual-root=/

scan-path=/htdocs/src

# Disable adhoc downloads of this repo
repo.snapshots=0

# Disable line-counts for this repo
repo.enable-log-linecount=0

# Restrict the max statistics period for this repo
repo.max-stats=month
</pre>

<p> And here is my <code>/etc/httpd.conf</code>. Add your ssl redirects here. </p>

<pre>
server "git.example.com" {
	listen on * port 80

	location "/cgit.*" {
		root "/cgit"
		no fastcgi
	}

	# cgi setup
	root "/cgi-bin/cgit.cgi"
	fastcgi socket "/run/slowcgi.sock"
}
</pre>

<h1> Adding repos </h1>
<p> The search path is set to <code>/src</code>, where <code>/</code> is the document root for httpd. Cgit searches recursively for git repositories here. Create a directory for each user, give them perms, etc. </p>

<h1> Push/Pull </h1>
<p> not yet </p>




</div>
</div>
<?php include "../includes/footer.html"; ?>
