<?php include "../includes/header.html"; ?>
<title>Building an OpenBSD Workstation</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>Getting an Install image</h1>
<p> OpenBSD might feel strange to Linux users. Instead of a oneshot installer image that works on a CD, DVD, or USB flash drive, OpenBSD provides many options. If you are installing from a USB Flash drive, choose the <code>install70.img</code> or <code>miniroot70.img</code> images. If installing from a CD or DVD, choose the <code>install70.iso</code> or the <code>cd70.iso</code> images. There is also a floppy disk option but I'm not sure if anyone has a floppy drive on their workstation. Visit <a href="https://www.openbsd.org/faq/faq4.html#Download">the OpenBSD downloads page</a> to get an image. </p>

<p> Choosing an image with or without the sets is your choice. In OpenBSD, <i>sets</i> are a preconfigured set of base applications. I prefer the image that includes the sets but only because I like having it around. </p>

<p> Additionally, there are many architecture options. If you know what you're doing, skip ahead. If you're a complete noob, you probably want an <code>amd64</code> image. This will probably work if your computer was manufactured within the last two decades. If your computer has an <i>Intel</i> or <i>AMD</i> sticker, do a quick web search for that CPU to figure out if it's i386 or amd64. If you're still not sure, run a <code>uname -m</code>. You might see <i>x86_64</i> this is just an alias for <i>amd64</i>. If you are a windows user, look <a href="https://lmddgtfy.net/?q=windows%20get%20CPU%20architecture">here</a></p>

<h1> Installation </h1>
<p> Boot the installation image. Typically this involves enabling USB booting in your BIOS. </p>


<p> When everything loads, there is a simple menu. </p>

<pre>
...
root on rd0a swap on rd0b dump on rd0b
erase ^?, werase ^W, kill ^U, intr ^C, status ^T

Welcome to the OpenBSD/amd64 7.0 installation program.
(I)nstall, (U)pgrade, (A)utoinstall or (S)hell?
</pre>

<p> If you want a normal installation, press <code>i [return]</code> and skip ahead. If you want full disk encryption, press <code> s [return] </code> and keep reading. </p>

<h1> Disk Encryption </h1>
<p> Once dropped to a shell, we can continue with installation on full disk encryption.</p>

<p> If OpenBSD cannot detect your internal hard drive, try messing around with the RAID settings in your BIOS. You can verify that your hard drive is detected by running the <code>sysctl hw.disknames</code>. You can then run <code>dmesg | grep $diskname</code> to determine which disk is which. Look for the manufacturer's name or <code>ATA</code> vs <code>USB DISK 2.0</code> or <code>DVD-ROM</code>to distinguish between your internal hard drive and your boot medium. My hard drive is at <code>sd0</code> so it will be used as the example. </p>

<p> Begin by making device nodes. </p>
<pre>
# cd /dev && sh MAKEDEV sd0
</pre>

<p> Then proceed to overwrite the disk with random data. </p>
<pre>
# dd if=/dev/urandom of=/dev/rsd0c bs=1m
</pre>

<p> Overwriting your hard drive with random data is not necessary for full disk encryption but it makes it harder for an attacker to determine how full the disk is. It also will prevent an attacker from recovering any old data that was previously stored on your hard drive. Overwriting the entire disk with random data will take some time. </p>

<p> After the random wipe is complete, it's time to create a bootrecord. You only need one, not both. I prefer MBR. </p>

<p> If your BIOS has some option about <code>Secure Boot</code> or <code>Trusted Boot</code>, you have a UEFI system. If you still want to use MBR, you can enable the <code>Legacy Boot</code> option. You can also just accept the inevitable by using GPT and pray that your PC manufacturer isn't forcing users into Microsoft branded cages. </p>

<p> For MBR: </p>
<pre>
# fdisk -iy sd0
</pre>

<p> For GPT and UEFI: </p>
<pre>
# fdisk -iy -g -b 960 sd0
</pre>

<p> After creating the partition table, it's time to create the partition layout. </p>
<pre>
# disklabel -E sd0
Label editor (enter '?' for help at any prompt)
sd0> a a			
offset: [64]
size: [39825135] *
FS type: [4.2BSD] RAID
sd0*> w
sd0> q
No label changes.
</pre>

<p> Now enable encryption. Be sure to choose a strong password and type it carefully. Also, be sure to pay attention to the line that contains <code>CRYPTO volume attached as sdX</code>. This is the encrypted partition we wannt to install OpenBSD to. </p>
<pre>
# bioctl -c C -l sd0a softraid0
New passphrase:
Re-type passphrase:
sd2 at scsibus2 targ 1 lun 0: <OPENBSD, SR CRYPTO, 006>
sd2: 19445MB, 512 bytes/sector, 39824607 sectors
softraid0: CRYPTO volume attached as sd2
</pre>

<p> Make device nodes again </p>
<pre>
# cd /dev && sh MAKEDEV sd2
# dd if=/dev/zero of=/dev/rsd2c bs=1m count=1
</pre>

<p> Now, return to the standard installation </p>
<pre>
# exit
</pre>

<h1> Using the Installer </h1>
<p> the OpenBSD installer is very simple. Spamming the <code>[return]</code> key repeatedly would result in a fully functional system but maybe not the ideal system for a workstation. </p>

<p> The first things the installer wants are a keyboard layout and a hostname. These are self explanatory. If you are unsure you can usually just choose the defaults. After that, the installer wants to set up networking. Ethernet is almost always required since the kernel modules for most WiFi cards are not distributed with the installer. The ethernet interface is typically called <code>em0</code>. After the first boot, kmods for WiFi cards will be automatically downloaded and installed with the fw_update(1) program. </p>

<p> Now, there is some real system configuration. Select a strong password for the root account. I run sshd on all of my machines so I want it enabled by default. Since this is a workstation, we'll want xenodm started by default also. </p>

<p> The complicated part of disks comes next. Pay attention. If you set up full disk encryption, use the disk name shown at <code>CRYPTO volume attached as sdX</code> after running bioctl. If full disk encryption is not used, sd0 is probably the disk you want but double check. Pressing <code>'?' for details</code> prevents foot shooting. </p>
<pre>
Available disks are: sd0 sd1 sd2.
Which disk is the root disk? ('?' for details) [sd0] sd2
</pre>

<p> The disk partitioning step is easier. Typically I use the <code>(W)hole</code> disk with the <code>(A)uto layout</code>. If you decided to try your luck with UEFI, you should choose <code>whole disk (G)PT</code> and <code>(A)uto layout</code>. 

<p> After partitioning the disk, the sets are installed. This step quickly becomes confusing so I'll be explicit: this is why I use the image that comes with the sets. </p>
<pre>Location of sets? (cd0 disk http or 'done') [cd0] disk
Is the disk partition already mounted? [no] no
Which disk contains the install media? (or 'done') [sd0] sd1
Available sd1 partitions are: a i.
Which sd1 partition has the install sets? (or 'done') [a] a
Pathname to the sets? (or 'done') [7.0/amd64]
</pre>

<p> Now that the sets are found, we can select which ones we want. If you're completely new you should select all of them. If you see a checksum error and you're installing the sets from a local disk, you should be fine.</p> 
<pre>
Directory does not contain SHA256.sig. Continue without verification? [no] yes
</pre>

<p> The sets will install and eventually we will be prompted to select a timezone. Once everything is done, it's safe to <code>reboot</code> and remove the install medium. If you set up full disk encryption you will be prompted to enter a password on first boot. </p>

<h1> WiFi Setup </h1>
<p> Typically, I perform this part as root. Once xenodm starts you can log in. The window manager that starts is called FVWM. It's ugly as all hell but it works. </p> 

<p> Locate the ethernet device </p>
<pre>
# ifconfig 
</pre>

<p> Submit a DHCP request </p>
<pre>
# dhclient em0
</pre>

<p> Get WiFi kmods </p>
<pre>
# fw_update
</pre>

<p> Connect to a network </p>
<pre>
# ifconfig iwn0 up
# ifconfig iwn0 scan
# ifconfig iwn0 nwid "NETWORK_SSID" wpakey "NETWORK_PASSWORD"
# dnclient iwn0
</pre>

<p> If it worked, you should see a line that says something like <code>iwn0: 192.168.0.x lease accepted . . . </code>. Now, we can create a permanent connection that will automatically start at boot and restart if dropped. </p>

<p> Set up OpenBSD to automatically connect to a WiFi network </p>
<p> Edit <code>/etc/hostname.iwn0</code> </p>
<pre>
join "NETWORK_SSID" wpakey "NETWORK_PASSPHRASE"
dhcp
inet6 autoconf
up
</pre>

<p> Now unplug the eth cable and test the configuration </p>
<pre>
# ifconfig em0 down
# ifconfig iwn0 down
# pkill dhclient
# sh /etc/netstart 
# ping -c 5 example.com 
</pre>

<h1> Privilege Escalation </h1>
<p> OpenBSD uses <code>doas(1)</code> instead of <code>sudo</code> for privilege escalation. It's much faster to configure. </p>

<p> Add your user to the <code>wheel</code> and <code>staff</code> groups, create <code>/etc/doas.conf</code> </p>
<pre>
# usermod -G wheel $your_username
# usermod -G staff $your_username
# cp /etc/examples/doas.conf /etc/
</pre>

<h1> Increasing Resource Limits </h1>
<p> If you want to run a web browser, you must modify <code>/etc/login.conf</code> or it will be killed for being a resource hog. Mine looks like this: </p>
<pre>
staff:\
        :datasize-cur=1024M:\
        :datasize-max=infinity:\
        :maxproc-max=1024:\
        :maxproc-cur=512:\
        :openfiles-cur=4096:\
        :openfiles-max=8192:\
        :stacksize-cur=32M:\
        :ignorenologin:\
        :requirehome@:\
	:tc=default:
</pre>

<h1> X11 Setup </h1>
<p> The default Xsession on OpenBSD is FVWM. It's ugly but it works. Using <code>pkg_add(1)</code>, you can install KDE, GNOME, or Xfce. I prefer dwm so I'll be installing that. I perform this part as a normal user so that all of my user-specific configurations aren't done as root. </p>

<p> If you can't figure out how to build from source: </p>
<pre>
$ doas pkg_add dwm st slock 
</pre>

<p> If you are the UNIXMENSCH: </p>
<pre>
$ doas pkg_add git xautolock
$ mkdir -p ~/src/suckless && cd ~/src/suckless
$ git clone https://git.suckless.org/dwm
$ git clone https://git.suckless.org/dmenu
$ git clone https://git.suckless.org/st
$ git clone https://git.suckless.org/slock
$ git clone https://git.suckless.org/slstatus
</pre>

<p> Each of these programs has a <code>config.mk</code> file. Edit each one so that it builds on OpenBSD. The file documents itself. Editing the <code>config.h</code> file is how you configure suckless tools. It's a typical <code> make && doas make install</code>. See <a href="https://suckless.org/">suckless.org</a> for tips on using and configuring suckless software. </p>

<p> Now, edit <code>~/.xsession</code>. Xenodm reads this file and runs it like a shell script to start the desktop session. </p>
<pre>
# set lang
export LANG=en_US.UTF-8

# set env
. $HOME/.profile

# x11 shit
xrdb -merge $HOME/.Xresources

# set bg color
xsetroot -solid black

# or, set wallpaper
# feh --bg-fill ~/.pape.png

# start
xautolock -time 10 -locker slock &
slstatus &
exec dwm 
</pre>

<h1> Making Xenodm less of an eyesore </h1>
<p> I like to remove the console log. I also like to disable the system beep. They are annoying. And change the background color to something less of an eye strain. Edit <code>/etc/X11/xenodm/Xsetup_0</code></p>

<p> Find the lines that look like this and comment them out with a <code>#</code>: </p>
<pre>
${exec_prefix}/bin/xsetroot -fg \#6f6f6f -bg \#bfbfbf -bitmap ${prefix}/include/X11/bitmaps/root_weave

${exec_prefix}/bin/xconsole -geometry 480x130-0-0 -daemon -notify -verbose -fn fixed -exitOnFail
</pre>

<p> also, append this: </p>
<pre>
${exec_prefix}/bin/xsetroot -solid black 
</pre>

<p> Now, edit <code>/etc/X11/xenodm/Xresources</code> to make the login prompt less ugly. The spleen font will need to be installed first. And it's never a bad idea to keep a backup of the original file. </p>
<pre>
$ doas pkg_add spleen
$ doas cp /etc/X11/xenodm/Xresources /etc/X11/xenodm/.Xresources.bak
$ chmod u+w /etc/X11/xenodm/xresources
</pre>

<pre>
! $OpenBSD: Xresources.in,v 1.3 2020/06/28 15:38:34 matthieu Exp $
!
!
!
!
!
xlogin*greeting: CLIENTHOST
xlogin*namePrompt: \040User:\040
xlogin*passwdPrompt: Passwd:\040
xlogin*fail: Login incorrect or forbidden

xlogin.Login.height:        500
xlogin.Login.width:        500
xlogin.Login.y:            0
xlogin.Login.frameWidth:    0
xlogin.Login.innerFramesWidth:    0

xlogin.Login.background: black
xlogin.Login.foreground: white
xlogin.Login.failcolor: white
xlogin.Login.inpColor: black
xlogin.Login.promptColor: white
xlogin*greetColor: white

xlogin*greetFace: spleen-24

xlogin.Login.face: spleen-24
xlogin.Login.failFace: spleen-24
xlogin.Login.promptFace: spleen-24
</pre>

<h1> Updating the system </h1>
<p> Before using the system, it's wise to update it to the latest version. OpenBSD ships it's base system separately from the packages. This might seem alien to the Linux user but it's actually a good idea. Separating the components required to boot and use the from tacked on packages prevents dependency hell. It also prevents a packaging conflict from removing our kernel or init system. </p>

<p> To update the base system: </p>
<pre>
$ doas syspatch
$ doas reboot
</pre>

<p> To update packages: </p>
<pre>
$ doas pkg_add -u 
</pre>

<p> To update firmware: </p>
<pre>
$ doas fw_update
</pre>

<h1> That's it </h1>
<p> The OpenBSD installation process is very easy. Without full disk encryption, the entire system can be installed in just a few keystrokes. Out of the box, OpenBSD provides an ugly, albeit highly functional environment. </p>

<p> If you need more help, try: </p>
<pre>
$ man man
$ man intro
$ man help
$ man builtins
$ man $(echo $SHELL | awk -F\/ '{print $NF}')
</pre>

<p> see also, the <a href="https://www.openbsd.org/faq/index.html">OpenBSD FAQ</a></p>





</div>
</div>
<?php include "../includes/footer.html"; ?>
