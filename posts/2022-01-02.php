<?php include "../includes/header.html"; ?>
<title>Gemini Server on OpenBSD</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>Install Packages</h1>
<pre>
# pkg_add vger
# echo "# it works" &gt;&gt; /var/gemini/index.gmi
</pre>

<h1> Configure inetd </h1>
<p> <code>inetd(8)</code> is a <i>super server</i>. It works by listening for incoming connections and launches a specific program when it receives one. Since no one actually uses gemini, this approach to services is better. Instead of running constantly, the gemini server only runs when someone sends it a packet. </p> 

<pre>
127.0.0.1:11965 stream tcp nowait _vger /usr/local/bin/vger vger
</pre>

<h1> Ronfigure relayd </h1>
<p> <code>relayd(8)</code> is proxying service. In this example, I've set it up to proxy requests on <code>:1965</code> to <code>:11965</code>. The <code>tls keypair</code> line is important because tls is a mandatory component of the gemini protocol. </p>

<pre>
log connection
tcp protocol "gemini" {
        tls keypair "example.com:1965"
}

relay "genimi" {
        listen on example.com port 1965 tls
        protocol "gemini"
        forward to 127.0.0.1 port 11965
}
</pre>

<h1> Get Certs </h1>
<p> Since I'm also running httpd, I just symlinked my existing ssl certs. The name <b>is</b> important here. After much fiddling around, I read the documentation and relalized that the name of the cert and key <b>must</b> be identical to <i>hostname:port.{key,crt}</i>. </p>
<pre>
# ln -s /etc/ssl/example.com.fullchain.pem /etc/ssl/example.com\:1965.crt
# ln -s /etc/ssl/private/example.com.key /etc/ssl/private/example.com\:1965.key
</pre>

<h1> Configure pf </h1>
<p> <code>pf(4)</code> is the firewall on OpenBSD. Thsis is a fairly basic configuration. Skip on the loopback device, use a macro to list services, block all incoming traffic unless it's in the list, pass all traffic out. The last line is was included in <code>/etc/examples/pf.conf</code> so I thought it was worthwhile to keep it. </p>

<pre>
set skip on lo
tcp_services="{ssh, http, https, 1965}"

block in all

pass in proto tcp to any port $tcp_services keep state
pass out all

block return in on ! lo0 proto tcp to port 6000:6010
</pre>

<h1> Smoke Test </h1>
<pre>
# pfctl -f /etc/pf.conf
# rcctl enable inetd relayd
# rcctl start inetd relayd
</pre>

<p> test if it works </p>
<pre>
# pkg_add bombadillo
# bombadillo gemini://example.com
</pre>

<p> Eventually, I will write a script that converts html to the gemini markdown format. I will likely set it up as a cron job so that I can serve this website over gemini and never have to touch it again. </p>

<h1> Gemini Markdown cheatsheet </h1>
<p> The 2 people that use genimi call this <i>gemtext</i>

<h1> Gemini Markdown cheatsheet </h1>
<p> The 2 people that use genimi call this <i>gemtext</i>. I simply call it <i>gemini markdown</i> because it's a very bare bones markdown format. </p>

<pre>
# Heading 1
## Heading 2
### Heading 3

=&gt;	http://example.com	a link to a website

=&gt;	gemini://example.com	a link to a gemini server

=&gt;	gopher://example.com	a link to a gopher server

* list
* items
* like
* this

&gt; blockquote

paragraphs are just plain text like this
</pre>


</div>
</div>
<?php include "../includes/footer.html"; ?>

