<?php include "../includes/header.html"; ?>
<title>Gopher server on OpenBSD</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1> Install Deps </h1>
<pre>
# pkg_add gophernicus bombadillo
</pre>

<h1> Read the manual </h1>
<pre>
# cat /usr/local/share/doc/pkg-readmes/gophernicus | less
</pre>
<p> the manual instructs the sysadmin to run gophernicus via inetd. </p>

<h1> Edit configs </h1>
<p> the manual is very useful, it instructs you where to put the confs. </p>

<p> Edit <code>/etc/inded.conf</code></p>
<pre>
gopher stream tcp nowait _gophernicus /usr/local/libexec/in.gophernicus in.cophernicus -h "example.com" -nm -nu -nx
</pre>

<h1> Modify pf </h1>
<pre>
set skip on lo
tcp_services="{ssh, http, https, 1965, gopher}"
udp_services="{gopher}"

block in all

pass in proto tcp to any port $tcp_services keep state
pass in proto udp to any port $udp_services keep state
pass out all

block return in on ! lo0 proto tcp to port 6000:6010
</pre>

<pre>
# pfctl -f /etc/pf.conf
# rcctl enable inetd 
# rcctl start inetd 
</pre>

<h1> Smoke test </h1>
<pre>
# bombadillo gopher://example.com
</pre>

<h1> Adding content </h1>
<p> If you read the manual, it helps you figure out valid syntax for a gophermap. </p>

<pre> cat /usr/local/share/doc/gophernicus/README.Gophermap | less </pre>

<p> An excerpt: </p>
<pre>
Valid filetypes include: 
	0	text file
	1 	directory
	3	error message
	5	archive file (zip, tar etc)
	7	search query
	8	telnet session
	9	binary file
	g	GIF image
	h	HTML file
	i	info text
	I	generic image file (other than GIF)
	d	document file (ps, pdf, etc)
	s	sound file
	;	video file
	c	calendar file
	M	MIME file (mbox, emails, etc)

Additional type characters supported by Gophernicus: 
	#		comment - rest of the line is ingored
	!title		menu title (use on the first line)
	:ext-type	change filetype (for this directory only)
	~		include a list of users with valid ~/public_gopher
	%		include a list of available virtual hosts
	=mapfile	include or execute other gophermap
	* 		stop processing gophermap, include file listing
	.		stop processing gophermap (default)

Examples of valid resource lines: 

1subdir
1Relative internal link 	subdir
1Absolute internal link 	/subdir
1External link 			/	gopher.floodgap.com	70
1External relative link 	subdir/ gopher.domain.tld	70
0Finger-to-gopher link 		user 	example.com 		79
hLink to a website 		URL:http://example.com

hLink to a local HTML page	/path/to/file.html
5Link to a tar archive 		/path/to/archive.tar.gz
9LInk to a binary file		/path/to/binary
7Search engine query		/query

8Telnet session		user	example.com	79
</pre>



<h1> SSL </h1>
<p> no </p>
</div>
</div>
<?php include "../includes/footer.html"; ?>

