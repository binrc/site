<?php include "../includes/header.html"; ?>
<title>When it's time to buy a new computer (and the operating cost of old computers). </title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1> Evaluate your use case</h1>
<p> What do you use your computer for? I use mine mainly for programming and as a terminal for accessing remote servers. Many people primarily use their computer for web browsing and email. Others, gaming almost exclusively. In order to determine what type of computer you actually need, you first need to determine your use case. I will elaborate on each of these use cases in order of least to most demanding in order to see where you fit in. </p>

<h3> Bantamweight computing </h3>
<p> This is truly the most brutalist form of computing. Anything other than strictly predefined tasks will be difficult or impossible. Many people who do this type of computing do it as a hobby. Very few people seriously use a raspberry pi as their main computer. Although entirely viable, many "normal" people do not have the knowledge required to assemble and use a minimum viable operating system. </p>

<p> The cost of a computer for this use case ranges anywhere from $45 to $80. </p>

<h3> Lightweight computing </h3>
<p> The lightest pragmatic use case is my use case. I interact with my computer almost exclusively through a command line. I rarely use a web browser, rarely play any games, and rarely do anything demanding. The minimum specs I need are: </p>

<ul>
	<li> 250 gigabytes of storage </li> 
	<li> 4 gigabytes of RAM </li>
	<li> 2 CPU cores </li>
</ul> 

<p> Although these specifications might seem excessive, it never hurts to have a little bit of head room. Anything less than 250G of storage can quickly become a problem when updating large programs. 4G of ram is just enough to run heavy programs like LibreOffice and Firefox when necessary. 2 CPU cores, although not absolutely necessary, gives the computer enough processing power to run both background processes and the user's programs without any unexpected slowdowns. </p>

<p> The cost of a computer for this use case ranges from $60 to $200. Instead of buying new, look for corporate e-waste (thinkpads and optiplexes) on ebay. </p>

<h3> Middleweight computing </h3> 
<p> This is where most people's use cases lie. They run heavy, slow software like Windows. Specifications look something like what follows: </p>

<ul>
	<li> 500-1000 gigabytes of storage </li> 
	<li> 8 gigabytes of RAM </li>
	<li> 4-8 CPU cores </li>
</ul> 

<p> These specifications are more than enough for grandma to go on facebook and more than enough to run all of the javascript malware that lies therein. Mom should be able to send all the business emails she needs. Even light gaming will be a possible. </p>

<p> The cost of a computer in this class will range anywhere from $400-$600. Buying a model from last few generation (on ebay) will save you anywhere from 30% to 40% of what the computer would have costed new. Again, a used thinkpad or optiplex will be more than enough. Just make sure to check the specifications and message the seller if you have questions. </p> 

<h3> Heavyweight computing </h3>
<p> The average person will never need a computer this powerful. Computers this powerful are either servers, video editing machines,  or custom built for video games. If you're in this tier currently and have a server: very cool. If you're in this tier and have a PC built for video games: why? These specifications are open ended: </p> 
<ul>
	<li> 2000+ gigabytes of storage </li> 
	<li> 16+ gigabytes of RAM </li>
	<li> 8+ CPU cores </li>
</ul> 

<p> And the caveat video game specific machines: </p>

<ul> 
	<li> The newest GPU on the market</li>
</ul> 

<p> These specifications are too high for the average person. If you need a computer this powerful, you already know what you're doing. The total cost of purchasing and running a computer this powerful is usually covered by your employer.  </p> 

<p> A server will cost you anywhere from $600-$2000+ (go to <a href="https://unixsurplus.com">UNIX Surplus</a> for used machines). A computer specifically designed for video games or video editing will cost you anywhere from $1000-$4000+. </p> 


<h1> Reducing OS overhead to increase performance </h1>
<p> For most people, they can actually reduce the required specifications by replacing proprietary software with free and open source software. Windows requires 4G of ram and 64G of storage. But here's the real kicker - it's not even usable with such low specs. 

<p> To contrast: The main Ubuntu version requires 4G of ram and only 25G of disk space. Getting even lower, The Xfce version of Ubuntu only requires 2G of ram and 20G of disk space. This does not mean "it's barely usable with the minimum requirements". This means "you will actually be able to use it". </p> 

<p> Switching to Linux from Windows means that you don't have to unnecessarily spend money on a more powerful computer just to accomplish basic tasks. It will also prevent the need to upgrade every other year. Instead of constantly buying new things, users can choose to 'upgrade' their old computers by upgrading to free software. </p> 

<p> The fact is: the physical computer is not getting slower, it's the software that's running on it. Proprietary software becomes slower over time. This is an intentional business decision that coerces users into buying newer computers "because the old one is super slow now!" The issue is not the hardware, it is the software. </p>

<p> Why have a massive initial investment with a series of recurring initial investments when you can just buy a single, cheaper computer that will function well for a 5-10 years? </p> 

<p> If you're already a Linux user, consider switching to FreeBSD, OpenBSD, or NetBSD. FreeBSD can run on something with 2G of ram and 8G of disk space. OpenBSD can run on something as small as 1000M of ram and 8G of disk space. NetBSD (with a full graphical environment) can run on something so minimal that only 8M of ram and 200M of disk space are required. </p> 

<p> Take a look back up at the Operating System specifications and decide if you <i>really</i> need the latest computer of if you can simply reduce the overhead of the OS on your current computer. In short: you probably don't even need a new computer, you just need better software. </p> 


<hr>

<h1> Calculate power consumption and cost. </h1>
<p> Another thing to consider is how much it costs to run your computer. If you're still using a very old computer, you might be paying more for power than you realize. To determine the cost, we need to know 3 things: how many watts your computer draws, how many hours per day you run it, and how much you pay the power company for one kilowatt hour. </p> 

<h3> Determining watts </h3>
<p> In order to determine how many watts a desktop computer draws, you can examine the Power Supply Unit. Opening up the computer, you should see a big metal box with a sticker on it. The sticker will have power information about the PSU on it. If you don't know what to look for, see <a href="https://en.wikipedia.org/wiki/Power_supply_unit_(computer)#Appearance">This Wikipedia article with pictures of Power Supply Units. </a> </p> 

<p> To determine the draw for a laptop computer, you can look at the big brick that's in the middle of the charging cable. This will either be a sticker or part of the plastic. All the electricity enters your computer through the Power Supply Unit. This means we don't have to do any complicated measurements, just look at the information that's required to be placed onto the PSU. </p> 

<p> Below, I have provided images of some of my own PSUs. You should look for something like <b>DC OUTPUT:  250W</b> or <b>MAX: 65W</b>. I have outlined what to look for with green boxes. </p>

<img src="assets/images/desktopPSU1.png" alt="picture of a newer desktop power supply">
<img src="assets/images/desktopPSU2.png" alt="picture of an older desktop power supply">
<img src="assets/images/laptopPSU.png" alt="picture of a laptop power brick">

<p> This <b>MAXIMUM OUTPUT</b> value is likely higher than the actual measurement of electricity that the computer draws. This value represents the point at which the PSU will fail in order prevent electrical fires, not the normal power consumption. Although it's a high estimate, determining maximum possible power consumption directly leads to determine the maximum possible cost of running the computer. </p> 

<h3> Determining time powered on </h3>
<p> This value is simpler to figure out. How long do you leave your computer on for? I leave my computers running for 24 hours every day. You might only turn yours on in the evening. Before you turn your computer off, you can run the <code>uptime</code> command on Linux or BSD to see how long it's been powered on for. If you weren't convinced that you can save money by using Linux, you can check the uptime of a Windows machine by running the <code>wmic os get lastbootuptime</code> command in a Windows CMD window. </p> 

<h3> Determine the cost of power </h3>
<p> In order to simplify things, I will use the US national average cost of power per kilowatt hour. If have a bill from the power company, you can easily do the math to get an accurate price per kWh. If you are less concerned about accuracy and just want to compare average prices per country, see <a href="https://en.wikipedia.org/wiki/Electricity_pricing#Price_comparison_across_countries">the Wikipedia article about average electricity price by country</a>. 

<h3> Doing the math </h3>
<p> Now that all the values are known, we can calculate how much it costs to run our computer. I will be using the data from the first image (180 W), 24 hours per day, and the US average price per kWh ($0.15). </p> 

<p> Here are the formulas so you can follow along: </p> 

<pre>
# kilo wart hours per day
kWh = ( W * hours ) / 1000

# cost of running for one hour 
costPerHour = (watts / 1000) * pricePerKWh

# cost of for one day
costPerDay = hoursPerDay * costPerHour

# cost of running for one month
costPerMonth = 30.42 * costPerDay

# cost of running for one year
costPerYear = 12 * costPerMonth
</pre>

<p> My newest desktop draws 180 W. This means it draws 4.32 kWh per day. The cost of running it for one hour costs $0.027. For one day, it costs $0.648. For one month, $19.71. For one year, $236.550. </p> 

<p> Now, let's do the older computer, the second image. The only variable that's changed is that it draws 250 W. This computer draws 6.00 kWh per day. It costs $0.037 to run it for one hour. For one day, it costs $0.9. For one month, $27.38. For one year, $328.54. </p> 

<p> let's do the newest computer, the laptop. The only variable that's changed is that it draws 65 W. This laptop draws 1.52 kWh per day. It costs $0.009 to run it for one hour, $0.23 for one day, $7.01 for one month, and $84.11 for one year. </p> 

<h1> Analysis </h1> 
<p> Use case and power consumption are important things to consider when upgrading (or not upgrading) your computer. If you are eager to upgrade your computer, consider what you actually use it for and if switching to free and open source software can save you money. </p> 

<p> 

<p> If you are hesitant to upgrade your computer, consider the actual cost of running the it. Older hardware tends to be cheaper to purchase but not to run. My oldest computer, the 250 W one, only cost me $15. By leaving it unplugged and unused, I am saving $300+ every year. This $300 is more than enough to buy something newer. My laptop, the 65 W one, cost me somewhere around $650 to purchase but I've already reclaimed half of it's cost by <i>not</i> running either of the desktops very often. The point is: if you're spending a new computer's worth on power every month, it's time to upgrade. </p> 

<p> To summarize it all: turn off your computer when you're not using it. There is probably a setting in the BIOS that will automatically power it on at a specific time. You can run a cron job to automatically shut down the system when you're not using it. And also: </p> 

<blockquote>
Put the fun back into computing. Use Linux, Run BSD. 
</blockquote>



</div>
</div>
<?php include "../includes/footer.html"; ?>

