<?php include "../includes/header.html"; ?>
<title>So you want to switch to Linux</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1> Prerequisites </h1> 
<p> Before making the switch, you should back up all of your existing files onto an external hard drive. Installing Linux will destroy all existing data on the computer. Most of the files you care about will be in <code>C:\Users\YourUsername</code>. You can simply copy this entire folder to a thumb drive or external ssd. </p> 

<p> You also need a USB flash drive with about 8 gigabytes of storage on it. Any data stored on this USB drive will be destroyed in the process so make a backup. </p> 

<p> Considering your hardware is another important step. Luckily, we can test our hardware with the installer. Most hardware will work just fine but some GPUs (nvidia) and WiFi cards (broadcom) might cause problems, especially for new users. </p> 

<h1> Making choices </h1>
<p> We will be installing Fedora today. Fedora is my favorite Linux distribution because it's a pragmatic balance of freedom and usability. Visit <a href="https://getfedora.org/">getfedora.org</a> and take a look around. Don't download anything yet, just look. Check out <a href="https://spins.fedoraproject.org/">spins.fedoraproject.org</a> also. Linux is all about user choice. The workstation version of Fedora is the 'official' release. It comes with a desktop environment called GNOME. The spins of Fedora offer different desktop environments. A 'desktop environment' just means a 'desktop paradigm'. Each desktop environment has a slightly different paradigm. </p>

<p><b> GNOME </b> is the best supported. It feels like a hybrid between MacOS and Android. </p> 
<p><b> KDE Plasma </b> is the most customizable. It feels like something between Windows 7 and 10. </p> 
<p><b> Xfce </b> is very fast. It feels like something between Windows 95 and XP. </p> 
<p><b> LXQt </b> is basic but fast.  It feels like lighter version of KDE. It's very intuitive. </p> 
<p><b> mate-COMPIZ </b> is a customizable, intuitive choice. It feels similar to Windows but without being actively hostile towards users. </p>
<p><b> Cinnamon </b> is beautiful and familiar. It feels like Windows but looks like GNOME. </p>
<p><b> LXDE </b> is basic but fast. It's similar to LXQt but with a slightly different look. </p>

<p> The ability to make decisions might seem overwhelming at first. After you get used to it, it feels liberating. If you still can't decide, I recommend using the workstation version with GNOME. It's well polished and tested. It comes prepackaged with many of the utilities you need. Even if GNOME's paradigm seems strange at first, you will quickly begin to appreciate how easy it is. It's distraction free and 'stays out of your way'. </p> 

<p> Don't worry too much about this choice. You can install multiple desktop environments at the same time and select which one you want to use when you log in to the computer. </p> 

<h1> Getting an installer </h1> 
<p> If you paid for Linux, you got scammed. Linux is free as in freedom and free as in free beer. Unlike Windows, you don't have to pay for Linux. This is because the people who develop Linux understand that sharing is a virtue. Fedora is free and easy to install. </p> 

<p> In the old days, we had to use tools like Rufus to make a Linux installer. Now, we can use the <a href="https://getfedora.org/en/workstation/download/">Fedora Media Writer</a> tool. This will automatically turn our USB drive into an installer. </p> 

<p> After downloading and installing the media writer tool, we can run it. It will look like this: </p> 

<img src="assets/images/mediawriter1.png" alt="Fedora Media Writer">

<p> If you click the three dots, you can choose to install one of the spins. I will be installing the Fedora Workstation version today. After clicking on the "Fedora 35 Workstation" button, we will see another button that says "Create Live USB". In Linux, sometimes people call the installer a "Live USB" because the installer is actually a live demo of the thing you're about to install. </p> 

<img src="assets/images/mediawriter2.png" alt="Fedora media writer image selection screen" >

<p> After clicking the "Create Live USB" button, the download begins. </p> 

<img src="assets/images/mediawriter3.png" alt="Fedora media writer downloading an image">

<p> Before clicking "Write to Disk", verify that the <i>only</i> the USB Flash drive plugged into your computer is the one you want to turn into an installer. This process will destroy any data that is stored on the USB drive. It's easier to remove everything we don't want to destroy than take a gamble and hope we selected the correct USB drive. You might need to enter an administrator password at this step. </p>

<img src="assets/images/mediawriter4.png" alt="fedora media writer creating an installer">

<p> The media writer tool will now create an installer. This will take a few minutes. After the installer is successfully created, you can safely remove the USB drive. I recommend <i>not</i> deleting the downloaded installer image just in case there is an issue with your thumb drive and you need to re-create the installer. </p> 

<img src="assets/images/mediawriter5.png" alt="fedora media writer is finished">

<h1> Rebooting </h1> 
<p> After we have an Live USB Installer, we need to modify some settings in our BIOS. The BIOS is the lowest level of settings for our computer. The BIOS is completely agnostic to whatever OS is running. </p> 

<p> How you enter the BIOS will depend on your PC's manufacturer. Typically, you will mash the <code>F2</code>, <code>F8</code>, <code>F10</code> or <code>F12</code> keys. It does not hurt to try all of them and pressing them repeatedly will not cause anything to break. On older computers, the BIOS looks like a blue and gray text based menu. On newer computers, it might be a graphical menu. </p> 

<p> Inside of the BIOS, you should look for a setting that says something like "USB BOOT" and enable it. You should also look for the "boot order" menu and move USB to the top of the list. </p> 

<p> Now you should be able to insert the USB installer and reboot. If everything worked, you should see a desktop eventually pop up. </p> 

<p> Sometimes, enabling USB booting is not enough. If you cannot get it to boot, try disabling the "secure boot" or "trusted boot" options in the BIOS. Disabling these will not actually make you any less secure. "Secure boot" is a misnomer. All it means is that someone paid Microsoft to endorse the Operating System you're trying to boot. </p> 

<h1> Testing Linux </h1> 
<p> Before you click install, you should take Linux for a test drive. Click <i>Try Fedora</i> for a live demo. </p> 

<img src="assets/images/fedorainstaller1.png" alt="first thing you see when the installer boots">

<p> Here are some things you should test: </p> 
<ul>
<li> Does my WiFi work? </li>
<li> Is there any screen garbling? </li>
<li> Can Linux see all of my hardware? </li>
<li> Do the mouse and keyboard work? </li>
<li> Do my speakers work? </li>
</ul> 

<p> In order to open the 'start menu', you can click the "Activities" button in the top left corner. Open the Settings program and scroll through it. This program contains everything you need to test your speakers, WiFi, keyboard, etc. It might be beneficial to adjust your screen resolution and keyboard layout here too. To see if Linux is utilizing all of your computer's resources, you can go to the About menu. </p> 

<img src="assets/images/fedorainstaller2.png" alt="fedora system settings">

<p> Does everything work? Time to install. </p> 

<h1> Installation </h1> 
<p> To install Fedora, launch the "Install to Hard Drive" program. This program will walk you through all the required steps. </p> 

<p> The first thing to do is choose a language, then click "continue". </p> 
<img src="assets/images/fedorainstaller3.png" alt="language seleciton screen">

<p> After choosing a language, we have three different menus to go through. </p> 
<img src="assets/images/fedorainstaller4.png" alt="menus to go through">

<p> The keyboard layout menu is simple, click the + button to add a layout, then rearrange the layouts in order of preference. The one you place at the top of the list will be the default. </p>
<img src="assets/images/fedorainstaller5.png" alt="keyboard layout selection screen">

<p> Next, we will choose a timezone. You can click approximately where you live on the map. I recommend leaving Network Time enabled so that your computer will automatically correct it's clock. </p> 
<img src="assets/images/fedorainstaller6.png" alt="timezone selection screen">

<p> The next step is to configure your hard drive. Although I don't like the way Fedora automatically partitions the drive, it's fine for most people. Make sure to select the "make additional space" button. </p> 
<img src="assets/images/fedorainstaller7.png" alt="hard drive configuration screen">

<p> When you click 'done', you will be presented with a menu. Click 'delete all' then 'reclaim space'. You might also want to click "Encrypt my data" and be asked to enter a password. Encryption is useful because it means that even if someone steals your laptop, they cannot get your data. </p> 

<p> If you are having difficulties here, try the 'custom' option. It's more aggressive and can punch through some of the quirks that the automatic partitioning cannot.</p>
<img src="assets/images/fedorainstaller8.png" alt="making additional space">

<p> Now you can click 'Begin installation'. When it's done, you can click 'Finish installation' then restart the computer. The menu in the top left corner has a "Power/log of" sub menu with a restart option. When the computer is completely powered off, you can remove the USB flash drive. </p> 
<img src="assets/images/fedorainstaller9.png" alt="it's installing">

<h1> Post installation </h1>
<p> On the first boot, you will have some basic setup to do. I recommend enabling "Third party repositories" when you are presented with the option. </p> 
<img src="assets/images/fedorainstaller10.png" alt="enabling third party repositories">

<p> You will also be creating a user account on the new installation. This is fairly simple. Pick a username, pick a password. </p> 
<img src="assets/images/fedorainstaller11.png" alt="setting a username">
<img src="assets/images/fedorainstaller12.png" alt="setting a password">

<p> You will then be presented with the option to take a tour of Fedora. I recommend this because it explains how to use GNOME. </p> 
<img src="assets/images/fedorainstaller13.png" alt="GNOME tour">

<h1> Tweaking Fedora, installing software </h1> 
<p> The "software" program makes it easy to install programs. You usually have to enter your password to install or remove software. Let's search for and install gnome-tweaks. </p> 
<img src="assets/images/fedoratweaks1.png" alt="GNOME software center">
<img src="assets/images/fedoratweaks2.png" alt="gnome-tweaks program description">

<p> Gnome-tweaks is a tool we can use to change the way GNOME looks. Under the Appearance menu, you can enable dark mode. Under the Window Titlebars menu, you can enable the minimize and maximize buttons you might be familiar with. </p> 

<img src="assets/images/fedoratweaks3.png" alt="setting normal window buttons with gnome-tweaks">

<h1> What's next? </h1> 
<p> You've done it, you've installed Linux. Have fun and explore. The 'Help' program will help you if you get stuck</p> 

<p> Everything in the software center is free to download and run. You now have the freedom to choose. Don't like Firefox? There are 20 other web browsers you can run. Don't like LibreOffice? Try OnlyOffice or Calligra. Don't like GNOME? Try to install the "KDE (K Desktop Environment)" group with dnf. </p> 

<p> After you've exhausted all of the graphical programs, it's time to try learning how to use the terminal. You now have absolute control over your computer and you are finally free. </p> 

</div>
</div>
<?php include "../includes/footer.html"; ?>

