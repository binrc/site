<?php include "../includes/header.html"; ?>
<title>Tips on succeeding for new Linux users</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<p> Very frequently, I see new Linux users try Linux only to give up a few weeks or months later. I hope to provide them with a correct approach and a general syllabus for becoming the UNIXMENSCH. The goal is to encourage the correct approach to Linux, then encourage the reader to learn how to teach themselves while learning Linux. At the end of the road, we realize that Linux was method and not the aim. Linux is the method for achieving autodidactism, any skills acquired are secondary because we have learned the most valuable skill: how to teach ourselves. </p> 

<h1> Debunking misconceptions </h1> 
<p> There are many statements about Linux floating around. Some are objectively true, some are exaggerated. In order to succeed, you must have the right mindset. Correctly setting your expectations will reduce overall frustration when something doesn't work the way you expect it to. This is a non-inclusive, non-exhaustive list but it covers the claims I see most frequently. </p>

<h3> <q> Linux is a drop in replacement for Windows </q> </h3> 
<p> This is a common phrase amongst those with the burning zeal of Linux enlightenment. Although this statement can be true in some circumstances, it's not true in others. You should not expect Linux to be a 100% bug-for-bug compatible replacement for Windows. Linux is an entirely different operating system. You should expect it to behave differently. </p> 

<h3> <q> Linux can run any and all programs </q> </h3> 
<p> To add on to the 'drop in replacement' statement: Linux cannot run <i>every</i> program. Windows cannot run <i> every</i> program. MacOS cannot run <i>every</i> program. Under the hood, these operating systems are completely different. It's just not possible even with compatibility layers. Linux can run <i>many</i> programs, just not <i>all</i> of them.</p> 

<h3> <q> Linux is good for gaming </q> </h3> 
<p> There are many Linux native games that are very fun. With the rise of Proton, 50% of the top 10 Steam games run on Linux. 80% of the top 100 Steam games run on Linux. 77% of the top 1000 Steam games run on Linux. Gaming on Linux is entirely viable but still, Linux is not Windows. If Linux cannot run <i>every</i> program, why would you expect a game (aka a program) to be any different?</p> 

<h3> <q> Linux is for servers only </q> </h3> 
<p> UNIX and Linux operating systems were designed for mainframe computers. A mainframe system is designed to be flexible and modular. Although Linux is most commonly associated with servers, the modular design enables it to effectively serve any purpose from cell phones to super computers. Even on a desktop, Linux is a workhorse. Linux does exactly what you tell it and stays out of your way. There are no forced updates, no ads in the start menu, no kernel level keyloggers, no spyware that automatically reports hashes of all the images you download to Microsoft and the NSA. Opinions aside, Linux is objectively better for <i>just getting shit done</i> because it was designed for <i>just getting shit done</i>. </p> 

<h3> <q> Certain distros are better than others </q> </h3> 
<p> Distro choice is arbitrary. Many newer users choose distros like football teams. Once you gain skill, you realize that all distros are the same because the only major differences are the defaults, package managers, and desktop environments. Don't let this deter you from exploring though. Exploration will make you wise. </p>

<h1> Step-by-step </h1>
<p> You will have to learn new things. Don't worry, every component of Linux is well documented. You will encounter undocumented issues. Don't worry, researching on the web will typically provide a simple solution. You will have to change the way you think about computers. Don't worry, you will soon see your efficiency increase. You might even break something. Don't worry, no stable system ever made a good sysadmin.</p> 

<p> Here are some general tasks. I will not go in to great detail. Linux is a self taught skill. The most difficult part of the process is learning how to teach yourself. If you don't know where to start, simply do a web search. Everything after that is rudimentary scientific method and practice. </p> 

<h3> Learn the command line </h3> 
<p> Many people are fearful of the terminal, thinking it's antiquated or for elitists only. The opposite is true. The terminal provides a way of controlling the computer using human language. If you can use a voice assistant, you can use the terminal. To start, I recommend <a href="https://www.linuxcommand.org/tlcl.php">The Linux Command Line</a> book. For sysadmin specific skills, I recommend a book called <i>The Linux Bible</i>. There is no free download for The Linux Bible (unless you look on libgen).</p>

<h3> Install packages from the command line </h3>
<p> Most software on Linux is installed through a package manager. This is a small utility that downloads packages from your distro maintainers. Instead of downloading random .exe files that are malware 11/10 (yes, 110%) of the time, your distro maintainers audit the packages and provide a central way of installing them. <i>Package</i> here is a synonym for <i>program</i>. The package manager is part of what makes Linux more secure. Learn it. </p> 

<h3> Learn a terminal based editor </h3> 
<p> I recommend learning vi or vim (with the vimtutor command) because they are part of the POSIX standard and available on every Linux system. Other choices include: nano, ed/ex, joe, ee, or even emacs with the -nw flag. Learning how to modify files without a graphical editor is an invaluable skill, especially when the desktop breaks and requires manual intervention. This is also beneficial for remote servers. </p> 

<h3> Rice your desktop </h3> 
<p> <i>Rice</i> here is a synonym for <i>customize</i>. The term is borrowed from Honda RICEr cars. Changing the background or theme is a given, but on Linux, you can customize <i>everything</i>. Try different desktops. Change your icon theme. Change themes. Change fonts. Change everything. Check out the <a href="https://wiki.installgentoo.com/wiki/GNU/Linux_ricing">install gentoo wiki</a> for more hints. </p>

<h3> Try other distros</h3> 
<small> And even non-Linuxes like BSD, Haiku, illumos, etc</small>
<p> Trying various distros will help refine your taste and expose you to alternative ways of doing things. </p> 

<h3> Compile something </h3> 
<p> This can be as simple as compiling someone's C small project or building a whole graphical program. Learning to troubleshoot a compiler is a valuable skill. It's the first step towards understanding software dependency. If you're ambitious, try Gentoo. </p> 

<h3> Build a server </h3> 
<p> This can be as simple as a static web server or as complicated as a mail server. You will learn all about security, networking, DNS, and automation when you build a server. Exposing a computer to the internet without NAT is a true trial by fire. </p> 

<h3> Don't reinstall when something breaks </h3> 
<p> A common trend I see among new users is that they reinstall Linux every time they break something. Learning how to fix things (or even just reinstall the modular components) is one way to become more skilled. Linux fails loudly. If you listen, it will tell you exactly what's wrong. </p> 

<h3> Help other people </h3> 
<p> After you've accomplished some of the tasks above, you're now capable of helping others. Go onto their forum or IRC channel (hey look, another task: connect to IRC) of your distro and answer questions. There is always someone who is less experienced than you. Helping and teaching others is a good way to build your confidence. Linux's power comes from it's community. There is no Windows or MacOS community. If we think that sharing source code is a virtue, we should also think that sharing knowledge is a virtue. </p>


<h1> Closing </h1> 
<p> Be not afraid, the Linux community would love to have you. If you get stuck and can't google your way out of something, don't be afraid to ask. I also recommend installing Linux on your main computer (or all your computers) because it forces you to learn rather than be lazy. </p> 


</div>
</div>
<?php include "../includes/footer.html"; ?>

