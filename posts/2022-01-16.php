<?php include "../includes/header.html"; ?>
<title>Hardening Firefox with a custom user.js</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1> What is user.js? </h1>
<p> The <code>user.js</code> file is a way of modifying the settings for Firefox. Most <i>normal</i> people use the settings menu, most <i>tech savvy</i> people use the <code>about:config</code> menu, but the most powerful way of modifying Firefox is with a <code>user.js</code> file. </p>

<p> The <code>user.js</code> file is read every time Firefox starts. In a sense, it is a <b>permanent</b> configuration file that overrides any and all settings a user might change in the settings menu or in <code>about:config</code>. This config file allows users to configure away many of the privacy violations, homing beacons, and generally annoyances that are default in Firefox. </p> 


<h1> Getting a custom user.js </h1> 
<p> A <code>user.js</code> file looks something like this: </p>
<pre>
[...]
user_pref("dom.serviceWorkers.enabled",				false);
user_pref("dom.webnotifications.enabled",			false);
user_pref("dom.enable_performance",				false);
[...]
</pre> 

<p> But luckily, we don't have to write our own. Mozilla documents most of their web browser's functionality but I genuinely do not have the time or energy to read API documentation and determine what does and does not work through trial and error. Today, I present you with two tools: <a href="https://github.com/arkenfox/user.js">Arkenfox, a template for writing your own user.js</a> and <a href="https://ffprofile.com/">Firefox Profile Maker, a tool that generates a usable user.js</a>. Since Arkenfox has it's own documentation, I won't cover it. Instead, I will cover Firefox Profile maker because most people will choose this option. </p> 

<p> Visit the website and begin clicking checkboxes. Once you get to the useragent part, I recommend inserting something generic into the box. A useragent is a string of information that your web browser automatically sends to the servers you connect to. This string can be used to track you. My useragent is pretty unique: </p>
<pre>
Mozilla/5.0 (X11; OpenBSD amd64; rv:91.0) Gecko/20100101 Firefox/91.0
</pre>

<p> How many people are actually using Firefox on Linux? Okay, now what about OpenBSD? I like to change my useragent to something generic and windowsy. Custom user agent strings are not bulletproof, but inserting a useragent that looks like Chrome on Windows will help prevent you from being the only guy on the internet with a useragent string that contains OpenBSD. <b>Note that this will break the Firefox addon store and possibly other internal Firefox menus.</b>  </p>
<pre>
Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36
</pre>

<p> When you get to the extensions part, I recommend selecting <code>uBlock Origin</code>, <code>ClearURLs</code>, <code>CanvasBlocker</code>, and <code>HTTPS Everywhere</code>. </p>

<p> When you are finished, you will be presented with multiple archives and JavaScript files. You have a few options: the <i>profile.zip</i> is probably the one you want, the <i>enterprise_policy.zip</i> will forcefully reset any user configurations each time Firefox is started, the <i>prefs.js</i> file can be appended to your existing prefs.js file, and the <i>addons.zip</i> contains the addons. You probably only need <i>profile.zip</i></p> 

<h1> Installing the generated custom user.js </h1>
<p> In order to install our scripts, we need to determine what our profile is. To do this, enter <a href="about:support">about:support</a> into the address bar and look for a line that says <i>Profile Folder</i>. This is the path we want to work with. The general process is to close firefox, delete everything in the profile folder, unzip our .zip files into the profile folder or copy over the <code>prefs.js</code> file, then clean up by removing the left over .zip files. Here are the console commands: </p> 
<pre>
$ cd ~/.mozilla/firefox/xxxxxxxx.default
$ pkill firefox
$ rm -rf ./*
$ for i in ~/Downloads/*.zip; do unzip $i -d ./; done
$ rm ~/Downloads/*.zip
</pre>

<p> After extracting the archive, you should start Firefox and verify that you have the configuration you wanted. You should also change your default search engine to DuckDuckGo instead of google, update the addons, fix proxy settings, etc. I recommend also installing these extensions: <a href="https://addons.mozilla.org/en-US/firefox/addon/librejs">GNU LibreJS</a>, <a href="https://addons.mozilla.org/en-US/firefox/addon/javascript-restrictor/">FSF's JShelter</a>, and <a href="https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/">Violentmonkey</a>. 

<h1> Testing </h1>
<p> Now that you have everything set up, you can visit <a href="https://coveryourtracks.eff.org/">The EFF Tracking Test</a>.  Even after all this hardening, it seems that they've managed to grab my useragent string with JavaScript. Disabling JavaScript with LibreJS seems to solve this problem. An addon that spoofs user agents will also solve this problem. </p> 


<h1> Alternatives to user.js </h1>
<p> For most non-paranoid people, Firefox is <q>private enough</q>. I recommend Firefox to <i>normal</i> people and tell them to avoid Google Chrome and any of it's metastasized clones (see Brave, Edge, Opera, etc). Default Firefox is significantly better for your online privacy than any of the chrome based browsers. Even if chromium is open source, I can't, in good faith, instruct anyone to use a program designed by a company explicitly for the purpose of data collection. If you're currently using Chrome or it's clones, consider switching to Firefox. Even without a hardened <code>user.js</code>, you will be much better off. We cannot let perfect be the enemy of good. </p>

<p> If you don't want to mess around with custom <code>user.js</code> files, consider using IceCat. I generally use and recommend GNU's IceCat to advanced users (people who are already using Firefox). IceCat changes much of the default behavior of Firefox and removes all of the nonfree (ie non redistributable) components. If you have firm convictions, you will probably enjoy IceCat. There is, however, one issue with IceCat: nonfree JavaScript is disabled by default in order to increase security and freedom. <a href="https://www.fsf.org/campaigns/freejs">The FSF has a very firm stance on JavaScript.</a>. </p>
<blockquote>
Contrary to popular perception, almost no JavaScript runs "on the Web site" -- even though these JavaScript programs are hidden from view, they are still nonfree code being executed on your computer, and they can abuse your trust.
</blockquote>

<h1> Conclusions </h1>
<p> On one hand, it might be worth going through all this effort to harden your web browser. On the other hand, hardening your web browser will make you stick out like a sore thumb. The question becomes: is it really privacy when I'm the only guy running Firefox without JavaScript on OpenBSD with extremely obscure customizations and niche addons? I would like to think that it is better to be loudly and obnoxiously private than to be quiet and easy to spy on. A web browser that loudly tells trackers and data brokers to go fuck themselves is better than a quiet web browser that silently allows it's user's privacy to be violated. </p> 

<p> I think I have bigger problems if someone finds out who I am because I'm the only guy running OpenBSD in my timezone. Being loudly private is a good thing because any <i>real</i> violations of my privacy are very likely to also be loud. At the end of the day, we cannot let perfect be the enemy of good. The perfectly private web browser is not running a web browser at all. Is this practical? Probably not. I'll take a good web browser any day over abandoning the http protocol entirely. At least for now. </p> 

</div>
</div>
<?php include "../includes/footer.html"; ?>

