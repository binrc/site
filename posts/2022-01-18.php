<?php include "../includes/header.html"; ?>
<title>Adding MariaDB to the OpenBSD web stack</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<p> MariaDB is just Mysql but it's actually maintained. </p> 
<pre>
$ doas pkg_add mariadb-server php-mysqli
</pre> 

<p> RTFM  and adjust accordingly</p> 
<pre>
less /usr/local/share/doc/pkg-readmes/mariadb-server
</pre>

<p> Install and enable services, I like to choose socket auth for root </p>
<pre>
$ doas mysql_install_db
$ doas rcctl enable mysqld
$ doas rcctl start mysqld
$ doas mysql_secure_installation
</pre> 

<p> change the socket location so that it's inside of our httpd chroot </p> 
<pre>
$ doas install -d -m 0711 -o _mysql -g _mysql /var/www/var/run/mysql
</pre> 

<p> and edit <code>/etc/my.cnf</code> to use this modified socket location. Append: </p>
<pre>
[client-server]
socket = /var/www/var/run/mysql/mysql.sock
</pre>

<p> And test mysql </p> 
<pre>
$ doas rcctl restart mydqld
$ doas su
# echo "show databases;" | mysql -uroot
</pre> 

<p> Add user, create tables </p>
<pre>
# echo "CREATE USER IF NOT EXISTS 'www'@'127.0.0.1; IDENTIFIED BY 'YouReallyShouldBePUttingThisInYourPhp.ini';" | mysql
# echo "CREATE DATABASE www;" | mysql
# echo "GRANT ALL PRIVILEGES ON www.* TO 'www'@'127.0.0.1';" | mysql
# echo "FLUSH PRIVILEGES;" | mysql
# echo 'CREATE TABLE www.testing (id int not null auto_increment primary key, data text not null);' | mysql
# echo 'INSERT INTO www.testing (data) VALUES ("foo");' | mysql
# echo 'INSERT INTO www.testing (data) VALUES ("bar");' | mysql
# echo 'INSERT INTO www.testing (data) VALUES ("baz");' | mysql
</pre>

<p> enable mysqli in <code>/etc/php-8.0.ini</code>, find and uncomment the line that says: </p>
<pre>
;     extension=mysqli
</pre>

<p> see if everythign works in php </p>
<pre>
&lt;?php
$sqlhost = "127.0.0.1";
$user = "www";
$pass = "pass";
$table = "www";
$db = new mysqli($sqlhost, $user, $pass, $table);

if($db-&gt;connect_error){
	die("con failed" . $db-&gt;connect_error);
}

$res = $db-&gt;query("SELECT * FROM www.testing;");

foreach($res as $row){
	echo "id = ". $row['id'] . " | data = " . $row['data'] . "&lt;br&gt;";
}
?&gt;
</pre>


</div>
</div>
<?php include "../includes/footer.html"; ?>
