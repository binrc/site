<?php include "../includes/header.html"; ?>
<title>9front CPU server</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<small>Or, how to become a 9 tourist</small>
<h1> Background </h1>
<p> A traditional Plan 9 network is distributed and is typically composed of 3 parts: Terminals, CPU servers, and Filesystem servers. A 9front network adds an Auth server, making the total 4 hosts. Luckily, we can configure a single machine to function as the CPU, FS, and Auth server. The Terminal here is another 9front machine or the drawterm program. </p> 

<p> There is no easy way to make a 1:1 comparison to a UNIX system but I will try. The FS server is similar to an NFS server. The Auth server is similar ot LDAP. The CPU server is similar to using SSH to connect to a remote host. The Term is your local machine. Behind the scenes, these analogies break down. But they are close enough for a 10,000 foot view. </p> 

<h1>Adding users</h1>
<p> First, we need to add a user to the file server. The <code>newuser</code> command is similar to <code>adduser</code> or <code>pw</code>. Then, we add the user to some groups, particarly the system, admin, and mail groups. Whether or not the mail group is necessary, I don't know. It's a standard group so it can't hurt. </p> 

<pre>
% con -C /src/cwfs.cmd 
newuser username 
newuser sys +username
newuser adm +username
newuser upas +username
</pre>

<h1>Set up user  homedir</h1>

<p> boot with the <code>user=username</code> bootarg, then run the following command. This is a similar concept to <code>cp -r /etc/skel /home/username</code> on a UNIX system. Now reboot again.</p>
<pre>
% /sys/lib/newuser
</pre>


<h1>Setting up headless booting</h1>
<p> Typically, a 9 install will ask the user which disk they want to boot from. We can configure it to automatically boot with specific options. First, we mount the 9fat partition. <i>Note that this step differs on systems that are not i386 or amd64</i>. </p>

<pre>
% 9fs 9fat
</pre>

<p> Then, we edit the config file with <code>acme</code> or <code>sam</code>. This is similar to editing <code>/boot/grub2/grub.cfg</code> on a Linux system or editing <code>/boot/loader.conf</code> on FreeBSD. </p>


<p> Make <code>/n/9fat/plan9.ini</code> look like this:</p>

<pre>
bootfile=9pc64
nobootprompt=local!/dev/sdC0/fscache
mouseport=ps2
monitor=vesa
vgasize=1024x768x14
user=username
tiltscreen=none
service=cpu # comment out if it breaks
</pre>

<h1>Storing hostowner info in the NVRAM </h1>
<p> The information we store in NVRAM will be automatically loaded at boot time. Make sure you type everything correctly. If your CMOS battery dies, you will have to repeat this step. </p>

<pre>
% nvram=/dev/sdC0/nvram auth/wrkey
bad nvram des key
bad authentication id
bad authentication domain
authid: username
authdom: 9front
secstore key: &lt;press the return key if you do not want to type this at boot time&gt;
password: &lt;make it 8 chars&gt;
</pre>

<h1>Auth server</h1>

<p> The auth server is very simple to set up. Since we have already added a user with a password to this system, we simply instruct 9 to start the start the process at boot time. Append this to <code>/lib/ndb/local</code></p>

<pre>
auth=cirno authdom=9front
</pre>

<h1>Adding a user to the Auth server </h1>
<p> We will now add our user to the Auth server. </p> 

<pre>
% auth/keyfs
% auth/changeuser username
Password: &lt;what you put earlier&gt;
Confirm password:
Assign new Inferno/POP secret? [y/n]: n
Expiration date (YYYYMMDD or never) [never]: never
Post id:
User's full name: 
Department #:
User's email address:
Sponsor's email address:
user username installed for Plan 9
</pre>

<h1>Add perms</h1>
<p> The next step is to add something similar to <code>sudo</code> permissions. This analogy is completely broken. Append this to /lib/ndb/auth</p>

<pre>
hostid=username
uid=!sys uid=!adm uid=*
</pre>

<h1>Reboot and pray</h1>
<p> If something fails, you will need to use ed or sed to fix it. Smile soldier, there are darker days ahead. </p>

<h1>Connect with drawterm</h1>
<p> To connect to our CPU server, we can use the <code>rcpu</code> or <code>drawterm</code> programs. <i>Note that the 9front and Plan 9 From Bell Labs versions of these programs differ substantially because 9front added the concept of auth servers</i>. <a href="http://drawterm.9front.org">You can download the source for drawterm here</a></p>
<p> To connect to a remote system, we specify the username, the CPU server, and the auth server. Since these are on the same box, we will repeat a few arguments. </p> 
<pre>
$ ./drawterm -u username -h 9cpuserv -a 9cpuserv
</pre>

</div>
</div>
<?php include "../includes/footer.html"; ?>
