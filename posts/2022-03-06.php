<?php include "../includes/header.html"; ?>
<title>Morality Clauses and Free Software</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<small>Or, how to make your software nonfree in an attempt to garner attention on Twitter. </small>
<h1> What is Free Software? </h1>
<p> There are multiple definitions for Free/Open source software. For my own purposes, I use the FSF's <i>Four Freedoms</i>. These four freedoms provide abstraction from legalese and allow us to determine what is and is not free software: </p>
<ul>
<li> The freedom to run the program as you wish, for any purpose (freedom 0). </li>
<li> The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.</li>
<li> The freedom to redistribute copies so you can help others (freedom 2).</li>
<li> The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.</li>
</ul>

<p> With the knowledge of these four freedoms, we gain the ability to discern between <i>free</i> and <i>nonfree</i> software. </p>
<p> For the purposes of this article, <i>Free</i> and <i>Open</i> should be treated as interchangeable. </p>

<h1> Morality clauses </h1>
<p> A morality clause is a condition specified the license that demands the user of the software adhere to some moral guideline or take a moral stance. Examples of this type of license are the JSON license and the Hippocratic license. They typically contain a phrase like "don't use this software for evil purposes". </p> 

<p> Let's examine the JSON license. This license is a common example of how a free software license can be converted into a nonfree license. The JSON license is nearly identical to the X11/Expat/MIT license except that it adds the phrase: </p> 
<blockquote>
The Software shall be used for Good, not Evil.
</blockquote>
<p> At first, this might seem like a good idea. After critical examination, however, it becomes clear that the subjectivity of morality clause is not only unenforcible in a court of law but also that this license condition conflicts with freedom 0, <i> The freedom to run the program as you wish, for any purpose</i>. The OSI has a more explicit definition when it comes to morality clauses: </p> 
<blockquote>
The license must not restrict anyone from making use of the program in a specific field of endeavor. For example, it may not restrict the program from being used in a business, or from being used for genetic research.
</blockquote>

<p> If a license demands a moral stance or action, it is nonfree. </p> 

<h1> Restricting who can use free software </h1>
<p> Other types of licenses that falsely assert that they are free are licenses that restrict specific groups or people from using the software. Examples of this include the "Anyone But Richard Stallman License", the "Non-White-Heterosexual-Male License", and various other identitarian licenses that litter repositories all over the internet. These types of licenses typically state that "only $type_of_person can use this software" or or "by using this software you must agree with $political_ideology". These licenses are in direct conflict with freedom 2, <i>The freedom to redistribute copies</i> because it does not give anyone and everyone permission to use the software. Again, the explicit OSI definition: </p>
<blockquote>
The license must not discriminate against any person or group of persons.
</blockquote>

<p> If a license prevents a person or group from using it, it is nonfree. </p>

<h1> The latest morality clause: "Terms of Use" </h1> 
<p> It seems to be a trend for developers on Github to include an anti-Russian README in their project while simultaneously asserting that their project remains Open Source. This is separate from the license and is entirely unenforcible. I can't detect any motivation for adding a "Terms of Use" but if I had to guess it has something to do with over-exposure to Twitter and Reddit. These notices are popping up in many repositories. As discussed above, including a condition that demands a moral or political stance makes the software nonfree.</p> 

<img src="assets/images/moralityclauses.png" alt="Terms of use: By using this project or its source code, for any purpose and in any shape or form, you grant your implicit agreement to all the following statements: You condemn Russia and its military aggression against Ukraine.  You recognize that Russia is an occupant that unlawfully invaded a sovereign state.  You support Ukraine's territorial integrity, including its claims over temporarily occupied territories of Crimea and Donbas.  You reject false narratives perpetuated by Russian state propaganda. ">

<p> Implicit agreement? I surely hope I'm not implicitly agreeing to anything without prior knowledge and consent. </p> 

<p> Is a project with this notice still free software? Not at all. The specific term for nonfree, source code available projects is <i>Public Source</i>. The source code remains public but it does not come attached with the necessary freedoms to be a true Free and Open Source program. </p> 

<p> After seeing screenshots of this notice pop up in a few forum threads, I decided to do some Google Dorks to see how many project maintainers have instituted this morality clause. As of writing, google is pulling up 1,200 results but not all of these are repositories. . </p>

<pre>
site:github.com intext:"condemn Russia and its military aggression against Ukraine"
</pre>

<p>Currently, Github search is returning somewhere around 300 results. 300 maintainers who failed to understand the spirit of FOSS. 300 projects to avoid because the maintainers care more about virtue signaling than preserving software freedom. <p>

<p>What happens when you, the reader, become the latest boogeyman? Are you ready to be discriminated against because of your personal beliefs and identity? Remember, <i>By anyone for any purpose</i> actually means <i>by anyone for any purpose</i>. Keep this in mind and act in logic, never out of anger. </p> 

<h1> Conclusion </h1>
<p> I think Theo de Raadt said it best: </p> 

<blockquote>
But software which OpenBSD uses and redistributes must be free to all (be they people or companies), for any purpose they wish to use it, including modification, use, peeing on, or even integration into baby mulching machines or atomic bombs to be dropped on Australia.
</blockquote>

</div>
</div>
<?php include "../includes/footer.html"; ?>
