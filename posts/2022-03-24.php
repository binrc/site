<?php include "../includes/header.html"; ?>
<title>Lost Causes</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<small>Or, Pavlovian dogs</small>
<p> <a href="https://web.archive.org/web/20220324192341/https://www.theverge.com/2022/3/15/22979251/microsoft-file-explorer-ads-windows-11-testing">Windows 11 gets even worse, now there are advertisements in the file manager</a>. I've been thinking about this for the last few days. It keeps getting worse. </p>

<p> The file manager isn't the only place microserfs see advertisements. Ads are also present in:</p>
<ul>
<li>The start menu</li>
<li>The lock screen </li>
<li>The task bar</li>
<li>The system tray</li>
<li>Below the menu bar of their office suite programs</li>
<li>And now, the file manager</li>
</ul>

<p> Ads in your browser, search engine, social media, and now your operating system? Sadly, I don't think the users will care. If they didn't leave when we told them that Windows is spyware, they won't leave because Windows is adware. </p>

<p> In past years, I thought of these people as ignorant or stupid. I always thought their excuses for continuing to use Windows were half baked at best. I'm reconsidering my perspective now. </p>

<p> The users have been conditioned to behave this way. The slowly boiled frog doesn't realize he is being boiled alive. The users have been taught to sit down, shut up, and look at the advertisements. There is no longer any difference between the Windows operating system and the viruses of old. Remember keyloggers, adware, spyware, and the wide variety of trojans that presented as something safe and innocent? Microsoft has cornered the malware market. They are the leading developer of PC malware. The users have been boiled alive. At any time they can jump out of the pot but I don't think they will. </p>

<p> I feel pity for these users. They are completely unaware of their current situation. They are neither stupid nor ignorant. They are victims. At times, I feel like I should forgo my free software advocacy attempts and become a GNU/Hermit. I have tried but it is very difficult to reason with an unreasonable person. They have been conditioned to accept a false reality, the reality where Windows is the safest, most reliable, professional, and private operating system on the planet. I'm at loss for what actions I can possibly take to empower the individual user to liberate themselves. I have provided all the tools, provided advice and encouragement, given the best arguments I am capable of yet the decision to be free is theirs. </p> 

<blockquote><q>The best slave is the one who thinks he is free.</q></blockquote>

<p> That's the thing about freedom. It's a choice, isn't it. We, as individuals, can choose between freedom and enslavement. It is not a choice I can make for anyone other than myself. And that's the funny thing about liberty. Every individual has the liberty to surrender their liberty in exchange for a quick hit. </p>

<p> What can we do but lead by example and provide tech support to those who chose to be free? Those who can do and those who can't will never know of the possibility of doing. </p>


</div>
</div>
<?php include "../includes/footer.html"; ?>
