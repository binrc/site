<?php include "../includes/header.html"; ?>
<title>UNIX is sublime</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1 >UNIX is sublime</h1>

<h6 >Or, &#8220;how to use a computer without hating yourself for it in the morning&#8221;</h6>

<h6 >Or, &#8220;Unix is basically a simple operating system &#8230;&#8221;</h6>

<h6 >Or, &#8220;My weariness and disdain for computers grow with each additional unit of knowledge&#8221;</h6>

<h6 >Or, &#8220;Worse is better&#8221;</h6>

<h1 >Origins</h1>

<p>UNIX is not Multics</p>

<p>Multics = Multiplexed Information and Computer Service</p>

<p>UNIX = Uniplexed Information and Computing Service</p>

<p>The name &#8216;UNIX&#8217; is a pun on the name &#8216;Multics&#8217;. Multics was entirely too large and complicated to be useful so the boys at Bell Labs cooked up something smaller, less complicated, and easier to use. </p>

<hr/>

<h3 >Ancient emulation interlude</h3>

<p><a href="https://www.multicians.org/sim-inst.html">How to run Multics in 2022.</a></p>

<p><a href="https://gunkies.org/wiki/Running_Unix_v5_in_SIMH">This wiki helped me emulate UNIXv5.</a></p>

<p><a href="https://gunkies.org/wiki/Installing_v7_on_SIMH">And this one helped me emulate UNIXv7.</a></p>

<p><a href="https://livingcomputers.org/Computer-Collection/Online-Systems.aspx">These guys host ancient systems accessible via guest accounts over ssh.</a></p>

<blockquote>
<p>&#8220;Cool, but useless.&#8221;</p>
</blockquote>

<hr/>

<p>I know almost nothing about Multics and I&#8217;m not sure if it&#8217;s even worth learning. This is about UNIX, not Multics. Maybe I&#8217;ll come back to it.</p>

<h1 >Philosophy, implementations, ducks</h1>

<p>When I think of &#8220;UNIX&#8221;, I do not think of the trademark. Instead, I think of the <a href="https://en.wikipedia.org/wiki/Unix_philosophy">Unix philosophy.</a> and the general design principles, interface, and behavior of a UNIX system. </p>

<p>A better way of thinking about &#8220;UNIX&#8221; is as something &#8220;POSIX-like&#8221; rather than &#8220;AT&#38;T&#8217;s commercial UNIX&#8221;. Example: although Linux and GNU are overly complicated, they pass the duck test for being a UNIX. Pedigree or not, you know a nix when you see one. </p>

<p>Also, when I say &#8220;UNIX&#8221;, I mean &#8220;Free UNIX&#8221;. I have no interest in proprietary implementations that only exist for the purpose of restricting users and disempowering&#47;discouraging sysadmins from becoming self-reliant. </p>

<p>So what is the philosophy? </p>

<ul>
<li>Do one thing and do it well</li>
<li>Design programs that work together using text as the common interface</li>
<li>KISS: Keep it simple, stupid</li>
<li>Test early, test often</li>
</ul>

<p>And additionally:</p>

<ul>
<li>everything is a file or a process</li>
</ul>

<h1 >Design</h1>

<h2 >10,000 Ft View</h2>

<p>UNIX is a multiuser time sharing networked operating system, running as an always online service. A UNIX system is a single mainframe computer running an operating system designed for multiple users to access concurrently over the network, equally (depending on implementation) sharing resources amongst the active users. </p>

<p>In a traditional network setup, there is one mainframe UNIX machine with multiple dumb terminals connected to it over the network. None of the users touch the mainframe physically. Instead, they interact with it exclusively through their own dumb terms. These dumb terminals have minimal or no computing power of their own because all of the actual computation takes place on the mainframe. Built in networking is a given.</p>

<p>As for the actual software running on the mainframe, it&#8217;s quite simple to visualize. A Unix system is an flexible but organized stack of concepts, each depending on the concept below, all working together for the sole purpose of enabling the end user to play video games and watch videos online. </p>

<pre><code>       &#47; user applications \
      &#47;       shells        \
     &#47;        daemons        \
    &#47;       file systems      \
   &#47;        kmods&#47;drivers      \
  &#47;           syscalls          \
 &#47;             kernel            \
&#47;             hardware            \
</code></pre>

<p>In order to fully explain why UNIX is sublime, I will start from the bottom and work my way upward. Before I discus the shell, I will explain the multiuser aspects of the system. Then, after a long arduous journey of verbosity, explain how to actually use the thing.</p>

<h2 >Kernel</h2>

<p>The kernel is something the user rarely interacts with. It abstracts all the hard parts away from the user. No more poking random memory addresses to load a program from tape. </p>

<h3 >Multitasking</h3>

<p>In order to support multiple users, resource sharing was implemented. When a user&#8217;s process requests CPU time, it&#8217;s put into a rotational queue along with the other requests for CPU time. Round robin style concurrency is one of the easiest to implement but most modern systems use a weighted model that prioritizes processes owned by specific users. Memory and disk space are typically assigned hard limits to prevent system crashes. &#8220;Ask your sysadmin if you need more resources.&#8221;</p>

<h3 >Virtual Memory</h3>

<p>Abstracting memory management from users is almost necessary in a multitasking system. The kernel must be the arbiter of all. The most interesting thing about virtual memory is that it doesn&#8217;t actually need to be a RAM stick, but can be a swap partition on a disk or even a remote cloud provider if you&#8217;ve actually lost your mind. This type of flexibility improves system stability. Instead of a kernel panic when memory runs out, the kernel can de-prioritize nonessential or idle processes by sending them to swap space. </p>

<h3 >Paged Memory (logical memory)</h3>

<p>No more fragmented memories! The kernel maintains a page table that maps logical locations to physical locations. Instead one continuous chunk of memory, the kernel divides memory into small sections called &#8220;pages&#8221;. When allocating memory, the kernel might not give a process continuous pages. The advantage of a paged memory scheme further enables multiuser computing. Example: When you have a large program like a web browser open, the pages that contains the unfocused tabs can be swapped out to disk without stalling the entire browser. </p>

<h3 >Programming Interface pt. 0 (syscalls, kmods, drivers)</h3>

<p>When a process requests a resource, it sends a syscall to the kernel. The kernel then responds to the system call. This allows for privilege separation. Does your web browser need direct access to all memory? What about all files? Do we even want to write assembly every time we want to access a file? Syscalls are dual purpose: abstraction and security. </p>

<p>Kernel modules are dynamic &#8220;extensions&#8221; that give the kernel new features (typically hardware support). The ability to dynamically load&#47;unload modules as hardware changes increases uptime because it means a new kernel doesn&#8217;t need to be compiled, installed, and booted into every time we plug in a different peripheral. </p>

<h2 >Filesystem</h2>

<h3 >Hierarchical structure</h3>

<p>A UNIX filesystem is hierarchical. Each directory contains files or other directories, each with a specific purpose. This type of organization makes it very easy to navigate and manage a system. Each child directory inherits ownership and permissions unless otherwise specified (see Access Control).</p>

<p>In order to visualize this, I imagine a tree-like structure descending from the root directory, <code>&#47;</code>. The <code>tree(1)</code> program shows this type of hierarchy. </p>

<h3 >Virtual Filesystems (logical filesystem)</h3>

<p>The idea behind virtual filesystems is, again, abstraction. Using the concept of a virtual file system, multiple disks can be presented to the user and programmer as a single unified filesystem. This means mounted local disks, NFS shares, and even the contents of a CDROM are presented as if the files contained therein are &#8220;just on the big hard drive&#8221;. </p>

<p>Additionally, using bind mounts, a directory can be mounted onto another directory as if it were just another filesystem. </p>

<p>The final interesting thing about virtual filesystems is the concept of a ramdisk: mounting a section of memory so that it can be used as if it was an ordinary directory. <strong>&#60;&#8211;Shoot foot here.</strong></p>

<h3 >Everything is a file</h3>

<p>Well, almost everything is <em>presented</em> as if it were a file. This greatly simplifies programming. </p>

<p>Prime example: <code>&#47;dev&#47;urandom</code> is a random entropy generator presented as a file, making it very simple for a programmer to implement seeded RNG in a program. </p>

<p>Another example: The kernel translates mouse input into a data stream that can be opened as a file. The programmer only needs to read from <code>&#47;dev&#47;mouse0</code> instead of writing hundreds of mouse drivers for a clicky GUI. </p>

<p>Exercise 1: Try running this command then wiggling your mouse:</p>

<pre><code class="language-sh"># Linux
$ sudo cat &#47;dev&#47;input&#47;mouse0

# FreeBSD
$ sudo cat &#47;dev&#47;sysmouse
</code></pre>

<p>Yet another example: the TTY is just a file. You can even print it to a text file using <code>setterm(1)</code> on Linux. </p>

<p>Exercise 2: </p>

<pre><code class="language-sh">[user@fedora ~]$ sudo setterm --dump 3
[user@fedora ~]$ cat screen.dump

Fedora Linux 36 (Workstation Edition)
Kernel 5.18.5-200.fc36.x86_64 on an x86_64 (tty3)

fedora login: root
Password:
Last login: Sat Jul 30 14:34:20 on tty3
[root@fedora ~]# &#47;opt&#47;pfetch&#47;pfetch
        ,&#39;&#39;&#39;&#39;&#39;.   root@fedora
       |   ,.  |  os     Fedora Linux 36 (Workstation Edition)
       |  |  &#39;_&#39;  host   XXXXXXXXXX ThinkPad T490
  ,....|  |..     kernel 5.18.5-200.fc36.x86_64
.&#39;  ,_;|   ..&#39;    uptime 20d 22h 40m
|  |   |  |       pkgs   3910
|  &#39;,_,&#39;  |       memory 6522M &#47; 15521M
 &#39;.     ,&#39;
   &#39;&#39;&#39;&#39;&#39;

[root@fedora ~]#






























[user@fedora ~]$
</code></pre>

<h3 >Links</h3>

<p>Yet another way of &#8220;mounting&#8221; a file or directory to another file or directory is linking. There are two types of links: hard links and symbolic links. </p>

<p>On UNIX, files are indexed by inodes (index nodes). Using links, we can make &#8220;shortcuts&#8221; to files. </p>

<p>Hard linking adds a &#8220;new index&#8221; to a file. They share an inode. If the original file is removed, the file persists in storage because the secondary file created by a hard link still exists. Think &#8220;different name, same file&#8221;</p>

<p>Symlinks are like pointers. A symlink points to the original file instead of the inode. If you remove the original file, the symlink breaks because it points to a file that points to an inode rather than simply pointing to an inode. </p>

<p>Using links, we can make files more convenient to access as if we are &#8220;copying&#8221; files without actually copying files. </p>

<h3 >Filename extensions</h3>

<p>On a UNIX system, file extensions are arbitrary. UNIX determines file type by reading the file headers. The file tells you exactly what type of file it is (just read it). The entire system does not break when a file extension doesn&#8217;t match the expected contents of the file. </p>

<p>Extensions only matter when you wilfully associate with the microsoft users leaving issues on your software repos. &#8220;Not my OS, not my issue, it&#8217;s open source so fork it if you don&#8217;t like it&#8221;</p>

<h2 >Multiuser (timesharing)</h2>

<p>See also: Multitasking.</p>

<p>Exercise 3: attempt to use Windows like a multiuser operating system and get back to me when you have realized that any and all claims made by microsoft about how their &#8220;multi user enterprise system&#8221; is in any way capable of competing with a genuine multi-user UNIX system are false advertising. </p>

<h3 >Users, Groups</h3>

<p>A multiuser system needs a way to manage users and categorize them for access control purposes. Every user has a single user account and belongs to 0 or more groups. Sorting users into groups at the time of account creation makes is significantly easier than granting&#47;revoking permissions user-by-user. Additionally, using something like <code>rctl(8)</code> on FreeBSD allows a systems administrator to allocate resources to specific users, groups, or login classes (like groups).  </p>

<h3 >Daemons (services)</h3>

<p>On a UNIX system, every process is owned by a user. In the case of a service, the process is owned by a daemon account. Daemon accounts have limited permissions and make it possible to run persistent services as a non-root user. </p>

<h3 >Access Control</h3>

<p>Since UNIX was designed to be a multiuser system, access control is required. We know about users, we know about groups, but what about permissions?</p>

<p>There are three types of operations that can be done to a file: read, write, and execute. Who can the admin grant these permissions to? The Owner, the Group, and the Other (all). This type of access control is called discretionary access control because the owner of the file can modify files <em>at their own discretion</em>. </p>

<h2 >Actually using the thing</h2>

<h3 >Programming interface Pt. 1 (data streams)</h3>

<p>All UNIX utilities worth using use 3 data streams:</p>

<ul>
<li>stidn

<ul>
<li>read from it the same way you read from a file</li>
</ul></li>
<li>stdout

<ul>
<li>print to it the same way you print to a terminal (file)</li>
</ul></li>
<li>stderr

<ul>
<li>print to it the same way print to a file, read from it the same way you read from a file</li>
</ul></li>
<li>env vars if you&#8217;re a CGI programmer</li>
</ul>

<h3 >Shell</h3>

<p>The shell is how a user actually interacts with a UNIX system. It&#8217;s a familiar interface that allows a human user to interact with a computer using real human language.</p>

<p>Explicitly telling the computer to do is infinitely less agonizing than dealing with a computer that tries to do what it thinks you want it to do by interpreting input from a poorly designed, overly engineered interface. </p>

<p>The shell, in addition to being an interactive interface, is also scriptable. Although math is a struggle, shell scripting is a fairly simple way of automating tasks. Taping together interoperable commands you already know makes everything easier. My favorite aspect about writing POSIX shell scripts is knowing that shell is a strongly, statically typed language where the only datatype is string. </p>

<p>Problem that are difficult or messy to solve in shell usually mean it&#8217;s time to write another small C program for your specific needs. Adding the new program into the shell pipeline is trivial. </p>

<h4 >Pipes</h4>

<p>Pipes, the concept that makes UNIX so scriptable. A shell utility that follows the UNIX philosophy will have a non-captive interface, write uncluttered data to stdout, read from stdin, and error to stderr. The <code>|</code> pipe character instructs programs to send their stdout to the next stdin in the pipeline instead of printing to the terminal. </p>

<p>All standard command line utilities are interoperable and can be easily attached like building blocks. &#8220;Meta programming&#8221; has never been easier. </p>

<p>Pipes make it so that every UNIX program is essentially a filter. Sure, you could just use awk, but I prefer shell. </p>

<h1 >Bonus:</h1>

<ul>
<li>plaintext configuration files</li>
<li>All logs are pretty much just a .csv</li>
<li>OS vendor doesn&#8217;t force you to upgrade to a newer version of spyware</li>
<li>modular design means explorer.exe crashes don&#8217;t take down your entire IT infrastructure</li>
<li>Portable design means write once, run everywhere with minimal effort</li>
</ul>

<h1 >Summary:</h1>

<p>UNIX is a non-simple modular operating system designed for 1970s big iron mainframes but we love it too much to let it go. Compared to minimal hobbyist operating systems, UNIX is BIG. Compared to commercial operating systems, free UNIX is small. Maybe slightly more than minimum viable but the papercuts are mild enough to forgive. </p>

<h1 >See Also:</h1>

<p><a href="https://web.mit.edu/~simsong/www/ugh.pdf">The UNIX-HATERS Handbook</a></p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
