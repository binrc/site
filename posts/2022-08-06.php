<?php include "../includes/header.html"; ?>
<title>Plan 9: an Exercise in Futility</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Plan 9: An exercise in futility</h1>

<h5>It is my right to exercise my futility wherever, whenever, and with whoever I please</h5>

<h1>Some ideas about Plan 9:</h1>

<blockquote>
<p>It&#8217;s like the uncanny valley of UNIX</p>

<p>Cool, but useless</p>

<p>Can you sum up plan 9 in layman&#8217;s terms? It does everything Unix does only less reliably - Ken Thompson</p>

<p>If you cannot imagine a use for a computer that does not involve a web browser, Plan 9 may not be for you - 9front FQA</p>

<p>init: error reading plan9.ini: phase error &#8211; cannot happen</p>
</blockquote>

<h1>History and description</h1>

<p>The boys at bell labs decide UNIX wasn&#8217;t good enough so they decided to build something better: a distributed multiuser operating system composed of many machines. Many of the same ideas behind UNIX were pushed to absurd extremes. The idea that &#8220;everything is a file&#8221; is made blatantly apparent to everyone and sometimes, in my opinion, can feel &#8216;overly-abstracted&#8217;. Additionally, the concept of private namespaces makes the concept of virtual filesystems seem like &#8216;baby&#8217;s first filesystem abstraction&#8217;. </p>

<p>Just like UNIX, 9 started as a research operating system. Both are enjoyed by hobbyists, both are interesting ways of using a computer, both have a lot of fun in store. But the systems do diverge in one major aspect: UNIX is mainstream and 9 is still a research operating system. Plan 9 is currently distributed under the MIT license. </p>

<p>&#8220;What is plan 9?&#8221;, Taken directly from <code>intro(1)</code>: </p>

<blockquote>
<p>Plan 9 is a distributed computing environment assembled from separate machines acting as terminals, CPU servers, and file servers. A user works at a terminal, running a window system on a raster display. Some windows are connected to CPU servers; the intent is that heavy computing should be done in those windows but it is also possible to compute on the terminal. A separate file server provides file storage for terminals and CPU servers alike.</p>
</blockquote>

<p>In practice, modern 9 users just run all of these services on a single machine because maintaining many machines to achieve a single usable &#8216;operating system&#8217; is unnecessary; the 9 user finds himself scared and alone without enough users (1 is rarely enough) to justify building a distributed environment. </p>

<h1>Use cases</h1>

<p>Intended: distributed multiuser network (ie not mainframe), later embedded since UNIX was too bad to be stopped</p>

<p>Actual: Acting like a UNIX hipster, pretending that 9 is anything other than vaporware, imagining that you are gaining social credit by posting screenshots of abandonware on internet forums. See also: Operating System Tourism</p>

<h1>9 in the wild</h1>

<ul>
<li>Unicode is now a plague</li>
<li>rfork</li>
<li>9p

<ul>
<li>leveraged by microsoft to discourage end users from actually running GNU+Linux as St Ignucius intended</li>
<li>QEMU&#8217;s VirtFS</li>
</ul></li>
<li>various window managers for UNIX, written by people who like the ideas behind 9 but not enough to actually run 9</li>
<li>&#8220;cool idea, I&#8217;m adding it to Linux&#8221;</li>
<li>private namespaces</li>
<li>union directories</li>
<li>see: docker</li>
</ul>

<h1>Design</h1>

<p>The goal of 9 was to build a distributed operating system that expands upon Unixy ideas, <strong>not</strong> to build something that&#8217;s backwards compatible. &#8220;We want to improve UNIX&#8221; is mutually exclusive to &#8220;we want to port UNIX to this wacky new kernel&#8221;. UNIX programs (and behemoths like FireFox) are difficult<sup>impossible</sup> to port to 9 because of this design decision. </p>

<h2>Distributed operating systems</h2>

<p>Since 9 was designed to be a distributed operating system, many of the internals are oriented towards networking. On a single system installation, all three of the components that make a 9 network are working together in a client-server model. The filesystem is presented as a service, the CPU is presented as a service, and the terminal is presented as a service. This type of &#8220;abstraction from the physical hardware&#8221; makes it difficult to succinctly describe and explain 9. </p>

<p>If you think about 9 as a heterogeneous network of machines the ideas start to make sense. If you think about 9 as a self-contained single-machine operating system the ideas only become more confusing. </p>

<p>One thing that has helped me wrap my head around the client&#47;server idea is actually thinking less. When running a MySQL server in a LAMP stack, the database server and client are running on the same machine. When writing a program, you instruct the client to access the database located at the address <code>localhost</code>. Despite the design intention to run the database as a separate machine, loopback device hacks ensue. The idea of client&#47;server permeates 9. </p>

<p>The filesystem? Presented as a server regardless of what physical machine it&#8217;s located on. The CPU? Presented as a server regardless of what physical machine it&#8217;s located on.  The terminal? Presented as a server regardless of the physical machine it&#8217;s located on.  </p>

<p>On a single machine 9 installation, all of these servers are running locally but accessed <em>as if</em> they were running remotely. Insanity ensues but at least it&#8217;s easier to write code for. </p>

<h2>9p: the Plan 9 Filesystem Protocol</h2>

<p>9p is a networking protocol that makes this client&#47;server model possible. Internally, the filesystem is served to the client over 9p. Many applications make use of 9p, including text editors, windowing systems, plumber, etc. In UNIX, everything is a file. In 9, everything is a filesystem accessed via 9p. </p>

<h2>Private Namespaces, Union Directories</h2>

<p>The most important aspect of 9: namespaces. </p>

<p>Namespaces have caused me much confusion until recently. In 9, each process constructs a unique view of the filesystem. The phrase that gets stuck in my head is &#8220;a private namespace is a per-process view of the filesystem&#8221;. The easiest way to think about namespaces is to think about a &#8220;virtual directory&#8221;. Unix has &#8220;virtual filesystems&#8221;, 9 has &#8220;virtual directories&#8221;. </p>

<p>The concept of namespaces allows a user to pull resources from all over the network and present them as &#8220;a single local filesystem&#8221; with absolute disregard for where these resources are actually coming from.  In order to construct a namespace, union directories are used.  A union directory is a directory made of several directories bound to the same directory. This concept is similar to a bind mount on UNIX. </p>

<p>The kernel keeps separate mount table for each process. Using namespaces, a user or admin can create more secure isolated environments (similar to a chroot). </p>

<p>Processes and their children are grouped together so that inheritance of the namespace occurs. These process groups can be customized. </p>

<hr/>

<p>The &#8216;per-process namespace&#8217; concept can be confusing to UNIX users at first, especially when binding (ie mounting) resources. When I first started using 9 I was very confused when I bound something in one terminal, switched to another, then became disoriented as the thing I just bound seemingly stopped existing. My big example is mounting the boot partition or a filesystem over ssh: </p>

<pre><code># In this window, I have bound the boot partition.
# It behaves expectedly. 
term% 9fs 9fat
term% lc &#47;n
9&#47;      9fat&#47;   other&#47;  ssh&#47;
term% lc &#47;n&#47;9fat
9bootfat        9pc64           oldplan9.ini    plan9.ini
9pc             efi&#47;            pbs.bak
term% 
</code></pre>

<pre><code># In this other window, the boot partition doesn&#39;t seem to be mounted.
# This causes much confusion for the end user. 
term% lc &#47;n
9&#47;      9fat&#47;   other&#47;  ssh&#47;
term% lc &#47;n&#47;9fat
term% 
</code></pre>

<hr/>

<h1>Files</h1>

<p>The second most important aspect of 9: &#8220;Everything is a file&#8221; taken to absurdist absolutes. The kernel presents hardware devices as files bound to &#47;dev. Within the namespace, devices are just files. Outside the namespace, devices are named with a leading <code>#</code> to help distinguish between pseudo-files and devices. These physical devices are bound to <code>&#47;dev&#47;</code> and presented as files for easy administration, access, and programming. Presenting everything as a file accessible via 9p greatly reduces the total number of system calls. </p>

<hr/>

<p>Examples of &#8220;Everything is a file&#8221;: </p>

<pre><code># The clipboard in 9 is called &#47;dev&#47;snarf
# We can easily write and read from this clipboard
term% cat &#47;dev&#47;snarf
SYNOPSIS
#include &#60;u.h&#62;

#include &#60;libc.h&#62;

#include term%  
term% fortune &#62; &#47;dev&#47;snarf
term% cat &#47;dev&#47;snarf
If at first you succeed, try to hide your astonishment.
term% 
</code></pre>

<pre><code># The display in 9 is called &#47;dev&#47;screen
# We can easily take a screenshot 
term% file &#47;dev&#47;screen
&#47;dev&#47;screen: plan 9 image, depth 32, size 1366x768
term% cat &#47;dev&#47;screen | topng &#62; screenshot.png
term% file screenshot.png
screenshot.png: PNG image
term% 
</code></pre>

<hr/>

<h2>Message oriented filesystem</h2>

<p>Continuing with the idea that &#8220;everything is a filesystem&#8221;, processes can offer services to other processes by placing virtual files into other processes&#8217; namespaces. File I&#47;O on this special virtual file becomes interprocess communication. This is similar to a UNIX socket but significantly less difficult to program against because all of the hard parts have been abstracted: it&#8217;s just simple file I&#47;O. </p>

<h2>Virtual filesystem (with more special files)</h2>

<p>The <code>&#47;proc</code> filesystem presents processes as a files in a filesystem. This makes writing programs that manage process extremely easy by reducing the total number of system calls to simple file I&#47;O. The <code>&#47;proc</code> filesystem allows users to manage processes using standard command line utilities like <code>cat(1)</code> and <code>ls(1)</code>.</p>

<p>Linux borrowed the idea of a <code>&#47;proc</code> filesystem.</p>

<h2>Unicode</h2>

<p>Although the implementation is not fully internationalized, UTF-8 is fully there. Unicode is fully backwards compatible with ASCII. Thanks to ⑨, we now have people writing exclusively with primitive hieroglyphics instead of words. </p>

<h2>Portability</h2>

<p>Just like UNIX, 9 was designed with portability in mind. 9 is written in a strange dialect of ANSI C which means it&#8217;s portable. Although the system is self hosting, images are rarely built on a self hosting environment. Instead, the end user will download a generic amd64 or i386 image, cross compile for the obscure target architecture, wrap it up in an install image, then burn that image to an install disk. After installation, it is generally a good idea to recompile the entire operating system so that your copy is self-hosted. </p>

<p>The compiler suite is quite clever in that each compiler is named according to the target architecture, the object files are named according to the target architecture, etc. The alnum prefix&#47;extensions are also shared by the various linkers and assemblers. </p>

<pre><code>0c spim    little-endian MIPS 3000 family

1c 68000   Motorola MC68000

2c 68020   Motorola MC68020

5c arm     little-endian ARM

6c amd64   AMD64 and compatibles (e.g., Intel EM64T)

7c arm64   ARM64 (ARMv8)

8c 386     Intel i386, i486, Pentium, etc.

kc sparc   Sun SPARC

vc mips    big-endian MIPS 3000 family
</code></pre>

<h2>Filesystems</h2>

<p>Multiple filesystems are supported, most suck. The only one the average tourist has heard of is FAT. The one I use is <code>cwfs64x(4)</code>. cwfs is a strange filesystem. Every night, it makes a dump of the filesystem. You can access these dumps by running: </p>

<pre><code>9fs dump
cd &#47;n&#47;dump&#47;YYYY&#47;MMDD&#47;
</code></pre>

<p>And, managing the file server (trying to uncorrupt cwfs), all while the kernel is spraying error messages</p>

<pre><code>term% con -C &#47;srv&#47;cwfs.cmd
help
check tag
check ream
check bad
</code></pre>

<p>The cache is a WORM: Write Once Read Many filesystem. Traditionally, the &#8220;fast&#8221; hard drives would be backed up to tape archives. In the modern era, we have a WORM partition. The worm partition stores data forever so it will eventually get full and need cleaning. It is possible to run without a WORM but it&#8217;s a bad idea. Built in version control. </p>

<p>Data integrity not guaranteed. </p>

<h2>Secstore</h2>

<p>stores various passwords to nvram. BIOS integrety not gauranteed. If you don&#8217;t like thrashing the nvram and it&#8217;s limited write ops, an partition can be created and mouted as if it were nvram. </p>

<h2>Factotum</h2>

<p>stores variosu passwords in memory (like ssh-agent)</p>

<h1>Known forks</h1>

<ul>
<li><p>Dead:</p>

<ul>
<li><a href="https://p9f.org/">Plan 9 From Bell Labs (also called &#8216;Labs 9&#8217;, the original)</a></li>
<li><a href="https://web.archive.org/web/20201111224911/http://www.9atom.org/">9atom (even the domain has expired)</a></li>
<li><a href="http://akaros.org">Akaros </a></li>
<li><a href="https://harvey-os.org/">Harvey (attempt to port 9 to GCC&#47;Clang)</a></li>
<li><a href="https://lsub.org/nix/">NIX </a></li>
<li><a href="http://jehanne.io/">jehanneOS </a></li>
<li><a href="https://github.com/jvburnes/node9">node9 </a></li>
<li><a href="https://www.vitanuova.com/inferno/">inferno (in permanent limbo)</a></li>
</ul></li>
<li><p>Life Support:</p>

<ul>
<li><a href="https://9front.org">9front (actively developed, many QOL patches)</a></li>
<li><a href="http://www.9legacy.org/">9legacy (patches applied to Labs9)</a></li>
<li><a href="https://9fans.github.io/plan9port/">Plan 9 From User Space (also called &#8216;plan9port&#8217;, you will be laughed at)</a></li>
</ul></li>
</ul>

<p><a href="https://9front.org">9front</a> is really the only &#8216;usable&#8217; one because the QOL modifications add important things like general stability, git client, mercurial, ssh, various emulators, audio, WiFi, and USB support. </p>

<h1>Using 9</h1>

<p>What does the 9 experience actually look like in 2022? You put 9 in a VM, posted a screenshot, shutdown the VM, then continued using Ubuntu because you can&#8217;t play video games or easily watch videos online in 9. </p>

<p>Hardware support in 9front is expanding but still limited. <a href="http://plan9.stanleylieber.com/hardware/">Refer to the list of supported hardware.</a> I run 9front on a Thinkpad x220 and it seems to just work. Some people run it on a Raspi but I&#8217;m not sure why. It works quite well with KVM and QEMU if you&#8217;re an OS tourist. I see no reason to add a dmesg because it will either work or it won&#8217;t. </p>

<h1>Available software</h1>

<p>GNU might not be UNIX but 9 isn&#8217;t even trying to be UNIX-like. </p>

<h2>GUI</h2>

<p>Unlink UNIX, 9 was designed with graphics in mind. Some people have said that the 9 GUI looks similar to a smalltalk machine but I think it&#8217;s just the only good stacking window manager. A three button mouse is necessary for using 9front. Shift-rightclick emulates middle click. </p>

<h3>Rio</h3>

<p>Rio is the Plan 9 windowing system. It&#8217;s the successor to 8½ window manager. Rio is lightweight compared to X11 because access to graphical hardware is built into the kernel and using files+namespaces to access input devices. </p>

<p>The most brief way of explaining rio is to think of it as a rectangle multiplexer, where each rectangle is served a file interface (9p). Although rectangles might seem counterintuitive at first, thinking less hard makes it easier to use. 
I still have difficulty efficiently using a mouse-centric interface after using terminal interfaces almost exclusively for many years. I dislike the windows way of using a mouse but the 9 way seems to make quite a lot of sense when I &#8220;think less hard&#8221; and allow the intuition to take control. </p>

<p>The argument for mouse-centric computing and text editing is that it&#8217;s faster. Of course, the average vim user is editing text faster than the speed of thought but most people aren&#8217;t the average vim user. Instead, they only know how to use arrow keys to move a cursor. Without memorizing hundreds of vim bindings (and forgetting the names and birth dates of your family members in the process), obviously a mouse is faster. </p>

<p>Mouse controls are confusing at first because they follow the &#8220;click and hold, hover to option, release&#8221; to select an option. They look something like follows: </p>

<ul>
<li>Right click (window management controls)

<ul>
<li>New</li>
<li>Resize</li>
<li>Move</li>
<li>Delete</li>
<li>Hide</li>
</ul></li>
<li>Middle click (text manipulation controls)

<ul>
<li>cut</li>
<li>paste</li>
<li>snarf (copy highlighted text)</li>
<li>plumb (send highlighted text to process, or, more effectively: open file with appropriate program)</li>
<li>look (search for highlighted text)</li>
<li>send (run highlighted text as a shell command)</li>
</ul></li>
<li>scroll (toggle autoscroll&#47;noautoscroll)

<ul>
<li>The left click button is used to select text and windows. </li>
</ul></li>
</ul>

<p>The concept of mouse-chording is also prominent in rio but it&#8217;s even more difficult to explain without a visual demonstration. </p>

<p>Rio and it&#8217;s windows also support UNIX style keyboard shortcuts: </p>

<ul>
<li>&#94;-u deletes from cursor to start of line</li>
<li>&#94;-w deletes word before cursor</li>
<li>&#94;-h deletes the character before the cursor</li>
<li>&#94;-a moves the cursor to the start of the line</li>
<li>&#94;-e moves the cursor to the end of the line</li>
<li>&#94;-b moves the cursor back to the prompt</li>
<li>&#94;-f is the autocomplete key, functionally equivalent to tab completion</li>
<li>&#94;? (DEL key) is the equivalent to &#94;-c on UNIX</li>
</ul>

<p>Additionally, in a text window, the arrow keys and PgUp&#47;PgDown keys behave as expected. The home&#47;end keys scroll the window to the top&#47;bottom of the text buffer respectively. </p>

<p>These text windows have a built in pager so there is no <code>more</code> or <code>less</code> command. I can&#8217;t decide if I like built in paging but it&#8217;s definitely a thing to think about. </p>

<p>The colorscheme of rio is dull and pastel and this is intentional. Less vibrant color schemes seem to fade away and become less obvious. Color themes like Tango, Linux Console, Solarized, all of KDE, and WIndows XP are very obvious but not in a good way. Bright colors are subtly distracting and make it difficult to concentrate. When I&#8217;m configuring a UNIX system with dwm, I borrow Rio&#8217;s color theme because it&#8217;s an anti-theme. Give it time. It&#8217;s charming in it&#8217;s own way. 
Modifying the source code for rio allows for custom color themes. It&#8217;s possible but you will be laughed at. Setting a wallpaper is also possible but I don&#8217;t do this because my windows are always covering the dull gray background.</p>

<p>As for X11, the equis X11 server can only be run via linux compat layers. The lack of a viable X server is yet another reason 9 has no programs. </p>

<h2>Command Line Utilities</h2>

<p>The shell on 9 is called <code>rc(1)</code>. It&#8217;s like any other shell you&#8217;ve used except that you expect it to be bourne-like but it isn&#8217;t. Standard UNIX shell concepts like pipes, file redirects, &#38;&#38; and ||, etc. Scripting is not POSIX-like at all so reading the man page and various scripts written in <code>rc</code> is the only way to learn. </p>

<p>Other various UNIX utilities exist and function as expected (although some of the ones you would like are missing). <code>awk</code>, <code>grep</code>, <code>sed</code>, <code>cat</code>, <code>tar</code>, <code>gzip</code>, <code>ed</code>, etc are present. </p>

<h2>Editors</h2>

<p>There are three primary ways of editing text on 9: <code>ed(1)</code>, <code>sam(1)</code>, and <code>acme(1)</code>. There is no <code>vi</code> aside from the MIPS emulator, there is no <code>emacs</code> except for a man page explaining why there is no emacs. </p>

<p>I have primarily used acme in the past, but sam is a much better editor. </p>

<p><code>sam</code> is a lot like a graphical version of <code>ed</code>. I still need to learn <code>ed</code> because it&#8217;s the standard editor. Some of the standard <code>vi</code> commands are available and regex works. I like sam quite a lot but it seems to corrupt files when the system crashes. </p>

<p><code>acme</code> is a window manager, file browser, terminal emulator, and email client that some people use as a text editor. The coolest part about acme is the ability to write arbitrary editor and system commands in the menu bar, highlight them, then middle click to execute those commands. </p>

<h2>(Some of the ) Supported Networking Protocols</h2>

<ul>
<li>IMAP

<ul>
<li>good luck</li>
</ul></li>
<li>NTP</li>
<li>IRC 

<ul>
<li>ircrc</li>
<li>other non-default implementations exist</li>
</ul></li>
<li>FTP</li>
<li>HTTP

<ul>
<li><code>mothra</code> is the standard web browser. It does not support CSS or all of the HTML tags. Obviously, javascript is unsupported. </li>
<li><code>abaco</code> exists. I&#8217;ve used it a few times. It renders slightly better than mothra but is a pain to use. </li>
<li>Various inferno vaporware exists but the ports don&#8217;t work</li>
<li>NetSurf has been ported to 9front by leveraging components of APE. It almost works</li>
<li><code>hget</code>, like curl</li>
</ul></li>
<li>SSH

<ul>
<li>it only works in conjunction with the <code>vt(1)</code> command. </li>
<li>sshfs</li>
<li>sshnet for proxying traffic</li>
</ul></li>
<li>VNC</li>
<li>Various torrent software (magnet links not supported)</li>
<li>Drawterm

<ul>
<li>no, good luck, you will be laughed at</li>
</ul></li>
<li>Of course, 9p</li>
</ul>

<h3>A Security aside</h3>

<p>Various server implementations for these protocols exist but you really shouldn&#8217;t use them on the WAN as they are ancient, unmaintained, unaudited, and easy to exploit. Prime example: the &#47;g&#47;entoomen found a path traversal vulnerability in the 9front httpd server, then leveraged that vuln to exploit a vuln in the authentication system. Not that the boys back home did anything malicious with this bug &#8230; but the ability to pwn a system by sending cleverly crafted GET requests should tell you enough about the current state of security in 9. </p>

<ul>
<li>Firewall

<ul>
<li>no</li>
</ul></li>
<li>Disk Encryption

<ul>
<li>unreliable</li>
</ul></li>
<li>Access control

<ul>
<li>what?</li>
</ul></li>
<li>filesystem

<ul>
<li>cwfs has an poorly documented special user called <code>none</code> that is allowed to connect to fossil, cwfs, and maybe hjfs without a password. Set the <code>nonone</code> option in cwfs if you are even thinking about putting 9 on the internet. </li>
</ul></li>
</ul>

<p>Don&#8217;t even think about putting 9 on the internet</p>

<h2>UNIX compat layer (ape)</h2>

<p>APE is the ANSI POSIX Emulator. It doesn&#8217;t work and is almost entirely empty. Lots of tiny programs to write, not much interest in writing lots of tiny program. There is a general attitude among 9 users that &#8220;9 is unique&#8221; porting POSIX libs to 9 would ruin the appeal. I almost think I agree with this sentiment. </p>

<h2>Secstore and Factotum</h2>

<p>similar to a password manager that stores things in nvram. I haven&#8217;t used it for anything other than building an auth server but it&#8217;s something for me to research more. </p>

<h2>Emulation</h2>

<ul>
<li>Linux

<ul>
<li>don&#8217;t</li>
</ul></li>
<li>GameBoy </li>
<li>GameBoyAdvance </li>
<li>NES </li>
<li>SNES </li>
<li>Sega MegaDrive&#47;Genesis </li>
<li>c64</li>
<li>vmx, a PC emulator (effectively virtualization)

<ul>
<li>It&#8217;s slow</li>
<li>it almost works</li>
<li>it crashes your system</li>
<li>cwfs gets corrupted</li>
<li>&#8220;runs&#8221; OpenBSD, Linux, and ancient Windows with graphics support</li>
</ul></li>
<li>and also various emulators for obscure architectures</li>
</ul>

<h2>VCS</h2>

<ul>
<li>Mercurial used to come with 9front but it has been removed.</li>
<li>CVS does exist but not in the base system. </li>
<li>A native git implementation exists and is in the base system. It&#8217;s bare bones but it mostly works. </li>
</ul>

<h2>Community Maintained Software</h2>

<p><a href="https://contrib.9front.org">The 9front community has been collecting known programs for some time</a> and <a href="https://wiki.9front.org/extra">various other community software can be found in the wiki</a>. Both are served as a ports system, similar to a BSD style ports system. There are no binary packages. Makefiles are broken. </p>

<h2>Programming Languages</h2>

<h3>mkfiles</h3>

<p>9 ships a program called <code>mk(1)</code>. Syntax (in the simplest ways) is identical to UNIX <code>make(1)</code>. </p>

<h3>The Absurdities of 9 C</h3>

<p>Plan 9 C is syntactically similar to ANSI C but it varies. The stdlibs on 9 are much simpler than the POSIX monster. </p>

<pre><code>&#47;* POSIX C example *&#47;
#include &#60;stdio.h&#62;

int main(){
    printf("hello, world\n");
    return 0;
}
</code></pre>

<pre><code>&#47;* 9 C example *&#47;
#include &#60;u.h&#62;
#include &#60;libc.h&#62;

void main(){
    print("hello, world\n");
    exits(0);
}
</code></pre>

<p><code>u.h</code> contains CPU specific instructions, <code>libc.h</code> contains all of the system calls, time functions, math functions, unicode functions, and print functions. In contrast to POSIX, functions in 9c return strings instead of ints. </p>

<pre><code># Compiling on UNIX
$ cc main.c
$ .&#47;a.out
hello, world
$
</code></pre>

<pre><code># Compiling on 9
% 6c main.c
% 6l main.6
% 6.out
hello, world
%
</code></pre>

<p>In the 9 compiler example, I&#8217;m using the amd64 compiler and linker. Notice how the <code>6</code> persists as the prefix&#47;suffix to help developers remember which architecture this specific program is written for. Instead of unspecific object files with a <code>.o</code> suffix, the object file&#8217;s suffix is actually representive of what types of opcodes the file contains. Similarly, after linking, the <code>6.</code> prefix tells us that the binary is for an amd64 processor. </p>

<p>And also, the simplest UNIX program with buffers: read from stdin and write directly to stdout: </p>

<pre><code>&#47;* POSIX C *&#47;
#include &#60;stdio.h&#62;

int main(int argc, char *argv[]){
    char buf[32];
    size_t bufs = sizeof(char)*32;
    size_t nread = 0;

    while((nread = fread(buf, 1, bufs, stdin)) &#62; 0){
        fwrite(buf, 1, nread, stdout);
    }

    return 0;
}
</code></pre>

<pre><code>&#47;* Plan 9 C *&#47;
#include &#60;u.h&#62;
#include &#60;libc.h&#62;

void main(int argc, char *argv[]){
    char buf[32];
    char bufs = sizeof(char)*32;
    int nread = 0;

    while((nread = read(0, buf, bufs)) &#62; 0){
        write(1, buf, nread);
    }

    exits(0);
}
</code></pre>

<p>In 9, <code>stdin</code> is file descriptor <code>0</code>, <code>stdout</code> is <code>1</code>, and <code>stderr</code> is <code>2</code>.</p>

<p>And, the binary sizes betwen the two. You probably recognize <code>a.out</code>, this one was compiled with GCC. <code>6.out</code> is an amd64 Plan 9 binary compiled on 9. </p>

<pre><code>$ ls -sh .&#47;*.out
4.0K .&#47;6.out
 28K .&#47;a.out
</code></pre>

<p>Binaries on plan 9 are statically linked. It&#8217;s somewhat strange to see that a statically linked binary is smaller than a dynamically linked one. Even compiling the plan 9 source on Linux using plan9port yeilds a large binary: 40K. </p>

<p>I have not written 9C in a long time so I cannot say much more with confidence and authority. Refer to <a href="https://doc.cat-v.org/plan_9/programming/c_programming_in_plan_9">C Programming in Plan 9 from Bell Labs</a> for more information. </p>

<p>The <code>acid(1)</code> debugger exists but it&#8217;s hard to use if you&#8217;re not fluent in assembly. </p>

<h3>Ancient Go</h3>

<p>Ancient Go once ran on 9. In 2022, you&#8217;re better off just writing C and rc. </p>

<h1>Why isn&#8217;t 9 more popular if it supposedly improves on &#8220;bad Unix ideas&#8221;?</h1>

<ul>
<li>Unix is &#8216;just good enough&#8217;</li>
<li>9 is not &#8216;better enough&#8217; to beat out &#8216;just good enough&#8217;</li>
<li>Porting software is difficult<sup>impossible</sup> because 9 was deliberately written to be <em>not</em> backwards compatible. 

<ul>
<li>&#8220;If you port it, they will come&#8221;</li>
</ul></li>
<li>9 is uncomfortable to use if you have Unix muscle memory</li>
<li>no modern web browser</li>
<li>no video games (I&#8217;m pretty sure there are doom and quake source ports though)</li>
<li>multimedia consumption is hard</li>
<li>no GNU</li>
</ul>

<h1>Why do people use 9 if it&#8217;s so bad?</h1>

<p>I can&#8217;t be sure about all other ~20 Plan 9 fans in the world, but for myself, it&#8217;s purely out of a genuine curiosity and love for computing. My motivation for learning obscure, unnecessary, and quite frankly boring things related to computers is that it brings me some sense of satisfaction&#47;accomplishment&#47;enjoyment. Linux stopped being fun for me when I came to the realization that all distributions are fundamentally the same. I started exploring the BSD world only to realize that all UNIX-like operating systems are fundamentally the same. Although BSD remains a store of fun for me, I occasionally feel burned out on UNIX even if it&#8217;s an abstract idea&#47;experience&#47;codebase I cherish. </p>

<p>When I sit down at a computer my goal is always to discover something new, learn a new concept, explore alternative paradigms, and, most of all, to have fun in the process.</p>

<p>For most people, 9 is a tourist experience. For me, it&#8217;s the final frontier. Although I have yet to learn as much about 9 as I have about UNIX, every time I swap hard drives and boot into 9 I feel a sense of coming home. Sometimes I think I am wilfully resisting becoming a 9 expert because it will result in me struggling to find the next non-bad OS paradigm to explore. </p>

<p>And when I think about &#8220;using a computer&#8221;, what do I really do on the computer? I learn about it, learn about the software running on it, and proceed to write about it so that I can reinforce the ideas in a Feynman-esque way. I&#8217;m not really providing a real tangible value to the world because it&#8217;s purely a &#8220;hey, here&#8217;s the things I learned the hard way so you don&#8217;t have to&#8221;. </p>

<h1>Conclusion:</h1>

<blockquote>
<p>How do I do xyz on 9?</p>
</blockquote>

<p>don&#8217;t. search engines won&#8217;t help. Man pages won&#8217;t help. &#47;sys&#47;doc might help. Reading the source code won&#8217;t help. have fun :)</p>

<p>Or consider: </p>

<pre><code>term% vt -xb
term% ssh user@host
$ tmux a
$ reset
# some commands 
$ reset
# some commands 
$ reset
</code></pre>

<p>Alternatively: </p>

<pre><code>term% vncv host:display
</code></pre>

<h1>Further reading:</h1>

<ul>
<li><a href="https://fqa.9front.org">9front FQA</a>. Very humorous, good information</li>
<li>read the papers in <code>&#47;sys&#47;doc</code> or <a href="https://doc.cat-v.org/plan_9/2nd_edition/papers/">on cat-v.org</a></li>
<li><a href="https://youtu.be/6m3GuoaxRNM">Plan 9: Not dead, Just resting</a></li>
<li><a href="https://youtu.be/Dt3Dr3jUPjo">A visual demonstration of rio</a></li>
<li><a href="https://youtu.be/dP1xVpMPn8M">A visual demonstration of acme</a></li>
<li><a href="https://doc.cat-v.org/plan_9/programming/c_programming_in_plan_9">C Programming in Plan 9 from Bell Labs</a></li>
<li><a href="https://pspodcasting.net/dan/blog/2019/plan9_desktop.html#conclusion">Plan 9 Desktop Guide</a>. Might be useful for someone. Not too useful for me. Man pages are better. </li>
<li><a href="https://www.youtube.com/user/C04tl3">C04tl3 youtube channel</a>. Lots of cool videos with information. </li>
<li><a href="https://archive.org/details/plan9designintro">Introduction to Operating System Abstractions using Plan 9 from Bell Labs </a></li>
</ul>
</div>
</div>
<?php include "../includes/footer.html"; ?>
