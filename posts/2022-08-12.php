<?php include "../includes/header.html"; ?>
<title>Using pf tables to drop packets from entire IP blocks</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Using pf tables to drop packets from entire IP blocks</h1>
<small>or, <em>the skiddies will have to get crafty</em></small>

<p> Every so often, I read through my httpd access logs to see what people are up to. Most of the time it's normal users but occasionally it's automated scanners looking for vulnerabilities. I run something like : </p>

<pre><code>$ cat /var/www/logs/access.log | grep -v 200\ 0 | sort | less
</pre></code>

<p> I then page through the file, looking at the types of things the scanners are trying to hit. Sometimes I run a <code>whois</code> on the IP addresses to see where the spam is coming from. The sources, in my case, are <b>constantly, always, and almost exclusively</b> IP addreses in Ukraine, Russia, China, and Hong Kong. </p> 

<p> I've had enough, it's time to start blocking spam so that normal users can access my website without waiting for httpd to finish responding to some guy submitting hundreds of requests per second for resources that don't even exist. Sometimes the spam is so bad that even I see error pages from httpd when the server is overloaded. When I try to log in to see what's happening, there is severe ssh latency. Blocking entire countries doesn't solve the hacker issue (wow, state sponsored hackers know about tor!?!?!). The goal is to reduce the total amount of bullshit requests per second, thus reducing overall load and improving the load times for legitimate users. </p>

<p> I found a few tools that help admins find IP blocks by country. My server is listening on IPv4 and IPv6 so I decided to make a single megalist with both types of addresses. <a href="https://www.countryipblocks.net/acl.php">This handy site will help you generate a list of IPv4 blocks by country</a>, <a href="https://www.countryipblocks.net/ipv6_acl.php">And this is their IPv6 counterpart.</a> I just grabbed the apache format and used vim to remove the parts I don't need. The table file is <b>a newline delimited a of ip addresses/ranges</b></p>

<p> The easiest way to create a table using pf is to make a file with a list of ip addresses or ranges. Taking the generated lists from the links above, I made a new file called <code>/etc/statesponsoredhackers.table</code>. It looks something like: </p>

<pre><code>0.0.0.0/24
0.0.0.0/16
0.0.0.0/8
0000:0000::/24

# and so on and so forth for another 60 thousand lines
</code></pre>

<p> In order to use this table in pf, we need to add a few rules to <code>/etc/pf.conf</code>. I added something like this in the section where I to tables. </p>

<pre><code># tables  
table &lt;whitelist&gt; persist file "/etc/whitelist.table"
pass in all from &lt;whitelist&gt; to any

table &lt;statesponsoredhackers&gt; persist file "/etc/statesponsoredhackers.table"
block in quick from &lt;statesponsoredhackers&gt; to any
</code></pre>

<p> Refreshing the ruleset and checking that the rules actually installed: </p>
<pre><code>$ pfctl -e /etc/pf.conf
$ pfctl -t statesponsoredhackers -T show

# a list of the 60 thousand ip addresses now prints
</code></pre>

<p> OpenBSD's pf uses a "drop" rather than "return" block policy by default. This means my server won't even response to pings from any of the blocked IP addresses. </p> 

<p> The last bit of advice: put the <code>block in quick</code> section before the rest of your firewall rules. The <code>quick</code> keyword stops processing additional rules as soon as there is a match. Obviously, you would need to put the whitelist section above any of the <code>quick</code> sections so that it's not ignored. </p> 

</div>
</div>
<?php include "../includes/footer.html"; ?>
