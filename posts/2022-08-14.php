<?php include "../includes/header.html"; ?>
<title>Upgrading the bios on an x220 from Linux</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Upgrading the bios on an x220 from Linux</h1>
<p> This is something I don't do often. I did it once before. That thinkpad no longer runs. This (new to me) thinkpad had an ancient BIOS. I refuse to go through the anguish that is installing windows just to upgrade the bios. Luckily, lenovo provides bootable CD images that make it easy to upgrade the bios. Unluckily, I'm unaware of any x220 with a CD drive. Converting the iso to a bootable disk USB is simple enough. </p>

<h2> 1. Get firmware </h2>
<p> There are other firmwares for other models. I'm sure the process is almost identical. </p>
<pre><code>$ wget https://download.lenovo.com/pccbbs/mobiles/8duj31us.iso
</code></pre>

<h2> 2. Convert nonsense formats to something that works on a bootable USB </h2>
<p> substitute <code>sdx</code> for the appropriate drive. Don't flash your hard drive. Use <code>fdisk -l</code> or <code>lsblk</code> to find the right one. 
<pre><code>$ sudo pkg install geteltorito
$ geteltorito -o x220bios.img 8duj32us.iso
$ sudo umount /dev/sdx*
$ sudo dd if=./x220bios.img of=/dev/sdx
$ sudo eject /dev/sdx
</code></pre>

<h2> 3. Booting from the USB </h2> 
<p> insert the usb into the x220, reboot, mash the blue 'ThinkVantage' button, press '<code>F12</code>', then use the arrow keys to select the "USB" option or something similar. </p>

<figure>
<img alt="If you've done everything correctly, you will see this screen" src="assets/images/x220bios1.png">
<figcaption>If you've done everything correctly, you will see this screen</figcaption>
</figure>

<p> To proceed with the upgrade, select option 2 and follow the instructions. The system will power cycle a few times but eventually the upgrade will be complete. Once it's started, leave it be for a few minutes. You can safely remove the usb flash drive when the system boots back into your OS. </p> 
</div>
</div>

<h1> UPDATE: adding a custom bootsplash </h1>
<p> You can replace the "Lenovo" logo with your own. This image must be encoded as a .BMP, .GIF, or .JPG, have a file size of 30K or less, and have a resolution of less than or equal to 640x480. </p> 

<pre><code>$ sudo su
# mount /dev/sdx1 /mnt
# cd /mnt/FLASH
# cat README.TXT | less
# cp /path/to/NEW_LOGO.GIF ./LOGO.GIF
</code></pre>

<p> And now we will modify the batch file, <code>lcreflsh.bat</code>. Make it look like this: </p>

<pre><code>@echo off
flash2.exe /U /LOGO:LOGO.GIF
</code></pre>

<p> And now to unmount the drive and follow the instructions above flash the bios on the x220: </p>
<pre><code># umount /dev/sdx*
# eject /dev/sdx*
</code></pre>

<figure>
<img alt="This is what my bootsplash looks like now" src="assets/images/x220bios2.png">
<figcaption>This is what my bootsplash looks like now. </figcaption>

</figure>


<?php include "../includes/footer.html"; ?>
