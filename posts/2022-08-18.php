<?php include "../includes/header.html"; ?>
<title>Securely erasing an ssd with hdparm on Linux</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Securely Erasing an SSD with hdparm on Linux</h1>

<p> The good thing about owning an SSD drive is the speed of IO. The bad thing about owning an SSD is that, due to wear leveling, a simple <code>dd if=/dev/urandom of=/dev/sdx</code> won't actually wipe all of the data on the drive. Do this from a live USB image so you don't fuck it up. </p>

<p> The first step is to unfreeze the ssd. I don't know what this means but it won't let you nuke the drive. </p>

<pre><code>$ sudo su
# dnf install hdparm
# hdparm -I /dev/sda | grep frozen
		frozen
</code></pre>

<p> Now, suspend and unsuspended the system. For some reason, this sets the frozen state to unfrozen. </p> 

<pre><code># hdparm -I /dev/sda | grep frozen
		not	frozen
</code></pre>

<p> Now, set the password. DO NOT REBOOT AFTER THIS STEP. </p>

<pre><code># hdparm --user-master u --security-set-pass pass /dev/sda
security_password: "pass"

/dev/sda: 
 Issuing SECURITY_SET_PASS command, password="pass", user=user, mode=high
</code></pre>

<p> To check if the master password is actually set: </p>
<pre><code># hdparm -I /dev/sda 
[ lots of nonsense ] 
Security: 
	Master password revision code = 65534
		supported
		enabled
[ more nonsense ] 
</code></pre>

<p>To check which erase types are supported, do: </p>
<pre><code># hdparm -I /dev/sda | grep ERASE
# </code></pre>

<p> To actually erase the drive, run: </p>
<pre><code># hdparm --user-master u --security-erase-enhanced pass /dev/sda
</code></pre>


<p> To check if the drive was actually erased: </p>
<pre><code># hdparm -I /dev/sda 
[ lots of nonsense ] 
Security: 
	Master password revision code = 65534
		supported
	not	enabled
[ more nonsense ] 
</code></pre>

<p> Now, reboot the system and pray. After a reboot, the drive should not contain any useful data. </p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
