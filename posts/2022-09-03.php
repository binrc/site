<?php include "../includes/header.html"; ?>
<title>9front webserver</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>9front on the web</h1>

<h1>Vultr</h1>

<ol>
<li>upload iso </li>
<li>add vps</li>
<li>open vnc console</li>
<li>install as normal</li>
<li>reboot</li>
<li>remove iso</li>
<li>reboot again</li>
</ol>

<hr>

<h1>set up a CPU server</h1>

<h2>Add users to file server</h2>

<pre><code>% con -C &#47;srv&#47;cwfs.cmd 
newuser &lt;ExampleUser&gt; 
newuser sys +&lt;ExampleUser&gt;
newuser adm +&lt;ExampleUser&gt;
newuser upas +&lt;ExampleUser&gt;
</code></pre>

<h2>set up environment</h2>

<p>reboot, set <code>user=&lt;ExampleUser&gt;</code> for the bootarg, then run <code>&#47;sys&#47;lib&#47;newuser</code></p>

<h2>set up headless booting</h2>

<pre><code>% 9fs 9fat
</code></pre>

<p>edit <code>&#47;n&#47;9fat&#47;plan9.ini</code></p>

<pre><code>bootfile=9pc64
nobootprompt=local!&#47;dev&#47;sdC0&#47;fscache
mouseport=ps2
monitor=vesa
vgasize=1024x768x14
user=&lt;ExampleUser&gt;
tiltscreen=none
service=cpu
</code></pre>

<h2>Add hostonwer info to nvram</h2>

<pre><code>% nvram=&#47;dev&#47;sdF0&#47;nvram auth&#47;wrkey
bad nvram des key
bad authentication id
bad authentication domain
authid: &lt;ExampleUser&gt;
authdom: 9front
secstore key: &#60;press the return key if you do not want to type this at boot time&#62;
password: &#60;make it 8 chars&#62;
</code></pre>

<h2>Auth server</h2>

<pre><code>% auth&#47;keyfs
% auth&#47;changeuser &lt;ExampleUser&gt;
Password: &#60;what you put earlier&#62;
Confirm password:
Assign new Inferno&#47;POP secret? [y&#47;n]: n
Expiration date (YYYYMMDD or never) [never]: never
Post id:
User&#39;s full name: 
Department #:
User&#39;s email address:
Sponsor&#39;s email address:
user &lt;ExampleUser&gt; installed for Plan 9
</code></pre>

<h2>Add permissions</h2>

<p>append to <code>&#47;lib&#47;ndb&#47;auth</code></p>

<pre><code>hostid=&lt;ExampleUser&gt;
uid=!sys uid=!adm uid=*
</code></pre>

<p>then reboot</p>

<h2>Test if it worked with drawterm</h2>

<pre><code>$ &#47;opt&#47;drawterm -u binrc -h example.com -a example.com
</code></pre>

<hr>

<h1>set up rc-httpd</h1>

<p>edit <code>&#47;rc&#47;bin&#47;rc-httpd&#47;select-handler</code></p>

<pre><code>#!&#47;bin&#47;rc
PATH_INFO=$location

        switch($SERVER_NAME) {
        case example.com
               FS_ROOT=&#47;usr&#47;binrc&#47;www&#47;$SERVER_NAME
               exec static-or-index

        case *
              error 503
}
</code></pre>

<p>To listen on port 80 and run the handler on port 80: </p>

<pre><code>% cp &#47;rc&#47;bin&#47;service&#47;!tcp80 &#47;rc&#47;bin&#47;service&#47;tcp80
% chmod +x &#47;rc&#47;bin&#47;rc-httpd&#47;select-handler
</code></pre>


<p> Reboot and test. </p>
<hr> 

<h1>set up werc</h1> 
 
<pre><code>% cd  
% mkdir /sys/www && cd www 
% hget http:&#47;&#47;werc.cat-v.org&#47;download&#47;werc-1.5.0.tar.gz 
% tar xzf werc-1.5.0.tar.gz  
% mv werc-1.5.0 werc 
 
# ONLY DO THIS IF YOU *MUST* RUN THE THINGS THAT ALLOW WERC TO WRITE TO DISK 
# EG. DIRDIR, BLAGH, ETC
# DON'T DO THIS, JUST USE DRAWTERM OVER THE NETWORK
# HTTP CLIENTS SHOULD NEVER BE ALLOWED TO WRITE TO DISK
# PLEASE I BEG YOU
% cd .. && for (i in `{du www | awk '{print $2}'}) chmod 777 $i 
 
% cd werc&#47;sites&#47; 
% mkdir example.com 
% mv default.cat-v.org example.com 
</code></pre> 
 
<p>now re-edit <code>&#47;rc&#47;bin&#47;rc-httpd&#47;select-handler</code></p> 
 
<pre><code>#!/bin/rc 
WERC=/sys/www/werc 
PLAN9=/ 
PATH_INFO=$location 
switch($SERVER_NAME){ 
case cirno 
        FS_ROOT=$WERC/sites/$SERVER_NAME 
        exec static-or-cgi $WERC/bin/werc.rc 
case * 
        error 503 
} 
</code></pre> 


<hr>
<h1>SSL</h1>

<p>no. </p>

<p> Actually, self signed is a yes. </p> 

Generate and install: 

<pre><code>% ramfs -p
% cd /tmp
% auth/rsagen -t 'service=tls role=client owner=*' &gt; key
% chmod 600 key
% cp key /sys/lib/tls/key 
% auth/rsa2x509 'C=US CN=example.com` /sys/lib/tls/key | auth/pemencode CERTIFICATE &gt; /sys/lib/tls/cert
% mkdir /cfg/$sysname
% echo 'cat /sys/lib/tls/key &gt;&gt; /mnt/factotum/ctl' &gt;&gt; /cfg/$cycname/cpustart
</code></pre>

Now add a listener in <code>/rc/bin/service/tcp443</code>:
<pre><code>#!/bin/rc
exec tlssrv -c /sys/lib/tls/cert -l /sys/log/https /rc/bin/service/tcp80 $*
</code></pre>

And make it executable: 
<pre><code>% chmod +x /rc/bin/service/tcp443
</code></pre>

</div>
</div>
<?php include "../includes/footer.html"; ?>
