<?php include "../includes/header.html"; ?>
<title>Flashing a Netgear R6080 router with OpenWRT</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Flashing a Netgear R6080 router with OpenWRT</h1>

<p> Install tooling </p>

<pre><code>[user@fedora ~]$ mkdir netgear
[user@fedora netgear]$ cd netgear/
[user@fedora netgear]$ git clone https://github.com/jclehner/nmrpflash
[user@fedora netgaer]$ cd nmrpflash/
[user@fedora nmrpflash]$ sudo dnf install libpcap-devel libnl3-devel 
[user@fedora nmrpflash]$ make
[user@fedora nmrpflash]$ wget https://downloads.openwrt.org/releases/22.03.0/targets/ramips/mt76x8/openwrt-22.03.0-ramips-mt76x8-netgear_r6080-squashfs-factory.img
</code></pre>

<p> Flash firmware </p>
<p> connect the router to the PC with an ethernet cable. This will not work if you connect the cable to the WAN port, you must use one of the switch ports. </p>

<pre><code>[user@fedora nmrpflash]$ ifconfig
[user@fedora nmrpflash]$ ./nmrpflash -L
[user@fedora nmrpflash]$ sudo ./nmrpflash -i enp0s31f6 -f ./openwrt-22.03.0-ramips-mt76x8-netgear_r6080-squashfs-factory.img 
# wait
[user@fedora nmrpflash]$ ssh root@192.168.1.1
</code></pre>

<p> With a shell on the router: </p>

<pre><code>root@R6080:~# jffs2reset
root@R6080:~# reboot
# wait
</code></pre>

<p> reconnect: </p>
<pre><code>[user@fedora nmrpflash]$ ssh root@192.168.1.1
# remove old keys, keys changed
[user@fedora nmrpflash]$ vim ~/.ssh/known_hosts
[user@fedora nmrpflash]$ ssh root@192.168.1.1
</code></pre>

<p> Update and install a web ui: </p>
<p> Connect the router to WAN (other router->WAN eth port or modem->WAN eth port)</p>
<pre><code>root@R6080:~# opkg update
root@R6080:~# opkg install luci
</code></pre>


<p> resume configuration via the web ui </p>
<pre><code>[user@fedora nmrpflash]$ firefox 192.168.1.1
</pre></code>

<h1> Post-install setup </h1>
<p><a href="https://openwrt.org/toh/netgear/r6080">As per the documentation</a>, default configuration might be limiting. Things you must manually do include: </p>
<ul>
<li> set a root password </li>
<li> enable wireless interfaces </li>
<li> set up crypto on said interfaces</li>
</ul>


</div>
</div>
<?php include "../includes/footer.html"; ?>
