<?php include "../includes/header.html"; ?>
<title>Flashing a Netgear R7000 router with FreshTomato</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Flashing a Netgear R7000 router with FreshTomato</h1>


<h1> Initial setup </h1>
Sadly, the R7000 model uses broadcom chips. Looks like OpenWRT cannot save us from OEM hellworld any longer. 

<p> This router is entirely new hardware for me. All of these instructions are from a fresh factory image. Factory reset your R7000. It makes things simpler. </p>

<p> Connect the router to a PC using an eth cable. Do not connect the PC to the WAN port, connect the PC to one of the switch ports. </p>

<p> The first step is to configure the router <i>as if you were a clueless user who just uses OEM firmware cluelessly</i>. 
<img class="posts" src="assets/images/R7000-freshtomato-1.png" alt="Configurint the router just to flash it and reconfigure it">

<p> Wow, ads in the web UI. Technology was a mistake. </p>
<img class="posts" src="assets/images/R7000-freshtomato-2.png" alt="Ads in the web ui">


<p> The R7000 is a strange model that requires us to flash two separate files in a 2 stage flash process. You can use a wget commands if you trust me or just browse the FreshTomato website and download them manually. </p>


<pre><code>[user@fedora nmrpflash]$ wget https://freshtomato.org/downloads/freshtomato-arm/2022/2022.5/Netgear%20initial%20files/freshtomato-R7000-2022.5-initial-64K.zip 
[user@fedora nmrpflash]$ wget https://freshtomato.org/downloads/freshtomato-arm/2022/2022.5/K26ARM/freshtomato-R7000-ARM_NG-2022.5-AIO-64K.zip
[user@fedora nmrpflash]$ for i in ./*.zip; do unzip $i; done
[user@fedora nmrpflash]$ ifconfig
[user@fedora nmrpflash]$ firefox 192.168.1.1
</pre></code>

<p> And the rest is webshit. I apologize for inaccessibility. </p>

<h1> Stage 1 flash </h1>

<p> In the web UI, navigate to <code>Advanced &gt; Administration &gt; Router Update</code>. Upload the <code>.chk</code> file. </p>

<img class="posts" src="assets/images/R7000-freshtomato-3.png" alt="Upload the .chk file">

<p> Verify that you know what you're doing. </p>
<img class="posts" src="assets/images/R7000-freshtomato-4.png" alt="confirmation dialog for nuking the firmware">

<p> Wait for the update to apply </p> 
<img class="posts" src="assets/images/R7000-freshtomato-5.png" alt="progress bar for the firmware update">

<p> Wait for the router to reboot </p>
<img class="posts" src="assets/images/R7000-freshtomato-6.png" alt="message that the router is rebooting">

<h2> Stage 2 flash </h2>
<p> Wait for the router to reboot. Repeatedly refresh the page. When the login prompt appears, reset the router using the small reset button on the back. I use a sim card slot ejector key but a paperclip works also. Hold for 10ish seconds then release. </p>

<p> The login prompt looks like this: </p>
<img class="posts" src="assets/images/R7000-freshtomato-7.png" alt="Router login prompt.">

<p>After this, repeat the spamming refresh process until a login prompt appears. Now you can log in with: </p>
<pre><code>user = root
pass = admin
</code></pre>

<p>After logging in, you might have to remove the <code>/cgi-bin/luci</code> path from the URL. </p>


<p> strange path </p>
<img class="posts" src="assets/images/R7000-freshtomato-8.png" alt="url path includes cgi-bin/luci so the interface isn't displaying properly">

<p> Your router is now freed from OEM advertisers </p>
<img class="posts" src="assets/images/R7000-freshtomato-9.png" alt="freshtomato web admin pannel">

<p> Now we need to clean out all of the OEM garbage from the NVRAM. Go to <code>Administration &gt; Configuration</code> and reset the NVRAM. </p> 
<img class="posts" src="assets/images/R7000-freshtomato-10.png" alt="wiping the NVRAM">

<img class="posts" src="assets/images/R7000-freshtomato-11.png" alt="waiting for NVRAM to finish wiping">

Go back to the Router's web page. You might have to get a new DHCP release with 
<pre><code>[user@fedora netgear]$ sudo killall dhclient && dhclient enp0s31f6
</code></pre>

<p>Go to <code>Administration &gt; Upgrade </code> and upload the <code>.trx</code> file.</p>
<img class="posts" src="assets/images/R7000-freshtomato-12.png" alt="uploading the trx file">
<img class="posts" src="assets/images/R7000-freshtomato-13.png" alt="waiting for firmware updates to finish">
<p>Wait for it reboot.</p>
<img class="posts" src="assets/images/R7000-freshtomato-14.png" alt="waiting for reboots again">



<p> You might have to kill and restart dhclient again to get back to the web interface. </p>
<img class="posts" src="assets/images/R7000-freshtomato-15.png" alt="updated freshtomato web ui">

<p> After all is said and done, we now have ssh access to our router like RMS intended. </p> 

<pre><code>
[user@fedora netgear]$ ssh root@192.168.1.1

FreshTomato 2022.5 K26ARM USB AIO-64K
size: 40314 bytes (25222 left)
 ======================================================== 
 Welcome to the Netgear R7000 [FreshTomato]
 Date: Thu Jan 1 1970
 Time: 01:21:04 up 21 min
 Load average: 0.00, 0.01, 0.02
 Mem usage: 9.4% (used 23.53 of 249.59 MB)
 LAN1: 192.168.1.1/24 @ DHCP:  -
 WL0 : 2.4GHz @ FreshTomato24 @ channel: 6 @ XX:XX:XX:XX:XX:XX
 WL1 : 5GHz @ FreshTomato50 @ channel: 36 @  XX:XX:XX:XX:XX:XX
 ======================================================== 

root@unknown:/tmp/home/root# 
</pre></code>

<p> After installation, I do things like change the theme to something less ugly, disable telnet, set a better root password, set wireless encryption, etc. </p>


</div>
</div>
<?php include "../includes/footer.html"; ?>
