<?php include "../includes/header.html"; ?>
<title>Social Media Revisited</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Social Media Revisited</h1>
<p> Recently, I remembered a series of old friends and the only method of connection I had to them: largely forgotten social media accounts. These friends were real life friends so I decided to make a post discussing my move towards <i>private personhood</i> and how it relates to the internet followed up with something like "send me a message if you want my email address or phone number". During this short, day long process of waiting for the account deletion deadline I realized a few things: </p> 

<ol>
<li> Social media still doesn't appeal to me </li>
<li> The addictive behaviors reform almost instantly </li>
<li> Most of these people don't even remember you, let alone care enough to go out of their way to send you an email </li>
</ol>

<p> And here is the elaboration: </p>

<p> Even in revisiting a few of these forgotten accounts, not for one second did I regret the decision to grab all the willing contacts and delete the accounts. The user interfaces are gaudy, unintuitive, and steer users towards self-centered consumerism. Not to mention the ocular assault of advertisements that I have become so resensitized to. The entire concept of throwing personal information into a crowd of disinterested people is still entirely unappealing. </p>

<p> In addition to the uncomley nature of the UI, the usage paradigms equally dissuade me. After logging back into these services, I almost instantly fell back into the "check for new stuff every x minutes" mindset. Noticing this mindset, I stopped myself and decided to wait until my timer was up before re-checking. I'm not sure how they do it, but something about social media almost coerces me into performing actions against my own will. The re-realization of the addictive nature of these services fills me with dispitefulness. </p> 

<p> With the common technical knowledge out of the way, the social aspects are the only thing left to write about. It has become very apparent that my choices have lead me to becoming somewhat of a passing character in the lives of the very few people I wilfully associated with in the past. A prodigal friend in the real world will likely behave similarly. I am neither offended nor pleased because I almost expected a situation where I remain a brief memory. As for the initializing contact in an extra-social-media way, I wait still. </p> 

<p> To conclude, I'm not sure why I ever placed so much value on interpersonal relationships that were supported by the single lynch pin that is social media. Abandoning accounts for years at a time until they fade into obscurity and no one thought to check in? Maybe it's the real life people who matter the most, not the previous real life people who have become nothing more than a series of unfamiliar words on a screen. </p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
