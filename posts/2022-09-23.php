<?php include "../includes/header.html"; ?>
<title>The Year of the FreeBSD Desktop</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>The Year of the FreeBSD Desktop</h1>

<hr/>

<h1>Getting an installer</h1>

<p><a href="https://www.freebsd.org/where/">Link to FreeBSD downloads</a></p>

<p>Choose the correct arch for your system. amd64 is probably the one you want if you know nothing about computer architectures. </p>

<p>you will have a lot of options:</p>

<ul>
<li>*-bootonly.iso is a netinstall image that is for burning to a CD</li>
<li>*-disc1.iso is a supplementary CD image for *-bootonly.iso</li>
<li>*-dvd1.iso is a complete DVD image with extra packages</li>
<li>*-memstick.img is a complete image for burning to a USB stick</li>
<li>*-mini-memstick.img  is a netinstall image for burning to a USB stick</li>
</ul>

<p>I typically download and use one of the compressed memstick images. The mini image is fine but you probably want the regular memstick image if this is the first time you&#8217;ve ever installed FreeBSD. It alleviates some of the stress that comes with installing wireless drivers. </p>

<p>To burn a memstick image, use the <code>disk destroyer</code> program: </p>

<pre><code>root@fbsd# xunz FreeBSD-13.1-RELEASE-amd64-memestick.img.xz
root@fbsd# sudo dd if=.&#47;FreeBSD-13.1-RELEASE-amd64-memestick.img of=&#47;dev&#47;sdx status=progress 
root@fbsd# sudo eject &#47;dev&#47;sdx
</code></pre>

<hr/>

<h1>Initial installation</h1>

<h2>pre-installation</h2>

<p>The standard steps for installing Linux apply: </p>

<ol>
<li>disable secure boot</li>
<li>enable USB booting</li>
<li>select boot device at startup time</li>
</ol>

<p>Because this is hardware specific, it&#8217;s a homework assignment for the audience. </p>

<h2>Installation</h2>

<p>FreeBSD has a menu driven installer that walks the user through various steps: </p>

<h3>1. set keymap (leave default if you don&#8217;t know)</h3>

<h3>2. set hostname</h3>

<h3>3. select sets</h3>

<p>There are many sets to choose from. New users probably want to install all of them. I typically only install the <code>lib32</code> set and add the rest later. </p>

<h3>4. Partitioning</h3>

<p><code>bsdinstall</code> makes it easy to partition your drives. The <code>Auto(ZFS)</code> option is probably what you want as the default UFS configuration is unjournaled. </p>

<p>In the <code>Auto(ZFS)</code> menu, for a single hard drive installation, you want to stripe one disk. Select your hard drive. </p>

<p>If you want full disk encryption, select the <code>Encrypt Disks</code> option. </p>

<p>You also want to bump up the swap size to <code>ram*1.5</code> as a general rule (so, for 4g of ram you will set 6g of swap, for 8g or ram you set 12g swap). If you selected <code>Encrypt Disks</code>, you should also select <code>Encrypt Swap</code></p>

<p>When you are done, proceed with the installation. You will gt a confirmation message asking if you want to destroy the disk(s) you selected. This is your last chance to go back. </p>

<p>If you selected <code>Encrypt Disks</code>, you will be presented with a password prompt. This is the disk encryption password, not any user password. </p>

<h3>5. Wait for sets to install</h3>

<h3>6. Configure root user</h3>

<p>After the sets are installed, you will set a root password. </p>

<h3>7. Network Config</h3>

<p>If your wireless card is supported, all the hard parts are already done for you. If your wireless card is not supported, you might need to plug in an ethernet cable and compile the drivers into the kernel. </p>

<p>Select your card (<code>em*</code> is ethernet, wifi cards are named after their drivers)</p>

<p>If you choose wifi, the installer will scan for networks and give you a menu to select one. If the network is encrypted, you will be presented with a password prompt. </p>

<h3>8. Time and date setup</h3>

<h3>9. Service setup</h3>

<p>You will be presented with a menu that enables&#47;disables services on system startup. You probably want all of them except <code>local_unbound</code>. </p>

<h3>10. Security config</h3>

<p>The next menu enables&#47;disables security features. If nothing else, select <code>disable_sendmail</code> and <code>clear_tmp</code></p>

<h3>11. Add users</h3>

<p>Simply add your user. You might want to add him to the wheel group if you plan on using sudo. I set my shell to tcsh but you can always change this later. A</p>

<h3>12. Final configuration</h3>

<p>You may want to install the handbook or modify any configurations you&#8217;ve made so far. This will take some time. When you are done, apply the config and exit. </p>

<h3>13. Manual config</h3>

<p>Before you reboot the system and exit the installer, you are given a last opportunity to make any manual configurations. This is rarely needed for the average desktop user. </p>

<h2>Post installation</h2>

<blockquote>
<p>What, no GUI? </p>
</blockquote>

<h3>Update system</h3>

<p>Login as root and update the system: </p>

<pre><code>root@fbsd# freebsd-update fetch
root@fbsd# freebsd-update install
root@fbsd# reboot
</code></pre>

<h3>Installing packages</h3>

<p>Before we begin modifying the system, we need a better editor. </p>

<p>The <code>pkg</code> utility is used in a nearly identical way to any Linux package manager. The syntax <code>pkg $verb $object</code> persists. Verbs include <code>install</code>, <code>remove</code>, <code>update</code>, <code>upgrade</code>, <code>search</code>, etc. </p>

<p>Because the only editors installed by default are <code>vi</code>, <code>ed</code>, and <code>ee</code>, let&#8217;s install vim. </p>

<p>There are multiple vim flavors, I like vim-tiny. </p>

<pre><code>root@fbsd# pkg bootstrap
root@fbsd# pkg update
root@fbsd# pkg search vim
root@fbsd# pkg install vim-tiny
</code></pre>

<p>We probably want sudo (or doas) also: </p>

<pre><code>root@fbsd# pkg install sudo
root@fbsd# visudo
</code></pre>

<p>Find the line that says: </p>

<pre><code># %wheel ALL=(ALL:ALL) ALL
</code></pre>

<p>and move the <code>#</code> from the beginning of the line to enable the wheel group to do actions as root. </p>

<h3>Bootloader tweaks</h3>

<p>We can tweak the bootloader to make the system more desktop-like. Edit <code>&#47;boot&#47;loader.conf</code></p>

<pre><code># &#47;boot&#47;loader.conf
# -----------------
[ lots of default stuff ] 

# custom stuff

# boot faster
autoboot_delay=2
</code></pre>

<p>Refer to <code>loader.conf(5)</code> for more tweaks and <code>&#47;boot&#47;defaults&#47;loader.conf</code> for examples. </p>

<h3>init tweaks</h3>

<p>We can tweak the init system also. Edit <code>&#47;etc&#47;rc.conf</code></p>

<pre><code># &#47;etc&#47;rc.conf
# -----------------
[ lots of default stuff ] 

# enable graphics
kld_list="i915kms"

# faster booting
background_dhclient="YES"
</code></pre>

<p>See <code>rc.conf(5)</code> and <code>&#47;etc&#47;defaults&#47;rc.conf</code> for more information on what you can do. </p>

<h3>Snapshotting a sane fresh installation</h3>

<p>At this point, it is wise to take a recursive snapshot of your FreeBSD installation. This provides us with an easy way to roll back to a fresh, known working system configuration. </p>

<pre><code>root@fbsd# zfs snapshot -r zroot@freshinstall
root@fbsd# zfs list - tsnapshot
</code></pre>

<p>If the system becomes unrepairable, we can simply rollback instead of reinstalling with a simple command: </p>

<pre><code>root@fbsd# zfs rollback -r zroot@freshinstall
</code></pre>

<p>To rollback every dataset, we can use xargs: </p>

<pre><code>root@fbsd# zfs list -t snapshot | grep freshinstall | cut -d &#39; &#39; -f 1 | xargs -I % zfs rollback % 
</code></pre>

<p>Using zfs snapshots before and after making any potentially dangerous configuration changes saves a lot of headache in the long run because zfs is accessible from the recovery shell. Rollback with caution, user data may be lost. </p>

<p>Homework assignment: write a series of cron jobs that automatically takes snapshots (and cleans up the old ones) of user data as a form of <em>last line of defense version control</em></p>

<hr/>

<h1>Graphical user interfaces</h1>

<h2>Install graphics drivers</h2>

<p>This varies depending on your GPU. </p>

<pre><code>root@fbsd# pkg install drm-kmod
</code></pre>

<p>After installing this package, you will see a message on how to enable the driver for your specific hardware: </p>

<pre><code>For amdgpu: kld_list="amdgpu"
For Intel: kld_list="i915kms"
For radeonkms: kld_list="radeonkms"
</code></pre>

<p>To enable one of these, you will need to add a line to your <code>&#47;etc&#47;rc.conf</code>. The earlier you place this line in the file, the sooner the kmods will load. For intel graphics, for example, you will add the following line: </p>

<pre><code># &#47;etc&#47;rc.conf
# -----------------
[ lots of other stuff ] 

# intel graphics drivers
kld_list="i915kms"
</code></pre>

<p>To load the kmod on the fly (for larger resolution vt), run: </p>

<pre><code>root@fbsd# kldload i915kms
</code></pre>

<p>You will also need to add your non-root user to the <code>video</code> group.</p>

<pre><code>root@fbsd# pw groupmod video -m $user
</code></pre>

<h2>Audio</h2>

<p>(hopefully) audio will just work. Supported audio interfaces are enumerated in <code>man snd(4)</code> and details on enabling&#47;disabling drivers in <code>&#47;boot&#47;lodaer.conf</code> are also explained. </p>

<p>To manage volume, use the <code>mixer</code> command. For example, setting the mic volume to 50% and the speaker volume to 95%: </p>

<pre><code>user@fbsd% mixer mic 50:50
user@fbsd% mixer vol 95:95
</code></pre>

<p>The <code>mixertui</code> command can also be used. This program functions similarly to <code>alsamixer</code> on Linux. </p>

<p>Depending on your hardware, the volume keys on your keyboard might not work. Adding a keybinding to a shell script is the usual solution and should be familiar to anyone who uses a desktop free window manager.</p>

<h2>Getting xorg</h2>

<pre><code>root@fbsd# pkg install xorg
</code></pre>

<p>The <code>twm</code> window manager is included with xorg by default. We can use it for testing our xorg configuration, mouse support, etc before continuing with larger desktop environments. Early troubleshooting always prevents foot shooting. Test early, test often. </p>

<pre><code>root@fbsd# startx
</code></pre>

<h2>Desktop Environments</h2>

<p>Refer to <a href="https://docs.freebsd.org/en/books/handbook/x11/#x11-wm">The handbook&#8217;s instructions on desktops</a> for instructions on non-suckless (ie suck<em>more</em> setups). I have tested some of them on FreeBSD. KDE and Xfce are reliable. GNOME is mostly reliable. If you are running a big DE, you might have to modify polkit rules to do things like reboot the system from the GUI. Many larger desktops rely on FreeDesktop.org components. I personally do not like dbus so instead I use the suckless tools. </p>

<p>But, for the sake of completeness, I will install a few for the masses. I installed each one of these independently and sequentially on the same system using zfs snapshots to roll back to a bare bones system without any DE installed. </p>

<h3>GNOME</h3>

<pre><code>root@fbsd# pkg install gnome
root@fbsd# printf &#39;proc\t&#47;proc\tprocfs\trw\t0\t0\n&#39; &#62;&#62; &#47;etc&#47;fstab
root@fbsd# sysrc dbus_enable="YES"
root@fbsd# sysrc gdm_enable="YES"
root@fbsd# sysrc gnome_enable="YES"
root@fbsd# reboot
</code></pre>

<p><img class="post" src="assets/images/gnome-on-freebsd.png" alt="Gnome screenshot on FreeBSD" /></p>

<h3>KDE</h3>

<pre><code>root@fbsd# pkg install kde5 sddm
root@fbsd# printf &#39;proc\t&#47;proc\tprocfs\trw\t0\t0\n&#39; &#62;&#62; &#47;etc&#47;fstab
root@fbsd# sysrc dbus_enable="YES"
root@fbsd# sysrc sddm_enable="YES"
root@fbsd# reboot
</code></pre>

<p><img class="post" src="assets/images/kde-on-freebsd.png" alt="KDE screenshot on FreeBSD" /></p>

<h3>Xfce</h3>

<pre><code>root@fbsd# pkg install xfce xfce4-goodies
root@fbsd# sysrc dbus_enable="YES"
</code></pre>

<p>Xfce does not provide it&#8217;s own login manager, unlike GNOME or KDE. Let&#8217;s pick lightdm because it&#8217;s small and the graphical toolkit matches Xfce.</p>

<pre><code>root@fbsd# pkg install lightdm-gtk-greeter
root@fbsd# sysrc lightdm_enable="YES"
root@fbsd# reboot

</code></pre>

<p><img class="post" src="assets/images/xfce-on-freebsd.png" alt="Xfce screenshot on FreeBSD" /></p>

<h3>Suckless</h3>

<p><a href="https://suckless.org">suckless: tools that suck less.</a></p>

<p>This is how I use FreeBSD (and how I use most computers). I wrote a makefile that modifies the compile options so that the tools will build on FreeBSD and (optionally) adds the theme I use. <a href="https://gitlab.com/binrc/suckless">You can find my suckless duct tape in this git repo</a>.</p>

<p>I also use xdm because it&#8217;s small and fast. </p>

<pre><code>user@fbsd% sudo pkg install xdm
user@fbsd% sudo service xdm enable
</code></pre>

<p><img class="post" src="assets/images/suckless-on-freebsd.png" alt="Suckless screenshot on FreeBSD" /></p>

<h3>A final note on desktops</h3>

<p>Sometimes desktops behave unexpectedly on FreeBSD (ie users cannot manage power settings, reboot the system, etc). Make sure your login user is in the wheel group (it&#8217;s your computer, you probably are already in the wheel group) and most of the issues will be resolved. For users you don&#8217;t want in the wheel group, you&#8217;ll need to write a few <code>polkit</code> rules. </p>

<p>Additionally, big desktops are typically compiled without the graphical components for modifying network connections. </p>

<p>Similar to Arch or Gentoo, there is a bit of legwork left to the end user. You&#8217;ll never know what you might learn about systems administration if you don&#8217;t wilfully give yourself the opportunity.</p>

<h2>Shell tweaks</h2>

<p>I like colors in the shell for systems I use regularly. I also like aliases. We can modify our csh configuration file to automatically do the fancy for us. </p>

<pre><code># ~&#47;.cshrc
# -----------------
[ lots of stuff ] 

# prompt section
if ($?prompt) then                                                                  
    # An interactive shell -- set some stuff up                                 
    #set prompt = "%N@%m:%~ %# "                                                
    #set prompt = "%{\033[31m%}%N@%m:%~ %#%{\033[0m%} "                         
    set prompt = "%{\033[1m%}%N@%m:%~ %#%{\033[0m%} "                           
    set promptchars = "%#"                                                      

    set filec                                                                   
    set history = 1000                                                          
    set savehist = (1000 merge)                                                 
    set autolist = ambiguous                                                    
    # Use history to aid expansion
    set autoexpand
    set autorehash
    set mail = (&#47;var&#47;mail&#47;$USER)

    if ( $?tcsh ) then
        bindkey "^W" backward-delete-word
        bindkey -k up history-search-backward
        bindkey -k down history-search-forwarrd
        bindkey "^R" i-search-back
    endif
endif
       
# alias section
alias la    ls -aF
alias lf    ls -FA
alias ll    ls -lAF
alias ls    ls -GF
alias lc    ls -GF
</code></pre>

<h2>Some other packages</h2>

<p>The things I like: </p>

<pre><code>user@fbsd% sudo pkg install firefox gimp feh mpv ffmpeg ImageMagick7 mutt newsboat
</code></pre>

<p>If you install a large DE, most of the applications are pulled in as well. If not, you can always use xargs to pull in hundreds of gigabytes of programs: </p>

<pre><code>user@fbsd% sudo pkg search $desktop | cut -d &#39; &#39; -f 1 | xargs sudo pkg install -y
</code></pre>

<p>Going GNU: </p>

<pre><code>user@fbsd% sudo pkg install coreutils emacs bash gcc gmake
</code></pre>

<p>Do a few package searches. What you want is probably there. If not, time to start porting :)</p>

<p>Once you have everything configured how you want it, it&#8217;s a good time to take another zfs snapshot. </p>

<hr/>

<h1>Quickstart</h1>

<h2>Init system</h2>

<p>Instead of systemd, FreeBSD uses rc scripts for starting and stopping services. Everything is pretty much shell scripts. To modify the startup process, you simply edit <code>&#47;etc&#47;rc.conf</code> in a text editor. </p>

<p>For <code>systemctl</code> like starting&#47;stopping&#47;enabling, you can do the following: </p>

<pre><code>root@fbsd# service sshd enable
root@fbsd# service sshd start
root@fbsd# service sshd restart
root@fbsd# service sshd stop
root@fbsd# service sshd disable
root@fbsd# service sshd onestart
root@fbsd# service sshd status
</code></pre>

<p>Each service has it&#8217;s own init file so sometimes a specific service might take different arguments than the standard ones you might expect. </p>

<h2>Networking</h2>

<p>Network interfaces are configured classically using <code>ifconfig(8)</code>. If you want a network interface to persist across reboots, you add the information in <code>&#47;etc&#47;rc.conf</code>. </p>

<p>WiFi is managed with <code>wpa_supplicant</code>. Refer to <code>man wpa_supplicant.conf(8)</code> for more information. </p>

<h2>Firewall</h2>

<p><a href="https://docs.freebsd.org/en/books/handbook/firewalls/#firewalls-pf">use the pf firewall, I like it</a></p>

<h2>General upgrade process</h2>

<pre><code>root@fbsd# pkg update &#38;&#38; pkg upgrade
root@fbsd# freebsd-update upgrade -r 13.1-RELEASE
root@fbsd# freebsd-update install
root@fbsd# reboot
root@fbsd# freebsd-update install
root@fbsd# pkg update &#38;&#38; pkg upgrade
root@fbsd# freebsd-update install
root@fbsd# reboot
</code></pre>

<h2>Shells</h2>

<p>FreeBSD uses <code>tcsh(1)</code> as the default shell and includes <code>sh(1)</code> for bourne-like compatibility. You can install bash if you want. </p>

<h2>Package management</h2>

<p>There are two primary ways of managing software: binary packages and ports. Don&#8217;t mix them if you don&#8217;t know what you&#8217;re doing, it can cause problems. </p>

<p>To be brief: ports are like Gentoo. You spend a lot of time watching compiler output. The following programs help: <code>synth</code>, <code>portmaster</code>, <code>poudriere</code>. </p>

<p>to be verbose: here is a quick guide on using the binary package management system: </p>

<pre><code>
pkg update
pkg upgrade
pkg search foobar
pkg install foobar
pkg remove foobar
pkg autoremove
</code></pre>

<p>As you can see, the syntax is nearly identical to dnf or apt. </p>

<h2>Filesystem</h2>

<p>The hierarchy of FreeBSD is slightly different than a typical Linux system. Refer to <code>man hier(7)</code> for more information. </p>

<p>The biggest difference is that FreeBSD a logically organized system. For example: On Linux, everything seems to end up in <code>&#47;bin</code> (which is a symlink to <code>&#47;usr&#47;bin</code>). Additionally, <code>&#47;sbin</code> is just a symlink to <code>&#47;usr&#47;sbin</code>. On FreeBSD, the system is more organized. For example: </p>

<p><code>&#47;bin</code> contains everything required to boot the system and <code>&#47;sbin</code> contains everything required for fundamental administration. </p>

<p><code>&#47;usr&#47;bin</code> contains most everything else</p>

<p><code>&#47;usr&#47;local</code> contains everything installed by the package management system. </p>

<p>User installed programs are configured in <code>&#47;usr&#47;local&#47;etc</code>. This might be confusing at first but you&#8217;ll get the hang of it. </p>

<p>This logical separation might cause confusion when compiling software from source on FreeBSD but it&#8217;s not too difficult to solve if you already know how about linker options and makefile modification. </p>

<p>As for filesystems, apparently ext2, ext3, and ext4 have read&#47;write support using the <code>ext2fs(5)</code> driver. I probably wouldn&#8217;t boot from them but this exists. UFS is not journaled by default, proceed with caution. ZFS is very good. </p>

<h2>ZFS non-starter</h2>

<p>ZFS is cool because we can create partitions on a whim. Here is some shell output demonstrating listing datasets, creating datasets with a quota, destroying datasets, creating and using encrypted datasets, etc. </p>

<pre><code>root@freebsd:&#47; #
root@freebsd:&#47; # zfs list
NAME                                        USED  AVAIL     REFER  MOUNTPOINT
zroot                                      3.97G   434G       96K  &#47;zroot
zroot&#47;ROOT                                 3.82G   434G       96K  none
zroot&#47;ROOT&#47;13.1-RELEASE_2022-09-18_143644     8K   434G     1.07G  &#47;
zroot&#47;ROOT&#47;default                         3.82G   434G     3.71G  &#47;
zroot&#47;tmp                                   208K   434G      112K  &#47;tmp
zroot&#47;usr                                   157M   434G       96K  &#47;usr
zroot&#47;usr&#47;home                              157M   434G      157M  &#47;usr&#47;home
zroot&#47;usr&#47;ports                              96K   434G       96K  &#47;usr&#47;ports
zroot&#47;usr&#47;src                                96K   434G       96K  &#47;usr&#47;src
zroot&#47;var                                  1.04M   434G       96K  &#47;var
zroot&#47;var&#47;audit                              96K   434G       96K  &#47;var&#47;audit
zroot&#47;var&#47;crash                              96K   434G       96K  &#47;var&#47;crash
zroot&#47;var&#47;log                               424K   434G      300K  &#47;var&#47;log
zroot&#47;var&#47;mail                              192K   434G      128K  &#47;var&#47;mail
zroot&#47;var&#47;tmp                               160K   434G       96K  &#47;var&#47;tmp
root@freebsd:&#47; # zfs list -t snapshot
NAME                                                     USED  AVAIL     REFER  MOUNTPOINT
zroot@freshinstall                                        64K      -       96K  -
zroot&#47;ROOT@freshinstall                                    0B      -       96K  -
zroot&#47;ROOT&#47;13.1-RELEASE_2022-09-18_143644@freshinstall     0B      -     1.07G  -
zroot&#47;ROOT&#47;default@2022-09-18-14:36:44-0                76.7M      -     1.07G  -
zroot&#47;ROOT&#47;default@freshinstall                         35.0M      -     1.21G  -
zroot&#47;tmp@freshinstall                                    96K      -      112K  -
zroot&#47;usr@freshinstall                                     0B      -       96K  -
zroot&#47;usr&#47;home@freshinstall                               96K      -      128K  -
zroot&#47;usr&#47;ports@freshinstall                               0B      -       96K  -
zroot&#47;usr&#47;src@freshinstall                                 0B      -       96K  -
zroot&#47;var@freshinstall                                     0B      -       96K  -
zroot&#47;var&#47;audit@freshinstall                               0B      -       96K  -
zroot&#47;var&#47;crash@freshinstall                               0B      -       96K  -
zroot&#47;var&#47;log@freshinstall                               124K      -      188K  -
zroot&#47;var&#47;mail@freshinstall                               64K      -       96K  -
zroot&#47;var&#47;tmp@freshinstall                                64K      -       96K  -
root@freebsd:&#47; # zfs create zroot&#47;crypt
root@freebsd:&#47; # zfs set quota=5g zroot&#47;crypt
root@freebsd:&#47; # zfs list zroot&#47;crypt
NAME                                        USED  AVAIL     REFER  MOUNTPOINT
zroot&#47;crypt                                  96K  5.00G       96K  &#47;zroot&#47;crypt
root@freebsd:&#47; # zfs destroy zroot&#47;crypt
root@freebsd:&#47; # zfs create -o encryption=on -o keylocation=prompt -o keyformat=passphrase zroot&#47;crypt
Enter new passphrase:
Re-enter new passphrase:
root@freebsd:&#47; # zfs list zroot&#47;crypt
NAME                                        USED  AVAIL     REFER  MOUNTPOINT
zroot&#47;crypt                                 200K   434G      200K  &#47;zroot&#47;crypt
root@freebsd:&#47; # touch &#47;zroot&#47;crypt&#47;supersecret
root@freebsd:&#47; # ls &#47;zroot&#47;crypt&#47;
supersecret
root@freebsd:&#47; # zfs get encryption zroot&#47;crypt
NAME         PROPERTY    VALUE        SOURCE
zroot&#47;crypt  encryption  aes-256-gcm  -
root@freebsd:&#47; # zfs unmount zroot&#47;crypt
root@freebsd:&#47; # zfs unload-key -r zroot&#47;crypt
1 &#47; 1 key(s) successfully unloaded
root@freebsd:&#47; # zfs mount zroot&#47;crypt
cannot mount &#39;zroot&#47;crypt&#39;: encryption key not loaded
root@freebsd:&#47; # zfs get keystats zroot&#47;crypt
root@freebsd:&#47; # zfs get keystatus zroot&#47;crypt
NAME         PROPERTY   VALUE        SOURCE
zroot&#47;crypt  keystatus  unavailable  -
root@freebsd:&#47; # zfs load-key -r zroot&#47;crypt
Enter passphrase for &#39;zroot&#47;crypt&#39;:
zfs 1 &#47; 1 key(s) successfully loaded
root@freebsd:&#47; # zfs mount -a
root@freebsd:&#47; # ls &#47;zroot&#47;crypt&#47;
supersecret
</code></pre>

<hr/>

<h1>A conclusion</h1>

<p>Really, I think FreeBSD is a viable desktop operating system for the types of people who already use Linux in a terminal-centric capacity. After all, UNIX is UNIX. </p>

<hr/>

<h1>Other stuff</h1>

<p><a href="https://wiki.freebsd.org/JailingGUIApplications">Running Firefox inside of a jail</a></p>

<p><a href="https://forums.freebsd.org/threads/how-to-execute-firefox-in-a-jail-using-iocage-and-ssh-jailme.53362/">Another way to run Firefox inside of a jail</a></p>

<h2>FreeBSD Distros that come with a desktop out of the box:</h2>

<p><a href="https://www.ghostbsd.org">GhostBSD - FreeBSD with MATE</a></p>

<p><a href="https://hellosystem.github.io">HelloSystem - FreeBSD with an Apple-like GUI (still in development)</a></p>

<p><a href="https://www.midnightbsd.org">MidnightBSD - FreeBSD with Xfce and a different package management system</a></p>

<p><a href="https://nomadbsd.org">NomadBSD - Live GUI FreeBSD with OpenBoX</a></p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
