<?php include "../includes/header.html"; ?>
<title>Pinebook pro review and notes </title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1> Pinebook pro review and notes </h1>
<hr>
<h1 id="why-the-pbp">Why the PBP?</h1>
<p>Lately I’ve been thinking a lot about power consumption when it comes to computing. Intuitively, I know that arm devices pull significantly less power than amd64 machines but I’ve never really tested this in the real world. So, some preliminary power consumption stats:</p>
<ul class="incremental">
<li>big amd64 laptops (thinkpad x220 and t490) pull at most 65 watts</li>
<li>small arm SOCs typically pull at most 15 watts</li>
<li>most android phones pull at most 18 watts</li>
<li>Pentium 4 pulls at most 250 watts</li>
</ul>
<p>These numbers are fairly easy to find: just look at the power supply for a MAXIMUM OUTPUT value or something similar. This is the point at which the power supply fails so we can safely assume this is the maximum power draw for any given computer. Of course, this is DC output and not AC output and anyone who knows anything about electricity knows that converting AC to DC is expensive but these values are useful as a general estimate. <a href="https://0x19.org/posts/2022-01-08.php">I wrote something similar about computer power consumption some time ago</a></p>
<p>My goal in all of this was to find a self contained computer that runs UNIX, doesn’t take much power, isn’t a consumption rectangle (smartphone), and can be charged from both AC with a rectifier and stored DC without an inverter. Charging from existing stored power was probably the most novel consideration. Everything else is a given.</p>
<p>A few obvious answers come to mind:</p>
<ul class="incremental">
<li><a href="https://www.raspberrypi.com/products/raspberry-pi-4-model-b/">Raspberry Pi 4</a> is not self contained and using a pitop in public is a good way to get the bomb squad called on you</li>
<li><a href="https://beagleboard.org/black">beaglebone black</a> is good too but neither self contained nor popular enough for wide OS support</li>
<li><a href="https://pine64.com/product/14%e2%80%b3-pinebook-pro-linux-laptop-ansi-us-keyboard/">Pinebook Pro</a> is self contained and is supported by some of the operating systems I’d like to run</li>
</ul>
<p>The PBP is an obvious choice. It’s an open hardware ARM laptop that can be charged via a barrel cable (AC-&gt;DC) or via USB-C. Charging from USB-C is a very useful feature because it means I can easily choose between charging from the mains where efficiency loss is acceptable and charging from a DC source where efficiency loss is unacceptable.</p>
<p>The actual use case is “what computer can I run off of a old car battery or the alternator in my car without burning power with an inverter?”. I’ll revisit this use case in a later section.</p>
<hr />
<h1 id="initial-notes">Initial notes</h1>
<p>I took these notes immediately upon opening the PBP. They remain unedited because I want to be honest on the first impressions.</p>
<h2 id="shipping">shipping</h2>
<p>I was worried about DHL dropping my package out of a plane. Or leaving it out in the rain. Or having one of the employees use it as a soccer ball. Or having the thing get stuck in customs. It ended up arriving safely and was packaged well. Two boxes within a padded envelope within another envelope. Surprising for DHL.</p>
<h2 id="hardware-impressions">hardware impressions</h2>
<ul class="incremental">
<li>Touchpad sucks and trackpad scrolling sucks (it’s probably just KDE). Installing synaptics drivers allegedly fix this problem.</li>
<li>keyboard is comfortable, clickly, full sized despite being a chicklet keyboard. I don’t like that the <code>&lt;ctl&gt;</code> and <code>&lt;fn&gt;</code> keys are backwards when compared to a thinkpad. I really like the thinkpad keyboard layout.</li>
<li>Shift+enter seems to type the <code>M</code> character. My muscle memory for key chording is now broken. This appears to be a fundamental design flaw with KDE.</li>
<li>Passively cooled, gets a bit warm.</li>
<li>display is sharp (IPS) and almost too high resolution for my eyes (1920x1080 instead of 1366x768). I can fix this in software.</li>
<li>enabling/disabling mic/wifi/camera through the keyboard is confusing and (seemingly) does not perform the “kill switch” via hotplugging like the Thinkpad X220’s wifi kill switch.</li>
<li>Charger comes with both US and EU prongs.</li>
</ul>
<h2 id="software-impressions">software impressions</h2>
<ul class="incremental">
<li>it’s manjaro :(</li>
<li>it’s KDE :(</li>
<li>it comes with mpv :)</li>
<li>bluez instead of bluetoothd :(</li>
<li>firewalld instead of UFW &lt;3</li>
<li>no vim, no vi, and no ed on the standard installation so this is completely useless and unusable.</li>
<li>firefox config uses google as the default search engine</li>
<li>It didn’t come with RHEL7 or so obviously I hate it</li>
<li>It didn’t come with BSD so obviously I hate it</li>
<li>Terminal color scheme is one of the worst I’ve seen in my life</li>
</ul>
<h1 id="secondary-notes">Secondary notes</h1>
<p>From my next available free day, largely unedited:</p>
<h2 id="software">software</h2>
<ul class="incremental">
<li>I still don’t like manjaro. I still don’t like pacman.</li>
<li>On the default manjaro installation, the <code>manjaro-arm-flasher</code> tool seems to create bootable SD cards (nice for the insurance policy)</li>
<li>when updating (lots of network IO throughput), the wireless card seems to stutter.</li>
<li>I Can’t boot another OS using the stock firmware. I need to flash the “SPI flash” (fancy ARM guy terminology for nvram) with newer versions of u-boot that are more hacker friendly (ie the ones that don’t require a serial display).</li>
</ul>
<h2 id="hardware">hardware</h2>
<ul class="incremental">
<li>The emmc is toggable with a DIP switch on the mainboard. <a href="https://wiki.pine64.org/wiki/Pinebook_Pro#Internal_Layout">My mainboard looks slightly different than the images on the wiki</a> but all of the components are generally in the same location</li>
</ul>
<hr />
<h1 id="technical-stuff">Technical stuff</h1>
<h2 id="upgrading-the-u-boot-on-storage-devices">upgrading the u-boot on storage devices</h2>
<p>It seems that the PBP has three possible locations for the bootloader: nvram, EMMC, and sd card. The boot process is something like:</p>
<ol class="incremental" type="1">
<li>load bootloader from nvram</li>
<li>query storage device</li>
<li>if the storage device has it’s own bootloader, chainload that</li>
<li>otherwise, use the bootloader from nvram</li>
</ol>
<p>It’s u-boot soup but upgrading the bootloader on the storage device seems to marginally increase stability.</p>
<pre><code>$ sudo pacman -Syuu
$ sudo pacman -S uboot-pinebookpro
$ sudo dd if=/boot/idbloader.img of=/dev/mmcblk2 seek=64 conv=notrunc,fsync
$ sudo dd if=/boot/u-boot.itb of=/dev/mmcblk2 seek=16384 conv=notrunc,fsync
$ sudo sync
$ sudo reboot</code></pre>
<h1 id="flashing-u-boot-externally-and-buying-more-stuff">Flashing u-boot externally and buying more stuff</h1>
<p><a href="https://nullr0ute.com/2021/05/fedora-on-the-pinebook-pro/">I found some fedora specific instructions that are actually just PBP specific</a> Using the u-boot and idb from fedora, The screen flashes with garbled nonsense when loading a kernel but at least I can see the output from u-boot. Different kernels show different garbled nonsense so it’s a bit of fun. The keyboard does not work in u-boot. The system will now also boot from the SD card (although it seems the system will always boot from EMMC once before allowing booting from the sd card).</p>
<p>Armbian and NetBSD seem to boot just fine from the sd card. I’m keeping a sd card with the stock Manjaro image as an insurance policy.</p>
<p>Because the keyboard does not work in u-boot, I ordered the pine64 serial over 3.5mm audio jack cable so that I can (hopefully) fix this in the future. The system still boots automatically but I’d really like to pass options to my bootloader to do things like enter single user mode, run an fsck at boot time, boot various versions of kernels, etc.</p>
<hr />
<h1 id="os-support">OS Support</h1>
<p>I will not use Arch. You cannot force me. You cannot coerce me. You cannot even bribe me. I will use anything other than Arch as long as it’s not ubuntu.</p>
<p>Sadly, BSD is not “super easy” on the PBP like most other SBCs but I’ve never stepped down from a challenge. Even sadder, no Plan 9 ported to PBP either. Running a “distro based on a distro based on a distro based on arch” is the antithesis of my computing philosophy. Luckily enough, there is a big <a href="https://wiki.pine64.org/wiki/Pinebook_Pro_Software_Release">list of operating systems that run on the PBP</a>. Most of these are distros based on distros based on distros. OpenBSD instantly appeals to me but there is no support for graphical TTYs as of yet (installation must be performed over serial) so this is a non-starter. NetBSD also appeals to me but no wifi in 9.x, only in -CURRENT. A wide selection of actually usable software, take your pick.</p>
<p><a href="https://github.com/manjaro-arm/pbpro-images/releases/">Official Manjaro images with a variety of DEs</a> also exist for when you lost (or corrupted) your insurance policy SD card.</p>
<figure>
<img src="assets/images/boot-loops.jpg" alt="android boot loops, my experience with booting Manjaro from an SD card" /><figcaption aria-hidden="true">android boot loops, my experience with booting Manjaro from an SD card</figcaption>
</figure>
<hr />
<h1 id="netbsd">NetBSD</h1>
<p>NetBSD seems to work just fine but 9.x doesn’t yet ship the broadcom drivers. These drivers are present in the -CURRENT branch but the issue with -CURRENT is that it’s not entirely stable. Additionally, the bootloader does not come with the system.</p>
<p><a href="http://cdn.netbsd.org/pub/pkgsrc/current/pkgsrc/sysutils/u-boot-pinebook-pro/index.html">u-boot from pkgsrc</a></p>
<p><a href="https://cdn.netbsd.org/pub/NetBSD/NetBSD-9.3/evbarm-aarch64/binary/gzimg/arm64.img.gz">aarch64 build of NetBSD</a></p>
<p>To get around the no wifi issue, a dongle is required. I use an <code>Atheros AR9271</code> USB-&gt;WiFi dongle. To get around no RJ45 port, I use a USB-&gt;RJ45 adapter. I have an <code>ASIX ax88772</code> dongle (UGREEN branded but I’m not sure that matters). Both of these dongles seem to work with every single operating system and hardware configuration I’ve tried them with.</p>
<p>Arm is strange, so we must boot from an SD card (running any OS, in my case NetBSD) in order to burn an image to the internal storage.</p>
<p>From a separate machine, the options passed to <code>dd</code> <em>are</em> important.</p>
<pre><code>$ wget https://cdn.netbsd.org/pub/NetBSD/NetBSD-9.3/evbarm-aarch64/binary/gzimg/arm64.img.gz
$ wget http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/x86_64/.9.0_2022Q2_pkgbuild/All/u-boot-pinebook-pro-2022.01nb1.tgz
$ gunzip ./arm64.img
$ tar xzf ./u-boot-pineboo-pro-2022.01nb1.tgz
$ sudo umount /dev/sdx*
$ sudo dd if=./arm64.img of=/dev/sda status=progress conv=fsync bs=1M
$ sudo sync
$ sudo dd if=./u-boot-pinebook-pro-2022.01nb1/share/u-boot/pinebook-pro/rksd_loader.img of=/dev/sda seek=64 conv=sync status=progress
$ sudo sync
$ sudo eject /dev/sda</code></pre>
<p>And, to install NetBSD to the internal EMMC, the process is similar. NetBSD’s version of <code>dd</code> varies slightly but the options passed <em>are</em> important.</p>
<pre><code># ftp https://cdn.netbsd.org/pub/NetBSD/NetBSD-9.3/evbarm-aarch64/binary/gzimg/arm64.img.gz
# gunzip ./arm64.img
# dd if=./arm64.img of=/dev/rl0d conv=sync bs=1m
# sync
# PKG_PATH=&quot;http://ftp.netbsd.org/pub/pkgsrc/packages/NetBSD/aarch64/9.3/All/&quot; pkg_add pkgin
# pkgin install u-boot-pinebook-pro
# sudo dd if=/usr/pkg/share/u-boot/pinebook-pro/rksd_loader.img of=/dev/rld0 seek=64 conv=sync
# sync
# reboot</code></pre>
<p>And, some more desktop centric things after booting from EMMC:</p>
<pre><code># passwd
# echo &quot;postfix=NO&quot; &gt;&gt; /etc/rc.conf
# echo &quot;xdm=NO&quot; &gt;&gt; /etc/rc.conf</code></pre>
<p>Installing pkgin (and some packages):</p>
<pre><code># PKG_PATH=&quot;https://cdn.NetBSD.org/pub/pkgsrc/packages/NetBSD/aarch64/9.3/All/&quot; pkg_add pkgin
# sed -i&#39;&#39; -e &#39;s/9.0/9.3/g&#39; /usr/pkg/etc/pkgin/repositories.conf
# pkgin install vim git mozilla-rootcerts mozilla-rootcerts-openssl</code></pre>
<p>The rest is NetBSD specific and I’ve avoided getting into it here because it doesn’t have anything to do with the PBP.</p>
<hr />
<h2 id="performance">Performance</h2>
<p>The PBP has 6 cores (2 fast, 4 slow) and 4gb ram. The cpu is fairly slow but entirely usable. On large procedural jobs like software compilation, it’s painful. For concurrent jobs, it’s mostly fine.</p>
<h2 id="compiler-performance">Compiler performance</h2>
<p>As expected, the PBP is slower when it comes to compilation than a standard amd64 machine. Surprisingly enough, NetBSD was significantly slower than Manjaro. This is likely due to the Linux kernel knowing how to better handle multiple CPUs with varying speeds.</p>
<h3 id="sequential-jobs">sequential jobs</h3>
<p>I used <a href="https://github.com/9fans/plan9port">plan9port</a> because it’s a fairly large but portable project. Compilation is largely sequential, invokes many standard shell utilities, and involves extra preprocessor steps to convert 9 C into something a standard UNIX compiler like GCC or Clang can compile.</p>
<p>On a T490 - 8th gen Core i8 (4 cores, 8 threads, 4.8GHz, vPro for maximum thermal output):</p>
<pre><code>real    232.51 (~4 minutes)
user    188.07
sys     65.01</code></pre>
<p>On an X220 - 2nd gen Core i5 (2 cores, 4 threads, 2.6GHz, vPro for maximum thermal output):</p>
<pre><code>real    249.98 (~4 minutes)
user    220.33
sys     65.52</code></pre>
<p>On the PBP (2 2.0GHz cores + 4 1.5GHz cores, no CPU fan for maximum thermal output) (running stock <strong>Manjaro</strong> image):</p>
<pre><code>real    1355.27 (~22 minutes)
user    1178.47
sys     347.71</code></pre>
<p>On the PBP (2 2.0GHz cores + 4 1.5GHz cores, no CPU fan for maximum thermal output) (running <strong>NetBSD</strong>):</p>
<pre><code>real    3715.24 (~60 minutes)
user    1946.84
sys     3435.29</code></pre>
<h3 id="concurrent-jobs">concurrent jobs</h3>
<p>I used <a href="https://github.com/vim/vim">vim</a> because it can be built in parallel without causing any issues.</p>
<p>Same 8th gen Core i8 (<code>make -j7</code>):</p>
<pre><code>real    27.36
user    170.21
sys     11.30</code></pre>
<p>Same 2nd gen Core i5 (<code>make -j7</code>, approaching the exponential decay of marginal returns on concurrent processing):</p>
<pre><code>real    77.07 
user    292.46
sys     10.00</code></pre>
<p>On the PBP (<code>make -j7</code>) (running stock <strong>Manjaro</strong> image):</p>
<pre><code>real    220.60
user    1145.40
sys     59.90</code></pre>
<p>On the PBP (<code>make -j7</code>) (running <strong>NetBSD</strong>):</p>
<pre><code>real    319.30
user    1560.87
sys     255.33</code></pre>
<h2 id="web-browser-testing">Web browser testing</h2>
<p>Because the PBP has similar hardware specifications to the adware subsidized craptops sold by google, I thought it would be a good idea to compare web browser performance on these systems as well.</p>
<p>I found a few <a href="https://browserbench.org/">web browser benchmark tests at browserbench.org</a>. They’re probably snakeoil but running JS tests is a good way to put a number on how performant $browser on $hardware is.</p>
<p>Scores from the <a href="https://browserbench.org/JetStream">JetStream2 test</a></p>
<blockquote>
<p>JetStream 2.1 is a JavaScript and WebAssembly benchmark suite focused on the most advanced web applications. It rewards browsers that start up quickly, execute code quickly, and run smoothly. For more information, read the in-depth analysis. Bigger scores are better.</p>
</blockquote>
<pre><code>Thinkpad T490   79.555
Thinkpad X220   39.983
PBP (manjaro)   19.148</code></pre>
<p>I don’t have an chromesumption book to test against, so all I can say is that the PBP is slower than a workhorse amd64 machine when it comes to interpreting javascript.</p>
<hr />
<h1 id="conclusion">Conclusion</h1>
<h2 id="did-the-pbp-fulfil-its-needs">Did the PBP fulfil it’s needs?</h2>
<p>The intended use case was “UNIX machine I can charge from an existing battery or alternator”. This immediately invokes ideas of “why would I even need wifi support?”</p>
<p>Ultimately, I ended up flashing a bad image to the SPI flash chip and I cannot get the system to boot (or even show signs of life). I have attempted to enter maskrom mode to re-flash the SPI but I am unsuccessful. There are a few other things I need to try. I’ll update this if I ever get it functional again.</p>
<p>I did not have the opportunity to test the machine in the exact environment I got it for but it was fun before I bricked it. Again, a place for updates.</p>
<h2 id="who-is-the-pbp-for">Who is the PBP for?</h2>
<p>HACKERS!</p>
<p>Obviously, the types of people who are interested in pine64 devices and similar SBCs are already computer owners (if not computer hoarders). It’s unlikely that the PBP will become my (or anyone’s) primary computer but that doesn’t mean that it’s useless. The entire point of arm SBCs is to have fun so why not have fun?</p>
<p>Just don’t flash your SPI if you want it to work as expected.</p>
<hr />
<h1 id="some-final-thoughts-on-open-hardware">Some final thoughts on open hardware</h1>
<p>Oftentimes, before purchasing freedom centric hardware, I search for a few reviews so that I can set my expectations correctly. Oftentimes these reviews are very epidermal: <em>they’re not even skin deep</em>. These reviewers are consumers producing reviews for a consumer audience, not hackers producing in-depth reviews for hacker audience. These types of reviews are frustrating for me but fundamental flaws seem to shine through the lack of thoroughness.</p>
<p>I think that the general negative reviews on open hardware largely stem from unrealistic expectations. The community seems to over-hype many of these devices out of ignorance, stating that $freedomDevice is the $proprietaryAlternative killer, the end all be all device that will usher in the year of the Linux $deviceCategory. Oftentimes, it seems like the high expectations fall flat when confronted with the reality of open hardware: it’s either way too expensive or way too experimental.</p>
<p>It seems like many of these devices are lacking both developer time and users who are both enthusiastic and knowledgable. Pitfalls of mobile UNIX include bad power management, difficulty hotplugging wireless chipsets, graphical interfaces attempting to cope with the fact that they don’t have a physical keyboard, etc. There is still much work to be done. As for users, it seems that the most enthusiastic users always have the impression that $linuxDevice will have 1:1 feature parity with $proprietaryDevice. Maybe it’s just that the loudest users are heard or that we only want to hear utopian dreams of a free software future.</p>
<p>A prime example of this conflict between expectations and reality: Linux smartphones. It doesn’t help when many linux smartphones over the years were advertised as a viable android competitor rather than anything other than what they actually were: an arm board attached to a touch screen and a modem.</p>
<p>I oftentimes ask myself “what is open source worth?”. How much money are you willing to throw at an idea you like? Surely, money thrown at an idea you like is being used better than money thrown at an idea you don’t like. In many cases, it seems like open hardware devices are more expensive than their proprietary counterparts for a few reasons. The two largest reasons are small batch manufacturing and the fact that open hardware isn’t subsidized by pre-installed adware (in the case of nearly every device that comes pre-loaded with proprietary software).</p>
<p>What is open source worth? A few extra dollars, a few extra hours of configuration, a few extra papercuts, and a clean conscious knowing that I didn’t pay for yet another windows license I will never use and will never get a refund for. Open source is worth investing in because the, albeit slow, improvements to open hardware and software have wider implications than just “buying a laptop with Linux pre installed”.</p>
<h1 id="future-projects">Future projects</h1>
<ul class="incremental">
<li><s>something with the raspi</s></li>
<li>NetBSD in depth</li>
<li>“why is my lightbulb running android?” and other Internet-Of-Terror ideas</li>
<li>turning a router into a general purpose computing device (probably MIPS because where else am I going to find a MIPS CPU? Might as well do something novel instead of $arm-project-1209)</li>
</ul>

</div>
</div>
<?php include "../includes/footer.html"; ?>
