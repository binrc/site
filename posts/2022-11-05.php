<?php include "../includes/header.html"; ?>
<title>Plan 9 on a raspberry pi (with a screen case)</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Plan 9 on a raspberry pi (with a screen case)</h1>

<hr/>
<h1>(hypothetical) wifi </h1>
<p>If the pi's bwfm chip was supported, this is how (I assume) it would work: </p>
<pre><code>term% cd /lib/firmware
term% hget http://firmware/openbsd.org/firmware/7.2/bwfm-firmware-20200316.1.3p3.tgz > firmware.tgz
term% tar xzvf firmware.tgz
term% cd firmware/
term% mv ./* ../
term% reboot
term% bind -a '#l1' /net
term% aux/wpa -s FCC_VIOLATING_WLAN -p /net/ether1
! Addidng key: proto=wpapsk essid=FCC_VIOLATING_WLAN
password: *******
!
term% ip/ipconfig ether /net/ether1
</code></pre>
<p>Until then, it's ethernet only (unless you can find a supported dongle).</p>
<h1>Official raspi displays</h1>
<p>I have a pi3 display and it's upside down. In order to fix this, do the following: </p>
<pre><code>term% 9fs pidos
term% cd /n/pidos
term% sam config.txt
</code></pre>
<p>and append the line: </p>
<pre><code>lcd_rotate=2
</code></pre>
<h1>automatic booting</h1>
<pre><code>term% 9fs pidos
term% cd /n/pidos
term% sam cmdline.txt
</code></pre>
<p>Note: you must use spaces, everything after the first newline seems to be ignored. This is in direct contrast to a <code>plan9.ini</code> on an i386 or amd64 installation where newlines are required. For all intents and purposes, <code>cmdline.txt</code> is the pi equivalent to a <code>plan9.ini</code>. </p>
<pre><code>console=0 nobootprompt=local!/dev/sdM0/fs user=glenda
</code></pre>
<p>Configuring 9 to automatically submit a DHCP request when it boots (so I don't have to run <code>ip/ipconfig</code> manually every time) and query an NTP server (because it's not 1970): </p>
<pre><code>term% cp /adm/timezone/GMT /adm/timezone/local
term% sam /usr/glenda/lib/profile
</code></pre>
<p>My profile looks something like: </p>
<pre><code>[lots of stuff]
webfs
plumber
ip/ipconfig ether /net/ether0
sleep 3 # because DHCP is SLOW
aux/timesync -n pool.ntp.org
[lots of other stuff]
</code></pre>
<p>Configuring the system in the hostowner's profile is hacky but it seems to work</p>
<h1>upgrading the system</h1>
<pre><code># get sources
sysupgrade

# build userland
cd /
. /sys/lib/rootstub
cd /sys/src
mk install
mk clean

# build manpages
cd /sys/man
mk

# build papers
cd /sys/doc
mk
mk html

# build kernel
cd /sys/src/9/bcm64
mk install
reboot
</code></pre>
<h1>Images</h1>
<img class="post" src="assets/images/pi-guts1.png"> <img class="post" src="assets/images/pi-guts2.png"> <img class="post" src="assets/images/pi-guts3.png"> <img class="post" src="assets/images/pi-screen-case.png"></p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
