<?php include "../includes/header.html"; ?>
<title>CGI programming in C</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>CGI programming in C</h1>

<hr>

<p>I have recently been thinking about the <a href="https://learnbchs.org/">bchs stack</a>. This web stack is BSD+C+httpd+Sqlite. It sounds like a joke at first but actually makes a lot of sense considering that these four components were once part of the OpenBSD base system. Sqlite has been removed from the OpenBSD base system because it's too big and messy to audit. You <em>might</em> not even need/want sqlite and could easily roll your own null delimited csv.</p>

<h1>Why CGI?</h1>

<p>GCI enables a webmaster to easily add logic to html documents. CGI is a much preferable alternative to the &ldquo;just run a python test server as a privileged user behind an nginx reverse proxy&rdquo; attitude that seems so common in the server side scripting community and the &ldquo;just do it all in javascript because we&rsquo;re so cheap we offload as much processing power as we can onto the client&rdquo; attitude that&rsquo;s ubiquitous in web development.</p>

<p>PHP is a well known example of doing logic with cgi.</p>

<h1>How CGI works</h1>

<p>CGI is fairly simple: GET requests come in as environmental variables, POST requests come in through stdin, and anything that is sent to the client is written to stdoud. This is somewhat similar to inetd. Running httpd+cgi on OpenBSD is very easy because both are part of the base system</p>

<pre><code># rcctl enable slowcgi httpd
# rcctl start slowcgi httpd 
# rcctl check slowcgi httpd 
</code></pre>


<h1>Caveats to writing CGI scripts in C</h1>

<ul>
<li>OpenBSD httpd runs in a chroot to improve security. This means that whatever libraries your program depends on will either need to be:

<ol>
<li>placed inside of the chroot so they can be dynamically linked</li>
<li>statically linked (easier option)</li>
</ol>
</li>
<li>Debugging can be difficult to setup (write a makefile rule and put your test string from stdin into a file)</li>
<li>things will behave unexpectedly break if your query string fields are not in the order that your hardcoded parser expects (like in the demo code)</li>
<li>people will crash your server if you do not write a check against POST requests with an infinite size (the demo code is susceptible. Do not run it in production)</li>
<li>your f<i>_rust_</i>rated friends will have a public meltdown at the idea of putting <q>DANGEROUS AND UNSAFE C PROGRAMS ON THE INTERNET</q> (largely a non-issue on OpenBSD) despite the fact that they run test servers as privileged users behind an nginx reverse proxy in production.</li>
</ul>


<h1>Benefits to writing CGI scripts in C</h1>

<ul>
<li>you learn a lot about static linking and the linker in general</li>
<li>avoiding shell escapes is easier in C than writing internet touching scripts in sh</li>
<li>ego++;</li>
</ul>


<h1>Programming</h1>

<p>Source code is available in my <a href="https://gitlab.com/binrc/bch-demo">bch-demo git repository</a>. This code is fairly simple. <code>index.c</code> is an index page with 2 form elements. One of these forms uses a POST request and the other uses a GET request. The form fields take a hex color code in order to set the background color of the document and the text color of the document. The body text is simply a dump of all the environmental variables which can be useful for debugging.</p>

<p>If you&rsquo;re not already using OpenBSD, refer to <a href="https://www.openbsd.org/faq/faq4.html">The OpenBSD FAQ</a>. OpenBSD runs very well as a virtual machine on a linux hypervisor with libvirt+kvm if you lack hardware.</p>

<h2>Printing a web page</h2>

<p>Printing a web page is the easiest part of this exercise: simply write to stdout. You should write your http headers before writing html so that the page displays properly. An example http header and html tag look like this:</p>

<pre><code>puts("Status: 200 OK\r");
puts("Content-Type: text/html\r");
puts("\r");
puts("&lt;h1&gt;It works&lt;/h1&gt;");
</code></pre>


<h2>Processing a GET request</h2>

<p>GET requests come in as an environmental variable. We can read these variables like this:</p>

<pre><code>char *req = getenv("QUERY_STRING");
if(req == NULL) return 1;
int reqlen = strlen(req);
</code></pre>


<p>It can be useful to create a copy of this query string to prevent mutilating the original string. It is also useful to create a backup pointer to the copy of the query (which we are modifying) so that we can still free the memory if the modification process mutilates pointers.</p>

<pre><code>char *reqcp = malloc(sizeof(char) * (reqlen + 1));
strlcpy(reqcp, req, sizeof(char) * (reqlen + 1));
char *ptr2reqcp = reqcp;    
</code></pre>


<p>The remainder of this program involves parsing the query string. This can be application specific and the demo code is not robust.</p>

<h2>Processing a POST request</h2>

<p>POST requests come in through stdin. When a POST request is sent the <code>CONTENT_LENGTH</code> variable is set and equal to the number of characters in the request string.</p>

<pre><code>unsigned int reqlen = atoi(getenv("CONTENT_LENGTH"));
</code></pre>


<p>After getting the length of the string we can safely allocate memory. A copy of the original request and a backup pointer should me made for the same reasons as in the GET example.</p>

<pre><code>char *req = malloc(sizeof(char) * (reqlen + 1));
char *reqcp = malloc(sizeof(char) * (reqlen + 1));
fread(req, reqlen, 1, stdin);
strlcpy(reqcp, req, sizeof(char) * (reqlen + 1));
char *ptr2reqcp = reqcp;
</code></pre>


<p>The remainder of this program is identical to the GET program. Only 9 lines differ between the GET and POST programs.</p>

<h2>Demo video</h2>

<video width="683px" height="384px" controls>
<source src='./assets/images/bch-demo.webm' type="video/webm"/></p>
<p> Your browser does not support .webm </p>
</video>

</div>
</div>
<?php include "../includes/footer.html"; ?>
