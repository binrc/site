<?php include "../includes/header.html"; ?>
<title>OpenBSD VM on a plan 9 host</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>OpenBSD VM on a plan 9 host</h1>
<p>I am back to playing with 9 again. It’s been a while since I last touched the system because I have been very busy. I am still wondering if I can get a ‘modern’ web browser running so that I can proceed with the nearly 5 year old idea of running 9 <em>exclusively</em>.</p>

<p>The state of web browsers in 9 hasn’t really evolved since it’s inception so the only foreseeable solution is to virtualize a more “modern” operating system with support for things like firefox so I can do the big boy tasks like “paying the mortgage” and so on and so forth.</p>

<h1>But it’s not all virtualization and cream</h1>
<p>One of my favorite things about virtualization (emulation) on 9 is that a guest can crash the host. When this occurs the host’s filesystem will become corrupted you will need to manually clean things up. 9 doesn’t seem to have a good/any OOM to prevent this from happening.</p>

<p>From <code>vmx(1)</code></p>
<pre><code>BUGS
          Vmx can and will crash your kernel.

          Currently only one core is supported.

          The Linux vga= option is not supported, as well as many of
          the OpenBSD boot variables.
</code></pre>

<h1>Setup</h1>

<p>The setup steps for installing OpenBSD are only marginally more complicated than the average user is used to. The most difficult part of the entire setup is figuring how to boot the system when it doesn’t want to boot.</p>


<h2>get files</h2>
<p>In order to boot the system we need 3 files: a <code>.fs</code> or <code>.img</code> OpenBSD installation image, the <code>bsd.rd</code> kernel, and the regular <code>bsd</code> kernel. I have no intention on trying to get the <code>bsd.mp</code> kernel working as there is no multithreading in <code>vmx(1)</code>.</p> <p><code>bsd.rd</code> comes gzipped so it must be extracted before use</p>


<pre><code>term% hget https://cdn.openbsd.org/pub/OpenBSD/7.3/amd64/install73.img &gt; install73.img 
term% hget https://cdn.openbsd.org/pub/OpenBSD/7.3/amd64/bsd.rd &gt; bsd.rd
term% hget https://cdn.openbsd.org/pub/OpenBSD/7.3/amd64/bsd &gt; bsd
term% file bsd.rd
bsd.rd: gzip compressed
term% gunzip -c bsd.rd &gt; bsd-unziped.rd
term% file bsd-unziped.rd
bsd-unziped.rd: AMD64 ELF executable
</code></pre>

<h2>Installation</h2>

<p>Before proceeding with installation, a virtual disk must be made. In this example I created a disk with a capacity of 20G.</p>

<pre><code>term% dd < /dev/zero -of openbsd73-disk.img -bs 1 -count 1 -seek `{echo 20*1024*1024*1024-1 | pc -n}
</code></pre>

<p>The next step is to boot the ramdisk kernel (<code>bsd.rd</code>) and attach the <code>install73.img</code> filesystem and the <code>openbsd73-disk.img</code> virtual block device. The installation procedure is largely standard.</p>

<pre><code>term% vmx -d openbsd73-disk.img -d install73.img -n ether0 -v vesa:640x480 bsd-unziped.rd
</code></pre>

<p>When the vm starts it will swallow the shell window.</p>

<p>The command flags are fairly self explanatory but I will proceed with verbosity.</p>

<ul>
<li><code>-d</code> specifies a “disk”. This can be a disk image or a physical hard drive. I specified 2: the drive I want to install to then the disk image that contains installation files.</li>
<li><code>-n</code> specifies a networking device. On my system ether0 is the rj45 port and ether0 is my wireless card. To avoid potential problems I decided to place the host and guest on separate devices. Do not fall for the POE scam. It’s way too slow.</li>
<li><code>-v</code> sets a graphics device and a virtual PS/2 keyboard and mouse are automatically attached to the guest. vesa:XxY specifies a screen resolution. Not all guests will support this.</li>
</ul>

<p>The final argument is the kernel to load. <code>vmx(1)</code> is strange in that it loads a kernel directly instead of going through a bootloader.</p>

<p>If OpenBSD panics, try re-running with the <code>-M 1G</code> flag to increase the memory available to the guest. The default is 64M and this isn’t quite enough to do memory intensive processes.</p>

<p>The rest of the installation procedure is easy if you remember to <code>(H)alt</code> instead of <code>(R)eboot</code>.</p>

<p>I also did not start xenodm by default in the case that X refuses to start for some reason. I manually start xenodm every time I start the guest. Console is king.</p>

<h1>Running the guest</h1>

<p>After the guest is installed it can be ran (almost) like a normal system. Again, the shell window will be swallowed.</p>

<pre><code>term% vmx -M 1G -n ether0 -d openbsd73-disk.img -v vesa:640x480,1024x768 bsd 'db_console=on'
</code></pre>

<p>In contrast to the installation steps: the <code>bsd</code> kernel is being loaded and we are passing it the argument of <code>db_console=on</code> which will allow acces to the kernel debugger.</p>

<p>On every boot, the root disk must be specified. I typically just mash the enter key, type 4 characters, then continue mashing the enter key like I do during the installation. The process for selecting a root disk looks something like:</p>

<pre style="background-color: black; color: white;"><code><span style="background-color: blue;">root device: </span>
<span style="background-color: blue;">use one of: exit vio0 sd0[a-p]</span>
<span style="background-color: blue;">root device: </span>sd0
<span style="background-color: blue;">swap device (default sd0b): </span> sd0b
</code></pre>

<p>The system will proceed to boot to a console. After booting, log in and start the <code>xenodm</code> service.</p>

<p><img class="posts" src='assets/images/obsd-9-vm-1.png'></p>

<pre><code>openbsd# rcctl -f start xenodm
</code></pre>

<p><img class="posts" src='assets/images/obsd-9-vm-2.png'></p>

<h1>Stopping the vm</h1>

<p>This should be fairly intuitive. In the guest:</p>

<pre><code>openbsd# halt
</code></pre>

<p>Press left control+alt to release the cursor then, on the host, run:</p>

<pre><code>term% kill vmx | rc
</code></pre>
</div>
</div>
<?php include "../includes/footer.html"; ?>
