<?php include "../includes/header.html"; ?>
<title>Software Minimalism is UNIX Maximalism</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<p><span><b>Software minimalism is UNIX maximalism</b></span></p>
<p><span><span>Frequently I find myself reading forum threads about
software minimalism. In these threads, UNIX users discuss the
concept of software minimalism. They are Arch, Gentoo, and BSD
users. They use tiling window managers, render web pages in their
heads, write C and use shell duct tape for everything in-between.
The initial goal for many of these users is to remove unnecessary
distractions, inefficient paradigms, and unnecessary system calls.
The end goal for many of these users is to assemble a minimum
viable operating system. Funnily enough, the minimum viable
operating system is really just a UNIX system.</span></span></p>
<p><span>The purpose of software minimalism stems from various
motivations which ultimately boil down to "reduce code complexity"
and "make it scriptable". The latter is an innate UNIX attribute
while the former is a lost virtue. Modular operating systems with
many small programs that work together reduce the total number of
application specific programs. Ultimately, minimalists strive to
avoid "bloat".</span></p>
<p><span>Reducing code complexity and avoiding superfluous features
increases the maintainability of software which, in turn, decreases
the total number of bugs in said program. Oftentimes this means
adhering to the UNIX philosophy. "Worse is better"; the programmer
sacrifices cleverness for readability. "Do one thing and do it
well"; the programmer decides that unused features are unaudited
attack vectors. The UNIX philosophy is far from a minimum viable
philosophy but it’s the least complicated general purpose
philosophy. Anything less seems to be application specific and is
lacking in the duct tape programming department.</span></p>
<p><span>Scripting and automation is the secondary aspect of
software minimalism. The ability to attach arbitrary commands
together by redirecting the output of one command into the input of
another is sometimes called "meta programming" or "duct tape
programming". The duct tape programmer rarely invents his own tools
and prefers to use existing tools instead. It is theoretically
possible for the meta programmer to write any possible program
considering the fact that some shells (bash) are turing complete
and various POSIX compliant programs are turing complete (awk, sed,
etc). When the programmer wants to take a shortcut, he can write
his own program that fits into the pipeline. An example of a
program like this is the "jq" program which turns the nightmare
fuel that is json into something that works with grep.</span></p>
<p><span>The goal of the software minimalist is to avoid
"bloatware". Any so called "windows power user" (misnomer) will be
familiar with so called "windows debloater scripts" (another
misnomer). The alleged function of such a script is to remove all
of the pre-installed garbage that increases resource utilization
with no real benefit to the end user. The UNIX minimalist is much
more skeptical though. He understands that removing random programs
from an operating system without a functional command line
interface is an exercise in futility. He is skeptical of graphical
programs in general because they prevent automation. The most
cynical definition of software bloat is "anything I don’t like". A
more robust definition is "anything that doesn’t follow the UNIX
philosophy". An operating system that feels generally UNIX-like and
is at least a crude mockery of the philosophy is effectively an
implementation of UNIX.</span></p>
<p><span>The term "UNIX implementations" was chosen intentionally
because the author finds the term "distro" to be auditorialy gaudy.
Any UNIX-like operating system is an implementation of UNIX. Some
might call this POSIX but Plan 9 is strictly not a POSIX system yet
it does a better job at implementing the spirit of UNIX than
GNU/Linux ever could. These minimal systems include Plan 9,
FreeBSD, OpenBSD, NetBSD, Alpine Linux, Gentoo Linux, etc. Any free
UNIX that is not Linux is sufficiently minimal. Any Linux without
{systemd, freedesktop, pulseaudio, dbus, wayland, GTK, KDE,
anything GNU whatsoever} is sufficiently minimal. There are,
however, still ways to rebloat a minimal system.</span></p>
<p><span>The most common way of adding bloat to a minimal system
(because all of the systems listed above are minimal by default) is
to install unnecessary components. Nearly every system is feature
complete out of the box because any system worth using comes with a
compiler, linker, debugger, standard utilities that can be used
together with a shell for scripting, standard daemons, etc. Is a
graphical version of the "ls" command</span> <span>necessary? What
about a graphical interface to search for files when the "find" and
"locate" commands already exist yet somehow manage to be faster and
more reliable than either KDE’s baloo or GNOME’s tracker indexers?
Graphical tools are largely unnecessary.</span></p>
<p><span>Avoiding bloat is quite easy. Checking what dependencies a
package requires when installing is an easy way to determine if a
package is bloat or not. A package that has 0 dependencies (please
check if they are already installed) is probably better than a
package with multiple gigabytes of dependencies. The terminal does
have a learning curve but eventually it becomes the most productive
environment possible. Typing blind is easy. Clicking blind is
impossible.</span></p>
<p><span>So, if gui is bloat and non-UNIXes are bloat, doesn’t that
mean that using a UNIX command line "as intended" is the only way
to accomplish general purpose software minimalism? It’s probably
not the</span> <span>way but it sure is</span> <span>way. The
average software minimalist has working knowledge of most "obscure
and scary" commands. The ideology applies to UNIX enthusiasts and
hipsters alike.</span></p>
<p><span>I highly encourage all current Linux and UNIX users to
follow me into the software minimalism rabbit hole. It is wonderful
down here. I have been the rabbit for a substantial amount of time
and looking out to the "normal people" use computers only fills me
with absolute confusion. The terminal is my home and I don’t want
to leave.</span></p>
</div>
</div>
<div class="row">
<div class="col-4"></div>
<div class="col-4">
<p style="text-align: center;">This article was written in roff using vim. It was compiled to HTML and plaintext on OpenBSD using troff and htmlroff from plan9port. I have updated this page and the tarball to use simpler (ie small screen friendly) html and have added support for pdf and simple-html in the makefile. These additional options require ghostscript and www/tidy. <a href="assets/software-minimalism-is-unix-maximalism.tgz">The source files are available for download in a tarball</a></p>
</div>
<div class="col-4"></div>
</div>
<?php include "../includes/footer.html"; ?>
