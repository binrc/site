<?php include "../includes/header.html"; ?>
<title>Alpine Linux vm on OpenBSD's vmd(8)</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>Alpine Linux vm on OpenBSD's vmd(8) </h1>
<h1 id="why-alpine">Why alpine?</h1>
<ul class="incremental">
<li>fedora bad
<ul class="incremental">
<li>anaconda sucks.</li>
<li>even passing <code>console=ttyS0</code>, anaconda still doesn't want
to give me a serial console.</li>
<li>red hat is very quickly marking all the things I actually use in
Fedora as "deprecated"</li>
</ul></li>
<li>alpine good
<ul class="incremental">
<li>automatically gives you a serial console</li>
<li>no grubbing around in the massive operating system that is
grub2</li>
<li>small</li>
<li>I need flatpak for the (2) friends I bullied into using signal and
won't listen to me when I suggest "more free" solutions</li>
</ul></li>
<li>really just want plan 9 but I can't tell if my hypervisor config is
bad or if the guest is bad (it was the hypervisor (I have difficulties
typing 255.255.255.0 (it always ends up being 225.225.225.0)))</li>
</ul>
<h1 id="the-setup">The setup</h1>
<p>shell commands:</p>
<pre><code>daedalus.localnode# mkdir -p /var/vmd &amp;&amp; cd /var/vmd
daedalus:localnode# curl https://dl-cdn.alpinelinux.org/alpine/v3.18/releases/x86_64/alpine-standard-3.18.0-x86_64.iso &gt; alpine-standard-3.18.0-x86_64.iso
daedalus.localnode# vmctl create -s 25G alpine.qcow2</code></pre>
<h2 id="vm">vm</h2>
<p>Write a configuration file for <code>/etc/vm.conf</code></p>
<pre><code>#   $OpenBSD: vm.conf,v 1.11 2021/11/11 09:38:14 claudio Exp $

vm &quot;alpine&quot;{
    disable 
    memory 2048M
    disk &quot;/var/vmd/alpine.qcow2&quot;
    cdrom &quot;/var/vmd/alpine-standard-3.18.0-x86_64.iso&quot;
    owner root
    interface{
        switch &quot;local&quot;
    }
}

switch &quot;local&quot; {
    interface bridge0
}</code></pre>
<h2 id="networking">networking</h2>
<p>2 interfaces are needed: <code>vether0</code> and
<code>bridge0</code>:</p>
<p><code>/etc/hostname.vether0</code></p>
<pre><code>inet 10.0.0.1 255.255.255.0</code></pre>
<p><code>/etc/hostname.bridge0</code></p>
<pre><code>add vether0</code></pre>
<p>ip forwarding should also be enabled if the vm should be allowed to
talk to the internet:</p>
<p><code>/etc/sysctl.conf</code></p>
<pre><code>#	$OpenBSD: sysctl.conf,v 1.5 2019/09/18 08:39:06 solene Exp $
net.inet.ip.forwarding=1
net.inet6.ip6.forwarding=1</code></pre>
<p>A dhcp server is also needed so that the guests can automatically
configure networking. An alternative option is to use static
addresses.</p>
<p><code>/etc/dhcpd.conf</code></p>
<pre><code>#   $OpenBSD: dhcpd.conf,v 1.1 2014/07/11 21:20:10 deraadt Exp $
# vmd
subnet 10.0.0.0 netmask 255.255.255.0 {
    option routers 10.0.0.1;
    range 10.0.0.2 10.0.0.255;
    option domain-name &quot;vmd.my.domain&quot;;
    option domain-name-servers 1.1.1.1;
}</code></pre>
<p>And finally, all the pieces must be assembled using pf.conf. This
configuration file contains extra things that aren't strictly necessary
for virtualizatoin with networking.</p>
<p><code>/etc/pf.conf</code></p>
<pre><code>#   $OpenBSD: pf.conf,v 1.55 2017/12/03 20:40:04 sthen Exp $

set skip on lo0

# hypervisor config, allow SSH and DHCP. 
# Note: this configuration can cause your system to become a rogue 
# DHCP service on the external LAN if your dhcpd flags contain more interfaces than &#39;vether0&#39;
tcp_services=&quot;{ssh bootps bootpc}&quot;
udp_services=&quot;{bootps bootps}&quot;
block in on {em0 iwn0}
pass in proto tcp to any port $tcp_services keep state
pass in proto udp to any port $tcp_services keep state
pass out all

# By default, do not permit remote connections to X11
block return in on ! lo0 proto tcp to port 6000:6010

# Port build user does not need network
block return out log proto {tcp udp} user _pbuild

# VMD config
pass out on egress from 10.0.0.0/24 to any nat-to (egress)
pass in proto { udp tcp } from 10.0.0.0/24 to any port domain rdr-to 1.1.1.1 port domain</code></pre>
<p>Run some commands to make things work:</p>
<pre><code>daedalus.localnode# sysctl net.inet.ip.forwarding=1
daedalus.localnode# sysctl net.inet6.ip6.forwarding=1
daedalus.localnode# rcctl enable dhcpd vmd
daedalus.localnode# rcctl set dhcpd flags vether0
daedalus.localnode# rcctl start dhcpd vmd
daedalus.localnode# pfctl -e /etc/pf.conf</code></pre>
<h1 id="installing-alpine">installing alpine</h1>
<p>Should be easy enough. If you're having trouble your
<code>pf.conf</code> is probably borked. Remember that pf matches
packets sequentially (from top to bottom) and the last matching rule is
selected. You can remedy this by moving more specific rules to the
bottom of the file or by using the <code>quick</code> keyword to
indicate that pf should stop here and use this rule as the logical
"final matching rule".</p>
<pre><code>x220# vmctl start -c alpine
Connected to /dev/ttyp5 (speed 115200)

Boot failed: not a bootable disk

ISOLINUX 6.04 6.04-pre1 ETCD Copyright (C) 1994-2015 H. Peter Anvin et al
boot:

Welcome to Alpine Linux 3.18
Kernel 6.1.27-2-lts on an x86_64 (/dev/ttyS0)

localhost login: root
Welcome to Alpine!

The Alpine Wiki contains a large amount of how-to guides and general
information about administrating Alpine systems.
See &lt;https://wiki.alpinelinux.org/&gt;.

You can setup the system with the command: setup-alpine

You may change this message by editing /etc/motd.

localhost:~# setup-alpine</code></pre>
<p>The rest of the alpine setup is documented in the <a
href="https://docs.alpinelinux.org/user-handbook/0.1a/index.html">Alpine
user handbook</a> and is about as easy as installing OpenBSD. When
finished, the system should be stopped using the <code>poweroff</code>
command.</p>
<p>The serial connection will hang and the system will (slowly) halt.
The .iso line should now be commented out in /etc/vm.conf and the vmd
service should be restarted. You can now restart alpine and proceed to
install the things you need. I will do this over ssh because it's better
than serial. This also enables me to test the guest's ssh config before
I spend 3-4 hours trying to figure out why X forwarding doesn't work
just to find out that it's something I overlooked. Keep in mind that
default openssh server configurations typically disable x11 forwarding
by default so this will also need to be modified.</p>
<p>Using VNC to access graphical programs running on the guest is also
possible but this would require making the guest significantly larger
than it needs to be.</p>
<p>To make this kluge work, xorg and flatpak must be installed. Note
that you will need to enable edge/main and edge/community repos to
install flatpak.</p>
<pre><code>alpine:~$ su 
alpine:~# setup-xorg-base
alpine:~# apk add flatpak
alpine:~# flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
alpine:~# flatpak install flathub org.signal.Signal
alpine:~# exit
alpine:~$ dbus-run-session flatpak run org.signal.Signal</code></pre>
<p>And now signal should run.</p>
<figure>
<img src="assets/images/signal-on-openbsd.png" alt="Signal on OpenBSD, running in a vm via x11 forwarding over ssh" />
<figcaption aria-hidden="true">Signal on OpenBSD, running in a vm via
x11 forwarding over ssh</figcaption>
</figure>
<p>I hate dbus so much it's unreal.</p>
</div>
</div>
<?php include "../includes/footer.html"; ?>
