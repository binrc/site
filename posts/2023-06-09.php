<?php include "../includes/header.html"; ?>
<title>XMPP server on OpenBSD</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>XMPP server on OpenBSD</h1>

<p>xmpp is an open protocol that is most often used for instant messaging. It has many extensions to the protocol and has potential to be the "$CHAT_APP killer". Oftentimes xmpp is overlooked due to the somewhat difficult onboarding process caused by the decentralized design of the protocol. Other open im protocols also exist but ultimately xmpp seems to be the optimal choice choice considering the common issues with other instant messaging solutions: </p>

<ul>
<li> p2p programs rely on a central tracker (in the bittorrent sense). Not supremely useful considering that you can't convince anyone to use them. </li>
<li> slack clones are webshit, typically unencrypted, and very processor intensive on both clients and servers</li>
<li> the matrix.org question</li>
	<ul>
	<li> e2e encrypted but leaks metadata</li>
	<li> functionally centralized (I have a suspicion that the devs intentionally made it as difficult as possible to self host in order to scrape more metadata and to sell more SAAS solutions)</li>
	<li> server is HEAVY (database is huge, memory usage will run up your vps bill)</li>
	<li> "default" client is HEAVY (electron webshit)</li>
	<li> <a href="https://web.archive.org/web/20201219014215/https://samba.noblogs.org/post/2018/08/27/matrix-org-a-federated-app-funded-by-a-mossad-company/">possibly/probably funded by alphabet soup intelligence agencies</a> </li>
	</ul>
<li> irc is dead. Long live irc </li>
</ul>

<p>xmpp is the ideal solution for my use case for the following reasons: dead simple to install and configure a server with the software of your choice, costs basically zero system resources to run an xmpp server, wide array of client choices, extensible protocol, decentralized in practice, has both client-server encryption and server-server encryption, can easily configure many popular clients to use OMEMO encryption (this is double ratchet, same thing signal.org uses), and is an open standard. </p>

<p>In all honesty, I would likely not even be considering xmpp as the "signal killer" if signal-desktop worked on OpenBSD. This xmpp idea would likely not even be a consideration for me if signal made it easier to self host signal without also recompiling and distributing clients for all of your contacts. The one guy who uses an iphone would likely never be able to use my own custom signal server without paying apple thousands of dollars. So xmpp it is. </p>

You should be using a client that supports OMEMO for maximum perfect forward secrecy. <a href="https://omemo.top/">You can find a list of clients that support omemo here. </a>.

<h1> The setup </h1> 

<p>I like <a href="https://prosody.im/">Prosody</a> because it's modular and easy to set up. You will need a domain name and ssl certs for this configuration to work. If you do not have ssl certs, you should probably use <a href="acme.sh">acme.sh</a> to obtain one because some (if not many) other xmpp  servers will refuse connections if your server has invalid (ie self signed) certificates. In my configuration, I am using the same certificates I use for my website because they seem to work. I simply add a cron job that copies the certs to where prosody expects. </p>

<pre><code>openbsd# pkg_add prosody
openbsd# cp /etc/ssl/my.domain.name.fullchain.pem /etc/prosody/certs/my.domain.name.crt 
openbsd# cp /etc/ssl/private/my.domain.name.key /etc/prosody/certs/my.domain.name.key
openbsd# chown root:_prosody /etc/prosoty/certs/my.domain.name.crt
openbsd# chown root:_prosody /etc/prosoty/certs/my.domain.name.key
openbsd# crontab -e
openbsd# crontab -l
0       0       1       *       *       /usr/sbin/acme-client -F my.domain.name; rcctl reload httpd
30      0       1       *       *       cp /etc/ssl/my.domain.name.fullchain.pem /etc/prosody/certs/my.domain.name.crt && cp /etc/ssl/private/my.domain.name.key /etc/prosody/certs/my.domain.name.key && rcctl restart prosody
</code></pre>

<p> The next step is to configure prosody. This is fairly simple. Find the section about Virtual hosts and add your domain name. The default settings are probably fine. </p> 

<pre><code> ----------- Virtual hosts -----------
-- You need to add a VirtualHost entry for each domain you wish Prosody to serve.
-- Settings under each VirtualHost entry apply *only* to that host.

-- VirtualHost "localhost"
-- Prosody requires at least one enabled VirtualHost to function. You can
-- safely remove or disable 'localhost' once you have added another.


VirtualHost "my.domain.name"
</code></pre>

<p> Firewall rules must also be modified and reloaded with <code>pfctl -ef /etc/pf.conf</code> </p> 
<pre><code>set skip on lo
tcp_services="{xmpp-client, xmpp-server}"
block in all
pass in proto tcp to any port $tcp_services keep state
pass out all
</code></pre>

<p> And now we can start the server, register our user, and connect to the server using a remove client. </p> 

<pre><code>openbsd# rcctl start prosody
openbsd# prosodyctl adduser my_username@my.domain.name
</code></pre>

<p> The remainder of the setup process is troubleshooting and finding someone to test with. In order to help others test their servers, clients, and OMEMO, you can message me via xmpp by referring to my <a href="/contact.php">contact page</a>. </p>




</div>
</div>
<?php include "../includes/footer.html"; ?>
