<?php include "../includes/header.html"; ?>
<title>Building a decentralized webring</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>Building a decentralized webring</h1>

<p> I have known about the decentralized <a href="https://sizeof.cat/post/lainring/">lainchan webring</a> for quite some time now but it wasn't until recently that I considered the actual mechanim behind this webring. It's decentralized in the sense that there is no central server hosting the webring. Users can add their own websites to this webring by creating a badge and linking to their website in a thread on lainchan's /tech/ board. This entire process seems somewhat frustrating to me because none of the existing lainring hosts seem to be entirely up to date with the thread. I propose an alternative to this decentralized webring (that still relies on a centralized imageboard to correlate peers). </p> 

<h2> Enter Dring </h2> 

<p> Dring is my attempt to remedy the main issues I see with lainring: it's not entirely automatic and not entirely peer to peer. Dring is intentionally bare bones as I intend for every user to be capable of quickly writing their own dring clients and servers so that they can easily join the ring without a complete and total redesign of their existing web stack. </p>

<h2> How it works </h2> 

<p> Dring is very simple. Every server in the webring hosts a file called <code>.dring.csv</code> in their document root. The location is common on purpose in order to simplify scripting. The csv is formatted as follows: </p> 

<pre><code>url,title,rss,tor,i2p
"https://0x19.org","0x19","https://0x19.org/posts/feed.php","http://ilsstfnqt4vpykd2bqc7ntxf2tqupqzi6d5zmk767qtingw2vp2hawyd.onion:8080",""
</code></pre>

<p> This csv file is dual purpose: it is used both for the client (script that pulls form peers) and the server (part that displays the .csv nicely for website viewers). </p> 

<p> Writing a client for dring is trivial: cut the first line out, take the first field of the csv, append "/.dring.csv" to all of the urls, then pull all of those urls and dump them into a single mega csv file. This mega csv file can be deduplicated quite easily using standard shell utilities. This client script should run via a cron job in order to make syncronization of links automatic. Pinging other people's web servers every time your web server gets a request is impolite so anything *too* automatic should be avoided. </p> 

<p> The server code is also trivial: convert the .csv to a fancy html table (or whatever format you would like). </p> 

<p> <a href="https://gitlab.com/binrc/dring">Source code for dring is available in this gitlab repository. </a></p>

<h2> Security considerations </h2> 

<p> I have not taken much time to make this system bulletproof. If a bad actor enters the webring it is likely that he might be able to achieve at least a code injection on certain servers. It is advised that you run the cron job as a daemon user to prevent too much damage from occuring. If you have any suggestions or see a major issue I overlooked please let me know or submit a merge request. Dring was not designed to be bulletproof as it is not a zero trust system. </p> 

<h2> Joining the webring </h2> 

<p> Currently, I am the only user of dring. If you would like to join dring please send me an email or submit a merge request for my repo. Please refer to the repo for more techncial information. </p> 

<p> If you would like to view a live copy of dring, you can go to <a href="/dring.php">my current copy of the dring link list. </a></p> 




</div>
</div>
<?php include "../includes/footer.html"; ?>
