<?php include "../includes/header.html"; ?>
<title>All about i2p (and running eepsites with i2pd)</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1> For clients: </h1> 

<h2> Garlic routing (onions++) </h2> 

<p> In order to explain i2p I will compare it to tor. On the tor network, there are clients, nodes, and endpoints. Every user running the torbrowser is a client that sends traffic through a circuit of nodes. Return traffic is sent from the endpoint back through the circuit to the client. The tor network has a few major issues: timing/correlation attacks, evil exit nodes, and the very fact that it can be (and is likely is) compromised by three letter agencies. <p> 

<p> i2p is somewhat similar to tor but it's very different in that it's a peer to peer network. Instead of using onion routing, i2p uses garlic routing. Every host in the i2p network is also a router in the network. When a router submits traffic it splits it up into multiple packets, garbage data is added to each packet so that all packets are the same size, the packet is mixed together with packets received from other routers, then each bundle of packets is forwarded to one of many random tunnels connected to the router. This makes it very difficult to determine where a packet came from, who a packet came from, and who the intended recipient of the packet is. When all of these packets reach the server they are reassembled and a response is sent back using the same process. Unlike tor, tunnels in i2p are unidirectional. </p> 

<h2> Software choices </h2> 

<p> There are two: i2p (java) and i2pd (c++). The java i2p client should be fine for most "client only" users. i2pd is typically preferred for "server only" use cases although the improved performance can make it a better choice for users who aren't afraid to modify configuration files by hand. Anecdotally, i2pd is significantly faster and lighter than the "official" java implementation. Either way, both will work just fine. If you are using java i2p, your web console will be at <code>127.0.0.1:7657</code>. If you are using i2pd, your web console will be at <code>127.0.0.1:7070</code>. Care should be taken to prevent this web console from being accessed on interfaces other than the loopback device. </p> 

<h2> Browser Configuration </h2> 

<p> Unlike tor, there is not really an official browser bundle for i2p. I am aware of the existence of a windows only i2p browser bundle but this is an unacceptable solution for me. Running an encrypted overlay network on top of a pile of backdoors is counterintuitive. </p>

<p> You probably should not follow the browser setup instructions on the official i2p project website. I followed these instructions just to see what would happen and it turns out that they cause dns leakage. Here is the output from wireshark as I try to load the i2p project's website over i2p. Note that dns queries <i> SHOULD NOT </i> be going to my system's regular dns servers. </p>

<pre><code>3014    192.168.0.101  9.9.9.9        DNS  Standard query 0x1431 A www.i2p-project.i2p OPT
3015    192.168.0.101  9.9.9.9        DNS  Standard query 0xa004 AAAA www.i2p-project.i2p OPT 
3016    9.9.9.9        192.168.0.101  DNS  Standard query response 0x1431 No such name A www.i2p-project.i2p SOA a.root-servers.net OPT
3017    192.168.0.101  9.9.9.9        DNS  Standard query 0x1431 A www.i2p-project.i2p
3018    9.9.9.9        192.168.0.101  DNS  Standard query response 0xa004 No such name AAAA www.i2p-project.i2p SOA a.root-servers.net OPT
</code></pre>

<p>Instead of leaking DNS all over the internet I decided to configure torbrowser for use with i2p instead. Torbrowser a modified version of firefox so we can use firefox profiles to make separate profiles for i2p and tor. The first step is to open <code>about:profiles</code> and create a new profile. Launch the i2p profile and modify some settings in <code>about:config</code></p>

<pre><code>javascript.enabled                    = false
network.proxy.http                    = 127.0.0.1
network.proxy.http_port               = 4444
network.proxy.no_proxies_on           = 127.0.0.1
dom.security.https_first_pbm          = false
dom.security.https_only_mode          = false
network.proxy.socks_remote_dns        = false
extensions.torbutton.use_nontor_proxy = true
</code></pre>

<p>I also had to open <code>about:preferences</code> and disable https-only mode there. I'm not sure if I overlooked something in <code>about:config</code> or if there is just a bug. You might need to tweak settings and restart the browser multiple times for it to work as expected. After making a custom profile in torbrowser I am no longer seeing DNS leaks. Be sure to use the correct profile for the correct network. </p>

<p> Users with something to lose should refer to <a href="/posts/2023-05-06.php">my previous article about tor</a> for a poorly worded introduction to opsec and how to <i>not</i> deanonymize yourself when using an overlay network. </p>


<h1> For servers: </h1> 

<p> As always, instructions for OpenBSD follow: </p> 

<pre><code>openbsd# pkg_add i2pd
</code></pre>

<p> Read <code>/usr/local/share/doc/pkg-readmes/i2pd</code>. As of writing, the instructions are as follows: </p> 

<pre><code>1. Configure i2pd login class in the login.conf(5) file:

        i2pd:\
                :openfiles-cur=8192:\
                :openfiles-max=8192:\
                :tc=daemon:

2. Adjust kern.maxfiles, if needed:

        # sysctl kern.maxfiles=16000
        # echo "kern.maxfiles=16000" &gt;&gt; /etc/sysctl.conf
</code></pre>

<p> You should run <code> cap_mkdb /etc/login.conf</code> for the login class changes to take effect. </p> 

<p> Once the limits for the <code>_i2pd</code> daemon are increased you should edit the i2pd configuration file at <code>/etc/i2pd/i2pd.conf</code>. You should come up with a random (static) port number to use so that you can appropriately create a firewall rule. I did this by running <code>seq 1000 9999 | sort -R | head</code> and picking one at random. In my configuration file I disabled pretty much everything but the web console (which I will be disabling at a later step) and decreased the default traffic limits. A pf rule to allow udp on this randomly selected port should also be created and loaded. </p> 

<p> Edit <code>/etc/i2pd/tunnels.conf</code>. Some examples exist but you can easily create your own. I am mirroring my existing website to i2p so I also added a virtual server in <code>/etc/httpd.conf</code> to listen on 9090 without ssl because my default httpd configuration redirects port 80 to 443 and I want to minimize potential issues.</p> 

<pre><code>[anon-http]
type = http
host = 127.0.0.1
port = 9090
keys = anon-http.dat
</code></pre>

<p> Restart services and wait. It may take 20 minutes or more for your eepsite to become accessible. While you are waiting you can find your b32 address and then proceed to disable the web console in your configuration file. I found my b32 address like this: </p> 

<pre><code>openbsd# curl --silent http://127.0.0.1:7070/?page=i2p_tunnels  | grep .b32.i2p | sed 's/&lt;[^&gt;]*&gt;//g'
anon-http ⇒ xzh77mcyknkkghfqpwgzosukbshxq3nwwe2cg3dtla7oqoaqknia.b32.i2p:9090
</code></pre>

<p> Note that there are other ways to find your b32 address but this was the easiest way I found for my remote server. If you expect your service to <i>not</i> routinely crash and burn you can create a fancy short host name by using one of the many identity services on the i2p network. </p> 

<p> Refer to any of my previous articles if you need help with OpenBSD's httpd or pf. </p>

<h1> Conclusion </h1> 
<p> I initially decided to learn more about overlay networks in the event that current encryption methods like double-ratchet messaging or tls become illegal if they are not sufficiently backdoored. Various anti-encryption government officials and agencies are yet again attempting to break any notion of online freedom, privacy, and security via the <a href="https://act.eff.org/action/stop-the-restrict-act-and-pass-real-privacy-legislation">RESTRICT act (eff)</a> and <a href="https://act.eff.org/action/the-earn-it-act-is-back-seeking-to-scan-us-all/">EARN IT act (eff)</a>. Ultimately these acts result in banning/backdooring all encryption and banning any and all data aggregation services ran by a non-US company (ie tik tok but really it will be any service that collects a nonzero amount of data and doesn't automatically submit that data to various alphabet boys). I do not want to live in a world where the internet is censored either by force or by instilling fear in those of us who have any thoughts that go against the accepted $CURRENT_AGENDA. I really do think that i2p is the future for many of us hacker types if these legislating leeches get their way, especially considering that i2p was designed to work with <i>any</i> existing protocol in contrast to tor which must be <i>retrofitted</i> to work with non-http protocols. </p> 

<p> My argument for the privacy of free software becomes even more poignant in a world like this. Proprietary software is <i>already</i> backdoored. It is very likely that your favorite baby duck proprietary operating system is capturing your packets before they're even encrypted. They already have your keystrokes, files, <a href="https://arstechnica.com/information-technology/2023/05/microsoft-is-scanning-the-inside-of-password-protected-zip-files-for-malware/">and are even cracking your encrypted archives in the name of "malware prevention"</a>. It really doesn't matter if users of proprietary software lose encryption and software freedom because <i>they never really had it to begin with</i>. For free software users, however, we really have something to lose. We already have secure and private operating systems but all of this will stop mattering if cryptography devs are forced to backdoor their existing tools. So what if we don't have in-kernel keyloggers and built in spyware if the only way to maintain our current level of security is by only ever using airgapped computers? I do not want to live in the future if anti-freedom legislation is what I have to look forward to. </p>

<p> So my conclusion here is to utilize anonymizing overlay networks. If true cryptography becomes illegal we can easily become harder to find "criminals" by pushing traffic through existing networks like tor or i2p. From a technical aspect, I think that i2p is superior to tor and it's where I'll be moving if the feds seize our freedom of speech and association with bait-and-switch legislation. </p> 

</div>
</div>
<?php include "../includes/footer.html"; ?>
