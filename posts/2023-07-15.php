<?php include "../includes/header.html"; ?>
<title>Bypassing the arbitrary hardware requirements for Win11</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1> Bypassing the arbitrary hardware requirements for Win11 </h1>

<p> <b>Motivation: </b> I need a winshit vm in order to capture packets from a windows machine. Win11 is intentionally difficult to virtualize but luckily we can modify certain values in the registry to make this possible. These modifications should work for most hardware configurations. I still, however, do not recommend using windows in any capacity. </p> 

<p> Boot the windows installation disk and when the language selection window appears, type <code>Shift+F10</code> to open the command prompt. In order to make this as foolproof as possible, below are registry commands that bypass TPM, secure boot, and ram requirements. </p> 

<pre><code>Microsoft Windows [Version 10.0.22621.1702]
(c) Microsoft Corporation. All rights reserved. 

X:\sources&gt;REG ADD HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig
The operation completed successfully. 

X:\sources&gt;REG ADD HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig /v BypassTPMCheck /t REG_DWORD /d 1
The operation completed successfully. 

X:\sources&gt;REG ADD HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig /v BypassSecureBootCheck /t REG_DWORD /d 1
The operation completed successfully. 

X:\sources&gt;REG ADD HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig /v BypassRamCheck /t REG_DWORD /d 1
The operation completed successfully. 

X:\sources&gt;REG ADD HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig /v BypassCPUCheck /t REG_DWORD /d 1
The operation completed successfully. 

X:\sources&gt;REG QUERY HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig 

HKEY_LOCAL_MACHINE\SYSTEM\Setup\LabConfig
	(Default)		REG_SZ
	BypassTPMCheck		REG_Dword	0x1
	BypassSecureBootCheck	REG_Dword	0x1
	BypassRamCheck		REG_Dword	0x1
	BypassCPUCheck		REG_Dword	0x1
</code></pre>

<p> Proceed with the installation as normal. <p> 

<p> To create an offline user account, simply disable internet connectivity during the installation process. If disconnecting from the internet does not work, open the command prompt again and run <code>ncpa.cpl</code>, navigate to and disable your network interface, proceed to create an offline account. Be sure to re-enable this interface if you intend to use this machine on the network. </p> 

<p> After installation there are a few nice things you can do. If you are installing win11 in KVM, you should download and drivers from <a href="https://www.linux-kvm.org/page/WindowsGuestDrivers">virtio-win.iso</a>. Do not give microsoft money, instead use <a href="https://github.com/massgravel/Microsoft-Activation-Scripts">the massgravel microsoft activation scripts</a> to spit in the face of the corporation that refuses to pay out on windows refund day. </p> 

<p> Updating the system will inevitably require repeating these steps. To disable updates, we can modify the registry again. </p>

<pre><code>Microsoft Windows [Version 10.0.22621.1702]
C:\Windows\System32&gt;REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate 
The operation completed successfully. 

C:\Windows\System32&gt;REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU
The operation completed successfully. 

C:\Windows\System32&gt;REG ADD HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU /v NoAutoUpdate /t REG_DWORD /d 1
The operation completed successfully. 
</code></pre>

<p> I will never pay microsoft. I will never use windows. All I want is to educate end users on how to use the things they own as they see fit with absolute disregard for any and all restrictions placed upon them by the manufacturer. </p> 
</div>
</div>
<?php include "../includes/footer.html"; ?>
