<?php include "../includes/header.html"; ?>
<title>Announcing a new essay based web project</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1 >Proposing a new, essay based website</h1>
<p>While I was thinking about existing social media sites, I realized that many of these sites share the same issues. These are <em>anti features</em> that harm the user or simply waste the user&#8217;s time. These include: </p>
<ul>
<li>privacy concerns</li>
<li>platform enforced censorship</li>
<li>low quality content
<ul>
<li>spam</li>
<li>ai generated trash</li>
<li>influencer garbage</li>
<li>everything is an advertisement</li>
</ul></li>
<li>voting systems that create user enforced censorship and&#47;or popularity contents</li>
</ul>
<p>I decided make my own website to remedy these issues in a few ways. This website is at <a href="https://justletmewrite.org">justletmewrite.org</a>. </p>
<ol>
<li>privacy and data</li>
</ol>
<p>My website does not allow image upload and encourages users to use pseudonyms. There are no ads and no collection of unnecessary data. You are free to delete your posts at any time (they are actually deleted from the database, not just hidden like with every other platform). </p>
<ol start="2">
<li>uncensored</li>
</ol>
<p>Because I have not allowed image uploads, I don&#8217;t see any need to moderate this website. If your post is legal in the US (it&#8217;s hard to make something illegal using only text) it&#8217;s allowed on the website. You can write about anything you want. Registration is free for everyone and you don&#8217;t need an account to view posts on the website.</p>
<ol start="3">
<li>High effort content only</li>
</ol>
<p>High effort != high quality but there is some correlation between effort and quality. Every post and comment must be unique (using a ROBOT9000 type system with sha256sums). Comments must be at least 500 characters long and posts must be at least 2000 characters long. The robot exists to reduce spam and the minimum character limit exists to force users to post high effort content only. 2000 characters is about 3 paragraphs so every post is at least a short essay. A post will take about 15 minutes to write and a comment will take a few minutes. </p>
<ol start="4">
<li>voting system</li>
</ol>
<p>I did implement a voting system but it&#8217;s not like other voting systems. On certain platforms, voting will directly increase or decrease post visibility on the website. On my website, the voting system is an average grade calculated from votes by other users. It exists for no other purpose than to inform the users if a post is worth reading or not. Voting will not change sort order or hide posts. Essentially, this voting system is a parody. </p>
<p>I also wanted to include a few quality of life features. The website supports simple markdown for posts and allows users to set their own colorscheme for the website. Currently, the only thing I need to write is an rss feed generator for each user&#8217;s posts. </p>
<h2 >Who is this for?</h2>
<p>This website is for everyone. If you have ever wanted to write about something and publish it online but didn&#8217;t know how&#47;want to set up a website this website is for you. Currently there are only two users, a blog poster and me, a tech poster. I hope that this website grows over time. If you know someone who wants to start blogging without bullshit, send them to <a href="https://justletmewrite.org">justletmewrite.org</a>. </p>

<h2 >Bugs</h2>
<p>I have not done extensive bug testing. If you encounter a bug please email me or file an issue on github. </p>
<h2 >0x19 meta</h2>
<p>Thanks for holding. I appreciate the patience. </p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
