<?php include "../includes/header.html"; ?>
<title>Comparasion of OS network activity using wireshark and R (Linux, Windows, OpenBSD) </title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Comparasion of OS network activity using wireshark and R (Linux, Windows, OpenBSD) </h1>
<p> I used the sqldf library to make things quite a bit easier on myself. This data was in no way normal so I made graphs instead of doing any real statistical analysis. <a href="assets/wiresharkcsvfiles.tgz">The full CSV files and RMarkdown can be downloaded here</a>. Every code block in this post is R with the exception of the first which is a combination of shell and windows cmd. </p> 

<pre><code>library(&quot;sqldf&quot;);</code></pre>

<h1>Capturing Packets</h1>
<p>I ran wireshark for 1 hour and captured packets from 3 separate operating systems to see what kinds of network traffic they do by default. These systems were installed as virtual machines. I waited approximately one minute after booting the systems before logging in.  The log in is important because “what is this system doing in the background that the user is unaware of?” was the primary question. It is typical for a system to spawn new processes upon login which can optionally do something on the network.</p>
<p>Specific version information (they are all the ‘default installation’): There are a few quirks. Fedora is the official workstation edition. Windows is reporting that it is windows 10 when it is actually windows 11 Home edition. OpenBSD 7.4 is not being deceptive about what it actually is.</p>

<pre><code>fedora:~$ uname -a
Linux fedora 6.6-13-200.fc39.x86_64 #1 SMP PREEMPT_DYNAMIC x86_64 GNU/Linux

openbsd# uname -a
OpenBSD openbsd.localdomain 7.4 GENERIC.MP#1379 amd64

C:\Users\user&gt;ver
Microsoft Windows [Version 10.0.22621.3007]</code></pre>

<h1>Loading data, counting packets</h1>
<p>Packet count is the number of packets the system both received and sent.</p>
<pre><code>fedora = read.csv(&quot;fedora/fedora.csv&quot;);
obsd = read.csv(&quot;obsd/obsd.csv&quot;);
win = read.csv(&quot;win/win.csv&quot;);

sqldf(&quot;select count(*) from fedora&quot;);
##   count(*)
## 1     1047
sqldf(&quot;select count(*) from obsd&quot;);
##   count(*)
## 1     1481
sqldf(&quot;select count(*) from win&quot;);
##   count(*)
## 1     6960</code></pre>

<h1>Traffic graphs</h1>
<p>The traffic graphs are a simple way to crudely illustrate “network load” which is very similar to bytes per second.</p>
<pre><code>plot(win$Length~win$Time, type=&#39;l&#39;, lwd=1, col=&quot;blue&quot;, xlab=&quot;time in seconds&quot;, ylab=&quot;packet size in bytes&quot;, main=&quot;Traffic Graph by time&quot;);
lines(fedora$Length~fedora$Time, lwd=1, type=&quot;l&quot;, col=&quot;red&quot;);
lines(obsd$Length~obsd$Time, type=&#39;l&#39;, lwd=1, col=&quot;orange&quot;);
legend(x=&#39;topright&#39;, legend=c(&quot;Windows&quot;, &quot;Fedora&quot;, &quot;OpenBSD&quot;), col=c(&quot;blue&quot;, &quot;red&quot;, &quot;orange&quot;), lwd=2)</code></pre>
<img src="assets/images/wireshark1.png">
<p>Because it is hard to see the finer points, individual graphs are useful.</p>
<pre><code>plot(obsd$Length~obsd$Time, type=&#39;l&#39;, lwd=1, col=&quot;orange&quot;, xlab=&quot;time in seconds&quot;, ylab=&quot;packet size in bytes&quot;, main=&quot;Traffic Graph by time (OpenBSD)&quot;);
legend(x=&#39;topright&#39;, legend=c(&quot;OpenBSD&quot;), col=c(&quot;orange&quot;), lwd=2)</code></pre>
<img src="assets/images/wireshark2.png">
<p>The major spikes in the OpenBSD graph are the HTTPS connections which are caused by OpenNTPD.</p>
<pre><code>plot(fedora$Length~fedora$Time, type=&#39;l&#39;, lwd=1, col=&quot;red&quot;, xlab=&quot;time in seconds&quot;, ylab=&quot;packet size in bytes&quot;, main=&quot;Traffic Graph by time (Fedora)&quot;);
legend(x=&#39;topright&#39;, legend=c(&quot;Fedora&quot;), col=c(&quot;red&quot;), lwd=2)</code></pre>
<img src="assets/images/wireshark3.png">
<p>The major spike in the fedora graph is a series of connections to github.com. This is because of gnome-software and PackageKit which automatically check for updates when a user logs in. If the user uninstalls PackageKit and gnome-software these connections to github don’t happen.</p>
<pre><code>plot(win$Length~win$Time, type=&#39;l&#39;, lwd=1, col=&quot;blue&quot;, xlab=&quot;time in seconds&quot;, ylab=&quot;packet size in bytes&quot;, main=&quot;Traffic Graph by time (Windows)&quot;);
legend(x=&#39;topright&#39;, legend=c(&quot;Windows&quot;), col=c(&quot;blue&quot;), lwd=2)</code></pre>
<img src="assets/images/wireshark4.png">
<p>Windows is sending and receiving a lot of data with multiple CDNs.</p>
<h1>Calculating total
size of all transmissions</h1>
<p>The wireshark <code>Length</code> field is a record of the packet size in bytes. We can use this field to graph the sum of data transmitted by time.</p>
<pre><code>sqldf(&quot;select sum(Length) as sum_bytes from fedora&quot;)/1024 # conversion to KB for easy reading
##   sum_bytes
## 1  169.3242
sqldf(&quot;select sum(Length) as sum_bytes from obsd&quot;)/1024 # conversion to KB for easy reading
##   sum_bytes
## 1  140.3525
sqldf(&quot;select sum(Length) as sum_bytes from win&quot;)/1024/1024 # conversion to MB for easy reading
##   sum_bytes
## 1  5.525936</code></pre>
<p>Some loops to get a running total:</p>
<pre><code>fedora_rtotal = vector();
fedora_x = vector();
n = 0;
for(i in fedora$No.){
  if(!is.na(fedora$Length[i])){
    n = fedora$Length[i] + n;
    fedora_rtotal = c(fedora_rtotal, n);
    fedora_x = c(fedora_x, fedora$Time[i]);
  }
}

obsd_rtotal = vector();
obsd_x = vector();
n = 0;
for(i in obsd$No.){
  if(!is.na(obsd$Length[i])){
    n = obsd$Length[i] + n;
    obsd_rtotal = c(obsd_rtotal, n);
    obsd_x = c(obsd_x, obsd$Time[i]);
  }
}

win_rtotal = vector();
win_x = vector();
n = 0;
for(i in win$No.){
  if(!is.na(win$Length[i])){
    n = win$Length[i] + n;
    win_rtotal = c(win_rtotal, n);
    win_x = c(win_x, win$Time[i]);
  }
}

plot(win_x, win_rtotal, type=&#39;l&#39;, col=&#39;blue&#39;, ylab=&#39;sum of packet sizes in bytes&#39;, xlab=&#39;time&#39;, main=&#39;Plot of running totals in bytes&#39;);
lines(fedora_x, fedora_rtotal, type=&#39;l&#39;, col=&#39;red&#39;);
lines(obsd_x, obsd_rtotal, type=&#39;l&#39;, col=&#39;orange&#39;);
legend(x = &quot;right&quot;, legend = c(&quot;Windows&quot;, &quot;Fedora&quot;, &quot;OpenBSD&quot;), col=c(&quot;blue&quot;, &quot;red&quot;, &quot;orange&quot;), lwd=2);</code></pre>
<img src="assets/images/wireshark5.png">
<p>Because it is hard to see the finer points, a secondary graph is helpful.</p>
<pre><code>plot(fedora_x, fedora_rtotal, type=&#39;l&#39;, col=&#39;red&#39;, ylab=&#39;sum of packet sizes in bytes&#39;, xlab=&#39;time&#39;, main=&#39;Plot of running totals in bytes (fedora vs openbsd)&#39;);
lines(obsd_x, obsd_rtotal, type=&#39;l&#39;, col=&#39;orange&#39;);
legend(x = &quot;right&quot;, legend = c(&quot;Fedora&quot;, &quot;OpenBSD&quot;), col=c(&quot;red&quot;, &quot;orange&quot;), lwd=2);</code></pre>
<img src="assets/images/wireshark6.png">

<h1>Protocol analysis</h1>
<p>Dplyr boys are laughing at me upon reading this utter nonsense.</p>
<pre><code># select data
fdf = sqldf(&quot;select Protocol, count(Protocol) as c from fedora group by Protocol order by Protocol ASC&quot;);
odf = sqldf(&quot;select Protocol, count(Protocol) as c from obsd group by Protocol order by Protocol ASC&quot;);
wdf = sqldf(&quot;select Protocol, count(Protocol) as c from win group by Protocol order by Protocol ASC&quot;);
# set row names
rownames(fdf) = fdf$Protocol;
rownames(wdf) = wdf$Protocol;
rownames(odf) = odf$Protocol;
protocols = c( &quot;ARP&quot;, &quot;DHCP&quot;, &quot;DNS&quot;, &quot;HTTP&quot;, &quot;HTTP/XML&quot;, &quot;IGMPv3&quot;, &quot;LLMNR&quot;, &quot;MDNS&quot;, &quot;NBNS&quot;, &quot;NTP&quot;, &quot;OCSP&quot;, &quot;QUIC&quot;, &quot;SSDP&quot;, &quot;TCP&quot;, &quot;TLSv1.2&quot;, &quot;TLSv1.3&quot;)

# insert missing rows
for(i in protocols){
  if(is.na(fdf[i,2])){
    fdf = rbind.data.frame(fdf, c(i, 0));
  }
  if(is.na(wdf[i,2])){
    wdf = rbind.data.frame(wdf, c(i, 0));
  }
  if(is.na(odf[i,2])){
    odf = rbind.data.frame(odf, c(i, 0));
  }
}

# re set row names
rownames(fdf) = fdf$Protocol;
rownames(wdf) = wdf$Protocol;
rownames(odf) = odf$Protocol;

# alphabetic sort
fdf = fdf[order(row.names(fdf)), ];
wdf = wdf[order(row.names(wdf)), ];
odf = odf[order(row.names(odf)), ];

# merge into a single dataframe
all = data.frame(Windows=c(as.numeric(t(wdf$c))), Fedora=c(as.numeric(t(fdf$c))), OpenBSD=c(as.numeric(t(odf$c))));
# set row names again
rownames(all) = c(t(wdf$Protocol));

# plot
barplot(t(all), beside=TRUE, las=2, col=c(&quot;blue&quot;, &quot;red&quot;, &quot;orange&quot;), ylab=&quot;number of packets&quot;, main=&quot;Frequency of packets by OS, grouped by protocol&quot;)
legend(x = &quot;topleft&quot;, legend=colnames(all), col=c(&quot;blue&quot;, &quot;red&quot;, &quot;orange&quot;), fill=c(&quot;Blue&quot;, &quot;red&quot;, &quot;orange&quot;));</code></pre>
<img src="assets/images/wireshark7.png">

<h1>Domains Accessed</h1>
<h3> Fedora</h3>
<p>Fedora workstation accessed a couple of domains. It is using the hypervisor for DHCP and DNS resolution. It is accessing an ntp server, fedoraproject.org (this is for detecting captive portals), and it is accessing github.com. github.com access is due to packagekit or gnome-software checking for updates.</p>
<pre><code>sqldf(&quot;select Destination, count(Destination) as c from fedora group by Destination order by Destination ASC&quot;)
##             Destination   c
## 1 2.fedora.pool.ntp.org 132
## 2             Broadcast   4
## 3          fedora.local 513
## 4     fedoraproject.org  64
## 5            github.com  38
## 6            hypervisor 271
## 7        igmp.mcast.net   2
## 8        mdns.mcast.net  23
sqldf(&quot;select Source, count(Source) as c from fedora group by Source order by Source ASC&quot;)
##                  Source   c
## 1 2.fedora.pool.ntp.org 131
## 2          fedora.local 534
## 3     fedoraproject.org  61
## 4            github.com  37
## 5            hypervisor 284</code></pre>

<h3> OpenBSD</h3>
<p>OpenBSD does a lot less. It is using the hypervisor for DHCP but it
is doing it’s own DNS resolution. It’s doing NTP. OpenNTPD has an
interesting feature where it sends an https request to google.com and
reads the timestamp from the HTTP header in order to prevent NTP
poisoning.</p>
<pre><code>sqldf(&quot;select Destination, count(Destination) as c from obsd group by Destination order by Destination ASC&quot;)
##           Destination   c
## 1           Broadcast   2
## 2      dns9.quad9.net  22
## 3    hypervisor.local 110
## 4       openbsd.local 740
## 5        pool.ntp.org 475
## 6 time.cloudflare.com 110
## 7      www.google.com  22
sqldf(&quot;select Source, count(Source) as c from obsd group by Source order by Source ASC&quot;)
##                Source   c
## 1      dns9.quad9.net  25
## 2    hypervisor.local 112
## 3       openbsd.local 741
## 4        pool.ntp.org 472
## 5 time.cloudflare.com 109
## 6      www.google.com  22</code></pre>

<h3> Windows</h3>
<p>Windows is doing a lot and none of it seems to make any sense at
first glance. It is doing the standard DHCP and DNS queries to the
hypervisor but after that it’s not making much sense. Below is a table
of domains the system accessed. It is mostly CDNs, advertisements, and
windows account authentication (because God forbid we log into a system
without sending credentials to the OS manufacturer).</p>
<table style="width:98%;">
<colgroup>
<col style="width: 48%" />
<col style="width: 49%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">
Domain
</th>
<th style="text-align: left;">
What it does
</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">
.*msedge.net
</td>
<td style="text-align: left;">
Azure Front Door, a CDN
</td>
</tr>
<tr class="even">
<td style="text-align: left;">
akadns.net
</td>
<td style="text-align: left;">
Akami, a CDN
</td>
</tr>
<tr class="odd">
<td style="text-align: left;">
dscg2.akamai.net
</td>
<td style="text-align: left;">
Akami, a CDN
</td>
</tr>
<tr class="even">
<td style="text-align: left;">
dscg.akamaiedge.net
</td>
<td style="text-align: left;">
Akami, a CDN
</td>
</tr>
<tr class="odd">
<td style="text-align: left;">
trafficmanager.net
</td>
<td style="text-align: left;">
Azure load balancer
</td>
</tr>
<tr class="even">
<td style="text-align: left;">
bingadsedgeextension-prod.azurewebsites.net
</td>
<td style="text-align: left;">
probably advertisements, I cannot find much about it
</td>
</tr>
<tr class="odd">
<td style="text-align: left;">
commerce.microsoft.com
</td>
<td style="text-align: left;">
endpoint for your “windows user login” since they did away with local
accounts
</td>
</tr>
<tr class="even">
<td style="text-align: left;">
mdns.mcast.net
</td>
<td style="text-align: left;">
multicast, not a real server
</td>
</tr>
<tr class="odd">
<td style="text-align: left;">
sb.scorecardresearch.com
</td>
<td style="text-align: left;">
a data aggregator and advertising company
</td>
</tr>
<tr class="even">
<td style="text-align: left;">
wpc.epsiloncdn.net
</td>
<td style="text-align: left;">
Edigo, A CDN
</td>
</tr>
<tr class="odd">
<td style="text-align: left;">
wpc.phicdn.net
</td>
<td style="text-align: left;">
Edigo, a CDN
</td>
</tr>
<tr class="even">
<td style="text-align: left;">
cloudapp.azure.com
</td>
<td style="text-align: left;">
Azure cloud services
</td>
</tr>
</tbody>
</table>
<pre><code>sqldf(&quot;select Destination, count(Destination) as c from win group by Destination order by Destination ASC&quot;)
##                                               Destination    c
## 1                                         192.168.124.255   12
## 2                                             224.0.0.252    6
## 3                                         239.255.255.250  135
## 4                                               Broadcast    7
## 5                                     a-0003.a-msedge.net   91
## 6                            a-0019.standard.a-msedge.net   37
## 7                                  a-9999.a-dc-msedge.net   18
## 8                                     a-9999.a-msedge.net   17
## 9                                  a1834.dscg2.akamai.net   22
## 10                                  a1847.dscd.akamai.net   11
## 11                                  a1858.dscd.akamai.net   12
## 12                                    a1943.g2.akamai.net  173
## 13                                    a1961.g2.akamai.net    5
## 14                                 a767.dspw65.akamai.net    7
## 15                             arc-9999.arc-dc-msedge.net   16
## 16                                arm-9999.arm-msedge.net   16
## 17                  array601.prod.do.dsp.mp.microsoft.com   12
## 18                  array603.prod.do.dsp.mp.microsoft.com   55
## 19                  array614.prod.do.dsp.mp.microsoft.com  110
## 20                                    b-0008.b-msedge.net   16
## 21                     c-msn-com-nsatc.trafficmanager.net    9
## 22                                     cs11.wpc.v0cdn.net   44
## 23                              cs1404.wpc.epsiloncdn.net   27
## 24                                      cs9.wpc.v0cdn.net   21
## 25                               dual-a-0001.a-msedge.net   31
## 26                               dual-a-0036.a-msedge.net   97
## 27                          dual-s-9999.dual-s-msedge.net   17
## 28                             e11290.dspg.akamaiedge.net   76
## 29                                e12358.d.akamaiedge.net  357
## 30                             e16646.dscg.akamaiedge.net   38
## 31                                e28578.d.akamaiedge.net  164
## 32                             e86303.dscx.akamaiedge.net  306
## 33                              e8652.dscx.akamaiedge.net    6
## 34                              e9659.dspg.akamaiedge.net   15
## 35               fe2cr.update.msft.com.trafficmanager.net   89
## 36                                  fp2e7a.wpc.phicdn.net    5
## 37                     g-msn-com-nsatc.trafficmanager.net   10
## 38               glb.cws.prod.dcat.dsp.trafficmanager.net   60
## 39               glb.sls.prod.dcat.dsp.trafficmanager.net   34
## 40                                       hypervisor.local  210
## 41                                         igmp.mcast.net   20
## 42       inference-orion-eastus.eastus.cloudapp.azure.com   14
## 43 iris-de-prod-azsc-v2-wus2-b.westus2.cloudapp.azure.com   74
## 44                                         mdns.mcast.net   33
## 45          onedscolprdcus03.centralus.cloudapp.azure.com   26
## 46          onedscolprdcus09.centralus.cloudapp.azure.com   57
## 47          onedscolprdcus10.centralus.cloudapp.azure.com   36
## 48          onedscolprdcus15.centralus.cloudapp.azure.com   39
## 49          onedscolprdcus16.centralus.cloudapp.azure.com   15
## 50             onedscolprdeus01.eastus.cloudapp.azure.com   14
## 51             onedscolprdeus04.eastus.cloudapp.azure.com   14
## 52             onedscolprdeus08.eastus.cloudapp.azure.com   20
## 53             onedscolprdeus10.eastus.cloudapp.azure.com   25
## 54             onedscolprdeus16.eastus.cloudapp.azure.com   15
## 55          onedscolprdjpe02.japaneast.cloudapp.azure.com   14
## 56          onedscolprdjpw01.japanwest.cloudapp.azure.com   15
## 57          onedscolprdjpw02.japanwest.cloudapp.azure.com   28
## 58        onedscolprdneu04.northeurope.cloudapp.azure.com   25
## 59            onedscolprduks05.uksouth.cloudapp.azure.com   33
## 60         onedscolprdweu06.westeurope.cloudapp.azure.com   15
## 61             onedscolprdwus00.westus.cloudapp.azure.com   28
## 62             onedscolprdwus09.westus.cloudapp.azure.com   63
## 63                                    s-0005.s-msedge.net   37
## 64                               sb.scorecardresearch.com    9
## 65        settings-prod-eus2-2.eastus2.cloudapp.azure.com   12
## 66 settings-prod-scus-2.southcentralus.cloudapp.azure.com   24
## 67         settings-prod-uks-2.uksouth.cloudapp.azure.com   12
## 68        settings-prod-wus2-2.westus2.cloudapp.azure.com   12
## 69        ssl.bingadsedgeextension-prod.azurewebsites.net   11
## 70                      us.configsvc1.live.com.akadns.net   15
## 71                         us2.odcsm1.live.com.akadns.net   11
## 72                vmss-prod-eus.eastus.cloudapp.azure.com   25
## 73            vmss-prod-weu.westeurope.cloudapp.azure.com   19
## 74                vmss-prod-wus.westus.cloudapp.azure.com   26
## 75     waws-prod-usw3-011-3570.westus3.cloudapp.azure.com    9
## 76      wd-prod-cp-us-east-3-fe.eastus.cloudapp.azure.com   23
## 77      wd-prod-cp-us-west-2-fe.westus.cloudapp.azure.com   22
## 78                                          windows.local 3581
## 79                          wns.notify.trafficmanager.net   56
## 80        wus2.frontdoor.licensing.commerce.microsoft.com   15
## 81                        www.tm.v4.a.prd.aadg.akadns.net   11
## 82                www.tm.v4.a.prd.aadg.trafficmanager.net   13
sqldf(&quot;select Source, count(Source) as c from win group by Source order by Source ASC&quot;)
##                                                    Source    c
## 1                                           40.83.247.108    8
## 2                                     a-0003.a-msedge.net  111
## 3                            a-0019.standard.a-msedge.net   39
## 4                                  a-9999.a-dc-msedge.net   18
## 5                                     a-9999.a-msedge.net   15
## 6                                  a1834.dscg2.akamai.net   28
## 7                                   a1847.dscd.akamai.net   14
## 8                                   a1858.dscd.akamai.net   17
## 9                                     a1943.g2.akamai.net  456
## 10                                    a1961.g2.akamai.net    5
## 11                                 a767.dspw65.akamai.net    6
## 12                             arc-9999.arc-dc-msedge.net   16
## 13                                arm-9999.arm-msedge.net   15
## 14                  array601.prod.do.dsp.mp.microsoft.com    9
## 15                  array603.prod.do.dsp.mp.microsoft.com   42
## 16                  array614.prod.do.dsp.mp.microsoft.com   80
## 17                                    b-0008.b-msedge.net   17
## 18                     c-msn-com-nsatc.trafficmanager.net    9
## 19                                     cs11.wpc.v0cdn.net   60
## 20                              cs1404.wpc.epsiloncdn.net   18
## 21                                      cs9.wpc.v0cdn.net   20
## 22                               dual-a-0001.a-msedge.net   35
## 23                               dual-a-0036.a-msedge.net  118
## 24                          dual-s-9999.dual-s-msedge.net   15
## 25                             e11290.dspg.akamaiedge.net   82
## 26                                e12358.d.akamaiedge.net  479
## 27                             e16646.dscg.akamaiedge.net   35
## 28                                e28578.d.akamaiedge.net  222
## 29                             e86303.dscx.akamaiedge.net  318
## 30                              e8652.dscx.akamaiedge.net    4
## 31                              e9659.dspg.akamaiedge.net   15
## 32               fe2cr.update.msft.com.trafficmanager.net  128
## 33                                  fp2e7a.wpc.phicdn.net    3
## 34                     g-msn-com-nsatc.trafficmanager.net    7
## 35               glb.cws.prod.dcat.dsp.trafficmanager.net   70
## 36               glb.sls.prod.dcat.dsp.trafficmanager.net   31
## 37                                       hypervisor.local  212
## 38       inference-orion-eastus.eastus.cloudapp.azure.com   14
## 39 iris-de-prod-azsc-v2-wus2-b.westus2.cloudapp.azure.com   52
## 40          onedscolprdcus03.centralus.cloudapp.azure.com   29
## 41          onedscolprdcus09.centralus.cloudapp.azure.com   55
## 42          onedscolprdcus10.centralus.cloudapp.azure.com   42
## 43          onedscolprdcus15.centralus.cloudapp.azure.com   23
## 44          onedscolprdcus16.centralus.cloudapp.azure.com   15
## 45             onedscolprdeus01.eastus.cloudapp.azure.com    9
## 46             onedscolprdeus04.eastus.cloudapp.azure.com   13
## 47             onedscolprdeus08.eastus.cloudapp.azure.com   29
## 48             onedscolprdeus10.eastus.cloudapp.azure.com   17
## 49             onedscolprdeus16.eastus.cloudapp.azure.com   15
## 50          onedscolprdjpe02.japaneast.cloudapp.azure.com   11
## 51          onedscolprdjpw01.japanwest.cloudapp.azure.com   11
## 52          onedscolprdjpw02.japanwest.cloudapp.azure.com   21
## 53        onedscolprdneu04.northeurope.cloudapp.azure.com   26
## 54            onedscolprduks05.uksouth.cloudapp.azure.com   22
## 55         onedscolprdweu06.westeurope.cloudapp.azure.com   11
## 56             onedscolprdwus00.westus.cloudapp.azure.com   24
## 57             onedscolprdwus09.westus.cloudapp.azure.com   84
## 58                                    s-0005.s-msedge.net   43
## 59                               sb.scorecardresearch.com   11
## 60        settings-prod-eus2-2.eastus2.cloudapp.azure.com   10
## 61 settings-prod-scus-2.southcentralus.cloudapp.azure.com   17
## 62         settings-prod-uks-2.uksouth.cloudapp.azure.com   10
## 63        settings-prod-wus2-2.westus2.cloudapp.azure.com   11
## 64        ssl.bingadsedgeextension-prod.azurewebsites.net    9
## 65                      us.configsvc1.live.com.akadns.net   12
## 66                         us2.odcsm1.live.com.akadns.net    9
## 67                vmss-prod-eus.eastus.cloudapp.azure.com    2
## 68            vmss-prod-weu.westeurope.cloudapp.azure.com    2
## 69                vmss-prod-wus.westus.cloudapp.azure.com    8
## 70     waws-prod-usw3-011-3570.westus3.cloudapp.azure.com    7
## 71      wd-prod-cp-us-east-3-fe.eastus.cloudapp.azure.com   21
## 72      wd-prod-cp-us-west-2-fe.westus.cloudapp.azure.com   22
## 73                                          windows.local 3379
## 74                          wns.notify.trafficmanager.net   49
## 75        wus2.frontdoor.licensing.commerce.microsoft.com   15
## 76                        www.tm.v4.a.prd.aadg.akadns.net   10
## 77                www.tm.v4.a.prd.aadg.trafficmanager.net   13</code></pre>
<h1>Conclusion</h1>
<p>How much should an operating system really be doing automatically?
NTP is a standard; modern networked operating systems need correct time
for HTTPS certificates to work. Automatically checking for updates can
be nice and in the case of Fedora it’s trivial to disable this
functionality. The concerning part is when an operating system is loudly
broadcasting to the entire world that the machine is online before
proceeding to automatically download and upload data that is encrypted
(invisible to the user) without any logical reason for doing so.</p>


</div>
</div>
<?php include "../includes/footer.html"; ?>
