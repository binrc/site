<?php include "../includes/header.html"; ?>
<title>Password hashing, Cracking &#47;etc&#47;shadow passwords using John the Ripper, and Breaking into Linux systems with Physical Access</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1 >Password hashing, Cracking &#47;etc&#47;shadow passwords using John the Ripper, and Breaking into Linux systems with Physical Access</h1>
<h2 >Theory</h2>
<p>When we store user passwords we to check that the user entered the correct password so that they can be authenticated. How can we do this? The caveman way is to just store the exact password for the user in plain text then check if the strings are the same. The smarter way is to use a <strong>hashing</strong> function. </p>
<p>A Hashing function is like a one way door. In theory, ignoring collisions, one input will always result in exactly one unique output. When we authenticate users, we hash their input and compare it to the stored password hash. A hash cannot be reversed because it is a one way function. </p>
<p>When we are &#8220;cracking hashes&#8221;, we are not trying to reverse the one way hash function. Instead, we are trying to guess which combination of characters will result in the target hash. It&#8217;s a lot like algebra. <code>hashfunct(string)=hash</code>. We know the <code>hashfunct</code>, we know the <code>hash</code>, we just need to solve for <code>string</code>. </p>
<h3 >Exercise</h3>
<p>For the reader at home, try to figure out which 4 digit pin will result in the following md5 hash. Not that md5sums are used in the real world for password storage, but because they are fast to compute and make the exercise less time consuming. </p>
<pre><code>e8eec49af0be2d32a90375926aeedb62  -
</code></pre>
<p>If you get stuck, I have included everything required to get to the solution below.</p>
<p>This is a simple C program I wrote that will generate all possible 4 digit pins.</p>
<pre><code>#include &#60;stdio.h&#62;
#include &#60;stdlib.h&#62;

int main(){
    int a,b,c,d;
    a=b=c=d=0;

    &#47;&#47; loop from 0000-9999
    for(d=0; d&#60;=9; d++){
        for(c=0; c&#60;=9; c++){
            for(b=0; b&#60;=9; b++){
                for(a=0; a&#60;=9; a++){
                    printf("%d%d%d%d\n", d,c,b,a);
                }
                a=0;
            }
            b=0;
        }
        c=0;
    }

    return 0;
}
</code></pre>
<p>Using a bit of shellghetti, we can easily calculate the md5sum for all 4 digit pins. </p>
<pre><code>main@0x19:~$ .&#47;a.out | xargs -L1 -I % sh -c &#39;printf "$(echo %) \t $(echo % | md5sum) \n"&#39;;
</code></pre>
<p>Use grep and you have the answer. </p>
<h2 >Cracking passwords using John</h2>
<p>Taking the previous section into something based in the real world, we can use a similar strategy of &#8220;slam everything through the one way door until we get the result we want. The use real world use case here is to bully your users for having shit passwords. You generally need root for this to work which will be covered in the next section. Either way, this is an exercise. Act accordingly. </p>
<p>First, we will make a dummy user. </p>
<pre><code>main@0x19:~$ sudo useradd dummy1
main@0x19:~$ sudo passwd dummy1
Changing password for user dummy1.
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.
main@0x19:~$
</code></pre>
<p>The first thing we need to do is combine <code>&#47;etc&#47;passwd</code> with <code>&#47;etc&#47;shadow</code>. We do this using the <code>unshadow</code> command. Let&#8217;s unshadow then grep for the dummy users because we are doing this purely for testing purposes. Normally <code>&#47;etc&#47;shadow&#47;</code> is only readable by root so some privilege escalation might be required to obtain this file.</p>
<pre><code>main@0x19:~$ sudo unshadow &#47;etc&#47;passwd &#47;etc&#47;shadow | grep dummy &#62; dummy-unshadowed
main@0x19:~$ cat dummy-unshadowed 
dummy1:$y$j9T$0djW7V6BBCVQguuU5Ylhb1$aRiarlND2jyWZxeP9SMp6d1&#47;lFTGHJIH6pvA...CRb&#47;:1002:1002::&#47;home&#47;dummy1:&#47;bin&#47;bash
main@0x19:~$ 
</code></pre>
<p>The next step is to determine the hashing algorithm used by the system. Fedora uses <code>yescrypt</code> by default. John only supports yescrypt on platforms that support it natively. Other possible algos used could include sha256 or sha512 which are easier to crack and distro agnostic. </p>
<pre><code>main@0x19:~$ cat &#47;etc&#47;login.defs  | grep -i encrypt_method
ENCRYPT_METHOD YESCRYPT
# Only works if ENCRYPT_METHOD is set to SHA256 or SHA512.
# Only works if ENCRYPT_METHOD is set to BCRYPT.
# Only works if ENCRYPT_METHOD is set to YESCRYPT.
main@0x19:~$ 
</code></pre>
<p>The final step is to actually crack the hashes. If your <code>&#47;etc&#47;login.defs</code> specifies sha256 or sha512 you don&#8217;t need to pass the <code>--format</code> flag. <code>--wordlist=</code> is an optional flag that might increase the speed at which the passwords are cracked. </p>
<h2 >Dictionary attack</h2>
<p>John comes with it&#8217;s own word list at <code>&#47;usr&#47;share&#47;john&#47;password.lst</code>. We can, however, use our own word lists. In this example I am using the system&#8217;s spelling dictionary. </p>
<pre><code>main@0x19:~$ john --format=crypt --wordlist=&#47;usr&#47;share&#47;dict&#47;words .&#47;dummy-unshadowed 
Loaded 1 password hashe (crypt, generic crypt(3) [?&#47;64])
Will run 8 OpenMP threads
Press &#39;q&#39; or Ctrl-C to abort, almost any other key for status
0g 0:01:18:57 100% 0g&#47;s 101.2p&#47;s 202.5c&#47;s 202.5C&#47;s zymotoxic..ZZZ
Session completed
main@0x19:~$ john --show .&#47;dummy-unshadowed 
0 password hashes cracked, 0 left
main@0x19:~$
</code></pre>
<p>At this point you can wait while your shitty laptop cooks itself. Eventually it will finish. It took my 8 core system about an hour and twenty minutes to work it&#8217;s way through the <code>&#47;usr&#47;share&#47;dict&#47;words</code> wordlist. If you are using this in the real world it&#8217;s probably a better idea to use a more appropriate password wordlist than the massive system dictionary typically used by spell checking programs. </p>
<p>In my case, checking the results, the dictionary attack did not work. This is because I intentionally created the password so that it would be resistant to dictionary attacks.</p>
<h2 >Brute Force</h2>
<p>If a dictionary attack fails, we can start a brute force attack using the <code>--incremental</code> flag. This flag will cause <code>john</code> to cycle through characters until it finds a string that matches the hash. This will typically be a lot slower than using a dictionary attack because every possible combination of characters is tried, not just the ones that are likely to be a password. </p>
<p>I aborted the brute force after 20 hours because nothing was being produced. Even testing with a single character password, it took too long. </p>
<pre><code>main@0x19:~$ john --format=crypt --incremental .&#47;dummy-unshadowed 
Loaded 1 password hash (crypt, generic crypt(3) [?&#47;64])
Will run 8 OpenMP threads
Press &#39;q&#39; or Ctrl-C to abort, almost any other key for status
0g 0:19:51:58 0g&#47;s 191.1p&#47;s 191.1c&#47;s 191.1C&#47;s clpe10..clpel1
Session aborted
</code></pre>
<h2 >Getting results</h2>
<p>Because I did not want to wait until the heat death of the universe to finish this article, I changed the test user&#8217;s password to something simpler and easier to crack: the single word &#8220;computer&#8221;. This password is susceptible to dictionary attacks and it took my system 4 seconds to crack. </p>
<pre><code>main@0x19:~$ passwd dummy1
passwd: Only root can specify a user name.
main@0x19:~$ sudo passwd dummy1
[sudo] password for main: 
Changing password for user dummy1.
New password: 
BAD PASSWORD: The password fails the dictionary check - it is based on a dictionary word
Retype new password: 
passwd: all authentication tokens updated successfully.
main@0x19:~$ sudo unshadow &#47;etc&#47;passwd &#47;etc&#47;shadow | grep dummy &#62; dummy-unshadowed
main@0x19:~$ john --format=crypt .&#47;dummy-unshadowed 
Loaded 1 password hash (crypt, generic crypt(3) [?&#47;64])
Will run 8 OpenMP threads
Press &#39;q&#39; or Ctrl-C to abort, almost any other key for status
computer         (dummy1)
1g 0:00:00:04 100% 2&#47;3 0.2293g&#47;s 226.6p&#47;s 226.6c&#47;s 226.6C&#47;s 123456..pepper
Use the "--show" option to display all of the cracked passwords reliably
Session completed
main@0x19:~$ john --show .&#47;dummy-unshadowed 
dummy1:&#60;&#47;b&#62;computer&#60;b&#62;:1002:1002::&#47;home&#47;dummy1:&#47;bin&#47;bash

1 password hash cracked, 0 left
main@0x19:~$ 
</code></pre>
<h2 >Breaking into the root account</h2>
<ul>
<li>you need physical access or access to the bootloader in some way</li>
<li>you need a system that does not require a power on password (or a way to reset it like removing the CMOS battery and draining the capacitors)</li>
<li>2 seconds of googling because every distro that isn&#8217;t RH approved is less predictable than tainted dog water when it comes to the system behaving how anyone actually expects it to. </li>
</ul>
<p>On a RH system (RHEL, CentOS, Fedora), we can reset the root password easily and reliably.</p>
<p>First, enter the grub splash screen. Every distro does this differently but most of the time you can either spam the arrow keys or hold shift&#47;escape while the system is booting. It will look like this: </p>
<p><img src="assets/images/grub-root-1.png" alt="The grub splash screen" /></p>
<p>Go to the top entry (this is usually the most recent kernel) and press the <code>e</code> key to edit the grub entry. You will be presented with an editable version of the boot options. It will look like this: </p>
<p><img src="assets/images/grub-root-2.png" alt="Grub boot options for a fedora linux system" /></p>
<p>Move the cursor to the end of the line that starts with <code>linux</code> and append <code>rd.break</code>. Press <code>control+x</code> to boot the system using your new, temporary boot options. Once edited, it should look something like this: </p>
<p><img src="assets/images/grub-root-3.png" alt="Modified grub boot options to include rd.break" /></p>
<p>The system will now boot into emergency mode. We have two options: press the <code>enter</code> key to open a shell or press <code>Control+d</code> to break and reboot into a normal, multiuser runlevel. For our purposes, we should enter a shell. </p>
<p><img src="assets/images/grub-root-4.png" alt="Linux booted into emergency mode" /></p>
<p>Once we get a shell we can run a couple of commands to do a bit of clandestine &#8220;maintenance&#8221; on the system. When we are finished, we can reboot the system and gain root access using our new password. </p>
<p><img src="assets/images/grub-root-5.png" alt="Shell commands for resetting the root password (text block below)" /></p>
<p>First we remount the root partition read write, then we <code>chroot</code> into the root partition (the actual path for chroot will vary depending on distro), then we run the <code>passwd</code> command, create the <code>&#47;.autorelabel</code> file so that SELinux can relabel the system, exit, and reboot. </p>
<pre><code>Press Enter for maintenance
(or press Control-D to continue): 
sh-5.2# mount -o remount rw &#47;sysroot
sh-5.2# &#47;sysroot&#47;usr&#47;sbin&#47;chroot &#47;sysroot
sh-5.2# pwd
&#47;
sh-5.2# whoami
root
sh-5.2# passwd
Changing password for user root. 
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
sh-5.2# touch &#47;.autorelabel
sh-5.2# exit
exit
sh-5.2# reboot
</code></pre>
<p>On some distributions, <code>rd.break</code> in the grub config will not work so instead you can change the line to linux ($root)&#47;vmlinuz-x.y.z root=UUID=x-x-x-x-x rw init=&#47;bin&#47;bash. This method is a bit messier but you won&#8217;t have to do any chrooting. </p>
<h2 >Conclusion</h2>
<p>To protect yourself from clandestine system maintenance, you should always use full disk encryption. A bad actor cannot boot the system into recovery mode if he cannot even unlock the encrypted volumes. Transparent &#8220;full disk encryption&#8221; like shiltocker is not good enough because it is oftentimes trivial to bypass: <a href="https://youtu.be/wTl4vEednkQ">Breaking Bitlocker - Bypassing the Windows Disk Encryption</a>. </p>
<p>In addition to full disk encryption, user passwords should be strong and hard to crack. Use a pass sentence instead of a password. Make it long, include words that don&#8217;t exist in a dictionary, use extra characters and numbers to increase entropy. </p>
<p>Secure your hardware. Secure your software. They can&#8217;t get root if they have to beat it out of you with a rubber hose. </p>



</div>
</div>
<?php include "../includes/footer.html"; ?>
