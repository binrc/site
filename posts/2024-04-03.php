<?php include "../includes/header.html"; ?>
<title>Server Software used in the various sites of the Lainchan Webring </title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Server Software used in the various sites of the Lainchan Webring </h1>

<p> I iterated through the sites in the lainchan webring and grabbed their headers. This was initially to check which were still up but I realized that some http daemons include their name in the headers. <a href="https://0x19.org/lainring/statuses.html">Results in an easily redable format</a></p>


<p> The current state of servers that haven't died yet looks like this: </p> 

<pre><code>x220$ cat lainchan-servers | sort | uniq  -c | sort -r 
  62 Server: neocities
  34 Server: nginx
  14 Server: Apache
   9 Server: GitHub.com
   7 Server: cloudflare
   4 Server: pages-server
   4 Server: OpenBSD httpd
   1 Server: openresty
   1 Server: lighttpd
   1 Server: ddos-guard
   1 Server: Netlify
</code></pre>

<p><img style="border: solid 1px black;" src="assets/images/lainchan-webring-server-software.png" alt="pie chart of the above data"></p>

<table>
<tr><th>Server Software</th><th>Count</th><th>Percentage</th></tr>
<tr><td>neocities	</td><td>62	</td><td>44.9%</td></tr>
<tr><td>nginx		</td><td>34	</td><td>24.6%</td></tr>
<tr><td>Apache		</td><td>14	</td><td>10.1%</td></tr>
<tr><td>GitHub.com (pages)</td><td>9	</td><td>6.5%</td></tr>
<tr><td>cloudflare	</td><td>7	</td><td>5%</td></tr>
<tr><td>pages-server (codeberg)	</td><td>4	</td><td>2.8%</td></tr>
<tr><td>OpenBSD httpd	</td><td>4	</td><td>2.8%</td></tr>
<tr><td>openresty	</td><td>1	</td><td>0.7%</td></tr>
<tr><td>lighttpd	</td><td>1	</td><td>0.7%</td></tr>
<tr><td>ddos-guard	</td><td>1	</td><td>0.7%</td></tr>
<tr><td>Netlify		</td><td>1	</td><td>0.7%</td></tr>
</table>

<p> Some extra information: 140 sites are online, 25 are offline, this totals to 165 sites. Of the online sites, 138 of them sent the HTTP <code>Server: </code> header (meaning that only 2/140 did not send the header). This data is probably as accurate as it can get. </p> 

<p>I want to start a new webring where members must run their own webserver. Realizing that lainchan webring is ~1/2 abandoned neocities web pages really made me think. A higher quality webring would be possible if everyone runs their own webserver because there is some incentive to keep it updated when it costs you $5-$10/month to run and $10/year to keep your domain name registered. Email me if you are interested. </p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
