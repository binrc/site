<?php include "../includes/header.html"; ?>
<title>&#8220;Some Statistics&#8221; about sizeof(cat)&#8217;s feed list</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>&#8220;Some Statistics&#8221; about sizeof(cat)&#8217;s feed list</h1>
<blockquote>
<p>Now, if only there was someone willing to generate some statistics on the contents of the OPML file, like how many Wordpress or Hugo websites, author nationality, what web server software, how many are behind Cancerflare, etc.</p>
</blockquote>
<p>In direct response to the <a href="https://sizeof.cat/post/personal-feedlist-and-updates/">Personal feedlist and update post</a></p>
<p>This took me about 4 hours. The easy part is fingerprinting servers. The hard part is cleaning the resulting data which is always 95% garbage. </p>
<p>This was written for <a href="https://sizeof.cat/notes/1712403435/">sizeof(cat)&#8217;s &#8220;guest posting&#8221;</a>. It is mirrored to 0x19 for multitirectional trafic flow. Thanks for hosting! <a href="https://sizeof.cat/guest/some-statistics-about-sizeofcat-feed-list/">Link to the original post</a></p> 

<pre><code >0x19# cat $(which cat) | wc -c 
  149856
0x19# ls -lh &#47;bin&#47;cat
-r-xr-xr-x  1 root  bin  149856 Mar 20 15:16 &#47;bin&#47;cat
</code></pre>
<h2 >Using Lainchan Statistics Script</h2>
<p>Refer to <a href="https://0x19.org/posts/2024-04-03.php">my post about the various server software used by the members of the Lainchan Webring</a> for more information. </p>
<h3 >1. <code>sed</code>, <code>grep</code>, <code>awk</code> the <code>sizeofcat.opml</code> file to get a newline delimited list of urls</h3>
<p>Baby&#8217;s first shell spaghetti. We are better than copy+pasting. </p>
<h3 >2. Grab http headers with a modified version of my previous lainchan script</h3>
<p>The modified version of the script grabs http headers and checks to see if the http server is sending the <code>Server:</code> header. It outputs .csv and looks like this: </p>
<pre><code >&#60;?php

$f = file_get_contents("sites.txt");

$sites = array_filter(explode("\n", $f));
printf("\"URL\",\"Status\",\"Server\",\"Redirect\"\n");

foreach($sites as $s){
    $status = null;
    $server = null;
    $location = null;

    $h = get_headers($s);

    if(!$h || count($h) === 0){
        $status = "offline";
    } else {
        for($i=0; $i&#60;count($h); $i++){
            if(str_starts_with($h[$i], "HTTP")){
                $status = $h[$i];
            }
            if(str_starts_with($h[$i], "Server:")){
                $server = $h[$i];
            }
            if(str_starts_with($h[$i], "Location:")){
                $location = $h[$i];
            }

        }
    }

    printf("\"%s\",\"%s\",\"%s\",\"%s\"\n", $s, $status, $server, $location);
}


?&#62;
</code></pre>
<h3 >3. Process the csv output from the php script</h3>
<p>I could have done more in php but I would rather do it in shell. It&#8217;s faster for me to think in shell. </p>
<pre><code >0x19# cat http-headers | tail -n +2 | wc -l
    134
0x19# cat http-headers | tail -n +2 | awk -F&#39;,&#39; &#39;{print $3;}&#39; | sed -e s&#47;\"&#47;&#47;g -e s&#47;Server:\ &#47;&#47;g -e s&#47;[[:digit:]].*$&#47;&#47;g | tr -d &#39;&#47;&#39; | sort | uniq -c | sort -r
  34 nginx
  24 cloudflare
  22 Apache
   8 GitHub.com
   5 Netlify
   4 openresty
   3 SucuriCloudproxy
   3 OpenBSD httpd
   3 Caddy
   2 Squarespace
   2 AmazonS3
   1 nginx-rc
   1 neocities
   1 lighttpd
   1 lantern
   1 Vercel
   1 Pepyaka
   1 Microsoft-IIS
   1 GSE
   1 CloudFront
   1 BunnyCDN-SIL
</code></pre>
<p>There are 14 &#8220;unknown&#8221; servers left over.  You might be thinking, &#8220;hey, this is pretty good already&#8221; but not quite. Some of the URLS actually get redirected. </p>
<h3 >4. Realize http headers do not provide the enough of the information syscall(cat) wanted, goto 1;</h3>
<h2 >New process</h2>
<p>I initially thought I could use <code>nmap</code> and it&#8217;s OS detection to figure out what operating systems the servers are running. This information is more interesting to me than <code>(^-.-^)</code> because he did not explicitly state that he wanted to know the server operating systems, only the server web stacks. Typically, running <code>nmap</code> against systems you don&#8217;t own is frowned upon and considered a declaration of war. </p>
<p>I used chose to use <code>whatweb</code> because it does not probe every port. Instead, it looks what the http server advertises, the contents of html files, and how the http daemon responds. </p>
<h3 >1. Scan</h3>
<p>I told <code>whatweb</code> to output using the sql format and loaded it into mariadb so it would be somewhat parsable. </p>
<pre><code >0x19# whatweb --log-sql-create=dbinit.sql
0x19# whatweb -U="Mozilla&#47;5.0 (X11; Ubuntu; Linux x86_64; rv:124.0) Gecko&#47;20100101 Firefox&#47;124.0" --input-file=sites.txt --log-sql=out.sql
0x19# mysql &#60; dbinit.sql
0x19# mysql &#60; out.sql
</code></pre>
<h3 >2. Process, Clean, Process again</h3>
<p>Neither of the sql files loaded into the database without manual modifications. I processed the records in database using a different PHP script that I will not include here because it only gets you halfway to something useful. I did the remainder of the processing using various shell commands to transform garbage into a series of newline delimited <code>key:value</code> pairs. The key is the name of the <code>whatweb</code> module used, the value is the <code>string+version+os</code> fields from the database. I dumped this into a text file which I have included. Emails and IP addresses have been removed. </p>
<p><a href="assets/sizeof-cat-stats/clean.txt">the text file with data in the key:value format</a></p>
<p>Using further combinations of <code>grep</code>, <code>awk</code>, <code>cut</code>, <code>sed</code>, <code>sort</code>, and <code>uniq</code>, I tabulated the data into something that I could copy and paste into libreoffice calc. Although it would be fun to do it in R or Gnuplot, spreadsheets are designed to be used by tech illiterate office drones. I was feeling braindead after manipulating garbage so I am no better. </p>
<h2 >Analysis</h2>
<p>These numbers are slightly different than the numbers from the first section because redirects were followed. The total number of servers is 140 (certain http to https redirects and path redirects are logged as if they are separate servers for some reason). </p>
<h3 >Servers by the country they are hosted in:</h3>
<p>The <code>RESERVED</code> servers all had the country code of <code>ZZ</code> which is often used to denote <code>Unknown or unspecified country</code>. The <code>Unknown</code> servers did not present a country code. </p>
<p><img src="assets/sizeof-cat-stats/countries.png" alt="Servers by Country" /></p>
<pre><code>    72 UNITED STATES
    17 RESERVED
    6  GERMANY
    4  UNITED KINGDOM
    4  NETHERLANDS
    4  GREECE
    4  CANADA
    3  FRANCE
    2  SPAIN
    2  EUROPEAN UNION
    1  UKRAINE
    1  SWITZERLAND
    1  SWEDEN
    1  SLOVAKIA (Slovak Republic)
    1  RUSSIAN FEDERATION
    1  POLAND
    1  NORWAY
    1  IRAN (ISLAMIC REPUBLIC OF)
    1  DENMARK
    1  CZECH REPUBLIC
    1  AUSTRIA
    1  AUSTRALIA
    10 Unknown
</code></pre>
<h3 >Servers by the httpd they run</h3>
<p>The servers reported different versions or slightly mangled names. I condensed them down into something without version numbers. Cloudflare and related CDNs will usually present and advertise themselves through the <code>Server:</code> http header, pretending to be a real httpd daemon and not software as a service in search of a solution. </p>
<p><img src="assets/sizeof-cat-stats/httpd.png" alt="Servers by HTTPD" /></p>
<pre><code>    42 nginx
    24 cloudflare
    23 Apache
    8  GitHub.com
    6  LiteSpeed
    5  Netlify
    4  openresty
    3  Sucuri&#47;Cloudproxy
    3  OpenBSD httpd
    3  Caddy
    3  AmazonS3
    2  Squarespace
    1  neocities
    1  lighttpd
    1  lantern
    1  Vercel
    1  Pepyaka
    1  Microsoft-IIS
    1  GSE
    7  Unknown
</code></pre>
<h3 >Servers by CMS&#47;SSG stack:</h3>
<p>Again, all of the records say something slightly different. Sites that reported a wordpress theme as their CMS got condensed into general &#8216;wordpress&#8217;. Hard and soft forks of wordpress were not condensed into general &#8216;wordpress&#8217;.  Hugo, Jekyll, and PHP all had a version number attached which I truncated. Some websites reported that they are powered by a random string or character. I left these alone because it is somewhat humorous to realize that <code>(^-.-^)</code> subscribes to not only a wix website but also multiple webservers where the admin actually changed the &#8220;powered by&#8221; http headers and HTML metadata to garbage. </p>
<p>52 servers have an unknown CMS&#47;SSG which can either mean &#8220;the admin turned off the skiddie bait&#8221; or that the software is custom. Additionally, server reporting just PHP are either running something custom or leaking php versions because the badmin didn&#8217;t rtfm. </p>
<p><img src="assets/sizeof-cat-stats/stack.png" alt="servers by stack" /></p>
<pre><code>    25 Hugo
    25 WordPress
    4  PHP
    5  Jekyll
    3  Hexo
    3  Ghost
    2  blogger
    1  werc
    1  two
    1  the
    1  fossil
    1  debian,dr
    1  a
    1  Write.as
    1  WooCommerce
    1  Wix.com Website Builder
    1  W
    1  Quartz
    1  Org Mode
    1  Newspack
    1  Nefelibata
    1  Love
    1  HTML Tidy for HTML
    1  Express
    1  Elementor
    1  Coffee
    1  Buttondown
    1  AGA
    52 Unknown
</code></pre>
<h3 >JavaScript usage</h3>
<p>If a &#60;script&#62; tag was detected on a website, it was counted. This is probably not very accurate but it was interesting nonetheless. </p>
<p><img src="assets/sizeof-cat-stats/js.png" alt="script tags detected" /></p>
<pre><code>    64 JS
    76 No JS
</code></pre>
<h3 >Servers reporting their OS (public shaming section)</h3>
<p>Most servers did not announce their OS. Either this means good defaults or good admins.</p>
<p>Debianoid admins will never learn. </p>
<p><img src="assets/sizeof-cat-stats/os.png" alt="Servers that reported their OS" /></p>
<pre><code>    8   Debian Linux
    6   Ubuntu Linux
    2   Unix
    1   Linux
    123 Good Sysadmin&#47;defaults
</code></pre>



</div>
</div>
<?php include "../includes/footer.html"; ?>
