<?php include "../includes/header.html"; ?>
<title>Building a pirate box with streaming capabilities (transmission-daemon, jellyfin)</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1 >Building a pirate box with streaming capabilities</h1>

<p>Written for a friend who said “I need to bookmark your incoherent rambling because it’s very cool that what you just said is possible”</p>

<p> The goal of this project is to create a seedbox and a media streaming server at the same time. A seedbox is an always online machine (many people build them from raspberry pi boards) that downloads and seeds torrents. Seeding is important for torrent health. Using jellyfin, we can easily and automatically stream dubiously obtained media without any manual intervention whatsoever. All we need to do is add a torrent, wait a while, then, when it is complete, we can watch it via jellyfin. </p>

<p>I don’t need all this extra stuff because the entire problem of “streaming video and torrenting on a remote host” has been solved with ssh. This is my actual setup:</p>

<ul >
	<li>torrent client and ssh server on one machine (the one with the most storage)</li>
	<li>use ssh and sshfs to access those files on another machine</li>
	<li><code>mpv ~/remote/torrents/whatever.mp4</code> on the “client” machines</li>
</ul>

<p>For this article, I will be including code blocks that are significantly more verbose than usual. DNF output will be abridged where it is extremely long but full examples will be included. In the code blocks, the lines that start with a green <span style="color:green;">0x19@</span> or <span style="color:green;">root@</span>are shell commands that you can type and run. Additionally, in a few places certain things in the code blocks will be highlighted green. These are to show interactive input. </p>

<h2 >OS Install</h2>

<p>We will be using <a href="https://fedoraproject.org/server/">fedora server</a> for this post because it’s familiar, “secure enough”, and easy to troubleshoot. If you do know what CPU architecture your computer has, you probably want the X86_64 iso image. I like the DVD install image because it comes with a lot of useful tools out of the box. If you do not know how to use the <code>dd</code> program you can use <a href="https://docs.fedoraproject.org/en-US/fedora/latest/preparing-boot-media/#_fedora_media_writer">Fedora Mediawriter</a> instead.</p>

<p>I will be doing this exercise in a virtual machine. The reader should use physical hardware that is capable of transcoding videos on the fly.</p>

<p>Refer to <a href="https://0x19.org/posts/2022-01-09.php">my fedora for new users guide</a> if you have trouble getting the system to boot or confusion during installation. It is fairly self explanatory. The server installation menus will look similar but not identical. After the installation is complete and the system reboots, you will not get a fancy desktop environment. Because this is a server, we will not be needing these things (but, at a later date, if we want a desktop we can always install it).</p>

<p>Generally, I like to enable the root user but prohibit ssh password logins for root. Be sure to create a normal user and make sure the ‘administrative privileges’ box is checked. This will add your non-root user to the ‘wheel’ group. On most UNIX systems, this group is usually “the group that can do things as root”. For the profane, this is analogous to “you are allowed to Run as Administrator” on windows.</p>

<p><a href="http://www.catb.org/jargon/html/W/wheel.html">Eric S.  Raymond’s jargon file</a> tells us that the ‘wheel’ group got it’s name.  It’s interesting UNIX lore that might be helpful to the new admin learning why components of UNIX have strange naming conventions.</p>

<h2 >Cockpit</h2>

<p>If you do not know how to use ssh, you can use Fedora’s cockpit.  Cockpit is automatically installed and enabled on ‘server’ flavors of fedora. Shell commands can be entered verbatim into cockpit’s terminal tab. When you first boot the system you will see a line that says “Web console at” followed by an ip address and a port. Open <code>http://$IP:9090</code> in the web browser on a separate machine. 

<img src="assets/images/piratebox-1.png" alt="first boot and web console information, ‘web console: https://localhost:9009 or https://192.168.124.81:9090’">

<p>You will see an SSL certificate error but this is fine. The cryptography is still good even if the certificate is self signed. You can safely accept the risk and continue</p>

<img src="assets/images/piratebox-2.png" alt="self signed certificate error page from firefox">

<p>The next thing you will see is a login page for cockpit. Log in using the same username and password you set during installation. Click on the “Turn on Administrative Access” button and re-enter your password. This is the same as enabling sudo mode and will allow you to make modifications to the system using the cockpit web ui.</p>

<img src="assets/images/piratebox-3.png" alt="Fedora cockpit login page">

<h2 >Standard post-install steps</h2>

<p>We will begin by installing updates. Updates for always online servers is incredibly important, even if it’s behind a firewall. During the update process you will be prompted to type <code>y</code> or <code>n</code> in response to various Yes or No questions. Unless dnf says something about uninstalling a package, you can almost always safely type <code>y</code> then <code>enter</code>.</p>

<p>On Linux, password prompts in the terminal do not provide any feedback whatsoever when you are typing. Type the password, press enter.  If it’s wrong you will be prompted again. If it’s correct the command will run.</p>

<pre><code><span style='color:green;'>[0x19@localhost ~]$</span> sudo dnf update -y

We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.

For security reasons, the password you type will not be visible.

[sudo] password for 0x19: 

#[ very long dnf output is here, see the next block for a shorter example ]#

<span style='color:green;'>[0x19@localhost ~]$</span> 
</code></pre>

<p>After updating, we will add the <a href="https://rpmfusion.org/">RPM Fusion repository</a> which provides packages that Red Hat does not ship. In our specific use case, RPM Fusion provides jellyfin and multimedia codecs.</p>

<pre><code><span style='color:green;'>[0x19@localhost ~]$</span> sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
[sudo] password for 0x19: 
Last metadata expiration check: 0:15:30 ago on Thu 11 Apr 2024 09:31:13 AM MDT.
rpmfusion-free-release-39.noarch.rpm                                                                                          9.2 kB/s |  11 kB     00:01    
rpmfusion-nonfree-release-39.noarch.rpm                                                                                        19 kB/s |  11 kB     00:00    
Dependencies resolved.
==============================================================================================================================================================
 Package                                            Architecture                    Version                       Repository                             Size
==============================================================================================================================================================
Installing:
 rpmfusion-free-release                             noarch                          39-1                          @commandline                           11 k
 rpmfusion-nonfree-release                          noarch                          39-1                          @commandline                           11 k

Transaction Summary
==============================================================================================================================================================
Install  2 Packages

Total size: 22 k
Installed size: 11 k
Is this ok [y/N]: <span style="color:green;">y</span>
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                                      1/1 
  Installing       : rpmfusion-nonfree-release-39-1.noarch                                                                                                1/2 
  Installing       : rpmfusion-free-release-39-1.noarch                                                                                                   2/2 
  Verifying        : rpmfusion-free-release-39-1.noarch                                                                                                   1/2 
  Verifying        : rpmfusion-nonfree-release-39-1.noarch                                                                                                2/2 

Installed:
  rpmfusion-free-release-39-1.noarch                                           rpmfusion-nonfree-release-39-1.noarch                                          

Complete!
<span style='color:green;'>[0x19@localhost ~]$ </span>
</code></pre>

<p>After upgrading, we will set a hostname. Setting a unique and memorable hostname allows us to create a name=ip association in our router's DNS tab or in the <code>/etc/hosts</code> file of our UNIX systems. Now reboot. You will be disconnected from the server. Wait a few minutes and try to get back in.</p>

<pre><code><span style='color:green;'>[0x19@localhost ~]$</span> sudo hostnamectl set-hostname piratebox
<span style='color:green;'>[0x19@localhost ~]$</span> hostname
piratebox
<span style='color:green;'>[0x19@localhost ~]$</span> sudo reboot

Broadcast message from root@localhost on pts/1 (Thu 2024-04-11 09:49:19 MDT):

The system will reboot now!

<span style='color:green;'>[0x19@localhost ~]$</span> Connection to 192.168.124.81 closed by remote host.
Connection to 192.168.124.81 closed.
</code></pre>


<h2 >Installing components</h2>

<p>We want two things to satisfy our goal: a torrent client and a media server. Transmission-daemon is the torrent client (there are others but I like transmission). Jellyfin is the media server (there are others but jellyfin is the most viable). Install these using dnf. The output has been shortened and modified. Dnf will pull in hundreds of dependencies. </p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo dnf install transmission-daemon jellyfin
[sudo] password for 0x19: 
Last metadata expiration check: 0:03:20 ago on Thu 11 Apr 2024 09:48:46 AM MDT.
Dependencies resolved.
==============================================================================================================================================================
 Package                                  Architecture          Version                                           Repository                             Size
==============================================================================================================================================================
Installing:
 jellyfin                                 x86_64                10.8.13-1.fc39                                    rpmfusion-free-updates                9.4 k
 transmission-daemon                      x86_64                4.0.5-2.fc39                                      updates                               757 k

Transaction Summary
==============================================================================================================================================================
Install  2 Packages

Total download size: 248 M
Installed size: 871 M
Is this ok [y/N]:  <span style="color:green;">y</span>
Downloading Packages:
(1/2): transmission-daemon-4.0.5-2.fc39.x86_64.rpm                                                                        4.0 MB/s | 757 kB     00:00    
(2/2): jellyfin-10.8.13-1.fc39.x86_64.rpm                                                                                  49 kB/s | 9.4 kB     00:00    
--------------------------------------------------------------------------------------------------------------------------------------------------------------
Total                                                                                                                          10 MB/s | 248 MB     00:24     
RPM Fusion for Fedora 39 - Free                                                                                               1.6 MB/s | 1.7 kB     00:00    
Importing GPG key 0xD651FF2E:
 Userid     : "RPM Fusion free repository for Fedora (2020) <rpmfusion-buildsys@lists.rpmfusion.org>"
 Fingerprint: E9A4 91A3 DE24 7814 E7E0 67EA E06F 8ECD D651 FF2E
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-rpmfusion-free-fedora-39
Is this ok [y/N]: <span style="color:green;">y</span> 
Key imported successfully
RPM Fusion for Fedora 39 - Nonfree                                                                                            1.6 MB/s | 1.7 kB     00:00    
Importing GPG key 0x94843C65:
 Userid     : "RPM Fusion nonfree repository for Fedora (2020) <rpmfusion-buildsys@lists.rpmfusion.org>"
 Fingerprint: 79BD B88F 9BBF 7391 0FD4 095B 6A2A F961 9484 3C65
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-rpmfusion-nonfree-fedora-39
Is this ok [y/N]:  <span style="color:green;">y</span>
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                                                  1/1 
  Installing       : jellyfin-10.8.13-1.fc39.x86_64                                                                                                   1/2 
  Running scriptlet: transmission-daemon-4.0.5-2.fc39.x86_64                                                                                          2/2 
  Installing       : transmission-daemon-4.0.5-2.fc39.x86_64                                                                                          2/2 
  Running scriptlet: transmission-daemon-4.0.5-2.fc39.x86_64                                                                                          2/2 
  Running scriptlet: transmission-daemon-4.0.5-2.fc39.x86_64                                                                                          2/2 
  Verifying        : transmission-daemon-4.0.5-2.fc39.x86_64                                                                                          1/2 
  Verifying        : jellyfin-10.8.13-1.fc39.x86_64                                                                                                   2/2 
Installed:
  jellyfin-10.8.13-1.fc39.x86_64                    transmission-daemon-4.0.5-2.fc39.x86_64

Complete!
<span style="color:green;">0x19@piratebox:~$</span> 
</code></pre>

<h2 >Configuring services</h2>

<h3 >Disable firewall temporarily</h3>

<p>In order to make things easier for ourselves, we will disable the firewall while configuring the system. We will re-enable when we are done, adding the correct ports and services.</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo systemctl stop firewalld
</code></pre>

<h3 >Transmission-daemon</h3>

<p>We want to enable the service so it will automatically start on boot.  We start the service to create an initial configuration file, then stop the service so it doesn’t nuke our custom config.</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo systemctl enable transmission-daemon
Created symlink /etc/systemd/system/multi-user.target.wants/transmission-daemon.service → /usr/lib/systemd/system/transmission-daemon.service.
<span style="color:green;">0x19@piratebox:~$</span> sudo systemctl start transmission-daemon
[sudo] password for 0x19: 
<span style="color:green;">0x19@piratebox:~$</span> sudo systemctl status transmission-daemon
● transmission-daemon.service - Transmission BitTorrent Daemon
     Loaded: loaded (/usr/lib/systemd/system/transmission-daemon.service; enabled; preset: disabled)
    Drop-In: /usr/lib/systemd/system/service.d
             └─10-timeout-abort.conf
     Active: active (running) since Thu 2024-04-11 10:02:24 MDT; 3s ago
   Main PID: 1924 (transmission-da)
     Status: "Idle."
      Tasks: 4 (limit: 2307)
     Memory: 2.1M
        CPU: 28ms
     CGroup: /system.slice/transmission-daemon.service
             └─1924 /usr/bin/transmission-daemon -f --log-level=error

Apr 11 10:02:24 piratebox systemd[1]: Starting transmission-daemon.service - Transmission BitTorrent Daemon...
Apr 11 10:02:24 piratebox systemd[1]: Started transmission-daemon.service - Transmission BitTorrent Daemon.
Apr 11 10:02:25 piratebox transmission-daemon[1924]: [2024-04-11 10:02:25.535] ERR utils.cc:105 Couldn't read '/var/lib/transmission/.config/transmission-dae
<span style="color:green;">0x19@piratebox:~$</span> sudo systemctl stop transmission-daemon
</code></pre>

<p>We need to edit the configuration file before we can begin using the web ui. I like vim but we will use dnf to install ‘nano’ and use that instead.</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo dnf install nano
<span style="color:green;">0x19@piratebox:~$</span> sudo nano /var/lib/transmission/.config/transmission-daemon/settings.json 
</code></pre>

<p>We need to fine the following lines and replace their values. Use <code>Control+w</code> to search. Use the arrow keys to move the cursor and change the variables. Make them look like the following:</p>

<pre><code>"rpc-username": "whateveryouwant",
"rpc-password": "somethingyouremember",
"rpc-whitelist-enabled": false,
"queue-stalled-enabled": false,
</code></pre>

<p>When you are done, type <code>Control+x</code>, <code>y</code>, and <code>enter</code> to save the configuration file. Restart the service and check it’s status to make sure our configuration file isn’t broken.  We will also run a command to get the server’s IP address because I forget.</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo systemctl start transmission-daemon
<span style="color:green;">0x19@piratebox:~$</span> sudo systemctl status transmission-daemon
● transmission-daemon.service - Transmission BitTorrent Daemon
     Loaded: loaded (/usr/lib/systemd/system/transmission-daemon.service; enabled; preset: disabled)
    Drop-In: /usr/lib/systemd/system/service.d
             └─10-timeout-abort.conf
     Active: active (running) since Thu 2024-04-11 10:18:19 MDT; 4s ago
   Main PID: 2188 (transmission-da)
     Status: "Idle."
      Tasks: 4 (limit: 2307)
     Memory: 2.1M
        CPU: 43ms
     CGroup: /system.slice/transmission-daemon.service
             └─2188 /usr/bin/transmission-daemon -f --log-level=error

Apr 11 10:18:19 piratebox systemd[1]: Starting transmission-daemon.service - Transmission BitTorrent Daemon...
Apr 11 10:18:19 piratebox systemd[1]: Started transmission-daemon.service - Transmission BitTorrent Daemon.
<span style="color:green;">0x19@piratebox:~$</span> hostname -I
192.168.124.81 
</code></pre>

<p>Very everything works by going to <code>http://$IP:9091</code> in your web browser and log in using the username+password you set in the transmission configuration file. Test that torrenting works by adding a torrent using the web ui. If you don’t see an error it’s all good.</p>

<img src="assets/images/piratebox-5.png" alt="adding big buck bunny to the transmission web ui"><br>

<img src="assets/images/piratebox-6.png" alt="downloading a torrent">

<p>Any changes to the settings you make in the transmission-daemon web ui will persist when the daemon restarts (ie when the machine reboots).</p>

<h2 >Jellyfin</h2>

<p>This process is similar to the above but we don’t have to edit a configuration file. Enable, start, check status. One slightly annoying thing about checking the status of a service (if the output is very long) is that it will capture your terminal. Press <code>q</code> to exit the pager.</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo systemctl enable jellyfin
Created symlink /etc/systemd/system/multi-user.target.wants/jellyfin.service → /usr/lib/systemd/system/jellyfin.service.
<span style="color:green;">0x19@piratebox:~$</span> sudo systemctl start jellyfin
<span style="color:green;">0x19@piratebox:~$</span> sudo systemctl status jellyfin
● jellyfin.service - Jellyfin Media Server
     Loaded: loaded (/usr/lib/systemd/system/jellyfin.service; enabled; preset: disabled)
    Drop-In: /usr/lib/systemd/system/service.d
             └─10-timeout-abort.conf
             /etc/systemd/system/jellyfin.service.d
             └─override.conf
     Active: active (running) since Thu 2024-04-11 10:46:15 MDT; 4s ago
   Main PID: 2698 (jellyfin)
      Tasks: 19 (limit: 2307)
     Memory: 69.3M
        CPU: 4.334s
     CGroup: /system.slice/jellyfin.service
             └─2698 /usr/lib64/jellyfin/jellyfin --webdir=/usr/share/jellyfin-web --restartpath=/usr/libexec/jellyfin/restart.sh

Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Emby.Server.Implementations.AppBase.BaseConfigurationManager: Saving system configuration
Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Emby.Server.Implementations.AppBase.BaseConfigurationManager: Setting cache path: /var/cache/j>
Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Jellyfin.Server.Migrations.MigrationRunner: Migration 'AddDefaultPluginRepository' applied suc>
Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Jellyfin.Server.Migrations.MigrationRunner: Applying migration 'ReaddDefaultPluginRepository'
Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Jellyfin.Server.Migrations.MigrationRunner: Migration 'ReaddDefaultPluginRepository' applied s>
Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Jellyfin.Server.Migrations.MigrationRunner: Applying migration 'AddPeopleQueryIndex'
Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Jellyfin.Server.Migrations.Routines.AddPeopleQueryIndex: Creating index idx_TypedBaseItemsUser>
Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Jellyfin.Server.Migrations.Routines.AddPeopleQueryIndex: Creating index idx_PeopleNameListOrder
Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Jellyfin.Server.Migrations.MigrationRunner: Migration 'AddPeopleQueryIndex' applied successful>
Apr 11 10:46:20 piratebox jellyfin[2698]: [10:46:20] [INF] [6] Main: Kestrel listening on Any IP4 Address
...skipping...<span style="color:green;">q</span>
<span style="color:green;">0x19@piratebox:~$</span> 
</code></pre>

<p>If the service started, go to <code>$IP:8096</code> in your web browser to get to the jellyfin web ui. Go through the setup menus. When you get to the “Setup Media Libraries” menu, click to add a media library.</p>

<img src="assets/images/piratebox-7.png" alt="jellyfin menu to add media libraries">

<p>Transmission downloads to <code>/var/lib/transmission/Downloads</code>. we will add this as the “torrent” library on jellyfin.</p>

<img src="assets/images/piratebox-8.png" alt="Adding /var/lib/transmissin/Downloads as a media source">

<p>Continue to set up the media library. For a torrent library, be sure to disable automatic medatata updates so that the torrent files will not change and allow you to continue seeding. For a library of DVD rips, you can safely enable automatic metadata updates. Continue through the setup and log in. Peruse the settings tab and admin dashboard to see what jellyfin is capable of. Add more media libraries (ie music, DVD rips) if you have them. </p>

<p><img src="assets/images/piratebox-9.png" alt="jellyfin web ui with videos">

<h2>Sharing jellyfin with people who don’t live in your house</h2>

<p>This step is purely optional as it adds a unnecessary complexity to the setup.</p>

<p>If you intend to forward ports and allow your friends on other networks to stream from your server, add him as a non-admin user. We will install nginx and set it up to function as a reverse proxy to jellyfin. We need to generate some self-signed SSL certs so that our friends can securely view our media library. They are logging into our system so we need SSL to make sure that their passwords do not get stolen. Openssl will ask a few questions, you could (and should) leave all of them blank. The SSL certificates will expire after 1 year but since you are already clicking through warning messages you probably won't notice. It is a good idea to regenerate your SSL certificates every year. </p> 

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo dnf install nginx
<span style="color:green;">0x19@piratebox:~$</span> sudo mkdir /etc/ssl/private
<span style="color:green;">0x19@piratebox:~$</span> sudo mkdir /etc/ssl/certs
<span style="color:green;">0x19@piratebox:~$</span> sudo openssl req -x509 -nodes -days 365 -newkey rsa:4096  -keyout /etc/ssl/private/nginx.key -out /etc/ssl/certs/nginx.crt
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]: <span style="color:green">ZZ</span>
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:
Email Address []:
<span style="color:green;">0x19@piratebox:~$</span> 
</code></pre>

<p>Before we start nginx, we need to edit the configuration file.</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo nano /etc/nginx/nginx.conf
</code></pre>

<p>You can (probably) replace the entire nginx.conf with the following block. We are instructing nginx to use our self signed certs. Any requests for http will be redirected to https. Any requests for https will be sent to jellyfin.</p>

<pre><code># For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log notice;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80;
        listen       [::]:80;
        #server_name  _;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;


    return 301 https://$host$request_uri;

    }

# Settings for a TLS enabled server.

    server {
        listen       443 ssl http2;
        listen       [::]:443 ssl http2;
        #server_name  _;
        #root         /usr/share/nginx/html;

    set $jellyfin 127.0.0.1;
    resolver 127.0.0.1 valid=30;

        ssl_certificate "/etc/ssl/certs/nginx.crt";
        ssl_certificate_key "/etc/ssl/private/nginx.key";
        ssl_session_cache shared:SSL:1m;
        ssl_session_timeout  10m;
        ssl_ciphers PROFILE=SYSTEM;
        ssl_prefer_server_ciphers on;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

    location = / {
        return 302 https://$host/web/;
    }

    location  = /web/ {
             proxy_pass http://$jellyfin:8096/web/index.html;
             proxy_set_header Host $host;
             proxy_set_header X-Real-IP $remote_addr;
             proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
             proxy_set_header X-Forwarded-Proto $scheme;
             proxy_set_header X-Forwarded-Protocol $scheme;
             proxy_set_header X-Forwarded-Host $http_host;
        }


    location / {
        proxy_pass http://127.0.0.1:8096;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwaded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
        proxy_buffering off;
    }

    location /socket {
            proxy_pass http://127.0.0.1:8096;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Forwarded-Protocol $scheme;
            proxy_set_header X-Forwarded-Host $http_host;
        }
    }

}
</code></pre>

<p>The last thing to do is modify selinux policies to allow nginx to talk to the network (in order to talk to jellyfin) then enable and start nginx.</p>

<pre><code><span style="color:green;">0x19@piratebox:/~$</span> sudo setsebool -P httpd_can_network_connect 1
<span style="color:green;">0x19@piratebox:/~$</span> sudo systemctl enable nginx
<span style="color:green;">0x19@piratebox:/~$</span> sudo systemctl start nginx
</code></pre>

<p>Verify that port 80 redirects to 443 and that port 443 gives you jellyfin. You can now set up port forwarding in your router to your server’s IP for ports 80 and 443. Your friends can now access your jellyfin server by visiting your external IP address in a web browser. </p>

<h2 >Firewall rules</h2>

<p>We need to re-enable the system’s firewall and add rules for our services.</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo systemctl start firewalld
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --permanent --add-service=http
success
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --permanent --add-service=https
success
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --permanent --add-service=jellyfin
success
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --permanent --add-port=51413/tcp
success
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --permanent --add-port=51413/udp
success
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --permanent --add-port=9091/tcp
success
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --permanent --add-port=9091/udp
success
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --reload
success
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --list-ports
9091/tcp 9091/udp
<span style="color:green;">0x19@piratebox:~$</span> sudo firewall-cmd --list-services
cockpit dhcpv6-client http https jellyfin ssh
<span style="color:green;">0x19@piratebox:~$</span> 
</code></pre>

<p>now that you are done, reboot the system and verify that everything is starting up correctly and that we can access all of the services.</p>

<h2 >Routine maintenance</h2>

<h3 >Automatic security updates</h3>

<p>We want our system to automatically update every Monday night at midnight. This means less babysitting for us. We will use the <code>cron</code> program to schedule this. Go to <a href="https://crontab.guru">Crontab guru</a> if you need help figuring out a different time. Avoid using nonstandard values.</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo su
[sudo] password for 0x19: 
<span style="color:green;">root@piratebox:/home/0x19#</span> EDITOR="nano" crontab -e
no crontab for root - using an empty one
crontab: installing new crontab
</code></pre>

<p> When the editor opens, type the following, save, and quit. </p>

<pre><code>0 0 * * 1	dnf update -y; wait; reboot;
</code></pre>

<p> Now check if the crontab actually installed (it should say the same thing as you typed) </p> 

<pre><code>
<span style="color:green;">root@piratebox:/home/0x19#</span> crontab -l
0 0 * * 1	dnf update -y; wait; reboot;
<span style="color:green;">root@piratebox:/home/0x19#</span> exit
exit
<span style="color:green;">0x19@piratebox:~$</span> 
</code></pre>

<p><strong>Security updates are not optional. Anything on the open internet should be updated regularly. You should be doing all of your security updates.</strong></p>

<h3 >Major version upgrades</h3>

<p>Every 6 months fedora releases a new major version. You should always upgrade to the next major version within a year so your system will get important security patches. Refer to <a href="https://docs.fedoraproject.org/en-US/quick-docs/upgrading-fedora-offline/">Upgrading Fedora using DNF system plugin</a> on the fedora documentation page for instructions. Major version upgrades are not automatic and you should watch the output to make sure everything goes smoothly. Do not take the commands from the fedora docs and place them into a crontab if you want your piratebox to be reliable and functional.</p>

<p><strong>Major version upgrades are not optional. Each Fedora version stops receiving all updates N+2 versions after version N releases (about a year and a half). Anything on the open internet should be running an operating system that still receive security patches (that you need to be applying). You should be doing all of your major version upgrades.</strong></p>

<h3 >Check disk capacity and health</h3>

<p>I like to check disks when I am doing major version upgrades.  You can check free space in cockpit or by running the following command:</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda3        29G  3.8G   26G  13% /
devtmpfs        4.0M     0  4.0M   0% /dev
tmpfs           982M  8.0K  982M   1% /dev/shm
tmpfs           393M  1.2M  392M   1% /run
tmpfs           982M     0  982M   0% /tmp
/dev/sda2       960M  284M  677M  30% /boot
tmpfs           197M   12K  197M   1% /run/user/1000
</code></pre>

<p>We should also check to see if our drives are failing using the <code>smartctl</code> command. If a test fails or you see failure messages, it’s time to replace the drive. The <code>lsblk</code> program will list all of our drives in a friendly table format. In my case, <code>sda</code> is the actual drive. <code>sda1</code> through <code>sda3</code> are the partitions on that drive. Do smart checks for every disk reported by <code>lsblk</code>.</p>

<pre><code><span style="color:green;">0x19@piratebox:~$</span> sudo dnf install smartmontools
<span style="color:green;">0x19@piratebox:~$</span> lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sr0     11:0    1 1024M  0 rom  
zram0  251:0    0  1.9G  0 disk [SWAP]
sda    252:0    0   30G  0 disk 
├─sda1 252:1    0    1M  0 part 
├─sda2 252:2    0    1G  0 part /boot
└─sda3 252:3    0   29G  0 part /
<span style="color:green;">0x19@piratebox:~$</span> sudo smartctl -t short /dev/sda
smartctl 7.4 2023-08-01 r5530 [x86_64-linux-6.7.5-200.fc39.x86_64] (local build)
Copyright (C) 2002-23, Bruce Allen, Christian Franke, www.smartmontools.org

Self-test has begun
Use smartctl -X to abort test

<span style="color:green;">0x19@piratebox:~$</span> sudo smartctl -l selftest /dev/sda
smartctl 7.4 2023-08-01 r5530 [x86_64-linux-6.7.5-200.fc39.x86_64] (local build)
Copyright (C) 2002-23, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF SMART DATA SECTION ===
Self-test Log
Self-test status: No self-test in progress
Num  Test_Description  Status                       Power_on_Hours  Failing_LBA  NSID Seg SCT Code
 0   Short             Completed without error                 800            -     -   -   -    -
 1   Short             Completed without error                 800            -     -   -   -    -
 2   Short             Completed without error                 800            -     -   -   -    -

<span style="color:green;">0x19@piratebox:~$</span> sudo smartctl -H  /dev/sda
smartctl 7.4 2023-08-01 r5530 [x86_64-linux-6.7.5-200.fc39.x86_64] (local build)
Copyright (C) 2002-23, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED
</code></pre>

<h2 >Make it even better</h2>

<p>Building the above setup on a system with zfs (so we can have data redundancy) or with mirrored linux logical volumes would help protect our stolen media collection from certain types of hard drive failure.  Doing backups of the media library onto an offline drive would also be a good idea if you have something rare in your collection.</p>

<p>Adding nfs or samba to upload dvd rips is another idea but since the system already has ssh running, you can just use <code>ssh</code> (specifically <code>sshfs</code> and <code>scp</code>) to copy files around. (hint: all the linux graphical file managers have ssh integration. Why are you not using desktop linux already?)</p>

<p>Downolad/sideload one of the <a href="https://jellyfin.org/downloads">jellyfin client apps</a> to your desktop, phone, and <em>pukes</em> smart tv.</p>

<p>probably more but I am done with this nonsense because ssh exists :)</p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
