<?php include "../includes/header.html"; ?>
<title>Cars and Computers - Lego for adults</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Cars and Computers - Lego for adults</h1>
<p>Cars are a lot like computers except that there is a copious amount of superstition surrounding them. A common experience for the technically inclined is realizing that computers are like magic for the uninitiated. Cars are very similar in that no one knows anything about cars and view the internal combustion engine as if a magician harnessed the power of fire and channeled it horizontal propulsion via arcane methods.</p>
<p>This is my personal checklist which is neither authoritative nor reliable. There is so much superstition in the car world that it is difficult to discern what is good advice from what is bad advice. If you know nothing about cars, use your critical thinking skills. If you have a conflicting opinions/superstitions, email me about it.</p>
<p>I neither a car guy nor a mechanic but I think that “fix your own car” falls in line with hacker culture. Obviously the number and degree of cheap “I just need it to drive” hacks you can get away with depends on how much of a nanny state you live in. My model for “car is fixed” is “can I drive it? it fixed” and “do I want to throw a match into the gas tank? car is broken”.</p>
<p>This post is a series of lists for your s/reading/skimming/ pleasure.</p>
<h1>Schedule</h1>
<p>✅ = always do</p>
<p>❓ = “I am still unsure” and/or “this is a gamble I am constantly taking”</p>
<p>⏰ = don’t do immediately but it is time to start watching for problems in this area</p>
<p>❌ = you probably don’t need it <em>OR</em> you will instantly know when it happens no matter how many miles you put on the car</p>
<p>Progress bars are included. You can think of them as “life of car remaining”. This metric is based on the cost/benefit analysis of fixing a completely busted car vs just buying a new(er) one.</p>

<h2>When you buy gas</h2>
<ul class="incremental">
<li>✅ put gas in
<ul class="incremental">
<li>octane is a measurement of the resistance to combust
<ul class="incremental">
<li>low octane = easier to combust (aka knocking and prefires)</li>
<li>higher octane = harder to combust and therefore better (in most cases)</li>
<li>high octane fuel will give you better fuel efficiency (usually)</li>
<li>low octane fuel gives you worse fuel efficiency and can cause engine knocking</li>
<li>haters claim that putting high octane ethanol free gasoline in my clapped out v6 is a waste of money, I actually did experiments with various octane ratings and my subjective result is that, with shit gas, my engine prefires and redlines without any noticeable power increase whatsoever (compared to redlining w/ higher octane gas).</li>
<li>your ECU will automatically attempt to adjust to whatever gas you put in</li>
</ul></li>
<li>Most gasoline contains some amount of ethanol
<ul class="incremental">
<li>ethanol free gasoline will give you better fuel efficiency because gasoline is more energy dense (a gallon of ethanol is only as energy dense as 0.67% of a gallon of gasoline <a href="https://afdc.energy.gov/fuels/properties">US Department of Energy page on energy density of alternative fuels</a>)</li>
<li>E85 is 85% ethanol, 15% gasoline. It can prematurely combust and generally results in poor fuel efficiency. Ethanol can absorb moisture and gasoline engines don’t have a fuel/water separator like a diesel engine. Generally, all the E85 sticker means is that the hoses and gas tank are made from materials that are not corroded by ethanol. Decide if you like water in your fuel tank before you buy E85 or any fuel that contains ethanol.</li>
</ul></li>
<li>If it is very cold (-40 degrees or more(this is the same in freedom and communism units)) I will put some amount of ethanol gas in my tank because</li>
</ul></li>
<li>❓ check oil, windshield washer fluid, etc
<ul class="incremental">
<li>this is always optional but you really do need to make sure you don’t run the engine out of oil.</li>
</ul></li>
</ul>
<h2>every 5000m/8000km (or 6-12 months)</h2>
<progress value="98" min="0" max="100">
2%
</progress>
<ul class="incremental">
<li>✅ oil change
<ul class="incremental">
<li>yes, you have to get a new oil filter</li>
</ul></li>
<li>✅ check air system, brakes, belts, hoses, coolant, tires and pressure</li>
</ul>
<h2>every 10000m/160000km</h2>
<progress value="96" min="0" max="100">
4%
</progress>
<ul class="incremental">
<li>✅ check tires, do rotation</li>
<li>✅ change air filters</li>
</ul>
<h2>every 50000m/80000km</h2>
<progress value="80" min="0" max="100">
20%
</progress>
<ul class="incremental">
<li>✅ brakes
<ul class="incremental">
<li>yes, you have to bleed them</li>
</ul></li>
<li>❓ fuel filter
<ul class="incremental">
<li>on a diesel, yes always. On a gasser, you can probably wait until you notice the engine being sluggish</li>
<li>not changing your fuel filter can cause extra wear on the fuel pump</li>
</ul></li>
<li>❓ transmission fluid
<ul class="incremental">
<li>superstition say if you don’t change it, never change it but instead top it off. If you do change it, actually change it.</li>
<li>many newer cars have transmission fluid that is supposed to “last the lifetime of the transmission”. The manufacturers don’t tell you that the lifetime of the transmission is only 60,000 miles.</li>
</ul></li>
<li>⏰ check exhaust and suspension (no muffler, no problem)</li>
</ul>
<h2>60000m/10000km</h2>
<progress value="76" min="0" max="100">
24%
</progress>
<ul class="incremental">
<li>✅ spark plugs
<ul class="incremental">
<li>they will be black or the engine will be knocking</li>
</ul></li>
<li>⏰ change tires, get an alignment</li>
<li>⏰ belts and hoses</li>
</ul>
<h2>100000m/160000km</h2>
<progress value="60" min="0" max="100">
40%
</progress>
<ul class="incremental">
<li>✅ coolant (I top this off when I do oil)</li>
<li>⏰ clutch</li>
</ul>
<h2>125000m/20100km</h2>
<progress value="50" min="0" max="100">
50%
</progress>
<ul class="incremental">
<li>⏰ oxygen sensor</li>
<li>✅ coolant</li>
<li>❌ air conditioning system (go lada mode, who needs air conditioning anyway?)</li>
</ul>
<h2>180000m/290000km</h2>
<progress value="28" min="0" max="100">
72%
</progress>
<ul class="incremental">
<li>❌ power steering (you will know when this goes out)</li>
<li>✅ airflow sensor (I clean this every time I do air filters)</li>
<li>⏰ look for drive line leaks</li>
<li>⏰ timing belt</li>
</ul>
<h2>250000m/400000km</h2>
<progress value="1" min="0" max="100">
100%
</progress>
<ul class="incremental">
<li>⏰ clean/replace fuel injectors
<ul class="incremental">
<li>gasoline is a caustic solvent so you probably don’t need this anymore. Use something with PEA because it actually works with 4 stroke engines running on crap gas</li>
</ul></li>
<li>⏰ verify catalytic converter has not been stolen</li>
<li>⏰ begin praying before, during, and after every trip in the car.</li>
</ul>
<h1>Maintenance schedule from my car’s owner manual</h1>
<p>I was looking for the fuse box reference and saw this. This is GM specific but is still somewhat interesting to compare the manufacturer’s schedule with my half-assed schedule. It splits maintenance up into a “minor” service and a “major” service, switching back and forth between each service at every oil change interval. Additionally, the manual instructs the driver on what minor things need to be checked if the
driver is devoid of any and all common sense.</p>
<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th>Service</th>
<th>Maintenance I</th>
<th>Maintenance II</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Change engine oil and filter. Reset oil life system.</td>
<td>•</td>
<td>•</td>
</tr>
<tr class="even">
<td>Visually check for any leaks or damage.</td>
<td>•</td>
<td>•</td>
</tr>
<tr class="odd">
<td>Inspect engine air cleaner filter. If necessary, replace filter. See Engine Air Cleaner/Filter on page 318.</td>
<td></td>
<td>•</td>
</tr>
<tr class="even">
<td>Rotate tires and check inflation pressures and wear.</td>
<td>•</td>
<td>•</td>
</tr>
<tr class="odd">
<td>Inspect brake system.</td>
<td>•</td>
<td>•</td>
</tr>
<tr class="even">
<td>Check engine coolant and windshield washer fluid levels and add fluid as needed.</td>
<td>•</td>
<td>•</td>
</tr>
<tr class="odd">
<td>Perform any needed additional services. See “Additional Required Services” in this section.</td>
<td>•</td>
<td>•</td>
</tr>
<tr class="even">
<td>Inspect suspension and steering components.</td>
<td></td>
<td>•</td>
</tr>
<tr class="odd">
<td>Inspect engine cooling system.</td>
<td></td>
<td>•</td>
</tr>
<tr class="even">
<td>Inspect wiper blades.</td>
<td></td>
<td>•</td>
</tr>
<tr class="odd">
<td>Inspect restraint system components.</td>
<td></td>
<td>•</td>
</tr>
<tr class="even">
<td>Lubricate body components.</td>
<td></td>
<td>•</td>
</tr>
<tr class="odd">
<td>Check transaxle fluid level and add fluid as needed.</td>
<td></td>
<td>•</td>
</tr>
<tr class="even">
<td>Replace passenger compartment air filter, if equipped.</td>
<td></td>
<td>•</td>
</tr>
<tr class="odd">
<td>Inspect throttle system.</td>
<td></td>
<td>•</td>
</tr>
</tbody>
</table>
<br>
<table>
<colgroup>
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th>Service and Miles (Kilometers)</th>
<th>25,000 (40000)</th>
<th>50,000 (80000)</th>
<th>75,000 (120000)</th>
<th>100,000 (160000)</th>
<th>125,000 (200000)</th>
<th>150,000 (240000)</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Inspect fuel system for damage or leaks.</td>
<td>•</td>
<td>•</td>
<td>•</td>
<td>•</td>
<td>•</td>
<td>•</td>
</tr>
<tr class="even">
<td>Inspect exhaust system for loose or damaged components.</td>
<td>•</td>
<td>•</td>
<td>•</td>
<td>•</td>
<td>•</td>
<td>•</td>
</tr>
<tr class="odd">
<td>Replace engine air cleaner filter.</td>
<td></td>
<td>•</td>
<td></td>
<td>•</td>
<td></td>
<td>•</td>
</tr>
<tr class="even">
<td>Change automatic transaxle fluid and filter (severe service).</td>
<td></td>
<td>•</td>
<td></td>
<td>•</td>
<td></td>
<td>•</td>
</tr>
<tr class="odd">
<td>Change automatic transaxle fluid and filter (normal service).</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>•</td>
</tr>
<tr class="even">
<td>Replace spark plugs and inspect spark plug wires.</td>
<td></td>
<td></td>
<td></td>
<td>•</td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
<p>The “severe transaxle service” is for vehicles that are driven hard, up mountains, pulling trailers, or running for multiple hours straight every day.</p>
<h1>Tools</h1>
<p>I keep all of these in my car. I buy most tools from Harbor Freight because they are cheap and basically the same quality as every other tool manufacturer other than Snap-on.</p>
<ul class="incremental">
<li>screwdrivers, sockets, wrenches, crescent wrenches</li>
</ul>

<img src="assets/images/carshit-tools.png" alt="something cheap from harbor freight works" />


<ul class="incremental">
<li>electrical repair kit (solder free wire nuts, electrical tape, wire cutters/strippers, etc)</li>
</ul>

<img src="assets/images/carshit-electricalkit.png" alt="again, get something cheap from walmart or harbor freight" />

<ul class="incremental">
<li>torque wrench and cheater pipe
<ul class="incremental">
<li>this is the appropriate tool for everything you think you need an impact driver for</li>
<li>set the torque in foot pounds, tighten until it clicks
<ul class="incremental">
<li>torque = force * length</li>
</ul></li>
</ul></li>
</ul>

<img src="assets/images/carshit-torquewrench.png" alt="torque wrench" />


<ul class="incremental">
<li>jacks and jack stands
<ul class="incremental">
<li>floor jacks are safer than scissor jacks</li>
<li>always put the car onto jack stands so you don’t get crushed if the floor jack fails</li>
</ul></li>
</ul>

<img src="assets/images/carshit-jack.png" alt="floor jack" />

<br>

<img src="assets/images/carshit-jackstands.png" alt="jack stands" />


<ul class="incremental">
<li>5 pound mini sledge
<ul class="incremental">
<li>when the only tool is a hammer, everything looks hammerable</li>
<li><strong>BANG</strong> <small><em>instant regret</em></small></li>
</ul></li>
<li>dremel tool with cutting wheels
<ul class="incremental">
<li>when the only tool is a cutting wheel, everything looks
cuttable</li>
</ul></li>
</ul>

<img src="assets/images/carshit-dremel.png" alt="dremel tool" />


<ul class="incremental">
<li>impact driver
<ul class="incremental">
<li>useful for rounding nuts</li>
<li>useful for shearing bolts</li>
<li>useful for shattering sockets</li>
<li>do not use this for everything, the typical impact driver can deliver 800-1500 foot pounds of torque</li>
<li>please, do not use the uggadugga for everything</li>
</ul></li>
</ul>

<img src="assets/images/carshit-impact.png" alt="impact driver" />


<h1>Fluids and disposable kludge supplies</h1>
<p>I keep all of these in my car</p>
<ul class="incremental">
<li>extra oil</li>
<li>extra windshield washer fluid</li>
<li>extra coolant</li>
<li>duct tape</li>
<li>electrical tape</li>
<li>brake cleaner</li>
<li>WD-40</li>
<li>PB Blaster (the better WD-40)</li>
<li>zip ties</li>
<li>bungee cords</li>
<li>air system cleaner (either carburetor cleaner or throttle body cleaner)</li>
<li>starting fluid (use sparingly)</li>
<li>blue locktite</li>
<li>JB Weld</li>
</ul>
<p>“I prepare to be stranded for a few days” supplies include</p>
<ul class="incremental">
<li>5 gallons of water</li>
<li>food that won’t expire (trail mix, box of pasta, etc)</li>
<li>white gas, white gas stove, lighter, matches, etc (it never hurts to have fire)</li>
<li>tent, sleeping bag, etc (it is not fun to sleep in a small car)</li>
<li>winter boots, coat, coveralls, gloves</li>
<li>small solar panel to charge my phone so I can call for help if I absolutely need it</li>
</ul>
<h1>Problems</h1>
<h2>Electrical</h2>
<ul class="incremental">
<li>buy a code scanner
<ul class="incremental">
<li><a href="https://en.wikipedia.org/wiki/On-board_diagnostics">OBD is a standard for car diagnostics</a>.  OBD support has been a requirement for all EU cars since 2001 and all US cars since 1996.</li>
<li><a href="https://en.wikipedia.org/wiki/ELM327">elm327 is a USB code scanner that works with linux</a>. You can get these cheaply from ebay.</li>
<li><a href="https://github.com/barracuda-fsh/pyobd">pyOBD is one of many programs that work with linux</a></li>
<li>I do not have an elm327, instead I have a generic <a href="https://www.innova.com/products/carscan-reader-5110">Innova 5110 code reader</a> that I leave plugged all the time. It beeps and flashed red when there is an issue, allowing me to decide if a check engine light is actually a problem or not.</li>
</ul></li>
</ul>

<img src="assets/images/carshit-elm327.png" alt="elm327 scanner" />

<br>

<img src="assets/images/carshit-innova.png" alt="innova 5110" />


<p>Avoid superstitious thinking. The car has a computer on board. The car’s computer will tell you error codes related to the actual problem <em>as long as the car computer itself is not broken</em>. Usually replacing the sensors associated with the code will solve the problem.  If you cannot solve the problem with a new sensor, you might need a multimeter and will have to chase the ghosts out of the electrical system. Troubleshoot the error code, not whatever some other guy tells you to troubleshoot.</p>
<h2>Mechanical</h2>
<p>You will know and you will cry at the prices associated with fixing.</p>
<p>Depending on the volumetric displacement of your hubris, you may or may not continue to drive with several severe mechanical issues.</p>
<h1>Learning</h1>
<p>Go on the computer and do research. The problem solving process in cars is the same as the problem solving process on Linux.</p>

<img src="assets/images/carshit-troubleshooting.png" alt="googling the error message" />


<p>One major difference between cars and computers is that the YouTubers who make car videos are infinitely more helpful than the you tubers who make “technology help” videos that do not actually do anything for you but increase frustration. Search for $manufacturer$model$task and include the year of manufacture if you need more hand holding.</p>
<p>The manual that comes with the car will tell you fluid capacities and (sometimes) parts numbers.</p>
<p>If you go to an actually good parts house they will, in my experience, help you get the parts you need. Other parts houses have sold me parts for totally different cars which is incredibly frustrating because it means walking 10 miles with 50 pounds of steel in my backpack.</p>
<h1>Things I have fixed recently</h1>
<ul class="incremental">
<li>brakes, rotors, (regrettably) calipers.</li>
<li>tire rotation</li>
<li>oil change</li>
<li>fixed coolant leak</li>
<li>clean throttle body and air intake system because “engine is running rich”</li>
<li>replaced stock air intake w/ higher flow air intake for marginal performance gains</li>
<li>replaced throttle position sensor and acceleration pedal position sensor to get my car to stop going into limp mode</li>
<li>new exhaust and muffler (everything behind the catalytic converter)</li>
<li>pulled the OnStar fuse and ripped out the OnStar computer in an attempt increase privacy and reduce electrical surging</li>
<li>headlights, tail lights, car-side ports for the headlights because the cheap halogen ones I bought got hot and melted everything</li>
</ul>
<h1>Things I need to actually do</h1>
<ul class="incremental">
<li>brake fluid change</li>
<li>transmission fluid</li>
<li>power steering fluid change</li>
<li>fix the problem in my front right suspension and wheel bearings to reduce road noise.</li>
<li>cry about the looming transaxle issues</li>
<li>find a new car for when the transmission shits the bed</li>
</ul>
<h1>“The thinkpad of cars”</h1>
<p>Anything cheap with parts readily available. In the US, these are Honda, Toyota, and GM vehicles. Do not buy euro crap if you like affordable parts and fluids.</p>
<p>I think that the increasing amount of apple style gimmicks being sold with new cars has proven itself as nothing more than an expensive liability. I do not like sensors for everything. I do not like the idea of a car that will pull the steering wheel or automatically apply the brakes when a camera detects that I am exercising my free agency. I do not like the idea of a car with a built in cell phone modem and GPS.</p>
<p>My software ideology extends to my car ideology: I <strong>own</strong> this physical object. The manufacturer should not be able to dictate what I do with my property. It is my car and I will drive whenever, wherever, and however I please using my own better judgement and exercising my own volition.</p>
<p>My ideal car is more of a list of “I want <strong>NOT</strong> this bullshit” rather than “wow, I actually <em>want</em> this”:</p>
<ul class="incremental">
<li>fuel injection = good (I drive up mountains on the daily)</li>
<li>v6 or v8 (enough for passing the automotively incompetent)</li>
<li>manual transmission (cheaper to put a clutch in than a borked automatic transmission)</li>
<li>minimal “smart” bullshit
<ul class="incremental">
<li>keyed locks</li>
<li>keyed ignition</li>
<li>keyed trunk</li>
<li>not tying absolutely everything to a fucking battery so I don’t have the classic catch-22 situation where it is impossible to jump start a car because the power locks refuse to unlock and, even if you get the door open, the power hood latch refuses to unlatch because the battery
is dead.</li>
<li>no gps built in</li>
<li>no cell phone modem</li>
<li>no 80 inch television on the dashboard</li>
<li>no lane correction that jerks my car back towards the deer I am trying to avoid hitting</li>
<li>no auto brakes that will “have a minor software glitch” resulting in my own death by the hands of the tailgating idiot behind me</li>
</ul></li>
</ul>
<h1>0x19 META</h1>

<img src="assets/images/carshit-uriel.png" alt="uriel was right. “I think the thing that upset him wasn’t actually technology but a deterioration in larger culture that pushed everyone to look for administrative solutions”" />

<p>I have become incredibly burned out and demoralized when it comes to computers. Computers are not fun anymore. It no longer seems like there is anything novel on the internet. Even cars seem to be plagued with “technology for the sake of technology”. I am tired.</p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
