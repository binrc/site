<?php include "../includes/header.html"; ?>
<title>Build a static site generator</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">


<h1>`s/build your own website/build your own static site generator/`</h1>

<p>This will (hopefully) be my final webshitter project. I initially wanted it to be a blog unrelated to 0x19 but I decided against this.  Instead, I will publish sources under 0x19. Instead of making more webshit, I want to start making something actually interesting and actually useful.</p>

<p>The purpose of this article is to demonstrate how easy it can be to build your own static site generator. You only need a few components:</p>

<ul>
<li>scripting language</li>
<li>markdown to html library <em>or</em> a standalone markdown processor</li>
<li>css</li>
</ul>

<p>For the purposes of this article, my ssg is written in shell.</p>

<h1>The process of building one yourself</h1>

<ol>
<li>verify that the markdown processor works</li>
<li>copy the directory hierarchy from the dev dir to the prod dir</li>
<li>copy non markdown files</li>
<li>create extra pages using markdown or xml
<ul>
<li>blog index</li>
<li>sitemap</li>
<li>rss feeds</li>
<li>source code display page</li>
</ul></li>
<li>convert all markdown files to html and copy them</li>
</ol>
<p>After the core of the website is complete, you can add things that are useful to have</p>
<ul>
<li>script to add todolist items</li>
<li>script to make new blog posts</li>
<li>script to add a random mascot (Lain) every time the site is generated</li>
<li>a makefile to make it easier to use</li>
<li>edit the pandoc template to make it look nicer</li>
<li>add some javascript to filter the people who try to view your site with javascript enabled</li>
</ul>

<h1>If you want to use mine</h1>
<p>Grab the code <a href="https://gitlab.com/binrc/sh-ssg">https://gitlab.com/binrc/sh-ssg</a>.  Remove the sample blog posts, todo items. Edit the index page.</p>
<p>The shell scripts have been thoroughly commented and are intended to be extensible. Some amount of spaghetti was required to accommodate GNU systems.</p>

<h2>CSS</h2>
<p>All the css is stored in <code>dev/assets</code>. The <code>general.css</code> is responsible for the Lain watermarks, the <code>style.css</code> is everything else.</p>
<p>Pandoc is a bit strange, the syntax highlighting theme is based on gruvbox and is a json file at <code>dev/assets/gruvbox-pandoc.theme</code>.</p>

<h2>HTML</h2>
<p>The pandoc template is at <code>dev/assets/pandoc.tpl</code>. You could easily replace pandoc with any other markdown processor.</p>

<h2>JS</h2>
<p><code>filter.js</code> can be removed. It is included for a bit of trolling.</p>

<h2>BACKEND</h2>
<p>Everything is a shell script tied together with a makefile.  OpenBSD/Linux are the only systems I use so they are all I cared to test against.</p>
<pre><code>conf.sh - the general configuration file for all scripts, sets env vars and does OS detection
gen-rss.sh  - generates both a plaintext and html rss feed (comment either out if you don&#39;t want/need them)
main.sh - calls various scripts in order 
new.sh - makes a new blog post
random-lain.sh - adds watermarks to every HTML file
todo.sh - make new todo item
render.sh - does everything not listed above</code></pre>

<p>The makefile has a few options:</p>
<pre><code>make -&gt; build the whole thing
make new -&gt; new blog post
make editlast -&gt; edit last blog post
make todo -&gt; new todo list item
make lint -&gt; run the scripts through shellcheck
make serve -&gt; run php test server on port 6900
make stop -&gt; stop the php test server</code></pre>

<p>Additionally, there are two directories. The <code>dev</code> directory is for pre-rendered files. The <code>prod</code> directory is for rendered files. No code to sync with a server has been included.</p>


<p> Licensed as BSD-2 so do whatever the hell you want. Let me know if you are using this and get stuck. I'm glad to help. </p>

</div>
</div>
<?php include "../includes/footer.html"; ?>
