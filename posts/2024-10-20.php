<?php include "../includes/header.html"; ?>
<title>Picoblogging - microblogging but smaller and better</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">

<h1>Picoblogging - microblogging but smaller and better</h1>
<h1>Picobloging</h1>
<p>The name &#8216;picoblog&#8217; is a parody of the concept of <a href="https://en.wikipedia.org/wiki/Microblogging">microblogging</a> which is short form blogging. X (formerly known as Twitter) is probably the most well known microblogging website. Depending on reader pedantry, Facebook and Tumblr might also be considered microblogging. Federated microblogging also exists but will never reach critical mass due to a wide variety of issues, least of which is the fact that the vast majority of the human population does not understand how the Domain Name System works.</p>
<h2>Boilerplate frustrations, tired rebuttals to old arguments</h2>
<p>New readers, please refer to previous articles: </p>
<p><a href="https://0x19.org/posts/2022-09-18.php">Social Media Revisited</a></p>
<p><a href="https://0x19.org/posts/2021-11-25.php">I Hate Social Media</a> - fediverse spammers, read this one</p>
<p><a href="https://0x19.org/posts/2020-10-17.php">AntiSocial Media</a></p>
<h2>Functionality and Use</h2>
<p>My definition of picoblog is &#8220;microblogging but less&#8221;. Generally, a picoblog should consist of posts that are too short to justify writing an entire long form blog post about. It should be hosted on the same website as your regular blog. It does not need more features than you think you will actually use. Tag systems, permalinks, and the ability to easily reference previous posts are all useful but not required. <strong>A picoblog MUST have an RSS feed</strong> because I like RSS and I don&#8217;t like opening 20 different websites just to check if any of the sites have new content today.</p>
<p>A picoblog cannot be hosted on a centralized or federated platform.  Commenting, liking, subscribing, following, friending, blocking, banning, etc are all functions with no real world use case beyond generating a false sense of aggrandisement. </p>
<p>The use case for a picoblog is simple: it&#8217;s for notes, ideas, future project&#47;blog ideas, images, shitposts, media reviews, etc. Anything that you want to publish but don&#8217;t want to write a blog post about can go into the picoblog. </p>
<h2>Other known picoblogs (send an email if you want to be included here)</h2>
<p>Some sites call these a &#8220;notes&#8221; page or similar. Naming conventions aside, I will either create a list or a &#8220;picoblog webring&#8221; if this gains any traction.</p>

<p><a href="http://https://0x19.org/picoblog/">0x19.org picoblog</a></p>
</div>
</div>
<?php include "../includes/footer.html"; ?>
