<?php header("Content-Type: application/xml; charset=utf-8"); ?>
<?php echo '<?xml version="1.0" encoding="utf-8" ?>'; ?>

<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
<title>0x19.org rss feed</title>
<link>https://0x19.org</link>
<atom:link href="http://0x19.org/posts/feed.php" rel="self" type="application/rss+xml" />
<description>0x19.org rss feed</description>
<copyright>All content is licensed as CC BY-NC-SA 4.0 unless otherwise specified.</copyright>

<?php
$files = array_reverse(glob("*.php"));
foreach($files as $f){
	if($f == "index.php" || $f == "feed.php"){
		continue;
	}
	$fp = file_get_contents($f);
	if (!$fp)
		return null;

	$res = preg_match("/<title>(.*)<\/title>/siU", $fp, $title_matches);
	if (!$res)
		return null;

	// Clean up title: remove EOL's and excessive whitespace.
	$title = preg_replace('/\s+/', ' ', $title_matches[1]);
	$title = trim($title);

	$link = "http://0x19.org/posts/" . $f;

	echo "<item>\n";
	echo "<guid>" . $link . "</guid>\n";
	echo "<title>" . $f . " " . $title . "</title>\n";
	echo "<link>" . $link . "</link>\n";
	echo "<pubDate>" .  str_replace("UTC", "GMT", date("D, d M Y H:i:s T", strtotime(pathinfo($f, PATHINFO_FILENAME)))) . "</pubDate>\n";
	echo "<description>";
	$start = strpos($fp, '<p>');
	$end = strpos($fp, '</p>', $start);
	$firstp = substr($fp, $start, $end-$start+4);
	echo strip_tags($firstp);
	echo "</description>\n";
	/*
	echo "<description><![CDATA["; 
	foreach(preg_split("/((\r?\n)|(\r\n?))/", $fp) as $line){
		if(strpos($line, "<?php include") === false){
			if(strpos($line, "<title>") === false){
				echo $line;
			}
		}
	} 
	
	echo "]]></description>\n";
	 */
	echo "</item>\n";

}
?>

</channel>
</rss>
