<?php include "../includes/header.html"; ?>
<title>Posts</title>
<?php include "../includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<h1>Read via the <a href="./feed.php">RSS Feed </a></h1>
<h1>Read Online:</h1>
<p>Submit comments via email. Refer to <a href="/contact.php">Contact</a> for more information. </p>
</div>
</div>
<div class="row">
<div class="col-12">
<?php
$files = array_reverse(glob("*.php"));
foreach($files as $f){
	if($f == "index.php" || $f == "feed.php"){
		continue;
	}
	$fp = file_get_contents($f);
	if (!$fp)
		return null;

	$res = preg_match("/<title>(.*)<\/title>/siU", $fp, $title_matches);
	if (!$res)
		return null;

	// Clean up title: remove EOL's and excessive whitespace.
	$title = preg_replace('/\s+/', ' ', $title_matches[1]);
	$title = trim($title);
	echo "<p><a href='/posts/" . $f . "'>" . $title . " | " . pathinfo($f, PATHINFO_FILENAME) . "</a></p>\n";

	if($f == "2021-11-21.php"){
		echo "<p>Everything after this point has been ported from my old blog. Many ideas have been recycled. I would like to think that I have become more refined over time but I am self aware. These thought loops never end. </p>";
	}
}
?>
</div>
</div>
<?php
include "../includes/footer.html";
?>
