<?php include "includes/header.html"; ?>
<title>Resources</title>
<?php include "includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<p> 404: resources not yet collected. </p>
<img src="/assets/images/tar.jpg">
</div>
</div>

<div class="row">
<div class="col-12">
<?php 
$page="resources.php";
include "includes/mktoc.php";
?>
</div>
</div>

<div class="row">
<div class="col-12">
<hr>
<h1 id='0'>Reclaiming your Freedom and Privacy</h1>
<p> Clean up any residual data you have. Delete the accounts you can and fill the ones you can't with garbage data. This means nothing if you insist on continuing to use a nonfree operating system.</p>

<p><a href="https://datadetoxkit.org/en/home">Data Detox Kit</a>
Reeks of global homogeneity but it's a good place to start and stop if you're hellbent on staying addicted to social media. </p>
<p><a href="https://www.privacytools.io">Privacy Tools</a>
Massive list of tools but incomplete as it doesn't explicitly condemn proprietary software. </p>
<p><a href="https://prism-break.org">Prism Break</a>
Massive list of tools. It's a bit outdated but it explicitly condemns proprietary software so it's objectively better. </p>

<hr>
<h1 id='1'> Operating systems </h1>
<p>If you're serious about privacy and freedom, you need to replace your proprietary operating system. </p>

<h2> Linuxes </h2>
<p> I do not endorse any non RHEL sponsored Linux distribution with the exception of Gentoo. Most of the <i>user friendly</i> distributions have insecure security policies where they <i>default to allow</i> rather than <i>default to deny</i>. Still though, I must emphasize, <b>Any Linux distribution is miles better than using any proprietary operating system</b>.
<p><a href="https://getfedora.org">Fedora Project</a>
Although Fedora has it's issues, it balances usability while limiting anti-features. It's reliable yet fresh: a perfect mix of features and stability. I run Fedora on my laptop and am very satisfied. For the unitiated, Fedora is a good initiatory operating system. Most things can be accomplished through the GUI and most server administration tasks can be accomplished through Cockpit. Everything that requires a shell for manual intervention is well documented and intuitive.  </p>

<h2> *BSDs </h2>
<p><a href="https://freebsd.org">FreeBSD</a>
FreeBSD is a great "I know what I want now shut up and let me build the damn thing myself" operating system. It's reliable and flexible. FreeBSD <b> IS NOT ENTIRELY BULLETPROOF BY DEFAULT</b> so you should consult <code>man security</code>. I run FreeBSD on my other laptop and am very happy with it.  </p>
<p><a href="https://openbsd.org">OpenBSD</a>
It's a great no nonsense general purpose operating system. I run it on my servers and my workstation. I find debugging somewhat more difficult on OpenBSD but their source code is a great example. </p>
<p><a href="https://netbsd.org">NetBSD</a>
A traditional BSD. I don't use it much but when I do it's an enjoyable experience. </p>

<h2> Fun Operating systems </h2>
<p><a href="http://9front.org/">9front</a>
9front is an actually maintained fork of Plan 9 from Bell Labs. It feels like a fever dream but many of the design concepts are interesting. </p>
<p><a href="https://www.haiku-os.org/">Haiku</a>
Haiku is a modern BeOS inspired operating system. I think it'll be in a perpetual beta state but it's entirely viable for daily use. </p>
<p><a href="https://github.com/minexew/Shrine">Shrine</a> TempleOS for TCP/IP heretics</p>

</h2>
<hr>
<h1 id='2'>Learning Linux and UNIX</h1>
<p><a href="https://0x19.org/posts/2022-01-09.php">My guide on installing Linux </a> Fedora specific. 
<p><a href="https://www.linuxcommand.org/tlcl.php">The Linux Command Line book </a>
Good place to start, linux specific but very comprehensive. </p>
<p><a href="https://www.wiley.com/en-us/Linux+Bible%2C+10th+Edition-p-9781119578895">The Linux Bible</a>
Noob to RHEL admin in ~2 months or less. Look on libgen </p>
<p><a href="https://www.grymoire.com/Unix/Sh.html">Posix Shell Scripting</a>
Posix specific shell scripting guide </p>
<p><a href="https://docs.freebsd.org/en/books/handbook/">FreeBSD Handbook</a>
very helpful </p>
<p><a href="https://www.openbsd.org/faq/index.html">OpenBSD FAQ</a>
very helpful </p>
<h4> When all else fails </h4>
<pre>
$ man man
$ man intro
$ man help
$ man builtins
$ man $(echo $SHELL | awk -F\/ '{print $NF}')
</pre>

<hr>
<h1 id='4'> Search engines </h1>
<p> <a href="https://duckduckgo.com">DuckDuckGo</a> Scrapes results from many websites and uses it's own crawler. This means it's not entirely immune to search result censoring but it's better than nothing. </p>
<p> <a href="https://wiby.me/">Wiby</a> Search engine for websites containing minimal CSS and little or no JavaScript. </p>
<p> <a href="https://metager.org/">MetaGer</a> I've not used this search engine much. It looks promising but it might be snake oil. </p> 
<p> <a href="https://www.mojeek.com/">mojeek</a> Claims to be privacy oriented. Search results are mixed. </p> 
<p> <a href="https://searx.space">Searx</a> Self hosted search engine. Choose one from the list or host your own. </p>
<p> <a href="https://yacy.net">YaCy</a> Self hosted peer to peer search engine. </p>

<hr>
<h1 id='5'> Other tools </h1>
<p><a href="https://emailselfdefense.fsf.org/en/">FSF's guide to GPG</a>
GPG is the only way to make email private. Email was not designed to be private. If you are transmitting private information over email, you need to be using GPG. </p>
<p><a href="https://kkovacs.eu/cool-but-obscure-unix-tools">cool but obscure unix tools</a> </p>
<p><a href="https://cyber.dabamos.de/unix/x11/">Obscure X tools</a> motif required. </p> 

<hr>
<h1 id='6'> Web Development </h1>
<p><a href="https://developer.mozilla.org/en-US/docs/Learn">Mozilla's guide for beginners</a> Whatever they tell you to do here, do the opposite. Use vim and test against all the browsers you can get your hands on. <q>If it doesn't work in lynx, it doesn't work at all. </q></p>
<p><a href="https://0x19.org/posts/2021-11-26.php">My guide on building a web server</a> Many people will tell you to run Nginx on Ubuntu. Don't listen to them. OpenBSD httpd is easy to use and is designed to prevent many foot shooting incidents. If you need more, try Apache on FreeBSD (shoot foot here). </p>

<hr>
<h1 id='7'> Programming </h1>
<p><a href="http://www.cprogramming.com/tutorial.html">cprogramming.org</a>
baby's first hello world</p>
<p><a href="https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/">ncurses tutorial</a>
using ncurses for TUI programs</p>
<p><a href="http://doc.cat-v.org/plan_9/programming/c_programming_in_plan_9">Plan 9 C</a>
You can get this to run on UNIX via plan9port. </p>
<p><a href="https://learnbchs.org">bchs - BSD, C, httpd, sqlite</a> 
<p><a href="https://github.com/koalaman/shellcheck">Shellcheck</a>Static analysis for shell scripts. </p>
<p><a href="https://github.com/Immediate-Mode-UI/Nuklear">Nuklear</a> A cross platform GUI toolkit written in ANSI C</p>
<p> When all else fails: </p>
<pre>
$ man 3 $function
$ apropos $function
</pre>
<p> useful debugging tools include:</p>
<pre>
$ gcc -ggdb -Wall -Wextra main.c
$ gdb ./a.out
$ valgrind ./a.out
</pre>

<hr>
<h1 id='8'> Other Sites </h1>
<p><a href="https://cheapskatesguide.org">Cheapskate's Guide to Computers and The Internet</a>
Guides and philosophy on cheap computing, internet privacy, self hosting, etc </p>

<p><a href="https://sizeof.cat">sizeof(cat)</a>
Guy who talks about cybersec, privacy, pentesting, etc </p>

<p><a href="https://tinfoil-hat.net/">tinfoil-hat</a>
He hosts various frontends for proprietary services (nitter, invidious, etc) and has a link list. </p>

<p><a href="https://unixsheikh.com">unixsheikh</a>
UNIX and Linux related articles and tutorials, software development, internet, opinion pieces, etc. </p>


<hr>
<h1 id='9'> RSS Feeds </h1>
<p><a href="https://undeadly.org">OpenBSD Journal</a>
OpenBSD news </p>
<p><a href="https://fedoramagazine.org">Fedora Magazine</a>
How-to guides and useful resources for Fedora </p>
<p><a href="https://klarasystems.com/articles">Klara Systems</a>
FreeBSD and ZFS related articles. Very helpful. </p>
<p><a href="https://developers.redhat.com/blog">Red Hat Developer</a>
Red Hat specific articles. </p>

<hr>
<h1 id='10'> Podcasts </h1>
<h2> Baby's first Linux shows </h2>
<p><a href="https://linuxunplugged.com">Linux Unplugged</a>
Very consumer centric but a good resource for new users to learn about what's out there. </p>
<p><a href="https://latenightlinux.com">Late Night Linux</a>
Generic Linux show, still good for new users to learn what's out there. </p>
<p><a href="https://www.badvoltage.org">Bad Voltage</a>
Bantering Linux cynics. </p>
<p><a href="https://twit.tv/shows/floss-weekly">Floss Weekly</a>
A decent amount of TWiT consumer nonsense but some of the interviews are very good. </p>
<p><a href="https://darknetdiaries.com/">Darknet Diaries</a>
Hacking/Pentesting stories. </p>

<h2> I've become so tired of all the hype around meme distros </h2>
<p><a href="https://bsdnow.tv">BSD Now</a>
BSD related news and tips. </p>
<p><a href="https://2.5admins.com">2.5 Admins</a>
Sysadmin related news, tips, etc. Lots of ZFS. </p>
<p><a href="https://coder.show">Coder Radio</a>
General cynicism towards technology as a whole. </p>
<p><a href="https://www.theopiniondominion.org/">The Opinion Dominion</a>
Technology adjacent, miscellaneous  rambling, light cynicism. </p>
<hr>

<h1 id='11'> Link Lists </h1>
<p><a href="http://biglist.terraaeon.com/">Big list of personal sites</a>
You can add your own. </p>
<p><a href="https://neocities.org/browse">Neocities</a>
Lots of personal websites here. </p>
<hr>

<h1 id='12'>Humor</h1>
<p><a href="http://motherfuckingwebsite.com/">motherfuckingwebsite</a> a website. </p>
<p><a href="http://bettermotherfuckingwebsite.com/">bettermotherfuckingwebsite</a> a better website</p>
<p><a href="http://bestmotherfuckingwebsite.com/">bestmotherfuckingwebsite</a>  The best website</p>
<p><a href="http://thebestmotherfuckingwebsite.com/">thebestmotherfuckingwebsite</a> <i><b>The</b></i> best website. The previous website, is in fact, much better. </p>
<p><a href="https://archive.org/details/rust-in-a-nutshell">Rust in a nutshell</a> Archived from <a href="https://youtu.be/kQcIV5389Ps">youtube</a></p>
<p><a href="https://datatracker.ietf.org/doc/html/rfc9225">Software Defects Considered Harmful</a> Rust dev mentality </p>
<p><a href="https://web.archive.org/web/20220428232149/https://www.albinoblacksheep.com/text/is-your-son-a-computer-hacker">Is your son a computer hacker?</a></p>
<p><a href="https://github.com/corollari/linusrants">linusrants</a> Dataset of Linus Torvalds' rants on mailing lists. </p>
<p><a href="https://archive.org/details/program-in-c-tas-Kaslai">Program in C</a> Ariel listen to me. OO languages? It's a mess. Programming in C is better than anything they got over there. The syntax is so much sweeter where objects and subtypes play but frills like inheritance will only get in the way. Admire C's simple landscape: Efficiently dangerous. No templates of fancy pitfalls . . . like Java and C++. Program in C. Program in C. Pointers, assembly, manage your memory with malloc() and free()! Don't sink your app with runtime bloat. Software in C will stay afloat. Do what you want there close to the hardware. Program in C. </p> 

<hr>

<h1 id='13'> SUCKS - software that sucks</h1>
<p><a href="https://gist.github.com/probonopd/9feb7c20257af5dd915e3a9f2d1f2277">Boycott wayland</a> <q>Wayland is not ready as a 1:1 compatible Xorg replacement just yet, and maybe never will.</q></p>
<p><a href="https://nosystemd.org/">nosystemd.org</a> Because pid #1 doesn't need to to journaling, authentication, networking, directory management, DNS resolution, manage devices, ntpd, crond, inetd, or be it's own bootloader. </p>

<hr>

<h1 id='99'> Unsorted </h1>
<p><a href="https://nandgame.com/">nandgame</a> Web game where you build circuts</p>
<p><a href="https://www.nngroup.com/articles/computer-skill-levels">The Distribution of Users’ Computer Skills: Worse Than You Think</a> This study has helped me with imposter syndrome numerous times. You <i>are</i> in fact smarter than the average bear. </p> 
<p><a href="https://wiki.installgentoo.com/wiki/Main_Page">installgentoo wiki</a> Somewhat useful wiki. Filled with memes and shitposts. </p>
<p><a href="https://www.music-map.com">Music map</a> music discovery tool</p>
<p><a href="https://why-openbsd.rocks/fact">Why OpenBSD rocks</a> A list of cool features found in OpenBSD</p>
<p><a href="https://pkgsrc.org">pkgsrc</a> A cross platform source based package management system. Originally from NetBSD</p>
</div>
</div>
<?php
include "includes/footer.html";
?>
