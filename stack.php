<?php include "includes/header.html"; ?>
<title>Stack</title>
<?php include "includes/nav.html"; ?>
<div class="row">
<div class="col-12">
<p> <i>E-waste Collector Extraordinaire</i> </p>
<img style="max-height: 60vh;" src="/assets/images/stack.jpg" alt="stack of e-waste">
<hr>
</div>
</div>
<div class="row">
<div class="col-12">
<h1> Software </h1>
<h3> "Real Computers" </h3>
<pre>
<b>OS:</b> <a href="https://fedoraproject.org">Fedora</a>, <a href="https://openbsd.org">OpenBSD</a>, <a href="http://9front.org">9front</a>
<b>Editor:</b> vim, sam, ed
<b>Langs:</b> C, /bin/sh, PHP
<b>coms:</b> signal, protonmail, xmpp {prosody, profanity}
<b>de:</b> GNOME, dwm
<b>terminal:</b> gnome-terminal, st, tmux
<b>browser:</b> firefox
<b>media player:</b> mpv
<b>multimedia:</b> ffmpeg, imagemagick, audacity, gimp
<b>passwdmgr:</b> keepassXC
</pre>

<h3> Phone </h3>
<pre>
<b>Hardware:</b> Pixel 5a
<b>OS:</b> <a href="https://grapheneos.org">GrapheneOS</a>
<b>repo:</b> <a href="https://f-droid.org">f-droid</a>
<b>browser:</b> <a href="https://github.com/fork-maintainers/iceraven-browser">Iceraven (firefox fork</a> via <a href="https://f-droid.org/en/packages/de.marmaro.krt.ffupdater/">ffupdater</a>
<b>rss:</b> <a href="https://f-droid.org/en/packages/com.nononsenseapps.feeder/">feeder</a>
<b>podcasts:</b> <a href="https://f-droid.org/en/packages/de.danoeh.antennapod/">AntennaPod</a>
<b>nav:</b> <a href="https://f-droid.org/en/packages/net.osmand.plus/">OsmAnd~</a>
<b>coms:</b> <a href="https://molly.im/">molly.im (signal)</a>, <a href="https://f-droid.org/en/packages/eu.siacs.conversations/">conversations.im</a>
<b>audio:</b> <a href="https://f-droid.org/en/packages/ch.blinkenlights.android.vanilla/">Vanilla Music</a>
</pre>




<hr>
<h1> Systems </h1>
<h2> Routers </h2>
<h3> Netgear R6080 </h3>
<p> Running openwrt. </p>
<pre><code>root@R6080:~# sh ./pfetch 
<span style="color:#FF5555"><b> _______</b></span>                        <span style="color:#55FF55"><b>root@R6080</b></span>
<span style="color:#FF5555"><b>|       |.-----.-----.-----.</b></span>    <span style="color:#FF5555"><b>os</b></span>     OpenWrt 22.03.0
<span style="color:#FF5555"><b>|   -   ||  _  |  -__|     |</b></span>    <span style="color:#FF5555"><b>host</b></span>   Netgear R6080
<span style="color:#FF5555"><b>|_______||   __|_____|__|__|</b></span>    <span style="color:#FF5555"><b>kernel</b></span> 5.10.138
<span style="color:#FF5555"><b> ________|__|    __</b></span>             <span style="color:#FF5555"><b>uptime</b></span> 30m
<span style="color:#FF5555"><b>|  |  |  |.----.|  |_</b></span>           <span style="color:#FF5555"><b>pkgs</b></span>   136
<span style="color:#FF5555"><b>|  |  |  ||   _||   _|</b></span>          <span style="color:#FF5555"><b>memory</b></span> 34M / 56M
<span style="color:#FF5555"><b>|________||__|  |____|</b></span>
root@R6080:~# 
</code></pre>

<h3> Netgear R7000 </h3>
<pre><code>root@FreshTomato:/tmp/home/root# ./neofetch --color_blocks off --ascii_distro linux
_small
<span style="color:#555555"><b>        #####</b></span>           <span style="color:#FFFFFF"><b>root</b></span>@<span style="color:#FFFFFF"><b>FreshTomato</b></span> 
<span style="color:#555555"><b>       #######</b></span>          ---------------- 
<span style="color:#555555"><b>       ##</b></span><b>O</b><span style="color:#555555"><b>#</b></span><b>O</b><span style="color:#555555"><b>##</b></span>          <b>OS</b>: Linux 2.6.36.4brcmarm armv7l 
<span style="color:#555555"><b>       #</b></span><span style="color:#FFFF55"><b>#####</b></span><span style="color:#555555"><b>#</b></span>          <b>Kernel</b>: 2.6.36.4brcmarm 
<span style="color:#555555"><b>     ##</b></span><b>##</b><span style="color:#FFFF55"><b>###</b></span><b>##</b><span style="color:#555555"><b>##</b></span>        <b>Uptime</b>: 1 hour, 27 mins 
<span style="color:#555555"><b>    #</b></span><b>##########</b><span style="color:#555555"><b>##</b></span>       <b>Packages</b>: 22 (opkg) 
<span style="color:#555555"><b>   #</b></span><b>############</b><span style="color:#555555"><b>##</b></span>      <b>Shell</b>: sh 
<span style="color:#555555"><b>   #</b></span><b>############</b><span style="color:#555555"><b>###</b></span>     <b>Terminal</b>: /dev/pts/0 
<span style="color:#FFFF55"><b>  ##</b></span><span style="color:#555555"><b>#</b></span><b>###########</b><span style="color:#555555"><b>##</b></span><span style="color:#FFFF55"><b>#</b></span>     <b>CPU</b>: Northstar Prototype (2) 
<span style="color:#FFFF55"><b>######</b></span><span style="color:#555555"><b>#</b></span><b>#######</b><span style="color:#555555"><b>#</b></span><span style="color:#FFFF55"><b>######</b></span>   <b>Memory</b>: 40MiB / 249MiB 
<span style="color:#FFFF55"><b>#######</b></span><span style="color:#555555"><b>#</b></span><b>#####</b><span style="color:#555555"><b>#</b></span><span style="color:#FFFF55"><b>#######</b></span>
<span style="color:#FFFF55"><b>  #####</b></span><span style="color:#555555"><b>#######</b></span><span style="color:#FFFF55"><b>#####</b></span>

root@FreshTomato:/tmp/home/root# 
</code></pre>

<h2> Laptops </h2>
<h3> Thinkpad x220 </h3>
<pre><code> term% ./gfetch.rc 
             glenda@cirno
    (\(\     -----------
   j". ..    os: Plan 9 from 9front/amd64
   (  . .)   shell: /bin/rc
   |   ° ¡   uptime: 0 days
   ¿     ;   ram: 431/902 MiB
   c?".UJ    cpu: amd64 Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
             resolution: 1366 x 768
             fs: cwfs
             sdE0: 465GB
term% 
</code></pre>

<h3> Thinkpad x220 <h3>
<pre><code>x220$ neofetch --ascii_distro openbsd_small --color_blocks off
<span style="color:#FFFF55"><b>      _____</b></span>       <span style="color:#FFFF55"><b>binrc</b></span>@<span style="color:#FFFF55"><b>x220.my.domain</b></span> 
<span style="color:#FFFF55"><b>    \-     -/</b></span>     -------------------- 
<span style="color:#FFFF55"><b> \_/         \</b></span>    <span style="color:#FFFF55"><b>OS</b></span>: OpenBSD 7.3 amd64 
<span style="color:#FFFF55"><b> |        </b></span><b>O O</b><span style="color:#FFFF55"><b> |</b></span>   <span style="color:#FFFF55"><b>Host</b></span>: LENOVO 42912WU 
<span style="color:#FFFF55"><b> |_  &lt;   )  3 )</b></span>   <span style="color:#FFFF55"><b>Uptime</b></span>: 19 hours, 39 mins 
<span style="color:#FFFF55"><b> / \         /</b></span>    <span style="color:#FFFF55"><b>Packages</b></span>: 340 (pkg_info) 
<span style="color:#FFFF55"><b>    /-_____-\</b></span>     <span style="color:#FFFF55"><b>Shell</b></span>: ksh v5.2.14 99/07/13.2 
                  <span style="color:#FFFF55"><b>Terminal</b></span>: /dev/ttyp7 
                  <span style="color:#FFFF55"><b>CPU</b></span>: Intel i5-2540M (4) @ 2.601GHz 
                  <span style="color:#FFFF55"><b>Memory</b></span>: 1071MiB / 8059MiB 

x220$ 
</code></pre>

<h3> Thinkpad t490 </h3> 
<pre><code>[main@t490 ~]$ neofetch --ascii_distro fedora_small --color_blocks off
<b>      _____</b>      <span style="color:#5555FF"><b>main</b></span>@<span style="color:#5555FF"><b>t490</b></span> 
<b>     /   __)</b><span style="color:#5555FF"><b>\</b></span>    --------- 
<b>     |  /  </b><span style="color:#5555FF"><b>\ \</b></span>   <span style="color:#5555FF"><b>OS</b></span>: Fedora Linux 38 (Workstation Edition) x86_64 
<b>  </b><span style="color:#5555FF"><b>__</b></span><b>_|  |_</b><span style="color:#5555FF"><b>_/ /</b></span>   <span style="color:#5555FF"><b>Host</b></span>: 20N3S5US00 ThinkPad T490 
<b> </b><span style="color:#5555FF"><b>/ </b></span><b>(_    _)</b><span style="color:#5555FF"><b>_/</b></span>    <span style="color:#5555FF"><b>Kernel</b></span>: 6.2.15-300.fc38.x86_64 
<span style="color:#5555FF"><b>/ /</b></span><b>  |  |</b>        <span style="color:#5555FF"><b>Uptime</b></span>: 14 days, 6 hours, 45 mins 
<span style="color:#5555FF"><b>\ \</b></span><b>__/  |</b>        <span style="color:#5555FF"><b>Packages</b></span>: 3222 (rpm), 28 (flatpak) 
<b> </b><span style="color:#5555FF"><b>\</b></span><b>(_____/</b>        <span style="color:#5555FF"><b>Shell</b></span>: bash 5.2.15 
                 <span style="color:#5555FF"><b>Resolution</b></span>: 1366x768 
                 <span style="color:#5555FF"><b>DE</b></span>: GNOME 44.2 
                 <span style="color:#5555FF"><b>WM</b></span>: Mutter 
                 <span style="color:#5555FF"><b>WM Theme</b></span>: Adwaita 
                 <span style="color:#5555FF"><b>Theme</b></span>: Adwaita [GTK2/3] 
                 <span style="color:#5555FF"><b>Icons</b></span>: Adwaita [GTK2/3] 
                 <span style="color:#5555FF"><b>Terminal</b></span>: gnome-terminal 
                 <span style="color:#5555FF"><b>CPU</b></span>: Intel i7-8665U (8) @ 4.800GHz 
                 <span style="color:#5555FF"><b>GPU</b></span>: Intel WhiskeyLake-U GT2 [UHD Graphics 620] 
                 <span style="color:#5555FF"><b>Memory</b></span>: 8722MiB / 15603MiB 

[main@t490 ~]$ 
</code></pre>

<h2> VPS </h2>
<p> I run 3 vps servers. All are thin (1cpu, 1gb ram, 32gb drive) and exist to do basic web things. Two run OpenBSD and one runs 9front. In the future I intend to run a pubnix</p>
</div>
</div>
<?php
include "includes/footer.html";
?>
